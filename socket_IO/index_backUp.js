const express = require('express');
const app = express();
var cors = require('cors');
app.use(cors());
app.options('*', cors());
const http = require("http");
const server = require('http').createServer(app);
const axios = require('axios');
const { Socket } = require('socket.io');
const io = require('socket.io')(server, {
    cors: 
	{
		origin: "*"
	}
});

var PORT = process.env.PORT || 3000;
var tabMessage = [];
  const getMessage = (id_conver, my_id) => 
	{
	  axios
		.get("https://tae.xelero.io/api/getMessagesByConversation/"+id_conver+"/"+my_id)
		.then
		(
			(data) => 
			{
				tabMessage = data.data;
				console.log("messages",data.data);
				return data
			}
		); 
  	}
io.on
(
	"connection",socket => 
	{
		let firstData
		console.log("a user connected");
		// socket.emit('chat message',obj);

		socket.on("initial_data", (firstObj) => {
			axios
			.get("https://tae.xelero.io/api/getMessagesByConversation/"+firstObj.id_conver+"/"+firstObj.id_user)
			.then
			(
				(getConverData) => 
				{
					socket.emit('get_data',getConverData.data);
				}
			)			
			.catch
			(
				function (error) 
				{
					console.log(error);
				}
			);
		});
		socket.on
		(
			"chat message", obj => 
			{
				//insertion a la base
				axios.post
				(
					'https://tae.xelero.io/api/sendMessage', 
					{
						conversation_id: obj.id_conver,
						my_id:  obj.id_user,
						message:  obj.message
					}
				)
				.then
				(
					function (response) 
					{
						io.emit("chat message",obj);
					}
				)
				.catch
				(
					function (error) 
					{
						console.log(error);
					}
				);
				//fin insert

			}
		);

	

	}
);

server.listen
(
	PORT, () => 
	{
		console.log('Server is running on port ',PORT);
	}
);