const express = require('express');
const app = express();
var cors = require('cors');
app.use(cors());
app.options('*', cors());
const http = require("http");
const server = require('http').createServer(app);
const axios = require('axios')
const io = require('socket.io')(server, {
    cors: 
	{
		origin: "*"
	}
});



var pts = [];

const getData = (id) => 
{	
	axios
	.get("https://tae.xelero.io/api/listConversations/"+id)
	.then
	(
		(data) => 
		{
			pts = data.data;
			console.log("this get data",data.data);
			return data
		}
	);
}

  var tabMessage = [];
  const getMessage = (id_conver, my_id) => 
	{
	  axios
		.get("https://tae.xelero.io/api/getMessagesByConversation/"+id_conver+"/"+my_id)
		.then
		(
			(data) => 
			{
				tabMessage = data.data;
				console.log("messages",data.data);
				return data
			}
		); 
  	}

io.on
(
	'connection', (socket) => 
	{
		console.log('connection');
		// console.log(getRequest("http://tae.test/api/test"));
		socket.on
		(
			'sendChatToServer',async (obj) => 
			{
				console.log("obj ==> ",obj);
				// we can use axios if we want to store the data in the database
			
				axios.post
				(
					'https://tae.xelero.io/api/sendMessage', 
					{
						conversation_id: obj.id_conver,
						my_id:  obj.id_user,
						message:  obj.message
					}
				)
				.then
				(
					function (response) 
					{
						// console.log(response);
					}
				)
				.catch
				(
					function (error) 
					{
						console.log(error);
					}
				);
				// io.sockets.emit('sendChatToClient', message);
				getMessage(obj.id_conver, obj.my_id)
				//socket.broadcast.emit('sendChatToClient_'+obj.id_conver, tabMessage);
				io.emit('sendChatToClient_'+obj.id_conver, obj);
				//io.emit('MyMessage_'+obj.id_conver, tabMessage);
				getData(obj.id_user) ; 
				//console.log("getDataVar ==> ",getDataVar);
				io.emit('MyDisscussion',pts);
				io.emit('user_typing_'+obj.id_conver);
			}
		);

		socket.on
		(
			'typing', (data)=>
			{
				if((data.typing==true) && (data.id_conver>0))
			   		io.emit('display', data)
				else
			   		io.emit('display', data)
		  	}
		);

		socket.on
		(
			'connect_user', (data)=>
			{
			   	//io.emit('display_connect', data.id_user)
				getMessage(data.id_conver, data.id_user)
				//tabMessage['lastMessage'] = data.message;
				io.emit('messages',tabMessage);
		  	}
		);

		socket.on
		(
			'disconnect', (socket) => 
			{
				console.log('Disconnect');
			}
		);
	}
);

var PORT = process.env.PORT || 3000;

app.get
(
	'/userDevices', (req, res) => 
	{
    	console.log("grabbing user list of available devices");
    	axios.get
		(
			"https://tae.xelero.io/api/listConversations/187", 
			{
        		headers: req.headers.authorization
    		}
    	).
		then
		(
			(response) => 
			{
        		res.send
				(
					{
						 data: response.data 
					}
				);
    		}
		)
		.catch
		(
			(err) => 
			{
        		console.log('failed to grab user devices');
        		res.send('error');
    		}
		);
	}
);

server.listen
(
	PORT, () => 
	{
		console.log('Server is running on port ',PORT);
	}
);
