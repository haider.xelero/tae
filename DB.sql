-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for tae
CREATE DATABASE IF NOT EXISTS `tae` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tae`;

-- Dumping structure for table tae.activites
CREATE TABLE IF NOT EXISTS `activites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.activites: ~0 rows (approximately)
DELETE FROM `activites`;
/*!40000 ALTER TABLE `activites` DISABLE KEYS */;
INSERT INTO `activites` (`id`, `titre`, `type`, `created_at`, `updated_at`, `status`) VALUES
	(1, 'activite P 1', 'Principale', '2021-04-19 10:05:04', '2021-04-19 10:05:04', 1);
/*!40000 ALTER TABLE `activites` ENABLE KEYS */;

-- Dumping structure for table tae.b2b_meets
CREATE TABLE IF NOT EXISTS `b2b_meets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prog_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `b2b_meets_prog_id_index` (`prog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.b2b_meets: ~0 rows (approximately)
DELETE FROM `b2b_meets`;
/*!40000 ALTER TABLE `b2b_meets` DISABLE KEYS */;
/*!40000 ALTER TABLE `b2b_meets` ENABLE KEYS */;

-- Dumping structure for table tae.b2b_meet_users
CREATE TABLE IF NOT EXISTS `b2b_meet_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile_user_id` int(10) unsigned DEFAULT NULL,
  `b2b_meet_id` int(10) unsigned DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_action` timestamp NULL DEFAULT NULL,
  `confirmer` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date_deb_meet` time DEFAULT NULL,
  `date_fin_meet` time DEFAULT NULL,
  `refuser` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `b2b_meet_users_mobile_user_id_index` (`mobile_user_id`),
  KEY `b2b_meet_users_b2b_meet_id_index` (`b2b_meet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.b2b_meet_users: ~0 rows (approximately)
DELETE FROM `b2b_meet_users`;
/*!40000 ALTER TABLE `b2b_meet_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `b2b_meet_users` ENABLE KEYS */;

-- Dumping structure for table tae.back_logs
CREATE TABLE IF NOT EXISTS `back_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.back_logs: ~2 rows (approximately)
DELETE FROM `back_logs`;
/*!40000 ALTER TABLE `back_logs` DISABLE KEYS */;
INSERT INTO `back_logs` (`id`, `titre`, `action`, `tab`, `created_at`, `updated_at`) VALUES
	(13, 'test trigger', 'delete', 'programmes', '2021-04-20 11:34:28', NULL),
	(14, 'test trigger 10000', 'insert', 'programmes', '2021-04-20 11:34:41', NULL),
	(15, 'test trigger 10000', 'update', 'programmes', '2021-04-20 11:34:51', NULL),
	(16, NULL, 'insert', 'speakers', '2021-04-20 13:50:39', NULL);
/*!40000 ALTER TABLE `back_logs` ENABLE KEYS */;

-- Dumping structure for table tae.boardings
CREATE TABLE IF NOT EXISTS `boardings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.boardings: ~0 rows (approximately)
DELETE FROM `boardings`;
/*!40000 ALTER TABLE `boardings` DISABLE KEYS */;
INSERT INTO `boardings` (`id`, `titre`, `description`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'test', 'test', 'boardings\\April2021\\9uj5AMxtTKEjOu5GqbA7.jpg', '2021-04-19 12:15:00', '2021-04-19 12:15:25');
/*!40000 ALTER TABLE `boardings` ENABLE KEYS */;

-- Dumping structure for table tae.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.categories: ~2 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, NULL, 1, 'Category 1', 'category-1', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
	(2, NULL, 1, 'Category 2', 'category-2', '2021-03-31 11:37:12', '2021-03-31 11:37:12');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table tae.certifications
CREATE TABLE IF NOT EXISTS `certifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `certification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discipline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organisation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.certifications: ~9 rows (approximately)
DELETE FROM `certifications`;
/*!40000 ALTER TABLE `certifications` DISABLE KEYS */;
INSERT INTO `certifications` (`id`, `certification`, `discipline`, `organisation`, `date`, `created_at`, `updated_at`, `user_id`) VALUES
	(1, 'certification 1', NULL, NULL, NULL, '2021-04-08 11:34:35', '2021-04-08 11:34:35', NULL),
	(2, 'certification 2', NULL, NULL, NULL, '2021-04-08 11:34:40', '2021-04-08 11:34:40', NULL),
	(3, 'Cet1', 'Disp', 'Orgg', '2019-04-13', '2021-04-13 10:42:36', '2021-04-13 10:42:36', 4),
	(4, NULL, NULL, NULL, '2021-04-13', '2021-04-13 10:44:11', '2021-04-13 10:44:11', 4),
	(5, 'Cert1', 'Disp1', 'Org1', '2018-04-13', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5),
	(6, 'Cert2', 'Disp2', 'Orgi', '2019-04-13', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5),
	(7, NULL, NULL, NULL, '2021-04-13', '2021-04-13 12:06:05', '2021-04-13 12:06:05', 5),
	(8, NULL, NULL, NULL, '2021-04-13', '2021-04-13 12:19:17', '2021-04-13 12:19:17', 5),
	(9, 'Certif', 'Angular', 'IT SIDI DAOUED', '2018-01-07', '2021-04-16 09:41:37', '2021-04-16 09:41:37', 10);
/*!40000 ALTER TABLE `certifications` ENABLE KEYS */;

-- Dumping structure for table tae.data_rows
CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.data_rows: ~190 rows (approximately)
DELETE FROM `data_rows`;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
	(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
	(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
	(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
	(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
	(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
	(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
	(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsTo","column":"role_id","key":"id","label":"display_name","pivot_table":"roles","pivot":0}', 10),
	(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsToMany","column":"id","key":"id","label":"display_name","pivot_table":"user_roles","pivot":"1","taggable":"0"}', 11),
	(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
	(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
	(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
	(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
	(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
	(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
	(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
	(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{"default":"","null":"","options":{"":"-- None --"},"relationship":{"key":"id","label":"name"}}', 2),
	(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{"default":1}', 3),
	(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
	(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{"slugify":{"origin":"name"}}', 5),
	(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
	(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
	(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
	(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
	(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
	(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
	(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
	(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}', 7),
	(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true},"validation":{"rule":"unique:posts,slug"}}', 8),
	(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
	(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
	(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{"default":"DRAFT","options":{"PUBLISHED":"published","DRAFT":"draft","PENDING":"pending"}}', 11),
	(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
	(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
	(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
	(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
	(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
	(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
	(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
	(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
	(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title"},"validation":{"rule":"unique:pages,slug"}}', 6),
	(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
	(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
	(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE","ACTIVE":"ACTIVE"}}', 9),
	(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
	(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
	(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
	(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(57, 7, 'certification', 'text', 'Certification', 0, 1, 1, 1, 1, 1, '{}', 2),
	(58, 7, 'discipline', 'text', 'Discipline', 0, 1, 1, 1, 1, 1, '{}', 3),
	(59, 7, 'organisation', 'text', 'Organisation', 0, 1, 1, 1, 1, 1, '{}', 4),
	(60, 7, 'date', 'date', 'Date', 0, 1, 1, 1, 1, 1, '{}', 5),
	(61, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
	(62, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
	(63, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(64, 8, 'role', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 2),
	(65, 8, 'entreprise', 'text', 'Entreprise', 0, 1, 1, 1, 1, 1, '{}', 3),
	(66, 8, 'date_debut', 'date', 'Date Debut', 0, 1, 1, 1, 1, 1, '{}', 4),
	(67, 8, 'date_fin', 'date', 'Date Fin', 0, 1, 1, 1, 1, 1, '{}', 5),
	(68, 8, 'actuellement', 'text', 'Actuellement', 0, 1, 1, 1, 1, 1, '{}', 6),
	(69, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
	(70, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
	(71, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(72, 9, 'ecole', 'text', 'Ecole', 0, 1, 1, 1, 1, 1, '{}', 2),
	(74, 9, 'date_debut', 'date', 'Date Debut', 0, 1, 1, 1, 1, 1, '{}', 4),
	(75, 9, 'date_fin', 'date', 'Date Fin', 0, 1, 1, 1, 1, 1, '{}', 5),
	(76, 9, 'diplome', 'text', 'Diplome', 0, 1, 1, 1, 1, 1, '{}', 6),
	(77, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
	(78, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
	(79, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(80, 10, 'nom', 'text', 'Nom', 0, 1, 1, 1, 1, 1, '{}', 3),
	(81, 10, 'prenom', 'text', 'Prenom', 0, 1, 1, 1, 1, 1, '{}', 4),
	(82, 10, 'date_de_naissance', 'date', 'Date De Naissance', 0, 1, 1, 1, 1, 1, '{}', 5),
	(83, 10, 'pays_de_residence', 'text', 'Pays De Residence', 0, 1, 1, 1, 1, 1, '{}', 6),
	(84, 10, 'telephone', 'text', 'Telephone', 0, 1, 1, 1, 1, 1, '{}', 7),
	(85, 10, 'sexe', 'text', 'Sexe', 0, 1, 1, 1, 1, 1, '{}', 8),
	(86, 10, 'entreprise', 'text', 'Entreprise', 0, 1, 1, 1, 1, 1, '{}', 9),
	(87, 10, 'site_web', 'text', 'Site Web', 0, 1, 1, 1, 1, 1, '{}', 10),
	(88, 10, 'localite_entreprise', 'text', 'Localite Entreprise', 0, 1, 1, 1, 1, 1, '{}', 11),
	(89, 10, 'domaine_activite_p', 'text', 'Domaine Activite P', 0, 1, 1, 1, 1, 1, '{}', 12),
	(90, 10, 'domaine_activite_s', 'text', 'Domaine Activite S', 0, 1, 1, 1, 1, 1, '{}', 13),
	(91, 10, 'role', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 14),
	(92, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 15),
	(93, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 16),
	(94, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(95, 11, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(96, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
	(97, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(98, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(99, 12, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(100, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
	(101, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(102, 10, 'mobile_user_belongstomany_objectif_relationship', 'relationship', 'objectifs', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Objectif","table":"objectifs","type":"belongsToMany","column":"id","key":"id","label":"titre","pivot_table":"users_objectifs","pivot":"1","taggable":"0"}', 17),
	(103, 10, 'mobile_user_belongstomany_interet_relationship', 'relationship', 'interets', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Interet","table":"interets","type":"belongsToMany","column":"id","key":"id","label":"titre","pivot_table":"users_interets","pivot":"1","taggable":"0"}', 19),
	(104, 10, 'mobile_user_belongstomany_formation_relationship', 'relationship', 'formations', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Formation","table":"formations","type":"hasMany","column":"user_id","key":"id","label":"ecole","pivot_table":"users_formations","pivot":"0","taggable":"0"}', 21),
	(105, 10, 'mobile_user_belongstomany_certification_relationship', 'relationship', 'certifications', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Certification","table":"certifications","type":"hasMany","column":"user_id","key":"id","label":"certification","pivot_table":"users_certifications","pivot":"0","taggable":"0"}', 23),
	(106, 10, 'mobile_user_belongstomany_experience_relationship', 'relationship', 'experiences', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Experience","table":"experiences","type":"hasMany","column":"user_id","key":"id","label":"entreprise","pivot_table":"users_experiences","pivot":"0","taggable":"0"}', 25),
	(107, 10, 'objectifs', 'text', 'Objectifs', 0, 0, 0, 0, 0, 0, '{}', 18),
	(108, 10, 'interets', 'text', 'Interets', 0, 0, 0, 0, 0, 0, '{}', 20),
	(109, 10, 'formations', 'text', 'Formations', 0, 0, 0, 0, 0, 0, '{}', 22),
	(110, 10, 'experiences', 'text', 'Experiences', 0, 0, 0, 0, 0, 0, '{}', 24),
	(111, 10, 'certifications', 'text', 'Certifications', 0, 0, 0, 0, 0, 0, '{}', 26),
	(112, 10, 'token_device', 'text', 'Token Device', 0, 1, 1, 1, 1, 1, '{}', 27),
	(113, 10, 'token_api', 'text', 'Token Api', 0, 1, 1, 1, 1, 1, '{}', 28),
	(114, 10, 'type_auth_api', 'text', 'Type Auth Api', 0, 1, 1, 1, 1, 1, '{}', 29),
	(115, 10, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 2),
	(116, 10, 'mot_de_passe', 'text', 'Mot De Passe', 0, 0, 0, 0, 0, 0, '{}', 30),
	(117, 7, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 8),
	(118, 8, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 9),
	(119, 9, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 9),
	(120, 11, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{"on":"Active","off":"Inactive","checked":"true"}', 5),
	(121, 12, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{"on":"Active","off":"Inactive","checked":"true"}', 5),
	(122, 7, 'certification_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\MobileUser","table":"mobile_users","type":"belongsTo","column":"user_id","key":"id","label":"email","pivot_table":"categories","pivot":"0","taggable":"0"}', 9),
	(123, 8, 'experience_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\MobileUser","table":"mobile_users","type":"belongsTo","column":"user_id","key":"id","label":"email","pivot_table":"categories","pivot":"0","taggable":"0"}', 10),
	(124, 8, 'site_web', 'text', 'Site Web', 0, 1, 1, 1, 1, 1, '{}', 10),
	(125, 9, 'formation_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\MobileUser","table":"mobile_users","type":"belongsTo","column":"user_id","key":"id","label":"email","pivot_table":"categories","pivot":"0","taggable":"0"}', 10),
	(126, 9, 'domaine', 'text', 'Domaine', 0, 1, 1, 1, 1, 1, '{}', 6),
	(127, 9, 'grade', 'text', 'Grade', 0, 1, 1, 1, 1, 1, '{}', 10),
	(128, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(129, 13, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(130, 13, 'date', 'date', 'Date', 0, 1, 1, 1, 1, 1, '{}', 3),
	(131, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
	(132, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
	(143, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(144, 15, 'nom', 'text', 'Nom', 0, 1, 1, 1, 1, 1, '{}', 2),
	(145, 15, 'poste', 'text', 'Poste', 0, 1, 1, 1, 1, 1, '{}', 3),
	(146, 15, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
	(147, 15, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
	(148, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
	(149, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(150, 16, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(151, 16, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
	(152, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(154, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(155, 17, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(156, 17, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
	(157, 17, 'date_debut', 'time', 'Date Debut', 0, 1, 1, 1, 1, 1, '{}', 4),
	(158, 17, 'date_fin', 'time', 'Date Fin', 0, 1, 1, 1, 1, 1, '{}', 5),
	(159, 17, 'type_id', 'text', 'Type Id', 0, 1, 1, 1, 1, 1, '{}', 6),
	(160, 17, 'event_id', 'text', 'Event Id', 0, 1, 1, 1, 1, 1, '{}', 7),
	(161, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
	(162, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
	(163, 17, 'programme_belongsto_evenement_relationship', 'relationship', 'evenements', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Evenement","table":"evenements","type":"belongsTo","column":"event_id","key":"id","label":"titre","pivot_table":"categories","pivot":"0","taggable":"0"}', 10),
	(164, 17, 'programme_belongsto_type_prog_relationship', 'relationship', 'type_progs', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\TypeProg","table":"type_progs","type":"belongsTo","column":"type_id","key":"id","label":"titre","pivot_table":"categories","pivot":"0","taggable":"0"}', 11),
	(165, 17, 'programme_hasmany_speaker_relationship', 'relationship', 'speakers', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Speaker","table":"speakers","type":"belongsToMany","column":"id","key":"id","label":"nom","pivot_table":"speakers_progs","pivot":"1","taggable":"0"}', 12),
	(166, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(167, 18, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(168, 18, 'date', 'date', 'Date', 0, 1, 1, 1, 1, 1, '{}', 3),
	(169, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
	(170, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
	(171, 18, 'b2b_meet_belongstomany_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\MobileUser","table":"mobile_users","type":"belongsToMany","column":"id","key":"id","label":"email","pivot_table":"b2b_meet_users","pivot":"1","taggable":"0"}', 6),
	(172, 18, 'b2b_meet_belongsto_programme_relationship', 'relationship', 'programmes', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Programme","table":"programmes","type":"belongsTo","column":"prog_id","key":"id","label":"titre","pivot_table":"b2b_meet_users","pivot":"0","taggable":null}', 7),
	(173, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(174, 19, 'mobile_user_id', 'text', 'Mobile User Id', 0, 1, 1, 1, 1, 1, '{}', 2),
	(175, 19, 'b2b_meet_id', 'text', 'B2b Meet Id', 0, 1, 1, 1, 1, 1, '{}', 4),
	(176, 19, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 6),
	(177, 19, 'date_env', 'timestamp', 'Date Env', 0, 1, 1, 1, 1, 1, '{}', 7),
	(178, 19, 'date_rec', 'timestamp', 'Date Rec', 0, 1, 1, 1, 1, 1, '{}', 8),
	(179, 19, 'action', 'text', 'Action', 0, 1, 1, 1, 1, 1, '{}', 9),
	(180, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
	(181, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
	(182, 19, 'date_deb_meet', 'text', 'Date Deb Meet', 0, 1, 1, 1, 1, 1, '{}', 12),
	(183, 19, 'date_fin_meet', 'text', 'Date Fin Meet', 0, 1, 1, 1, 1, 1, '{}', 13),
	(184, 19, 'b2b_meet_user_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\MobileUser","table":"mobile_users","type":"belongsTo","column":"mobile_user_id","key":"id","label":"email","pivot_table":"b2b_meet_users","pivot":"0","taggable":"0"}', 3),
	(185, 19, 'b2b_meet_user_belongsto_b2b_meet_relationship', 'relationship', 'b2b_meets', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\B2bMeet","table":"b2b_meets","type":"belongsTo","column":"b2b_meet_id","key":"id","label":"titre","pivot_table":"b2b_meet_users","pivot":"0","taggable":"0"}', 5),
	(186, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(187, 20, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(188, 20, 'type', 'select_dropdown', 'Type', 0, 1, 1, 1, 1, 1, '{"default":"Principale","options":{"Principale":"Principale","Secondaire":"Secondaire"}}', 3),
	(189, 20, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(190, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
	(191, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(192, 21, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(193, 21, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
	(194, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(195, 20, 'status', 'checkbox', 'Statu', 0, 1, 1, 1, 1, 1, '{"on":"Active","off":"Inactive","checked":"true"}', 6),
	(196, 21, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{"on":"Active","off":"Inactive","checked":"true"}', 5),
	(197, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(198, 22, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
	(199, 22, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
	(200, 22, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
	(201, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
	(202, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
	(203, 23, 'mobile_user_id', 'text', 'Mobile User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
	(204, 23, 'objectif_id', 'text', 'Objectif Id', 1, 1, 1, 1, 1, 1, '{}', 3),
	(205, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
	(206, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
	(207, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

-- Dumping structure for table tae.data_types
CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.data_types: ~21 rows (approximately)
DELETE FROM `data_types`;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
	(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-03-31 11:36:56', '2021-03-31 11:36:56'),
	(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-03-31 11:36:56', '2021-03-31 11:36:56'),
	(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-03-31 11:36:56', '2021-03-31 11:36:56'),
	(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2021-03-31 11:37:10', '2021-03-31 11:37:10'),
	(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2021-03-31 11:37:13', '2021-03-31 11:37:13'),
	(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2021-03-31 11:37:15', '2021-03-31 11:37:15'),
	(7, 'certifications', 'certifications', 'Certification', 'Certifications', NULL, 'App\\Certification', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-08 10:25:25', '2021-04-13 10:45:56'),
	(8, 'experiences', 'experiences', 'Experience', 'Experiences', NULL, 'App\\Experience', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-08 10:25:54', '2021-04-13 10:47:09'),
	(9, 'formations', 'formations', 'Formation', 'Formations', NULL, 'App\\Formation', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-08 10:26:12', '2021-04-13 10:48:02'),
	(10, 'mobile_users', 'mobile-users', 'Mobile User', 'Mobile Users', NULL, 'App\\MobileUser', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-08 10:26:31', '2021-04-12 11:29:15'),
	(11, 'interets', 'interets', 'Interet', 'Interets', NULL, 'App\\Interet', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-08 10:26:41', '2021-04-12 11:49:27'),
	(12, 'objectifs', 'objectifs', 'Objectif', 'Objectifs', NULL, 'App\\Objectif', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-08 10:26:54', '2021-04-12 11:50:32'),
	(13, 'evenements', 'evenements', 'Evenement', 'Evenements', NULL, 'App\\Evenement', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
	(15, 'speakers', 'speakers', 'Speaker', 'Speakers', NULL, 'App\\Speaker', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
	(16, 'type_progs', 'type-progs', 'Type Prog', 'Type Progs', NULL, 'App\\TypeProg', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-13 13:00:50', '2021-04-13 13:01:05'),
	(17, 'programmes', 'programmes', 'Programme', 'Programmes', NULL, 'App\\Programme', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-13 13:18:25', '2021-04-13 13:31:50'),
	(18, 'b2b_meets', 'b2b-meets', 'B2B Meet', 'B2B Meets', NULL, 'App\\B2bMeet', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-13 13:41:07', '2021-04-14 09:50:04'),
	(19, 'b2b_meet_users', 'b2b-meet-users', 'B2B Meet User', 'B2B Meet Users', NULL, 'App\\B2bMeetUser', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-14 14:56:45', '2021-04-14 14:59:55'),
	(20, 'activites', 'activites', 'Activite', 'Activites', NULL, 'App\\Activite', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-16 10:29:25', '2021-04-16 11:43:21'),
	(21, 'wizard_roles', 'wizard-roles', 'Wizard Role', 'Wizard Roles', NULL, 'App\\WizardRole', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2021-04-16 10:29:40', '2021-04-19 10:06:09'),
	(22, 'boardings', 'boardings', 'Boarding', 'Boardings', NULL, 'App\\Boarding', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
	(23, 'users_objectifs', 'users-objectifs', 'Users Objectif', 'Users Objectifs', NULL, 'App\\UsersObjectif', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2021-04-19 12:10:27', '2021-04-19 12:10:27');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

-- Dumping structure for table tae.evenements
CREATE TABLE IF NOT EXISTS `evenements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.evenements: ~4 rows (approximately)
DELETE FROM `evenements`;
/*!40000 ALTER TABLE `evenements` DISABLE KEYS */;
INSERT INTO `evenements` (`id`, `titre`, `date`, `created_at`, `updated_at`) VALUES
	(1, 'jour 1', '2021-11-08', '2021-04-13 13:04:32', '2021-04-13 13:04:32'),
	(2, 'jour 2', '2021-11-09', '2021-04-13 13:04:47', '2021-04-13 13:04:47'),
	(3, 'jour 3', '2021-11-10', '2021-04-13 13:05:01', '2021-04-13 13:05:01'),
	(4, 'jour 4', '2021-11-11', '2021-04-13 13:05:12', '2021-04-13 13:05:12');
/*!40000 ALTER TABLE `evenements` ENABLE KEYS */;

-- Dumping structure for table tae.experiences
CREATE TABLE IF NOT EXISTS `experiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `actuellement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `site_web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.experiences: ~11 rows (approximately)
DELETE FROM `experiences`;
/*!40000 ALTER TABLE `experiences` DISABLE KEYS */;
INSERT INTO `experiences` (`id`, `role`, `entreprise`, `date_debut`, `date_fin`, `actuellement`, `created_at`, `updated_at`, `user_id`, `site_web`) VALUES
	(1, NULL, 'experience 1', NULL, NULL, NULL, '2021-04-08 11:34:10', '2021-04-08 11:34:10', NULL, NULL),
	(2, NULL, 'experience 2', NULL, NULL, NULL, '2021-04-08 11:34:19', '2021-04-08 11:34:19', NULL, NULL),
	(3, 'ole1', 'Soc1', '2017-04-13', '2020-04-13', '0', '2021-04-13 10:39:48', '2021-04-13 10:39:48', 4, 'Www.t.tn'),
	(4, 'ole1', 'Soc1', '2018-04-13', '2019-04-13', '0', '2021-04-13 10:40:52', '2021-04-13 10:40:53', 4, 'Yguyu.com'),
	(5, NULL, NULL, '2021-04-13', '2021-04-13', '0', '2021-04-13 10:42:36', '2021-04-13 10:42:36', 4, NULL),
	(6, NULL, NULL, '2021-04-13', '2021-04-13', '0', '2021-04-13 10:44:11', '2021-04-13 10:44:11', 4, NULL),
	(7, 'CEO', 'Lezats', '2013-04-13', '2017-04-13', '0', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5, 'Lzd.fr'),
	(8, 'Exp2', 'Societe2', '2021-04-13', '2021-05-13', '0', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5, 'web.tn2'),
	(9, NULL, NULL, '2021-04-13', '2021-04-13', '0', '2021-04-13 12:06:05', '2021-04-13 12:06:05', 5, NULL),
	(10, NULL, NULL, '2021-04-13', '2021-04-13', '0', '2021-04-13 12:19:17', '2021-04-13 12:19:17', 5, NULL),
	(11, 'Dev', 'Xelero', '2021-01-01', '2021-04-16', '0', '2021-04-16 09:41:37', '2021-04-16 09:41:37', 10, 'Xelero.io');
/*!40000 ALTER TABLE `experiences` ENABLE KEYS */;

-- Dumping structure for table tae.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table tae.formations
CREATE TABLE IF NOT EXISTS `formations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecole` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diplome` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `domaine` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.formations: ~8 rows (approximately)
DELETE FROM `formations`;
/*!40000 ALTER TABLE `formations` DISABLE KEYS */;
INSERT INTO `formations` (`id`, `ecole`, `diplome`, `date_debut`, `date_fin`, `domaine`, `created_at`, `updated_at`, `user_id`, `grade`) VALUES
	(1, 'formation 1', NULL, NULL, NULL, NULL, '2021-04-08 11:33:00', '2021-04-12 11:30:36', 2, NULL),
	(2, 'formation 2', NULL, NULL, NULL, NULL, '2021-04-08 11:33:00', '2021-04-12 11:30:23', 2, NULL),
	(3, 'ECole espit', 'Ingenieur', '2020-04-13', '2020-04-13', 'IT', '2021-04-13 10:44:11', '2021-04-13 10:44:11', 4, NULL),
	(4, 'Ecole1', 'Diplome1', '2006-04-13', '2008-04-13', 'DOmain1', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5, 'Ingénieur'),
	(5, 'Ecole2', 'Diplome2', '2015-04-13', '2016-04-13', 'Domaine2', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5, NULL),
	(6, NULL, NULL, '2021-04-13', '2021-04-13', NULL, '2021-04-13 12:06:05', '2021-04-13 12:06:05', 5, NULL),
	(7, NULL, NULL, '2021-04-13', '2021-04-13', NULL, '2021-04-13 12:19:17', '2021-04-13 12:19:17', 5, NULL),
	(8, 'Esprit', 'Ingenieur', '2019-09-01', '2021-04-16', 'IT', '2021-04-16 09:41:37', '2021-04-16 09:41:37', 10, '3eme cycle');
/*!40000 ALTER TABLE `formations` ENABLE KEYS */;

-- Dumping structure for table tae.interets
CREATE TABLE IF NOT EXISTS `interets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.interets: ~7 rows (approximately)
DELETE FROM `interets`;
/*!40000 ALTER TABLE `interets` DISABLE KEYS */;
INSERT INTO `interets` (`id`, `titre`, `created_at`, `updated_at`, `status`) VALUES
	(1, 'interet 1', '2021-04-08 10:53:38', '2021-04-08 10:53:38', NULL),
	(2, 'interet 2', '2021-04-08 10:53:51', '2021-04-08 10:53:51', NULL),
	(3, 'Business', '2021-04-13 10:37:34', '2021-04-13 10:37:34', 0),
	(4, 'Marketing', '2021-04-13 10:37:34', '2021-04-13 10:37:34', 0),
	(5, 'UX/UI', '2021-04-13 10:39:48', '2021-04-13 10:39:48', 0),
	(6, 'Finance', '2021-04-13 10:39:48', '2021-04-13 10:39:48', 0),
	(7, 'Stratégie digital', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 0);
/*!40000 ALTER TABLE `interets` ENABLE KEYS */;

-- Dumping structure for table tae.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.menus: ~0 rows (approximately)
DELETE FROM `menus`;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', '2021-03-31 11:36:57', '2021-03-31 11:36:57');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping structure for table tae.menu_items
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.menu_items: ~34 rows (approximately)
DELETE FROM `menu_items`;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
	(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-03-31 11:36:57', '2021-03-31 11:36:57', 'voyager.dashboard', NULL),
	(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 15, 3, '2021-03-31 11:36:57', '2021-04-16 10:31:42', 'voyager.media.index', NULL),
	(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 15, 1, '2021-03-31 11:36:57', '2021-04-06 11:13:34', 'voyager.users.index', NULL),
	(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 15, 2, '2021-03-31 11:36:57', '2021-04-16 10:31:42', 'voyager.roles.index', NULL),
	(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2021-03-31 11:36:57', '2021-04-19 12:52:42', NULL, NULL),
	(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2021-03-31 11:36:57', '2021-04-06 11:13:32', 'voyager.menus.index', NULL),
	(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2021-03-31 11:36:58', '2021-04-06 11:13:32', 'voyager.database.index', NULL),
	(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2021-03-31 11:36:58', '2021-04-06 11:13:32', 'voyager.compass.index', NULL),
	(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2021-03-31 11:36:58', '2021-04-06 11:13:32', 'voyager.bread.index', NULL),
	(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2021-03-31 11:36:58', '2021-04-19 12:52:42', 'voyager.settings.index', NULL),
	(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, 15, 6, '2021-03-31 11:37:12', '2021-04-19 12:52:42', 'voyager.categories.index', NULL),
	(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, 15, 4, '2021-03-31 11:37:14', '2021-04-19 12:52:42', 'voyager.posts.index', NULL),
	(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, 15, 5, '2021-03-31 11:37:16', '2021-04-19 12:52:42', 'voyager.pages.index', NULL),
	(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2021-03-31 11:37:21', '2021-04-06 11:13:32', 'voyager.hooks', NULL),
	(15, 1, 'Back Office', '', '_self', 'voyager-params', '#000000', NULL, 6, '2021-04-06 11:13:23', '2021-04-19 12:52:55', NULL, ''),
	(16, 1, 'Certifications', '', '_self', NULL, NULL, 22, 4, '2021-04-08 10:25:25', '2021-04-16 10:31:58', 'voyager.certifications.index', NULL),
	(17, 1, 'Experiences', '', '_self', NULL, NULL, 22, 3, '2021-04-08 10:25:54', '2021-04-16 10:31:58', 'voyager.experiences.index', NULL),
	(18, 1, 'Formations', '', '_self', NULL, NULL, 22, 2, '2021-04-08 10:26:12', '2021-04-16 10:31:58', 'voyager.formations.index', NULL),
	(19, 1, 'Users', '', '_self', NULL, '#000000', 22, 1, '2021-04-08 10:26:31', '2021-04-08 10:29:04', 'voyager.mobile-users.index', 'null'),
	(20, 1, 'Interets', '', '_self', NULL, NULL, 34, 3, '2021-04-08 10:26:41', '2021-04-16 10:32:11', 'voyager.interets.index', NULL),
	(21, 1, 'Objectifs', '', '_self', NULL, NULL, 34, 4, '2021-04-08 10:26:54', '2021-04-16 10:32:11', 'voyager.objectifs.index', NULL),
	(22, 1, 'Mobile Users', '', '_self', 'voyager-person', '#000000', NULL, 3, '2021-04-08 10:28:53', '2021-04-13 13:03:09', NULL, ''),
	(23, 1, 'Evenements', '', '_self', NULL, NULL, 27, 1, '2021-04-13 12:56:56', '2021-04-13 13:01:39', 'voyager.evenements.index', NULL),
	(25, 1, 'Speakers', '', '_self', NULL, NULL, 27, 3, '2021-04-13 12:59:51', '2021-04-13 13:20:25', 'voyager.speakers.index', NULL),
	(26, 1, 'Type Programmes', '', '_self', NULL, '#000000', 27, 4, '2021-04-13 13:00:50', '2021-04-13 13:20:22', 'voyager.type-progs.index', 'null'),
	(27, 1, 'Evenements', '', '_self', 'voyager-calendar', '#000000', NULL, 2, '2021-04-13 13:01:33', '2021-04-13 13:03:09', NULL, ''),
	(28, 1, 'Programmes', '', '_self', NULL, NULL, 27, 2, '2021-04-13 13:18:25', '2021-04-13 13:20:25', 'voyager.programmes.index', NULL),
	(29, 1, 'B2B Meets', '', '_self', NULL, NULL, 33, 1, '2021-04-13 13:41:07', '2021-04-16 10:30:37', 'voyager.b2b-meets.index', NULL),
	(30, 1, 'B2B Meet Users', '', '_self', NULL, NULL, 33, 2, '2021-04-14 14:56:45', '2021-04-16 10:30:40', 'voyager.b2b-meet-users.index', NULL),
	(31, 1, 'Activites', '', '_self', NULL, NULL, 34, 1, '2021-04-16 10:29:25', '2021-04-16 10:32:11', 'voyager.activites.index', NULL),
	(32, 1, 'Roles', '', '_self', NULL, '#000000', 34, 2, '2021-04-16 10:29:40', '2021-04-16 10:32:21', 'voyager.wizard-roles.index', 'null'),
	(33, 1, 'Meets', '', '_self', 'voyager-people', '#000000', NULL, 5, '2021-04-16 10:30:31', '2021-04-19 12:52:55', NULL, ''),
	(34, 1, 'Info Inscription', '', '_self', 'voyager-file-text', '#000000', NULL, 4, '2021-04-16 10:31:34', '2021-04-19 12:52:55', NULL, ''),
	(35, 1, 'Boardings', '', '_self', NULL, NULL, NULL, 7, '2021-04-19 12:52:24', '2021-04-19 12:52:55', 'voyager.boardings.index', NULL),
	(36, 1, 'Users Objectifs', '', '_self', NULL, NULL, NULL, 10, '2021-04-19 12:10:27', '2021-04-19 12:10:27', 'voyager.users-objectifs.index', NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

-- Dumping structure for table tae.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.migrations: ~27 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_01_01_000000_add_voyager_user_fields', 1),
	(4, '2016_01_01_000000_create_data_types_table', 1),
	(5, '2016_05_19_173453_create_menu_table', 1),
	(6, '2016_10_21_190000_create_roles_table', 1),
	(7, '2016_10_21_190000_create_settings_table', 1),
	(8, '2016_11_30_135954_create_permission_table', 1),
	(9, '2016_11_30_141208_create_permission_role_table', 1),
	(10, '2016_12_26_201236_data_types__add__server_side', 1),
	(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
	(12, '2017_01_14_005015_create_translations_table', 1),
	(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
	(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
	(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
	(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
	(17, '2017_08_05_000000_add_group_to_settings_table', 1),
	(18, '2017_11_26_013050_add_user_role_relationship', 1),
	(19, '2017_11_26_015000_create_user_roles_table', 1),
	(20, '2018_03_11_000000_add_user_settings', 1),
	(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
	(22, '2018_03_16_000000_make_settings_value_nullable', 1),
	(23, '2019_08_19_000000_create_failed_jobs_table', 1),
	(24, '2016_01_01_000000_create_pages_table', 2),
	(25, '2016_01_01_000000_create_posts_table', 2),
	(26, '2016_02_15_204651_create_categories_table', 2),
	(27, '2017_04_11_000000_alter_post_nullable_fields_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table tae.mobile_users
CREATE TABLE IF NOT EXISTS `mobile_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_de_naissance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays_de_residence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `localite_entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domaine_activite_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domaine_activite_s` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `objectifs` int(11) DEFAULT NULL,
  `interets` int(11) DEFAULT NULL,
  `formations` int(11) DEFAULT NULL,
  `experiences` int(11) DEFAULT NULL,
  `certifications` int(11) DEFAULT NULL,
  `token_device` text COLLATE utf8mb4_unicode_ci,
  `token_api` text COLLATE utf8mb4_unicode_ci,
  `type_auth_api` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mot_de_passe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.mobile_users: ~2 rows (approximately)
DELETE FROM `mobile_users`;
/*!40000 ALTER TABLE `mobile_users` DISABLE KEYS */;
INSERT INTO `mobile_users` (`id`, `nom`, `prenom`, `date_de_naissance`, `pays_de_residence`, `telephone`, `sexe`, `entreprise`, `site_web`, `localite_entreprise`, `domaine_activite_p`, `domaine_activite_s`, `role`, `created_at`, `updated_at`, `objectifs`, `interets`, `formations`, `experiences`, `certifications`, `token_device`, `token_api`, `type_auth_api`, `email`, `mot_de_passe`) VALUES
	(10, 'Ayechi', 'Enis', '1996-04-18 00:00:00', 'Tunisie', '+21652044805', 'Homme', 'Xelero', 'Xelero.io', 'Tunisie', 'IT', 'ACTSEC2', 'CEO', '2021-04-16 09:38:25', '2021-04-16 09:41:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'enis@gmail.com', '$2y$10$IDhGO73fsNeT5mAUEYBfaeFS5asCSD6827kQeXme6T6GROF7K3rue'),
	(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-16 10:10:17', '2021-04-16 10:10:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'enis2@gmail.com', '$2y$10$j4VRw/UFvlh9cVk.de/.pOzzCi1zVPXmpGc4YNGQajalg1XFsBFtO');
/*!40000 ALTER TABLE `mobile_users` ENABLE KEYS */;

-- Dumping structure for table tae.objectifs
CREATE TABLE IF NOT EXISTS `objectifs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.objectifs: ~8 rows (approximately)
DELETE FROM `objectifs`;
/*!40000 ALTER TABLE `objectifs` DISABLE KEYS */;
INSERT INTO `objectifs` (`id`, `titre`, `created_at`, `updated_at`, `status`) VALUES
	(1, 'objectif 1', '2021-04-08 10:44:52', '2021-04-08 10:44:52', NULL),
	(2, 'objectif 2', '2021-04-08 10:45:03', '2021-04-08 10:45:03', NULL),
	(3, 'objectif 3', '2021-04-08 10:45:11', '2021-04-08 10:45:11', NULL),
	(4, 'Découvrir', '2021-04-13 10:37:34', '2021-04-13 10:37:34', 0),
	(5, 'Elargir mon réseau professionnel', '2021-04-13 10:37:34', '2021-04-13 10:37:34', 0),
	(6, 'Trouver de nouveaux partenaires', '2021-04-13 10:39:48', '2021-04-13 10:39:48', 0),
	(7, 'Trouver un emploi', '2021-04-13 10:50:00', '2021-04-13 11:42:54', 1),
	(8, 'Enis', '2021-04-16 09:41:37', '2021-04-16 09:41:37', 0);
/*!40000 ALTER TABLE `objectifs` ENABLE KEYS */;

-- Dumping structure for table tae.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.pages: ~0 rows (approximately)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
	(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2021-03-31 11:37:16', '2021-03-31 11:37:16');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table tae.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table tae.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.permissions: ~116 rows (approximately)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
	(1, 'browse_admin', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
	(2, 'browse_bread', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
	(3, 'browse_database', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
	(4, 'browse_media', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
	(5, 'browse_compass', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
	(6, 'browse_menus', 'menus', '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
	(7, 'read_menus', 'menus', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(8, 'edit_menus', 'menus', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(9, 'add_menus', 'menus', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(10, 'delete_menus', 'menus', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(11, 'browse_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(12, 'read_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(13, 'edit_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(14, 'add_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(15, 'delete_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(16, 'browse_users', 'users', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(17, 'read_users', 'users', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
	(18, 'edit_users', 'users', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
	(19, 'add_users', 'users', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
	(20, 'delete_users', 'users', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
	(21, 'browse_settings', 'settings', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
	(22, 'read_settings', 'settings', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
	(23, 'edit_settings', 'settings', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
	(24, 'add_settings', 'settings', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
	(25, 'delete_settings', 'settings', '2021-03-31 11:37:01', '2021-03-31 11:37:01'),
	(26, 'browse_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
	(27, 'read_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
	(28, 'edit_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
	(29, 'add_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
	(30, 'delete_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
	(31, 'browse_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
	(32, 'read_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
	(33, 'edit_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
	(34, 'add_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
	(35, 'delete_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
	(36, 'browse_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
	(37, 'read_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
	(38, 'edit_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
	(39, 'add_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
	(40, 'delete_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
	(41, 'browse_hooks', NULL, '2021-03-31 11:37:21', '2021-03-31 11:37:21'),
	(42, 'browse_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
	(43, 'read_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
	(44, 'edit_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
	(45, 'add_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
	(46, 'delete_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
	(47, 'browse_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
	(48, 'read_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
	(49, 'edit_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
	(50, 'add_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
	(51, 'delete_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
	(52, 'browse_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
	(53, 'read_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
	(54, 'edit_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
	(55, 'add_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
	(56, 'delete_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
	(57, 'browse_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
	(58, 'read_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
	(59, 'edit_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
	(60, 'add_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
	(61, 'delete_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
	(62, 'browse_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
	(63, 'read_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
	(64, 'edit_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
	(65, 'add_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
	(66, 'delete_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
	(67, 'browse_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
	(68, 'read_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
	(69, 'edit_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
	(70, 'add_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
	(71, 'delete_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
	(72, 'browse_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
	(73, 'read_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
	(74, 'edit_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
	(75, 'add_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
	(76, 'delete_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
	(82, 'browse_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
	(83, 'read_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
	(84, 'edit_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
	(85, 'add_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
	(86, 'delete_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
	(87, 'browse_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
	(88, 'read_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
	(89, 'edit_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
	(90, 'add_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
	(91, 'delete_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
	(92, 'browse_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
	(93, 'read_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
	(94, 'edit_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
	(95, 'add_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
	(96, 'delete_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
	(97, 'browse_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
	(98, 'read_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
	(99, 'edit_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
	(100, 'add_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
	(101, 'delete_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
	(102, 'browse_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
	(103, 'read_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
	(104, 'edit_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
	(105, 'add_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
	(106, 'delete_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
	(107, 'browse_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
	(108, 'read_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
	(109, 'edit_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
	(110, 'add_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
	(111, 'delete_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
	(112, 'browse_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
	(113, 'read_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
	(114, 'edit_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
	(115, 'add_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
	(116, 'delete_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
	(117, 'browse_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
	(118, 'read_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
	(119, 'edit_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
	(120, 'add_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
	(121, 'delete_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
	(122, 'browse_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
	(123, 'read_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
	(124, 'edit_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
	(125, 'add_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
	(126, 'delete_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table tae.permission_role
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.permission_role: ~115 rows (approximately)
DELETE FROM `permission_role`;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 1),
	(23, 1),
	(24, 1),
	(25, 1),
	(26, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(30, 1),
	(31, 1),
	(32, 1),
	(33, 1),
	(34, 1),
	(35, 1),
	(36, 1),
	(37, 1),
	(38, 1),
	(39, 1),
	(40, 1),
	(42, 1),
	(43, 1),
	(44, 1),
	(45, 1),
	(46, 1),
	(47, 1),
	(48, 1),
	(49, 1),
	(50, 1),
	(51, 1),
	(52, 1),
	(53, 1),
	(54, 1),
	(55, 1),
	(56, 1),
	(57, 1),
	(58, 1),
	(59, 1),
	(60, 1),
	(61, 1),
	(62, 1),
	(63, 1),
	(64, 1),
	(65, 1),
	(66, 1),
	(67, 1),
	(68, 1),
	(69, 1),
	(70, 1),
	(71, 1),
	(72, 1),
	(73, 1),
	(74, 1),
	(75, 1),
	(76, 1),
	(82, 1),
	(83, 1),
	(84, 1),
	(85, 1),
	(86, 1),
	(87, 1),
	(88, 1),
	(89, 1),
	(90, 1),
	(91, 1),
	(92, 1),
	(93, 1),
	(94, 1),
	(95, 1),
	(96, 1),
	(97, 1),
	(98, 1),
	(99, 1),
	(100, 1),
	(101, 1),
	(102, 1),
	(103, 1),
	(104, 1),
	(105, 1),
	(106, 1),
	(107, 1),
	(108, 1),
	(109, 1),
	(110, 1),
	(111, 1),
	(112, 1),
	(113, 1),
	(114, 1),
	(115, 1),
	(116, 1),
	(117, 1),
	(118, 1),
	(119, 1),
	(120, 1),
	(121, 1),
	(122, 1),
	(123, 1),
	(124, 1),
	(125, 1),
	(126, 1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping structure for table tae.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.posts: ~4 rows (approximately)
DELETE FROM `posts`;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
	(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
	(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
	(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
	(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-31 11:37:15', '2021-03-31 11:37:15');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table tae.programmes
CREATE TABLE IF NOT EXISTS `programmes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_debut` time DEFAULT NULL,
  `date_fin` time DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.programmes: ~3 rows (approximately)
DELETE FROM `programmes`;
/*!40000 ALTER TABLE `programmes` DISABLE KEYS */;
INSERT INTO `programmes` (`id`, `titre`, `description`, `date_debut`, `date_fin`, `type_id`, `event_id`, `created_at`, `updated_at`) VALUES
	(1, 'Cocktail de bienvenue', 'Cocktail de bienvenue en l’honneur  des Délégations Etrangères offert par  les Autorités Ivoiriennes', '20:00:00', '22:00:00', 1, 1, '2021-04-13 13:07:00', '2021-04-13 13:22:29'),
	(10, 'test trigger 10000', 'hsan', NULL, NULL, NULL, NULL, '2021-04-20 10:34:00', '2021-04-20 10:34:51');
/*!40000 ALTER TABLE `programmes` ENABLE KEYS */;

-- Dumping structure for table tae.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.roles: ~2 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Administrator', '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
	(2, 'user', 'Normal User', '2021-03-31 11:36:58', '2021-03-31 11:36:58');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table tae.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.settings: ~10 rows (approximately)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
	(1, 'site.title', 'Site Title', 'TAE', '', 'text', 1, 'Site'),
	(2, 'site.description', 'Site Description', 'TAE', '', 'text', 2, 'Site'),
	(3, 'site.logo', 'Site Logo', 'settings/April2021/bgvMhGMNgrjmyodojbU6.png', '', 'image', 3, 'Site'),
	(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
	(5, 'admin.bg_image', 'Admin Background Image', 'settings/April2021/DZdamJPPwlxYCJBA8L5y.jpg', '', 'image', 5, 'Admin'),
	(6, 'admin.title', 'Admin Title', 'Back Office', '', 'text', 1, 'Admin'),
	(7, 'admin.description', 'Admin Description', 'TAE', '', 'text', 2, 'Admin'),
	(8, 'admin.loader', 'Admin Loader', 'settings/April2021/KPHtdwDLKicEAVnw1gf5.png', '', 'image', 3, 'Admin'),
	(9, 'admin.icon_image', 'Admin Icon Image', 'settings/April2021/ebmPYEUC3FTsYQy2i8yH.png', '', 'image', 4, 'Admin'),
	(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table tae.speakers
CREATE TABLE IF NOT EXISTS `speakers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.speakers: ~2 rows (approximately)
DELETE FROM `speakers`;
/*!40000 ALTER TABLE `speakers` DISABLE KEYS */;
INSERT INTO `speakers` (`id`, `nom`, `poste`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'speaker 1', 'poste 1', NULL, '2021-04-13 13:23:09', '2021-04-13 13:23:09'),
	(2, 'speaker 2', 'poste 2', 'speakers\\April2021\\IYbOQq82DiHRGBcDBP6y.jpg', '2021-04-13 13:23:17', '2021-04-19 12:19:44');
/*!40000 ALTER TABLE `speakers` ENABLE KEYS */;

-- Dumping structure for table tae.speakers_progs
CREATE TABLE IF NOT EXISTS `speakers_progs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `speaker_id` int(10) unsigned DEFAULT NULL,
  `programme_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_formations_mobile_user_id_index` (`speaker_id`),
  KEY `users_formations_formation_id_index` (`programme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.speakers_progs: ~3 rows (approximately)
DELETE FROM `speakers_progs`;
/*!40000 ALTER TABLE `speakers_progs` DISABLE KEYS */;
INSERT INTO `speakers_progs` (`id`, `speaker_id`, `programme_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, NULL, NULL),
	(2, 2, 2, NULL, NULL),
	(3, 1, 1, NULL, NULL),
	(4, 1, 4, NULL, NULL),
	(5, 1, 10, NULL, NULL);
/*!40000 ALTER TABLE `speakers_progs` ENABLE KEYS */;

-- Dumping structure for table tae.translations
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.translations: ~30 rows (approximately)
DELETE FROM `translations`;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
	(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
	(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
	(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
	(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
	(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
	(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
	(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
	(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
	(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
	(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(21, 'menu_items', 'title', 2, 'pt', 'Media', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
	(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
	(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
	(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
	(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
	(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
	(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
	(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
	(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
	(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2021-03-31 11:37:19', '2021-03-31 11:37:19');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

-- Dumping structure for table tae.type_progs
CREATE TABLE IF NOT EXISTS `type_progs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.type_progs: ~3 rows (approximately)
DELETE FROM `type_progs`;
/*!40000 ALTER TABLE `type_progs` DISABLE KEYS */;
INSERT INTO `type_progs` (`id`, `titre`, `created_at`, `updated_at`) VALUES
	(1, 'Cocktail', '2021-04-13 13:20:44', '2021-04-13 13:20:44'),
	(2, 'Panel', '2021-04-13 13:21:06', '2021-04-13 13:21:06'),
	(3, 'Forum', '2021-04-13 13:21:19', '2021-04-13 13:21:19');
/*!40000 ALTER TABLE `type_progs` ENABLE KEYS */;

-- Dumping structure for table tae.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.users: ~3 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$wW7/KNKoOtguoBucOE2Jn.HhNM/7SnZcHV1/OVwahzxOpD4N49mQ6', 'GwPdOcrhZxhH5tYoEEXfxrvFQAqvdHtmTPorkZ0BYdt2qgKIyeZ8z9JlRdAj', NULL, '2021-03-31 11:37:13', '2021-03-31 11:37:13'),
	(2, 1, 'Admin', 'tae@xelero.io', 'users/April2021/niWGtZ4BG2p0XCGKVZtd.png', NULL, '$2y$10$REhBTrkiFOiiuPKI.d9jveYJe3fW0DEQAVPNrZE//VoJrMWjqBUeC', 'RY4mUw3yp39TNKKqIHytbcpP53dIh2MSa8i7fqdOTV6zlbGNlZ51mFGFVk83', '{"locale":"fr"}', '2021-04-06 10:59:43', '2021-04-06 10:59:43'),
	(3, 1, 'enis', 'enis.a@xelero.io', 'users/default.png', NULL, '$2y$10$MR3FADqjcGcYJ9w.ImC4h.5r8zOUgK.GicGXh6WLWG81vIJXnFrUy', 'ibeCQDhvzLN0te0DadA4dJQYsYRe81GYTPOvJjtt7r5sNnxOhubwinoprIzR', '{"locale":"fr"}', '2021-04-13 10:05:07', '2021-04-13 10:05:07');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table tae.users_interets
CREATE TABLE IF NOT EXISTS `users_interets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile_user_id` int(10) unsigned NOT NULL,
  `interet_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_interets_mobile_user_id_index` (`mobile_user_id`),
  KEY `users_interets_interets_id_index` (`interet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.users_interets: ~10 rows (approximately)
DELETE FROM `users_interets`;
/*!40000 ALTER TABLE `users_interets` DISABLE KEYS */;
INSERT INTO `users_interets` (`id`, `mobile_user_id`, `interet_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, NULL, NULL),
	(2, 2, 2, NULL, NULL),
	(3, 4, 3, '2021-04-13 10:37:34', '2021-04-13 10:37:34'),
	(4, 4, 4, '2021-04-13 10:37:34', '2021-04-13 10:37:34'),
	(5, 4, 5, '2021-04-13 10:39:48', '2021-04-13 10:39:48'),
	(6, 4, 6, '2021-04-13 10:39:48', '2021-04-13 10:39:48'),
	(7, 5, 6, '2021-04-13 10:50:10', '2021-04-13 10:50:10'),
	(8, 5, 7, '2021-04-13 10:50:10', '2021-04-13 10:50:10'),
	(9, 10, 3, '2021-04-16 09:41:37', '2021-04-16 09:41:37'),
	(10, 10, 6, '2021-04-16 09:41:37', '2021-04-16 09:41:37');
/*!40000 ALTER TABLE `users_interets` ENABLE KEYS */;

-- Dumping structure for table tae.users_objectifs
CREATE TABLE IF NOT EXISTS `users_objectifs` (
  `mobile_user_id` int(10) unsigned NOT NULL,
  `objectif_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `users_objectifs_objectif_id_index` (`objectif_id`),
  KEY `users_objectifs_mobile_user_id_index` (`mobile_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.users_objectifs: ~11 rows (approximately)
DELETE FROM `users_objectifs`;
/*!40000 ALTER TABLE `users_objectifs` DISABLE KEYS */;
INSERT INTO `users_objectifs` (`mobile_user_id`, `objectif_id`, `created_at`, `updated_at`, `id`) VALUES
	(1, 1, NULL, NULL, 1),
	(2, 1, NULL, NULL, 2),
	(2, 2, NULL, NULL, 3),
	(4, 4, '2021-04-13 10:37:34', '2021-04-13 10:37:34', 4),
	(4, 5, '2021-04-13 10:37:34', '2021-04-13 10:37:34', 5),
	(4, 6, '2021-04-13 10:39:48', '2021-04-13 10:39:48', 6),
	(5, 6, '2021-04-13 10:50:10', '2021-04-13 10:50:10', 7),
	(5, 7, '2021-04-13 10:50:10', '2021-04-13 10:50:10', 8),
	(10, 4, '2021-04-16 09:41:37', '2021-04-16 09:41:37', 9),
	(10, 5, '2021-04-16 09:41:37', '2021-04-16 09:41:37', 10),
	(10, 8, '2021-04-16 09:41:37', '2021-04-16 09:41:37', 11);
/*!40000 ALTER TABLE `users_objectifs` ENABLE KEYS */;

-- Dumping structure for table tae.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.user_roles: ~0 rows (approximately)
DELETE FROM `user_roles`;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
	(2, 1);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Dumping structure for table tae.wizard_roles
CREATE TABLE IF NOT EXISTS `wizard_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tae.wizard_roles: ~0 rows (approximately)
DELETE FROM `wizard_roles`;
/*!40000 ALTER TABLE `wizard_roles` DISABLE KEYS */;
INSERT INTO `wizard_roles` (`id`, `titre`, `created_at`, `updated_at`, `status`) VALUES
	(1, 'role 1', '2021-04-19 10:06:52', '2021-04-19 10:06:52', 1);
/*!40000 ALTER TABLE `wizard_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

