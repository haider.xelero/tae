<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MobileUser extends Model
{
    public  $activiteP;
    public  $activiteS;

	protected $fillable =
	[
		'token_device'
	];
    protected $hidden = [
        'mot_de_passe',
        'created_at',
        'updated_at',
        // 'formations',
        // 'experiences',
        // 'certifications',
        // 'interets',
        // 'objectifs',
    ];
}
