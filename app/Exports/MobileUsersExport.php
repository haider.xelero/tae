<?php

namespace App\Exports;

use App\MobileUser;
use Maatwebsite\Excel\Concerns\FromCollection;

class MobileUsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $user = new MobileUser();
        $user->nom = "nom";
        $user->prenom = "prenom";
        $user->email = "email";
        $user->email_professionnel = "email_professionnel";
        $user->entreprise = "entreprise";
        $users =  MobileUser::all('nom','prenom','email','email_professionnel','entreprise');

        // Add a new item to the beginning of the collection.
        $users->prepend($user);
        
        return $users;
    }
}
