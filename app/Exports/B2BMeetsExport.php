<?php

namespace App\Exports;

use App\B2bMeet;
use App\Programme;
use Maatwebsite\Excel\Concerns\FromCollection;

class B2BMeetsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $b2bMeet = new B2bMeet();
        $b2bMeet->titre = "Titre";
        $b2bMeet->date = "Date";
        $b2bMeet->status1 = "Status";
        $b2bMeet->prog_id = "Programme";

        $all = B2bMeet::select("titre","date","status1","prog_id")->orderBy('created_at', 'DESC')->get();

        foreach($all as $v)
        {
            $p = Programme::find($v->prog_id);
            if(isset($p))
            {
                $v->prog_id = $p->titre;
            }
            else
            {
                $v->prog_id = "";
            }
        }
        $all->prepend($b2bMeet);
        return $all;
    }

    
}
