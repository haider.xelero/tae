<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UsersDiscussion extends Model
{
    public function discussion()
    {
        // return $this->belongsTo(Discussion::class);
        return $this->belongsTo('App\Discussion', 'id_discu');

    }
}
