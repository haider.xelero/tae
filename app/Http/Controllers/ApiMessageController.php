<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MobileUser;
use App\Conversation;
use App\Message;
use PHPUnit\Exception;

class ApiMessageController extends Controller
{
    //public function which declared in the route

    public function sendMessageOneToOne(Request $request)
    {
        try
        {
            $user1 = MobileUser::find($request->sender_id);
            $user2 = MobileUser::find($request->receiver_id);

            if((!isset($user1)) OR (!isset($user2)))
                return ['status' => 'a user not exist'];

            $conversation = Conversation::where('users' , json_encode(array("id1" => $request->sender_id, "id2" => $request->receiver_id)))
                ->OrWhere('users' , json_encode(array("id1" => $request->receiver_id, "id2" => $request->sender_id)))->get()->first();
            if(isset($conversation))
            {
                $this->insertMessage($request->sender_id, $request->receiver_id, $request->contents, $conversation->id);
                return ['status' => 'success'];
            }

            $this->insertMessage($request->sender_id, $request->receiver_id, $request->contents, $this->start_conversation($request->sender_id, $request->receiver_id));
            return ['status' => 'success'];
        }
        catch(Exception $e)
        {
            return ['status' => 'error'];
        }
    }

    public function getAllMessagesByConversation($conversation_id, $my_id)
    {
        try
        {
            $messages = Message::where('conversation_id',$conversation_id)->orderBy('created_at', 'DESC')->get(); //->forPage(1, 3);

            $tab = [];
            $messagesBook = [];

            foreach($messages as $v)
            {
                if($v->sender_id == $my_id)
                {
                    $user = "Me";
                }
                else
                {
                    $mobileUser = MobileUser::find($v->sender_id);
                    $user = $mobileUser->prenom.' '.$mobileUser->nom;
                }
                $tab [] =
                [
                    'id' => $v->id,
                    'contents' => $v->contents,
                    'is_seen' => $v->is_seen,
                    'created_at' => $v->created_at,
                    'user' => $user
                ];
            }
            $index = 1;
            $array = [];
            foreach($tab as $v)
            {
                $array[] = $v ;
                if($index % 2 == 0)
                {
                    $messagesBook[] = $array;
                    $array = [];
                }
                if($index == count($tab))
                {
                    $messagesBook[] = $array;
                    $array = [];
                    break;
                }
                $index++;
            }
            $messagesBook = array_filter($messagesBook);
            $result =
            [
                'nbr_page' => count($messagesBook),
                'messages' => $messagesBook
            ];
            return response()->json($result);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function seenMessage(Request $request)
    {
        try
        {
            $message = Message::find($request->message_id);
            $message->is_seen = 1;
            $message->save();
            return ["status" => "success"];
        }
        catch(Exception $e)
        {
            return ["status" => "error"];
        }
    }

    public function getConversationByUser($id)
    {
        try
        {
            $this->conversation = Conversation::where('users','like','%\"'.$id.'\"%')->get();
            if(count($this->conversation) == 0)
                return response()->json(["message"=>"There is no any conversation"]);
            else
            {
                $tab = array();
                foreach ($this->conversation as $v)
                {
                    $array = $this->getConversationObjectFromUser($id, $v->users);
                    $v->image = asset("/storage/".$array['photo_de_profile']);
                    $v->users = $array['nom'].' '.$array['prenom'];
                    $tab[] = array("conversation" => $v, "last_message" => Message::where('conversation_id', $v['id'])->get()->last()->contents);
                }
                return $tab;
            }
        }
        catch(Exception $e)
        {
            return response()->json(["message"=>"Exception error"]);
        }
    }

    public function myAllConversation($id)
    {
        try
        {
            $listConversation = Conversation::all();
            $myConversation = [];
            foreach ($listConversation as $v)
            {
                $usersIdS = json_decode($v->users);
                if($id == $usersIdS->id1)
                {
                    $messages = Message::where('conversation_id',$v->id)->orderBy('created_at', 'DESC')->get();
                    $myConversation[] = array
                    (
                        "user" => MobileUser::find($usersIdS->id2),
                        "messages" => $messages,
                        "last_message" => $messages->first(),
                        "time" => $messages->first()->created_at,
                        "message_id" => $messages->first()->id
                    );
                }
                elseif ($id == $usersIdS->id2)
                {
                    $messages = Message::where('conversation_id',$v->id)->orderBy('created_at', 'DESC')->get();
                    $myConversation[] = array
                    (
                        "user" => MobileUser::find($usersIdS->id1),
                        "messages" => $messages,
                        "last_message" => $messages->first(),
                        "time" => $messages->first()->created_at,
                        "message_id" => $messages->first()->id
                    );
                }
            }
            $time = array_column($myConversation, 'time');
            array_multisort($time, SORT_DESC, $myConversation);
            return $myConversation;
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    public function findMyConversation($id)
    {
        try
        {

        }
        catch(Exception $e)
        {

        }
    }

    //private function which can be used only in this class
    private function start_conversation($id1, $id2)
    {
        $conversation = new Conversation();
        if($id1<$id2)
            $conversation->users = json_encode(array("id1" => $id1, "id2" => $id2));
        else
            if($id1>$id2)
                $conversation->users = json_encode(array("id1" => $id2, "id2" => $id1));
        $conversation->save();
        return $conversation->id;
    }

    private function insertMessage($sender_id, $receiver_id, $contents, $conversation_id)
    {
        $message = new Message();
        $message->contents = $contents;
        $message->is_seen = 0;
        $message->sender_id = $sender_id;
        $message->receiver_id = $receiver_id;
        $message->conversation_id = $conversation_id;
        $message->created_at = date('Y-m-d H:i:s');
        $message->updated_at = date('Y-m-d H:i:s');
        $message->save();
    }

    private function getConversationObjectFromUser($id, $json)
    {
        try
        {
            $array = json_decode($json);
            if($array->id1 == $id)
                $user = MobileUser::where('id', $array->id2)->first();
            else
                $user = MobileUser::where('id',$array->id1)->first();
            return  ["prenom" => $user->prenom, "nom" => $user->nom , "photo_de_profile" => $user->photo_de_profile];
        }
        catch (Exception $e)
        {
            return [];
        }
    }

}
