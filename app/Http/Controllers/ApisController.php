<?php
namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Mail\SendMail;
use Carbon\Carbon;
use Exception;
use App\MobileUser ;
use App\Formation ;
use App\Experience ;
use App\Certification ;
use App\Objectif ;
use App\Interet ;
use App\UsersObjectif ;
use App\UsersInteret ;
use App\Evenement ;
use App\Programme ;
use App\Speaker ;
use App\SpeakersProg ;
use App\B2bMeet ;
use App\B2bMeetUser ;
use App\Activite ;
use App\WizardRole ;
use App\Boarding ;
use App\BackLog ;
use App\Partenaire ;
use App\Organisateur ;
use App\Propo ;
use App\TypeProg;
use App\PartenairesEvent;
use App\OrgsEvent;
use App\React;
use App\Stand;
use \Storage;
use App\UserDetail;
use App\Notification;
use App\B2bActive;

class ApisController extends Controller
{
    private $UrlPushNotif = "https://exp.host/--/api/v2/push/send";

    private function isValidEmail($email) // this function can verify the email format
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    //************ Function Wizard ************
    public function Wizard(request $request)
    {
        $date = Carbon::now();
        $date_de_naissance = Carbon::createFromDate($request->date_de_naissance);
        $pass = Hash::make($request->mot_de_passe);
        $verifEmail = MobileUser::where('email', $request->email)->orWhere('id',$request->id)->first();
        $objectifs = Objectif::all();
        $interets = Interet::all();
        $ListUsersObjectif = UsersObjectif::all();
        $ListUsersInteret = UsersInteret::all();

        if (isset($verifEmail))
        {
            if($verifEmail->type_auth_api == "appel")
            {
                $verifEmail->nom = "UserSitic";
                $verifEmail->prenom = "#".$verifEmail->id;
                $verifEmail->email = "";
            }
            else
            {
                $verifEmail->nom = $request->nom;
                $verifEmail->prenom = $request->prenom;
            }
            $verifEmail->date_de_naissance = Carbon::createFromDate($request->date_de_naissance);
            $verifEmail->pays_de_residence = $request->country;
            $verifEmail->telephone = $request->telephone;
            $verifEmail->email_professionnel = $request->email_pro;
            $verifEmail->sexe = $request->gender;
            $verifEmail->entreprise = $request->entreprise_name;
            $verifEmail->site_web = $request->website;
            $verifEmail->new = 0;
            $verifEmail->access_b2b = 0; // active b2b
            $verifEmail->localite_entreprise = $request->entreprise_country;
            $verifEmail->domaine_activite_p = $request->activity_domain; // we should change the variable type
            //$verifEmail->domaine_activite_s = $request->sec_activity_domain; // we should change the variable type
            if(empty($request->sec_activity_domain))
                $verifEmail->domaine_activite_s = null;
            else
                $verifEmail->domaine_activite_s = $request->sec_activity_domain;
            $verifEmail->role = $request->role; // integer
            $verifEmail->updated_at = $date;
            if ($verifEmail->type_auth_api != "email") //comment it by Habib
            {
                //  $verifEmail->email = $request->email;
                $verifEmail->mot_de_passe = $pass;
            }
            $verifInscription = $verifEmail->save();

            //  insert data into interets
            foreach ($request->interets as $key => $value)
            {
                $SearchString = $this->SearchString($value, $interets);
                if ($SearchString == 0)
                {
                    $Interet = new Interet();
                    $Interet->titre = $value;
                    $Interet->status = 0;
                    $Interet->save();
                }

                if (!$this->SearchUserIdInteret($verifEmail->id, $SearchString == 0 ? $Interet->id : $SearchString, $ListUsersInteret))
                {
                    $UsersInteret = new UsersInteret();
                    $UsersInteret->mobile_user_id = $verifEmail->id;
                    $UsersInteret->interet_id = $SearchString == 0 ? $Interet->id : $SearchString;
                    $UsersInteret->created_at = $date;
                    $UsersInteret->save();
                }
            }
            //  insert data into objectifs
            foreach ($request->objectifs as $key => $value)
            {
                $SearchString = $this->SearchString($value, $objectifs);
                if ($SearchString == 0)
                {
                    $Objectif = new Objectif();
                    $Objectif->titre = $value;
                    $Objectif->status = 0;
                    $Objectif->save();
                }

                if (!$this->SearchUserIdObjectif($verifEmail->id, $SearchString == 0 ? $Objectif->id : $SearchString, $ListUsersObjectif))
                {
                    $UsersObjectif = new UsersObjectif();
                    $UsersObjectif->mobile_user_id = $verifEmail->id;
                    $UsersObjectif->objectif_id = $SearchString == 0 ? $Objectif->id : $SearchString;
                    $UsersObjectif->created_at = $date;
                    $UsersObjectif->save();
                }
            }

            // insert data into experiences
            foreach ($request->experiences as $key => $value)
            {
                $Experience = new Experience();
                $Experience->role = $value['role'];
                $Experience->entreprise = $value['company'];
                $Experience->site_web = $value['company_website'];
                $Experience->date_debut = Carbon::createFromDate($value['dateDebFormatted']);
                $Experience->date_fin = Carbon::createFromDate($value['dateFinFormatted']);
                $Experience->actuellement = $value['currently_work'];
                $Experience->created_at = $date;
                $Experience->user_id = $verifEmail->id;
                $Experience->save();
            }

            // insert data into formations
            foreach ($request->formations as $key => $value)
            {
                $formation = new Formation();
                $formation->ecole = $value['school'];
                $formation->diplome = $value['degree'];
                $formation->domaine = $value['field_of_study'];
                $formation->date_debut = Carbon::createFromDate($value['dateDebFormatted']);
                $formation->date_fin = Carbon::createFromDate($value['dateFinFormatted']);
                $formation->grade = $value['grade'];
                $formation->created_at = $date;
                $formation->user_id = $verifEmail->id;
                $formation->save();
            }
            // insert data into certifications
            foreach ($request->certifs as $key => $value)
            {
                $certif = new Certification();
                $certif->certification = $value['certif'];
                $certif->discipline = $value['discipline'];
                $certif->organisation = $value['org'];
                $certif->date = Carbon::createFromDate($value['dateDebFormatted']);
                $certif->created_at = $date;
                $certif->user_id = $verifEmail->id;
                $certif->save();
            }
            return response()->json(isset($verifInscription));
        }
        return response()->json(false);
    }

    //************ Function WizadInfo ************
    public function WizardInfo()
    {
        $objectifsDB = Objectif::where('status', 1)->get("titre");
        $interetsDB = Interet::where('status', 1)->get("titre");
        $interestsFiltresDB = Interet::where('status', 1)->get();
        $activitesPDB = Activite::where('status', 1)->where('type', 'Principale')->get();
        $activitesSDB = Activite::where('status', 1)->where('type', 'Secondaire')->get();
        $rolesDB = WizardRole::where('status', 1)->get();

        $objectifs = [];
        $interets = [];
        $activitesP = [];
        $activitesS = [];
        $roles = [];
        $interestsFiltres = [];

        foreach ($objectifsDB as $key => $value) {
            $objectifs[] = $value->titre;
        }
        foreach ($interetsDB as $key => $value) {
            $interets[] = $value->titre;
        }
        foreach ($activitesPDB as $key => $value) {
            $obj = ["label" => $value->titre, "value" => $value->titre, "id" => $value->id];
            $activitesP[] = $obj;
        }
        foreach ($activitesSDB as $key => $value) {

            $obj = ["label" => $value->titre, "value" => $value->titre, "id" => $value->id];
            $activitesS[] = $obj;
        }
        foreach ($rolesDB as $key => $value) {
            $obj = ["label" => $value->titre, "value" => $value->titre, "id"=> $value->id];
            $roles[] = $obj;
        }

        foreach($interestsFiltresDB as $v)
        {
            $interestsFiltres[] =
                [
                    "id" => $v->id,
                    "titre" => $v->titre
                ];
        }
        $obj = [
            "objectifs" => $objectifs,
            "interets" => $interets,
            "activitesP" => $activitesP,
            "activitesS" => $activitesS,
            "roles" => $roles,
            "interestsFiltres" => $interestsFiltres,
        ];
        return response()->json($obj);
    }

    //************ Function Inscription ************
    public function Inscription(request $request)
    {
        $mobile_user = MobileUser::where('email', $request->email)->first();
        if (empty($mobile_user)) 
        {
            $mobile_user = new MobileUser();
            $mobile_user->created_at = Carbon::now();
            $mobile_user->updated_at = Carbon::now();
            $mobile_user->email = strtolower($request->email);
            $mobile_user->type_auth_api = "email";
            $mobile_user->mot_de_passe = Hash::make($request->mot_de_passe);
            $mobile_user->new = 1;
            $tab = array();
            if(isset($request->token_device))
                $tab[] = $request->token_device;
            $mobile_user->token_device = json_encode($tab);
            $mobile_user->save();
            return response()->json
            (
                $this->getUser($mobile_user->id)
            );
            //return response()->json(['response' => 'Your registration is successful']);
        } 
        else 
        {
            return response()->json(['response' => 'This is email is already exist']);
        }
    }

    //*******************************
    
    //******************************************

    //___________________Auth test ___________________
    public function Auth(Request $request)
    {
        //try
        {
            if(isset($request->type_auth_api))
            {
                //if(isset($request->token_device))
                {
                    switch (strtolower($request->type_auth_api))
                    {
                        case "email" :      if((isset($request->email)) AND (isset($request->mot_de_passe)))
                        {
                            if(!$this->isValidEmail($request->email))
                            {
                                return response()->json("is not email format");
                            }
                            else
                            {
                                return $this->AuthNormal($request->email,$request->mot_de_passe, $request->token_device);
                            }
                        }
                        else
                        {
                            return response()->json
                            ("to authenticate with the normal method you should enter your email and password");
                        }
                            break;
                        case "gmail" :      if(isset($request->data))
                        {

                            return $this->AuthGmail($request->data, $request->token_device);
                        }
                        else
                        {
                            return response()->json
                            ("The presence of the object is mandatory (it's json object)");
                        }
                            break;
                        case "linkedin" :   if(isset($request->token))
                        {
                            return $this->AuthLinkedin($request->token, $request->token_device );
                        }
                        else
                        {
                            return response()->json("The presence of the token is mandatory");
                        }
                            break;
                        case "appel" : return $this->AuthAppel($request->data,$request->token_device);
                            break;
                        default :   return response()->json("type_auth_api is not available");
                            break;
                    }
                }
                //else
                //{
                    //return response()->json
                    //("The presence of the device code is mandatory");
                //}
            }
            else
            {
                return response()->json("You should enter the paramter type_auth_api");
            }
        }
        //catch(Exception $e)
        {
            //return response()->json
            //("Exception error");
        }
    }

    //________________ function AuthNormal ______________
    private function AuthNormal($email, $password, $token_device)
    {
        $user_mobile = MobileUser::where('email',$email)->first();
        if(isset($user_mobile))
        {
            if (strtolower($user_mobile->type_auth_api) == "email")
            {
                $user_mobile = MobileUser::where('email',strtolower($email))->first();
                if(isset($user_mobile))
                {
                    if(Hash::check($password, $user_mobile->mot_de_passe))
                    {
                        if(isset($token_device))
                        {
                            if (strlen($user_mobile->token_device) > 2)
                            {
                                if (!$this->SearchTokenDevice($user_mobile->token_device,$token_device))
                                {
                                    $tab = (array)json_decode($user_mobile->token_device); //we should cast the object stdClass to array
                                    $tab[] = $token_device; //add new element in the table $tab
                                    $user_mobile->token_device = json_encode($tab) ;
                                }
                            }
                            else
                            {
                                $user_mobile->token_device = json_encode([$token_device]) ;
                            }
                        }
                        $user_mobile->updated_at = date("Y-m-d H:m:s");
                        $user_mobile->save();
                    }
                    else
                    {
                        return response()->json
                        (
                            "Your password is not correct"
                        );
                    }
                }
                else
                {
                    return response()->json
                    (
                        'Your email does not exist'
                    );
                }
                //return response()->json($user_mobile); //user mobile connected
                return response()->json($this->getUser($user_mobile->id));
            }
            else
            {
                return response()->json
                (
                    "This user is already exist but he registered with other social media"
                );
            }
        }
        else
        {
            return response()->json
            (
               "Your email does not exist"
            );
        }
        //return response()->json($user_mobile); //user mobile connected
    }

    public function AuthAppel($object,$token_device)
    {
        try
        {
            $json = (stripslashes($object));
            $tab = json_decode($json);
            if((isset($tab->email)) AND (isset($tab->user))) //the first Auth with appel we can see the email
            {
                $verifMobile = MobileUser::where('email',$tab->email)
                    ->where('appel_id', $tab->user)
                    ->first();
                if(!isset($verifMobile))
                {
                    $mobileUser = new MobileUser();
                    $mobileUser->email = $tab->email;
					$mobileUser->new = 1;
                    $mobileUser->type_auth_api = "appel";
                    $mobileUser->appel_id = $tab->user;
                    //insert the token_device
                    $tab = array();
                    if(isset($token_device))
                        $tab[] = $token_device;
                    $mobileUser->token_device = json_encode($tab);
                    $mobileUser->save();
                    return $this->getUser($mobileUser->id);
                }
            }
            if((isset($tab->user)) AND (!isset($tab->email)))
            {
                $verifMobile = MobileUser::where('appel_id', $tab->user)->first();
                if(isset($verifMobile))
                {
                    if(isset($token_device))
                    {
                        if(strlen($verifMobile->token_device) > 2)
                        {
                            if (!$this->SearchTokenDevice($verifMobile->token_device,$token_device))
                            {
                                $tab = (array)json_decode($verifMobile->token_device); //we should cast the object stdClass to array
                                $tab[] = $token_device; //add new element in the table $tab
                                $verifMobile->token_device = json_encode($tab) ;
                            }
                        }
                        else
                        {
                            $verifMobile->token_device = json_encode([$token_device]) ;
                        }
                    }
                    $verifMobile->save();
                    return $this->getUser($verifMobile->id);
                }
                $mobileUser = new MobileUser();
                $mobileUser->type_auth_api = "appel";
				$mobileUser->new = 1;
                $mobileUser->appel_id = $tab->user;
                //insert the token_device
                $tab = array();
                if(isset($token_device))
                    $tab[] = $token_device;
                $mobileUser->token_device = json_encode($tab);
                $mobileUser->save();
                return $this->getUser($mobileUser->id);
            }
            return response()->json("This email already exist");

            //the user is already signed with
        }
        catch(Exception $e)
        {
            return response()->json("exception error");
        }
    }
    //____________________________________________________

    //***************************** Function logout ****************************************
    public function Logout(Request $request)
    {
        $user_mobile = MobileUser::find($request->id);
        if (isset($user_mobile))
        {
            $tab = (array)json_decode($user_mobile->token_device); // we should cast it to get an array
            unset($tab[array_search($request->token_device, $tab)]); //search the key of the element to get the key than we use it to delete the element
            $user_mobile->token_device = json_encode($tab);
            $user_mobile->save();
            return response()->json
            (
                [
                    'response' => 'You logged out'
                ], 200
            );
        }
        else
        {
            return response()->json
            (
                'This user is not found'
            );
        }
    }

    //************ Function WizadInfo ************
    public function ListParticipant()
    {
        $T = MobileUser::all();
        foreach ($T as $v)
        {
            $role = WizardRole::find($v->role);
            $v->photo_de_profile = asset('/storage/' . $v->photo_de_profile);
            if(isset($v->role))
                $v->role = $role->titre;
        }
        return response()->json([$T], 200);
    }

    //************ Function Detail Profil ************
    public function DetailProfil($id)
    {
        return response()->json($this->getUser($id));
        /*
        $MobileUser = MobileUser::where('id', $id)->first();
        $experiences = Experience::where('user_id', $id)->get();
        $formations = Formation::where('user_id', $id)->get();
        $certifications = Certification::where('user_id', $id)->get();


        $interetsDB = DB::table('users_interets')
            ->Join('interets', 'users_interets.interet_id', '=', 'interets.id')
            ->Join('mobile_users', 'users_interets.mobile_user_id', '=', 'mobile_users.id')
            ->where('mobile_users.id', $id)
            ->select('interets.titre')
            ->get();

        $interets = [];

        foreach ($interetsDB as $key => $value) {
            $interets[] = $value->titre;
        }

        $objectifsDB = DB::table('users_objectifs')
            ->Join('objectifs', 'users_objectifs.objectif_id', '=', 'objectifs.id')
            ->Join('mobile_users', 'users_objectifs.mobile_user_id', '=', 'mobile_users.id')
            ->where('mobile_users.id', $id)
            ->select('objectifs.titre')
            ->get();

        $objectifs = [];

        foreach ($objectifsDB as $key => $value) {
            $objectifs[] = $value->titre;
        }

        $MobileUser->interets = $interets;
        $MobileUser->objectifs = $objectifs;
        $MobileUser->experiences = $experiences;
        $MobileUser->formations = $formations;
        $MobileUser->certifications = $certifications;
        $MobileUser->activiteP = "titre"; //$MobileUser->domaine_activite_p;
        $MobileUser->activiteS = $MobileUser->domaine_activite_s;
        $role = WizardRole::where('id', $MobileUser->role)->first();
        $activitesPDB = Activite::where('status', 1)->where('type', 'Principale')->where('id',$MobileUser->domaine_activite_p)->first();
        $titreP = "";
        $titreS = "";
        if(isset($activitesPDB))
            $titreP = $activitesPDB->titre;
        $activitesSDB = Activite::where('status', 1)->where('type', 'Secondaire')->where('id',$MobileUser->domaine_activite_s)->first();
        if(isset($activitesSDB))
            $titreS = $activitesSDB->titre;
        $tab =
            [
                "id" => $MobileUser->id,
                "nom" => $MobileUser->nom,
                "prenom" => $MobileUser->prenom,
                "date_de_naissance" => $MobileUser->date_de_naissance,
                "pays_de_residence" => $MobileUser->pays_de_residence,
                "telephone" => $MobileUser->telephone,
                "sexe" => $MobileUser->sexe,
                "entreprise" => $MobileUser->entreprise,
                "site_web" => $MobileUser->site_web,
                "localite_entreprise" => $MobileUser->localite_entreprise,
                "objectifs" => $objectifs,
                "interets" => $interets,
                "formations" => $formations,
                "experiences" => $experiences,
                "certifications" => $certifications,
                "token_device" => $MobileUser->token_device,
                "token_api" => $MobileUser->token_api,
                "type_auth_api" => $MobileUser->type_auth_api,
                "email" => $MobileUser->email,
                "photo_de_profile" => asset($MobileUser->photo_de_profile),
                "description" => $MobileUser->description,
                "role" => $role->titre,
                "domaine_activite_p" => $titreP,
                "domaine_activite_s" => $titreS
            ];
        return response()->json($tab);
        */
    }

    private function findSpeakersByIdProgramme($id)
    {
        $speaker = Speaker::where('id', $id)->get()->first();
        $speaker->image = asset('storage/' . $speaker->image);
        return $speaker;
    }

    public function findSpeakersByProgramme($id)
    {
        $speakersProg = SpeakersProg::where('programme_id', $id)->get();
        //return $speakersProg;
        $tab = array();

        foreach ($speakersProg as $v)
        {
            $tab[] = $this->findSpeakersByIdProgramme($v['speaker_id']);
        }
        return $tab;
    }

    private function findProgByEventAndDate($event, $date)
    {
        $progs = Programme::where('event_id', $event)->Where('date_prog', $date)->orderBy('heure_debut')->get();
        $tab = array();
        foreach ($progs as $v) 
        {
            $typeProg = TypeProg::where('id', $v['type_id'])->get()->first(); //->titre;
            if(!isset($typeProg))
                $typeProg = "";
            else
                $typeProg = $typeProg->titre;
            $tab[] = array
            (
                "id" => $v['id'],
                "titre" => $v['titre'],
                "description" => $v['description'],
                "heure_debut" => $v['heure_debut'],
                "heure_fin" => $v['heure_fin'],
                "date_prog" => $v['date_prog'],
                //"type" => TypeProg::where('id', $v['type_id'])->get()->first()->titre,
                "type" => $typeProg,
                "event_id" => $v['event_id'],
                "location_programme" => $v['location_programme'],
                "date_debut_pause" => $v['date_debut_pause'],
                "date_fin_pause" => $v['date_fin_pause'],
                "created_at" => $v['created_at'],
                "updated_at" => $v['updated_at'],
                "speakers" => $this->findSpeakersByProgramme($v['id'])
            );
        }
        return $tab;
    }

    public function getAllEvent()
    {
        $events = Evenement::all();
        $tab = array();
        foreach ($events as $v) {
            $tab[] = $this->getEvent($v['id']);
        }
        $events = array("Events" => $tab);
        return $events;
    }

    private function findOrganisateurByEvenet($id)
    {
        $organisateur = Organisateur::where('id', $id)->get()->first();
        $organisateur->image = asset('storage/' . $organisateur->image);
        return $organisateur;
    }

    private function findPartenaireByEvent($id)
    {
        $partenaire = Partenaire::where('id', $id)->get()->first();
        if(isset($partenaire->image))
            $partenaire->image = asset('storage/' . $partenaire->image);
        return $partenaire;
    }

    public function getEvent($id)
    {
        $event = Evenement::where('id', $id)->get()->first(); //when I use the method find it return all the Event, I don't know why ?
        $orgs_event = OrgsEvent::where('evenement_id', $event->id)->get();
        $partenaire_event = PartenairesEvent::where('evenement_id', $event->id)->get();
        $tabOrganisateurs = array();
        $tabPartenaires = array();
        foreach ($orgs_event as $v) {
            $tabOrganisateurs[] = $this->findOrganisateurByEvenet($v['organisateur_id']);
        }
        foreach ($partenaire_event as $v) {
            $tabPartenaires[] = $this->findPartenaireByEvent($v['partenaire_id']);
        }
        $begin = new \DateTime($event->date_debut);
        $end = new \DateTime($event->date_fin);

        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);
        $days = array();
        $i = 0;
        foreach ($period as $dt) {
            $nb = $i + 1;
            //$days[$i] = $Array=array("day $nb" => $this->findProgByEventAndDate($event->id,$dt->format("Y-m-d")));
            $days[$i] = $Array = array($dt->format("Y-m-d") => $this->findProgByEventAndDate($event->id, $dt->format("Y-m-d")));
            $i++;
        }
        $tab = array();
        $tab = array
        (
            "id" => $event->id,
            "titre" => $event->titre,
            "date_debut" => $event->date_debut,
            "date_fin" => $event->date_fin,
            "description" => $event->description,
            "created_at" => $event->created_at,
            "updated_at" => $event->updated_at,
            "location_event" => $event->location_event,
            "image_event" => asset('storage/' . $event->image_event),
            "image_couverture" => asset('storage/' . $event->image_couverture),
            "longitude" => $event->longitude,
            "latitude" => $event->latitude,
            "address_event" => $event->address_event,
            "Organisateurs" => $tabOrganisateurs,
            "Partenaires" => $tabPartenaires,
            "days" => $days
        );
        return $tab;
    }

    //************ Function Liste des evenements ************
    public function Events()
    {
        $events = Evenement::all();
        foreach ($events as $key => $event) {

            $progs = Programme::where('event_id', $event->id)->get();
            foreach ($progs as $key2 => $prog) {

                $SpeakersProgs = SpeakersProg::where('programme_id', $prog->id)->get();

                $tab = new Collection();
                foreach ($SpeakersProgs as $key3 => $SpeakersProg) {
                    $Speaker = Speaker::where('id', $SpeakersProg->speaker_id)->first();
                    $Speaker['image'] = str_replace("\\", "/", $Speaker['image']);
                    $Speaker['image'] = asset('storage/' . $Speaker['image']);
                    $tab->push($Speaker);
                }

                $prog->SpeakersProgs = $tab;
            }
            $event->progs = $progs;
        }
        return response()->json($events);
    }

    /*show the free days for a user : Interface ( selection jour + timing slot )*/
    public function showFreeTimeSlots(Request $request)
    {
        $userid = $request->rec_user_id;//reciver id
        $prog_id = $request->prog_id;
        $intervallaps = 30;// 30 MINUTES
        /*// OLD get all event days
         $events = Evenement::where('is_active', 1)->orderBy('id', 'asc')->get();
         */
        //GET ONLY THE PROGRAM DAY(S)
        $events = DB::table('programmes')
            ->Join('evenements', 'evenements.id', '=', 'programmes.event_id')
            ->where('programmes.id', $prog_id)
            ->select('evenements.date_debut')
            ->get();


        $prog = Programme::where('id', $prog_id)->first();
        //return $prog;
        if ($prog)
        {
            $date_debut = substr($prog->heure_debut, 0, 5);
            $date_fin = substr($prog->heure_fin, 0, 5);
            //return $date_fin;
            $date_debut_pause = substr($prog->date_debut_pause, 0, 5);
            $date_fin_pause = substr($prog->date_fin_pause, 0, 5);
        } 
        else
        {
            $data = ["Program not found on our database"];
            return response()->json($data);
        }
        $lapsoftimes = $this->SplitTime($date_debut, $date_fin, "$intervallaps");

        $data = [];
        foreach ($events as $key => $event) 
        {
            $infoday = [];
            $infoday2 = [];
            $data[$key]['eventday'] = $event->date_debut;
            $date = \Carbon\Carbon::parse($event->date_debut);
            $date = $date->format('Y-m-d');
            //return $date;
            $meets = DB::select('select * from b2b_meet_users where DATE(date_action)=?', [$date]); //select * from b2b_meet_users where DATE(date_action) = '2022-11-02';
            //return $meets;
            $meetsbyuserNotavailble = DB::select('select * from b2b_meet_users where DATE(date_action)=? and mobile_user_id=? and confirmer=1 and refuser=0', [$date, $userid]);
            //return $meetsbyuserNotavailble;
            $notavailbdebfin = [];
            foreach ($meetsbyuserNotavailble as $meetnotavailb) {
                $notavailbdebfin[] = substr($meetnotavailb->date_deb_meet, 0, 5) . '-' . substr($meetnotavailb->date_fin_meet, 0, 5);
            }
            //return $notavailbdebfin;

            $tabdebfin = [];

            foreach ($meets as $meet) {
                $tabdebfin[] = substr($meet->date_deb_meet, 0, 5) . '-' . substr($meet->date_fin_meet, 0, 5);
            }
            //return $tabdebfin;
            //compter le nombre de meeting on same hours no more than 50
            $countvals = array_count_values($tabdebfin);
            //return var_dump($countvals);
            //return $lapsoftimes;
            foreach ($lapsoftimes as $key1 => $interval) 
            {
                $infoday = [];
                if (isset($lapsoftimes[$key1 + 1])) 
                {
                    //return "good is habib";
                    $begin = $lapsoftimes[$key1];
                    $fin = $lapsoftimes[$key1 + 1];
                    if (strtotime($fin) <= strtotime($date_debut_pause)) 
                    {
                        $infoday['time'] = "$begin-$fin";
                        $infoday['status'] = true;
                        $infoday['occurence'] = 0;
                        if (isset($countvals["$begin-$fin"])) 
                        {
                            $infoday['occurence'] = $countvals["$begin-$fin"];
                            if (($countvals["$begin-$fin"] >= 50) || (in_array("$begin-$fin", $notavailbdebfin))) 
                            {
                                $infoday['status'] = false;
                            }
                        }
                        $infoday2[] = $infoday;
                    } else if (strtotime($fin) > strtotime($date_fin_pause)) {
                        $infoday['time'] = "$begin-$fin";
                        $infoday['status'] = true;
                        $infoday['occurence'] = 0;
                        if (isset($countvals["$begin-$fin"])) {
                            $infoday['occurence'] = $countvals["$begin-$fin"];
                            if (($countvals["$begin-$fin"] >= 50) || (in_array("$begin-$fin", $notavailbdebfin))) {
                                $infoday['status'] = false;
                            }
                        }
                        $infoday2[] = $infoday;
                    }
                }
            }
            //return $infoday;

            $data[$key]["infosday"] = $infoday2;
        }//fin foreach
        return response()->json($data);
    }

    //updated at 17-05-2022

    public function showFreeTimeByProgram($rec_user_id, $prog_id)
    {
        $userid = $rec_user_id;//reciver id
        $prog_id = $prog_id;
        $intervallaps = 30;// 30 MINUTES
        /*// OLD get all event days
         $events = Evenement::where('is_active', 1)->orderBy('id', 'asc')->get();
         */
        //GET ONLY THE PROGRAM DAY(S)
        $events = DB::table('programmes')
            ->Join('evenements', 'evenements.id', '=', 'programmes.event_id')
            ->where('programmes.id', $prog_id)
            ->select('evenements.date_debut')
            ->get();


        $prog = Programme::where('id', $prog_id)->first();
        //return $prog;
        if ($prog)
        {
            $date_debut = substr($prog->heure_debut, 0, 5);
            $date_fin = substr($prog->heure_fin, 0, 5);
            //return $date_fin;
            $date_debut_pause = substr($prog->date_debut_pause, 0, 5);
            $date_fin_pause = substr($prog->date_fin_pause, 0, 5);
        } 
        else
        {
            $data = ["Program not found on our database"];
            return response()->json($data);
        }
        $lapsoftimes = $this->SplitTime($date_debut, $date_fin, "$intervallaps");

        $data = [];
        foreach ($events as $key => $event) 
        {
            $infoday = [];
            $infoday2 = [];
            //$data[$key]['eventday'] = $event->date_debut; $prog
            $data[$key]['eventday'] = $prog->date_prog;
            $date = \Carbon\Carbon::parse($event->date_debut);
            $date = $date->format('Y-m-d');
            //return $date;
            $meets = DB::select('select * from b2b_meet_users where DATE(date_action)=?', [$date]); //select * from b2b_meet_users where DATE(date_action) = '2022-11-02';
            //return $meets;
            $meetsbyuserNotavailble = DB::select('select * from b2b_meet_users where DATE(date_action)=? and mobile_user_id=? and confirmer=1 and refuser=0', [$date, $userid]);
            //return $meetsbyuserNotavailble;
            $notavailbdebfin = [];
            foreach ($meetsbyuserNotavailble as $meetnotavailb) {
                $notavailbdebfin[] = substr($meetnotavailb->date_deb_meet, 0, 5) . '-' . substr($meetnotavailb->date_fin_meet, 0, 5);
            }
            //return $notavailbdebfin;

            $tabdebfin = [];

            foreach ($meets as $meet) {
                $tabdebfin[] = substr($meet->date_deb_meet, 0, 5) . '-' . substr($meet->date_fin_meet, 0, 5);
            }
            //return $tabdebfin;
            //compter le nombre de meeting on same hours no more than 50
            $countvals = array_count_values($tabdebfin);
            //return var_dump($countvals);
            //return $lapsoftimes;
            foreach ($lapsoftimes as $key1 => $interval) 
            {
                $infoday = [];
                if (isset($lapsoftimes[$key1 + 1])) 
                {
                    //return "good is habib";
                    $begin = $lapsoftimes[$key1];
                    $fin = $lapsoftimes[$key1 + 1];
                    if (strtotime($fin) <= strtotime($date_debut_pause)) 
                    {
                        $infoday['time'] = "$begin-$fin";
                        $tab = $this->bookedB2bProgram($userid, $prog_id);
                        /*
                        foreach($tab as $v)
                        {
                            if($v == "$begin-$fin")
                            {
                                $test = false;
                                break;
                            }
                            else
                            {
                                $test = true;
                            }
                        }

                        $infoday['status'] = $test;
                        */
                        $infoday['status'] = true;
                        $infoday['occurence'] = 0;
                        if (isset($countvals["$begin-$fin"])) 
                        {
                            $infoday['occurence'] = $countvals["$begin-$fin"];
                            if (($countvals["$begin-$fin"] >= 50) || (in_array("$begin-$fin", $notavailbdebfin))) 
                            {
                                $infoday['status'] = false;
                            }
                        }
                        $infoday2[] = $infoday;
                    } else if (strtotime($fin) > strtotime($date_fin_pause)) {
                        $infoday['time'] = "$begin-$fin";
                        $tab = $this->bookedB2bProgram($userid, $prog_id);
                        /*$test = false;
                        foreach($tab as $v)
                        {
                            if($v == "$begin-$fin")
                            {
                                $test = false;
                                break;
                            }
                            else
                            {
                                $test = true;
                            }
                        }
                        $infoday['status'] = $test;
                        */
                        $infoday['status'] = true;
                        if (isset($countvals["$begin-$fin"])) {
                            $infoday['occurence'] = $countvals["$begin-$fin"];
                            if (($countvals["$begin-$fin"] >= 50) || (in_array("$begin-$fin", $notavailbdebfin))) {
                                $infoday['status'] = false;
                            }
                        }
                        $infoday2[] = $infoday;
                    }
                }
            }
            //return $infoday;
            $data[$key]['prog_id'] = $prog_id;
            $data[$key]["infosday"] = $infoday2;
        }//fin foreach
        return $data;
    }

    public function getTimeSlotsofAllB2bPrograms(Request $request)
    {
        $type_prog = TypeProg::where('titre','B2B meeting')->first();
        $programs = Programme::where('type_id',$type_prog->id)->orderBy('date_prog', 'asc')->get();
        $tab = [];
        foreach($programs as $v)
        {
            $tab[] = $this->showFreeTimeByProgram($request->rec_user_id, $v->id);
        }
        return $tab;
    }

    public function bookedB2bProgram($user_id, $program_id)
    {
        $b2b =  B2bMeet::where('prog_id', $program_id)->get();
        $tab = [];
        $tab1 = [];
        foreach($b2b as $v)
        {
            $b2bMeetUser = B2bMeetUser::where('mobile_user_id',$user_id)
                            ->where('b2b_meet_id',$v->id)
                            ->first();
            if(isset($b2bMeetUser))
            {
                $tab1[] = $b2bMeetUser;
                if($b2bMeetUser->confirmer == 1)
                {
                    $b2bMeetUser->date_deb_meet = strtotime($b2bMeetUser->date_deb_meet);
                    $b2bMeetUser->date_deb_meet = date('H:i', $b2bMeetUser->date_deb_meet);

                    $b2bMeetUser->date_fin_meet = strtotime($b2bMeetUser->date_fin_meet);
                    $b2bMeetUser->date_fin_meet= date('H:i', $b2bMeetUser->date_fin_meet);
                    $tab[] = $b2bMeetUser->date_deb_meet.'-'.$b2bMeetUser->date_fin_meet;
                }
            }
        }
        return $tab;
    }

    public function testBookedB2bProgram()
    {
        return $this->bookedB2bProgram(98, 27);
        //return $this->bookedB2bProgram(251, 32);
    }

    //************ Function Send invitation B2b ************
    public function SendB2b(Request $request)
    {
        $date = Carbon::now();
        $verif = null;
        // $verif2 = B2bMeetUser::where('mobile_user_id',$request->rec_user_id)->get();
        //verifier si il y a une reservation deja dans la date est pris ou pas
        $verif = DB::table('b2b_meet_users')
            ->Join('b2b_meets', 'b2b_meet_users.b2b_meet_id', '=', 'b2b_meets.id')
            ->Join('mobile_users', 'b2b_meet_users.mobile_user_id', '=', 'mobile_users.id')
            ->where('mobile_user_id', $request->rec_user_id)
            ->where('date_deb_meet', $request->date_deb)
            ->where('date_fin_meet', $request->date_fin)
            ->where('b2b_meets.prog_id', $request->prog_id)
            ->where('confirmer', 1)
            // ->where('refuser', 0)
            ->select('b2b_meet_users.*', 'mobile_users.email', 'b2b_meets.prog_id')
            ->first();

        // return response()->json($request);

        if (!isset($verif)) 
        {
            $verifB2b = $this->verifB2bMeet($request->send_user_id, $request->rec_user_id , $request->prog_id);
            if($verifB2b)
            {
                $meet = new B2bMeet();
                $meet->titre = $request->titre;
                $meet->prog_id = $request->prog_id;
                $p = Programme::find($request->prog_id);
                $meet->date = $p->date_prog;
                $meet->status1 = "En attente";
                $meet->save();

                //on ajout un test qui vérif l'existnnce de userMeetB2b
                $userMeetSend = new B2bMeetUser();
                $userMeetSend->mobile_user_id = $request->send_user_id;
                $userMeetSend->b2b_meet_id = $meet->id;
                //$userMeetSend->status = "sent";
                $userMeetSend->status = "received";
                $userMeetSend->date_action = $date;
                $userMeetSend->date_deb_meet = $request->date_deb;
                $userMeetSend->date_fin_meet = $request->date_fin;
                $userMeetSend->confirmer = 0;
                $userMeetSend->refuser = 0;
                $userMeetSend->save();

                $userMeetRec = new B2bMeetUser();
                $userMeetRec->mobile_user_id = $request->rec_user_id;
                $userMeetRec->b2b_meet_id = $meet->id;
                //$userMeetRec->status = "received";
                $userMeetRec->status = "sent";
                $userMeetRec->date_action = $date;
                $userMeetRec->date_deb_meet = $request->date_deb;
                $userMeetRec->date_fin_meet = $request->date_fin;
                $userMeetRec->confirmer = 0;
                $userMeetRec->refuser = 0;
                $userMeetRec->save();


                $myObj3 = (object)["meeting_id" => $meet->id, "type" => "B2B", "payload" => "invite"];
                $txt = "Vous avez reçu une invitaiton pour un rendez-vous B2B";

                $recepteur = MobileUser::where('id', $request->rec_user_id)->first();
                $emetteur = MobileUser::where('id', $request->send_user_id)->first();


                
                //handle recepteur if field are null
                if(!isset($recepteur->nom))
                    $recepteur->nom = "";
                if(!isset($recepteur->prenom))
                    $recepteur->prenom = "";
                if(!isset($recepteur->entreprise))
                    $recepteur->entreprise = "";

                //handle emetteur if field are null
                if(!isset($emetteur->nom))
                    $emetteur->nom = "";
                if(!isset($emetteur->prenom))
                    $emetteur->prenom = "";
                if(!isset($emetteur->entreprise))
                    $emetteur->entreprise = "";

                //send Email Recepteur 
                if((isset($recepteur->email)) AND (isset($recepteur->email_professionnel)))
                {
                    $email = $recepteur->email;
                    $email_professionnel = $recepteur->email_professionnel;

                    //in this case we will send two mail email and email_pro
                    //for email
                    $data = array
                    (
                        "email" => $email,
                        "nom" => $emetteur->nom,
                        "nom_recepteur" => $recepteur->nom,
                        "prenom_recepteur" => $recepteur->prenom,
                        "prenom" => $emetteur->prenom,
                        "entreprise" => $emetteur->entreprise,
                        "heure" => $p->heure_debut,
                        "id_rendez_vous" => $meet->id,
                        "programme" => $p->titre
                    );

                    Mail::send
                    (
                        'mail/mail_recepteur', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('no-reply@siticafrica.com','dontreply@siticafrica.com');
                            $message->subject("Nouvelle demande de RDV B2B de ".$data['prenom_recepteur']." ". $data['nom_recepteur']." reçue");
                            $message->to($data['email'], $data['nom'], $data['prenom'], $data['entreprise'] , $data['heure'] , $data['id_rendez_vous'], $data['programme']);
                        }
                    );
                    
                    //for email pro
                    $data = array
                    (
                        "email" => $email_professionnel,
                        "nom" => $emetteur->nom,
                        "prenom" => $emetteur->prenom,
                        "entreprise" => $emetteur->entreprise,
                        "heure" => $p->heure_debut,
                        "id_rendez_vous" => $meet->id,
                        "programme" => $p->titre
                    );

                    Mail::send
                    (
                        'mail/mail_recepteur', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('no-reply@siticafrica.com','dontreply@siticafrica.com');
                            $message->subject("Nouvelle demande de RDV B2B de ".$data['prenom']." ". $data['nom']." reçue");
                            $message->to($data['email'], $data['nom'], $data['prenom'], $data['entreprise'] , $data['heure'] , $data['id_rendez_vous'] , $data['programme']);
                        }
                    );
                }
                elseif(isset($recepteur->email)) // if email is not null and email_pro is null
                {
                    $email = $recepteur->email;
                    $data = array
                    (
                        "email" => $email,
                        "nom" => $emetteur->nom,
                        "prenom" => $emetteur->prenom,
                        "entreprise" => $emetteur->entreprise,
                        "heure" => $p->heure_debut,
                        "id_rendez_vous" => $meet->id,
                        "programme" => $p->titre
                    );

                    Mail::send
                    (
                        'mail/mail_recepteur', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('no-reply@siticafrica.com','dontreply@siticafrica.com');
                            $message->subject("Nouvelle demande de RDV B2B de ".$data['prenom']." ". $data['nom']." reçue");
                            $message->to($data['email'], $data['nom'], $data['prenom'], $data['entreprise'] , $data['heure'] , $data['id_rendez_vous'], $data['programme']);
                        }
                    );
                }
                elseif(isset($recepteur->email_professionnel)) // if email is null and email_pro is not null
                {
                    $email_professionnel = $recepteur->email_professionnel;
                    $data = array
                    (
                        "email" => $email_professionnel,
                        "nom" => $emetteur->nom,
                        "prenom" => $emetteur->prenom,
                        "entreprise" => $emetteur->entreprise,
                        "heure" => $p->heure_debut,
                        "id_rendez_vous" => $meet->id,
                        "programme" => $p->titre
                    );

                    Mail::send
                    (
                        'mail/mail_recepteur', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('no-reply@siticafrica.com','dontreply@siticafrica.com');
                            $message->subject("Nouvelle demande de RDV B2B de ".$data['prenom']." ". $data['nom']." reçue");
                            $message->to($data['email'], $data['nom'], $data['prenom'], $data['entreprise'] , $data['heure'] , $data['id_rendez_vous'], $data['programme']);
                        }
                    );
                }
                //____________________________________________________________________________________

                //send Email Emetteur
                if((isset($emetteur->email)) AND (isset($emetteur->email_professionnel)))
                {
                    $email = $emetteur->email;
                    $email_professionnel = $emetteur->email_professionnel;
                    //send email to both mail
                    
                    //for email
                    $data = array
                    (
                        "email" => $email,
                        "nom" => $recepteur->nom,
                        "prenom" => $recepteur->prenom,
                        "nom_emetteur" => $emetteur->nom,
                        "prenom_emetteur" => $emetteur->prenom,
                        "entreprise" => $recepteur->entreprise,
                        "heure" => $p->heure_debut,
                        "id_rendez_vous" => $meet->id,
                        "programme" => $p->titre
                    );

                    Mail::send
                    (
                        'mail/mail_emetteur', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('no-reply@siticafrica.com','dontreply@siticafrica.com');
                            $message->subject("Demande de RDV B2B à  ".$data['nom_emetteur']." ". $data['prenom_emetteur']." envoyée avec succès");
                            $message->to($data['email'], $data['nom'], $data['prenom'], $data['entreprise'] , $data['heure'] , $data['id_rendez_vous'] , $data['programme']);
                        }
                    );

                    //for email_pro
                    $data = array
                    (
                        "email" => $email_professionnel,
                        "nom" => $recepteur->nom,
                        "prenom" => $recepteur->prenom,
                        "entreprise" => $recepteur->entreprise,
                        "heure" => $p->heure_debut,
                        "id_rendez_vous" => $meet->id,
                        "programme" => $p->titre
                    );

                    Mail::send
                    (
                        'mail/mail_emetteur', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('no-reply@siticafrica.com','dontreply@siticafrica.com');
                            $message->subject("Demande de RDV B2B à  ".$data['prenom']." ". $data['nom']." envoyée avec succès");
                            $message->to($data['email'], $data['nom'], $data['prenom'], $data['entreprise'] , $data['heure'] , $data['id_rendez_vous'], $data['programme']);
                        }
                    );
                }
                elseif(isset($emetteur->email))
                {
                    $email = $emetteur->email;

                    $data = array
                    (
                        "email" => $email,
                        "nom" => $recepteur->nom,
                        "prenom" => $recepteur->prenom,
                        "entreprise" => $recepteur->entreprise,
                        "heure" => $p->heure_debut,
                        "id_rendez_vous" => $meet->id,
                        "programme" => $p->titre
                    );

                    Mail::send
                    (
                        'mail/mail_emetteur', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('no-reply@siticafrica.com','dontreply@siticafrica.com');
                            $message->subject("Demande de RDV B2B à  ".$data['prenom']." ". $data['nom']." envoyée avec succès");
                            $message->to($data['email'], $data['nom'], $data['prenom'], $data['entreprise'] , $data['heure'] , $data['id_rendez_vous'],  $data['programme']);
                        }
                    );
                }
                elseif(isset($emetteur->email_professionnel))
                {
                    $email_professionnel = $emetteur->email_professionnel;

                    $data = array
                    (
                        "email" => $email_professionnel,
                        "nom" => $recepteur->nom,
                        "prenom" => $recepteur->prenom,
                        "entreprise" => $recepteur->entreprise,
                        "heure" => $p->heure_debut,
                        "id_rendez_vous" => $meet->id,
                        "programme" => $p->titre
                    );

                    Mail::send
                    (
                        'mail/mail_emetteur', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('no-reply@siticafrica.com','dontreply@siticafrica.com');
                            $message->subject("Demande de RDV B2B à  ".$data['prenom']." ". $data['nom']." envoyée avec succès");
                            $message->to($data['email'], $data['nom'], $data['prenom'], $data['entreprise'] , $data['heure'] , $data['id_rendez_vous'], $data['programme']);
                        }
                    );
                }

                //____________________________________________________________________________________

                $this->PushNotif($recepteur->token_device, $txt, $txt, $myObj3);
                // test send push then insert in database
                $this->insertNotification($txt, "connect", $recepteur->token_device, $recepteur->id,null,$request->send_user_id);
                //insertion notif dans la base
                return response()->json("reservation valider");
            }
        }
        return response()->json("Le meet b2b est deja reserver");
    }

    public function SendB2b1(Request $request)
    {
        $date = Carbon::now();
        $verif = null;
        // $verif2 = B2bMeetUser::where('mobile_user_id',$request->rec_user_id)->get();
        //verifier si il y a une reservation deja dans la date est pris ou pas
        $verif = DB::table('b2b_meet_users')
            ->Join('b2b_meets', 'b2b_meet_users.b2b_meet_id', '=', 'b2b_meets.id')
            ->Join('mobile_users', 'b2b_meet_users.mobile_user_id', '=', 'mobile_users.id')
            ->where('mobile_user_id', $request->rec_user_id)
            ->where('date_deb_meet', $request->date_deb)
            ->where('date_fin_meet', $request->date_fin)
            ->where('b2b_meets.prog_id', $request->prog_id)
            ->where('confirmer', 1)
            ->select('b2b_meet_users.*', 'mobile_users.email', 'b2b_meets.prog_id')
            ->first();

        // return response()->json($request);


            $verifB2b = $this->verifB2bMeet($request->send_user_id, $request->rec_user_id , $request->prog_id);
            if($verifB2b)
            {
                $meet = new B2bMeet();
                $meet->titre = $request->titre;
                $meet->prog_id = $request->prog_id;
                $p = Programme::find($request->prog_id);
                $meet->date = $p->date_prog;
                $meet->save();
                //on ajout un test qui vérif l'existnnce de userMeetB2b
                $userMeetSend = new B2bMeetUser();
                $userMeetSend->mobile_user_id = $request->send_user_id;
                $userMeetSend->b2b_meet_id = $meet->id;
                //$userMeetSend->status = "sent";
                $userMeetSend->status = "received";
                $userMeetSend->date_action = $date;
                $userMeetSend->date_deb_meet = $request->date_deb;
                $userMeetSend->date_fin_meet = $request->date_fin;
                $userMeetSend->confirmer = 0;
                $userMeetSend->refuser = 0;
                $userMeetSend->save();

                $userMeetRec = new B2bMeetUser();
                $userMeetRec->mobile_user_id = $request->rec_user_id;
                $userMeetRec->b2b_meet_id = $meet->id;
                //$userMeetRec->status = "received";
                $userMeetRec->status = "sent";
                $userMeetRec->date_action = $date;
                $userMeetRec->date_deb_meet = $request->date_deb;
                $userMeetRec->date_fin_meet = $request->date_fin;
                $userMeetRec->confirmer = 0;
                $userMeetRec->refuser = 0;
                $userMeetRec->save();
                $myObj3 = (object)["meeting_id" => $meet->id, "type" => "B2B", "payload" => "invite"];
                $txt = "Vous avez reçu une invitaiton pour un rendez-vous B2B";

                $recepteur = MobileUser::where('id', $request->rec_user_id)->first();
                $emetteur = MobileUser::where('id', $request->send_user_id)->first();

                //handle recepteur if field are null
                if(!isset($recepteur->nom))
                $recepteur->nom = "";
                if(!isset($recepteur->prenom))
                    $recepteur->prenom = "";
                    if(!isset($recepteur->entreprise))
                    $recepteur->entreprise = "";
                    
                    //____________________________________________________________________________________
                    
                    $this->PushNotif($recepteur->token_device, $txt, $txt, $myObj3);
                    // tester le +++++++ 
                    $this->insertNotification($txt, "connect", $recepteur->token_device, $recepteur->id,null,$request->send_user_id);
                //insertion notif dans la base
                return response()->json("reservation valider");
            }
        
        return response()->json("Le meet b2b est deja reserver");
    }

    //new version
    // public function SendB2b(Request $request)
    // {
    //     $date = Carbon::now();
    //     $verif = null;
    //     // $verif2 = B2bMeetUser::where('mobile_user_id',$request->rec_user_id)->get();
    //     //verifier si il y a une reservation deja dans la date est pris ou pas
    //     $verif = DB::table('b2b_meet_users')
    //         ->Join('b2b_meets', 'b2b_meet_users.b2b_meet_id', '=', 'b2b_meets.id')
    //         ->Join('mobile_users', 'b2b_meet_users.mobile_user_id', '=', 'mobile_users.id')
    //         ->where('mobile_user_id', $request->rec_user_id)
    //         ->where('date_deb_meet', $request->date_deb)
    //         ->where('date_fin_meet', $request->date_fin)
    //         ->where('b2b_meets.prog_id', $request->prog_id)
    //         ->where('confirmer', 1)
    //         ->select('b2b_meet_users.*', 'mobile_users.email', 'b2b_meets.prog_id')
    //         ->first();

    //     // return response()->json($request);

    //     if (!isset($verif)) {
    //         $meet = new B2bMeet();
    //         $meet->titre = $request->titre;
    //         $meet->prog_id = $request->prog_id;
    //         $meet->save();

    //         $userMeetSend = new B2bMeetUser();
    //         $userMeetSend->mobile_user_id = $request->send_user_id;
    //         $userMeetSend->b2b_meet_id = $meet->id;
    //         $userMeetSend->status = "received";
    //         $userMeetSend->date_action = $date;
    //         $userMeetSend->date_deb_meet = $request->date_deb;
    //         $userMeetSend->date_fin_meet = $request->date_fin;
    //         $userMeetSend->confirmer = 0;
    //         $userMeetSend->refuser = 0;
    //         $userMeetSend->save();

    //         $userMeetRec = new B2bMeetUser();
    //         $userMeetRec->mobile_user_id = $request->rec_user_id;
    //         $userMeetRec->b2b_meet_id = $meet->id;
    //         $userMeetRec->status = "sent";
    //         $userMeetRec->date_action = $date;
    //         $userMeetRec->date_deb_meet = $request->date_deb;
    //         $userMeetRec->date_fin_meet = $request->date_fin;
    //         $userMeetRec->confirmer = 0;
    //         $userMeetRec->refuser = 0;
    //         $userMeetRec->save();
    //         $myObj3 = (object)["meeting_id" => $meet->id, "type" => "B2B", "payload" => "invite"];
    //         $txt = "Vous avez reçu une invitaiton";

    //         $receiver = MobileUser::where('id', $request->rec_user_id)->first();

    //         $this->PushNotif($receiver->token_device, $txt, "", $myObj3);

    //         return response()->json("reservation valider");
    //     }
    //     return response()->json("Le meet b2b est deja reserver");
    // }


    //test to verify the meet b2b between two user
    public function verifB2bMeet($send_user_id, $rec_user_id , $prog_id)
    {
        $test = false;
        $b2bMeet = B2bMeet::where('prog_id', $prog_id)->get();
        if(isset($b2bMeet))
        {
            foreach($b2bMeet as $v)
            {
                $verif1 = B2bMeetUser::where('b2b_meet_id',$v->id)->where('mobile_user_id',$send_user_id)->first();
                $verif2 = B2bMeetUser::where('b2b_meet_id',$v->id)->where('mobile_user_id',$rec_user_id)->first();
                if((isset($verif1)) AND (isset($verif2)))
                {
                    return false;
                }
                else
                {
                    $test = true;
                }
            }
        }
        return $test;
    }

    public function testVerifB2b()
    {
        $verif = $this->verifB2bMeet(100, 98 , 27);
        if($verif)
        {
            return "good";
        }
        else
        {
            return "bad";
        }
    }

    //************ Function liste meet B2b pour un utilisateur $id=senderid(connected mobile user)************
    public function ListMeetB2b($id, $status = null)
    {
        //b2b_meet_id
        $ListB2bMeet = DB::table('b2b_meet_users')
            ->Join('b2b_meets', 'b2b_meet_users.b2b_meet_id', '=', 'b2b_meets.id')
            ->Join('mobile_users', 'b2b_meet_users.mobile_user_id', '=', 'mobile_users.id')
            ->where('mobile_user_id', $id);
        if ($status != "all") {
            if (($status != "confirmed") && ($status != "refused")) 
            {
                $ListB2bMeet->where('status', $status);
            }
            else if ($status == "confirmed") 
            {
                $ListB2bMeet->where('confirmer', 1)->where('refuser', 0);
            } 
            else if ($status == "refused") 
            {
                $ListB2bMeet->where('confirmer', 0)->where('refuser', 1);
            }
        }
        $ListB2bMeet = $ListB2bMeet->select('b2b_meet_users.*', 'mobile_users.email', 'mobile_users.photo_de_profile', 'b2b_meets.prog_id')
            ->get();
        $tb = [];
        foreach ($ListB2bMeet as $meet) {
            $b2b_meet_id = $meet->b2b_meet_id;
            $ress = DB::table('b2b_meet_users')
                ->Join('mobile_users', 'b2b_meet_users.mobile_user_id', '=', 'mobile_users.id')
                ->Join('b2b_meets', 'b2b_meet_users.b2b_meet_id', '=', 'b2b_meets.id')
                ->where('b2b_meet_users.b2b_meet_id', $b2b_meet_id)
                ->where('b2b_meet_users.mobile_user_id', '<>', $id)
                ->select()
                ->first();
            $tb[] = $ress;
        }
        //return response()->json($tb);
        foreach ($tb as $v)
        {
            if(isset($v->confirmer))
            {
                if(($v->confirmer == 1) AND ($v->refuser == 0))
                    $v->status = "confirmed";
                elseif(($v->confirmer == 0) AND ($v->refuser == 1))
                    $v->status = "refused";
            if(isset($v->photo_de_profile))
            {
                $v->photo_de_profile = asset('/storage/' . $v->photo_de_profile);
                
            }
            if(isset($v->role))
                $role = WizardRole::find($v->role);
            else
                $role = [];
            if(isset($v->domaine_activite_p))
            {
                $domaine_activite_p = Activite::where('status', 1)->where('type', 'Principale')->where('id', $v->domaine_activite_p)->first();
            }
            else
            {
                $domaine_activite_p = [];
            }

            if(isset($v->domaine_activite_s))
            {
                $domaine_activite_s = Activite::where('status', 1)->where('type', 'Secondaire')->where('id', $v->domaine_activite_s)->first();
            }
            else
            {
                $domaine_activite_s = [];
            }
            

            if(isset($role))
            {
               if(isset($role->titre))
               {
                    $v->role = $role->titre;
               }

            }
                
            if(isset($domaine_activite_p))
            {
                if(isset($domaine_activite_p->titre))
                    $v->domaine_activite_p = $domaine_activite_p->titre;
            }
                
            if(isset($domaine_activite_s))
            {
                if(isset($domaine_activite_s->titre))
                    $v->domaine_activite_s = $domaine_activite_s->titre;
            }
            }
                
        }
        //$tb = array_filter($tb);
        $tab = [];
        foreach($tb as $v)
        {
            if(isset($v))
                $tab[] = $v;
        }
        return response()->json([$tab], 200);
    }

    public function chnageB2Bstatus(Request $request)
    {
        $idmetting = $request->idmeeting;
        $statusmeeting = $request->statusmeeting;
        $result = [];
        if ($statusmeeting == "confirmed") {
            //check if not already 50
            $meet = B2bMeet::where('id', $idmetting)->first();
            $progid = $meet->prog_id;
            $meetuser = B2bMeetUser::where('b2b_meet_id', $idmetting)->first();
            $heure_debut = $meetuser->date_deb_meet;
            $heure_fin = $meetuser->date_fin_meet;
            $ListB2bMeetCount = DB::table('b2b_meet_users')
                ->Join('b2b_meets', 'b2b_meet_users.b2b_meet_id', '=', 'b2b_meets.id')
                ->where('b2b_meets.prog_id', $progid)
                ->where('b2b_meet_users.confirmer', 1)
                ->where('b2b_meet_users.refuser', 0)
                ->where('b2b_meet_users.b2b_meet_id', '<>', $idmetting)
                ->where('b2b_meet_users.date_deb_meet', $heure_debut)
                ->where('b2b_meet_users.date_fin_meet', $heure_fin)
                ->distinct()
                ->count('b2b_meet_id');

            if ($ListB2bMeetCount < 50) 
            {
                $data = ['confirmer' => 1, 'refuser' => 0];
                $affected = DB::table('b2b_meet_users')
                    ->where('b2b_meet_id', $idmetting)
                    ->update($data);
                $b2b = B2bMeet::find($idmetting);
                if(isset($b2b))
                {
                    $b2b->status1 = "confirmer";
                    $b2b->save();
                }
                //begin check if ther is 2 recived invitation from diffrent user on the sams time slot
                //We will decline the other one
                $reciveruserid = B2bMeetUser::where('b2b_meet_id', $idmetting)->where('status', 'received')->first();
                //select all meeting with the same recieved id
                $declienthem = B2bMeetUser::where('mobile_user_id', $reciveruserid->mobile_user_id)->where('status', 'received')
                    ->where('date_deb_meet', $heure_debut)->where('date_fin_meet', $heure_fin)->where('b2b_meet_id', '<>', $idmetting)->get();
                //seleect all sender from the previous meeting list
                $textrefuse1 = "Votre meeting est refusé";

                if (!empty($declienthem)) {
                    foreach ($declienthem as $declie_it) {

                        $usersenderdecline = DB::table('b2b_meet_users')->where('b2b_meet_users.b2b_meet_id', $declie_it->b2b_meet_id)
                            ->Join('mobile_users', 'b2b_meet_users.mobile_user_id', '=', 'mobile_users.id')
                            ->select('mobile_users.token_device', 'mobile_users.email')
                            ->first();
                        $datar = ['confirmer' => 0, 'refuser' => 1];
                        DB::table('b2b_meet_users')
                            ->where('b2b_meet_id', $declie_it->b2b_meet_id)
                            ->update($datar);
                            $b2b = B2bMeet::find($declie_it->b2b_meet_id);
                            if(isset($b2b))
                            {
                                $b2b->status1 = "refuser";
                                $b2b->save();
                            }
                        $datarefuse = (object)["meeting_id" => $declie_it->b2b_meet_id, "type" => "B2B", "payload" => "reserved"];
                        $this->PushNotif($usersenderdecline->token_device, $textrefuse1, "", $datarefuse);
                    }
                }

                //end check if there is 2
                $text = "Votre meeting est confirmé";
                //ADD PUSH NOTIFICATION //sent to sender to inform that the meeting are confirmed by the receiver
                $sender = DB::table('b2b_meet_users')->select('mobile_user_id')->where('b2b_meet_id', $idmetting)->where('status', 'sent')->first();
                $sender_user = MobileUser::where('id', $sender->mobile_user_id)->first();
                $result = ["status" => 1, "message" => "Update completed"];
                $myObj = (object)["meeting_id" => $idmetting, "type" => "B2B", "payload" => "confirmed"];
                $this->PushNotif($sender_user->token_device, $text, "", $myObj);
                //check if meeting =50  than close all other meeting and send push notification
                if ($ListB2bMeetCount == 49) {
                    //if(true){
                    $ListB2bMeetRefuse = DB::table('b2b_meet_users')
                        ->Join('b2b_meets', 'b2b_meet_users.b2b_meet_id', '=', 'b2b_meets.id')
                        ->Join('mobile_users', 'b2b_meet_users.mobile_user_id', '=', 'mobile_users.id')
                        ->where('b2b_meets.prog_id', $progid)
                        ->where('b2b_meet_users.confirmer', 0)
                        ->where('b2b_meet_users.refuser', 0)
                        ->where('b2b_meet_users.status', 'sent')
                        ->where('b2b_meet_users.b2b_meet_id', '<>', $idmetting)
                        ->where('b2b_meet_users.date_deb_meet', $heure_debut)
                        ->where('b2b_meet_users.date_fin_meet', $heure_fin)
                        ->select('mobile_users.token_device', 'mobile_users.email', 'b2b_meet_users.b2b_meet_id')
                        ->get();
                    $textrefuse = "Time slot is no more available due to maximum number of meetings is reached ";
                    foreach ($ListB2bMeetRefuse as $refuseit) {
                        $dataref = ['confirmer' => 0, 'refuser' => 1];
                        DB::table('b2b_meet_users')
                            ->where('b2b_meet_id', $refuseit->b2b_meet_id)
                            ->update($dataref);


                        $b2b = B2bMeet::find($refuseit->b2b_meet_id);
                        if(isset($b2b))
                        {
                            $b2b->status1 = "refuser";
                            $b2b->save();
                        }
                        $myObj3 = (object)["meeting_id" => $refuseit->b2b_meet_id, "type" => "B2B", "payload" => "full"];
                        $this->PushNotif($refuseit->token_device, $textrefuse, "", $myObj3);
                    }
                }

            }
            else
            {
                $result =
                    [
                        "status" => 0,
                        "message" => "There is no more available place on the current time slot $heure_debut - $heure_fin"
                    ];
            }
        }
        else
        {
            $data = ['confirmer' => 0, 'refuser' => 1];
            $affected = DB::table('b2b_meet_users')
                ->where('b2b_meet_id', $idmetting)
                ->update($data);
            $textdec = "Votre meeting est refusé";
            //ADD PUSH NOTIFICATION //sent to sender
            $sent = DB::table('b2b_meet_users')->select('mobile_user_id')->where('b2b_meet_id', $idmetting)->where('status', 'sent')->first();
            $sender_user = MobileUser::where('id', $sent->mobile_user_id)->first();
            $result = ["status" => 1, "message" => "Update completed"];
            $myObj2 = (object)["meeting_id" => $idmetting, "type" => "B2B", "payload" => "refused"];
            $this->PushNotif($sender_user->token_device, $textdec, "", $myObj2);
        }

        return response()->json($result);

    }

    //************ Function Onboarding ************
    public function Onboarding()
    {

        $Onboardings = Boarding::all();

        foreach ($Onboardings as $key => $value) {
            $value->image = str_replace("\\", "/", $value->image);
            $value->image = asset('storage/' . $value->image);
        }

        return response()->json($Onboardings);

    }

    //************ Function Partenaires ************
    public function Partenaires()
    {

        $Partenaires = Partenaire::all();

        foreach ($Partenaires as $key => $value) {
            $value->image = str_replace("\\", "/", $value->image);
            $value->image = asset('storage/' . $value->image);
        }

        return response()->json($Partenaires);

    }

    //************ Function Onboarding ************
    public function Organisateurs()
    {

        $Organisateurs = Organisateur::all();

        foreach ($Organisateurs as $key => $value) {
            $value->image = str_replace("\\", "/", $value->image);
            $value->image = asset('storage/' . $value->image);
        }
        return response()->json($Organisateurs);
    }

    //************ Function Onboarding ************
    public function Propos()
    {
        $Propos = Propo::all();
        return response()->json($Propos);
    }

    //************ Function check mise a jour ************
    public function CheckUpdate()
    {
        $count = BackLog::count();
        return response()->json($count);
    }

    //************ Function recherche ************
    public function Search(Request $request)
    {
        $progs = DB::table('programmes')
            ->join('evenements', 'programmes.event_id', '=', 'evenements.id')
            ->join('type_progs', 'programmes.type_id', '=', 'type_progs.id')
            ->where('programmes.titre', 'like', '%' . $request->titre . '%')
            ->select(array('evenements.titre as titre evenement',
                'type_progs.titre as Type programme',
                'programmes.id as ID programme',
                'programmes.titre as Titre programme',
                'programmes.description as Description programme',
                'programmes.date_debut',
                'programmes.date_fin'))
            ->get();

        return response()->json($progs);

    }

    //************ Function Test send email ************
    public function testSendEmail()
    {
        $test = 'hmed';
        Mail::to('jouinihaider1@gmail.com')
            // ->cc($moreUsers)
            // ->bcc($evenMoreUsers)
            ->send(new SendMail($test));

        // return view('welcome') ;
    }

    //************ Function SearchString ************
    public function SearchString(string $ch, $tab)
    {
        foreach ($tab as $key => $value) {
            if ($value->titre == $ch) {
                return $value->id;
            }
        }
        return 0;
    }

    //************ Function SearchUserIdInteret ************
    public function SearchUserIdInteret($idUser, $id, $tab)
    {
        foreach ($tab as $key => $value) {
            if (($value->mobile_user_id == $idUser) && ($value->interet_id == $id)) {
                return true;
            }
        }
        return false;
    }

    //************ Function SearchUserIdObjectif ************
    public function SearchUserIdObjectif($idUser, $id, $tab)
    {

        foreach ($tab as $key => $value) {
            if (($value->mobile_user_id == $idUser) && ($value->objectif_id == $id)) {
                return true;
            }
        }
        return false;
    }

    //HELPER METHODES

    function SplitTime($StartTime, $EndTime, $Duration = "60")
    {
        $ReturnArray = array();// Define output
        $StartTime = strtotime($StartTime); //Get Timestamp
        $EndTime = strtotime($EndTime); //Get Timestamp

        $AddMins = $Duration * 60;

        while ($StartTime <= $EndTime) //Run loop
        {
            $ReturnArray[] = date("G:i", $StartTime);
            $StartTime += $AddMins; //Endtime check
        }
        return $ReturnArray;
    }

    public function verifyDateProg(Request $request)
    {
        $event = Evenement::where('id', $request->event_id)->get()->first();
        if (!empty($event))
        {
            if (($event->date_debut <= $request->date_prog) and ($event->date_fin >= $request->date_prog))
            {
                return response()->json
                (
                    [
                        'response' => 'success', //the date of program is betwwen the start day and the last day
                        'code' => 1, // if the response is success
                        'date_debut' => $event->date_debut,
                        'date_fin' => $event->date_fin
                    ]
                );
            }
            else
            {
                return response()->json
                (
                    [
                        'response' => 'error',  //the date of program is not between the start day and the last day
                        'code' => 0, // if the response is error
                        'date_debut' => $event->date_debut,
                        'date_fin' => $event->date_fin
                    ]
                );
            }
        }
        else
        {
            return response()->json
            (
                [
                    'response' => 'error',  //the date of program is not between the start day and the last day
                    'code' => -1, // if the response is error
                    'date_debut' => null,
                    'date_fin' => null
                ]
            );
        }
    }

    public function insertImage(Request $request)
    {
        $userMobile = MobileUser::find($request->id);
        if (empty($userMobile))
        {
            return response()->json
            (
                'This is is not exist'
            );
        }
        else
        {
            if(isset($request->photo))
            {
                $base64_image = "data:image/jpeg;base64," . $request->photo;

                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image))
                {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);

                    $data = base64_decode($data);
                    $img = "public/users/user" . $userMobile->id . ".gif";
                    Storage::disk('local')->put($img, $data);
                    $userMobile->photo_de_profile = "users/user" . $userMobile->id . ".gif"; //$request->photo; //base 64 bit
                    $userMobile->save();
                    return response()->json
                    (
                        [
                            'response' => 'The image has been correctly inserted'
                        ], 201
                    );
                }
            }
            $userDetail = UserDetail::where('type','unknown male')->first();
            $userMobile->photo_de_profile = $userDetail->photo;
            $userMobile->save();
            return response()->json
            (
                [
                    'response' => 'The image has been correctly inserted'
                ], 201
            );
        }
    }

    public function getAllMobileUser()
    {
        $T = MobileUser::all();
        foreach ($T as $v)
        {
            $v->photo_de_profile = asset('/storage/' . $v->photo_de_profile);
        }
        return response()->json
        (
            [
                $T
            ], 200
        );
    }

    //********************************************Auth Linkedin ************************
    public function AuthLinkedin($token, $token_device)
    {
        try
        {
            if ($this->getUserLinkedin($token) == null)
            {
                return response()->json("This token is expired or invalid");
            }
            else
            {
                $mobileUser = MobileUser::where('email', $this->getUserLinkedin($token)['email'])->get()->first();
                if (!empty($mobileUser))
                {
                    if ($mobileUser->type_auth_api == "linkedin")
                    {
                        if(isset($token_device))
                        {
                            if (strlen($mobileUser->token_device) > 2)
                            {
                                if (!$this->SearchTokenDevice($mobileUser->token_device,$token_device))
                                {
                                    $tab = (array)json_decode($mobileUser->token_device); //we should cast the object stdClass to array
                                    $tab[] = $token_device; //add new element in the table $tab
                                    $mobileUser->token_device = json_encode($tab) ;
                                }
                            }
                            else
                            {
                                $mobileUser->token_device = json_encode([$token_device]) ;
                            }
                        }
                        $mobileUser->save();
                        //return response()->json($mobileUser);
                        return response()->json($this->getUser($mobileUser->id));
                    }
                    else
                    {
                        return response()->json("This user is registered with other social media");
                    }
                }
                else
                {
                    $mobileUser = new MobileUser();
                    $mobileUser->nom = $this->getUserLinkedin($token)['last_name'];
                    $mobileUser->prenom = $this->getUserLinkedin($token)['first_name'];
                    $mobileUser->email = $this->getUserLinkedin($token)['email'];
                    $mobileUser->type_auth_api = "linkedin";
                    $mobileUser->new = 1;
                    $tab = array();
                    $mobileUser->token_device = json_encode($tab);
                    $mobileUser->created_at = Carbon::now();
                    $mobileUser->save();
                    return $this->getUser($mobileUser->id);
                }
            }
        }
        catch (Exception $e)
        {
            return response()->json
            ("Exception error");
        }
    }

    public function getUserLinkedin($token)
    {
        try
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/me');
            curl_setopt
            (
                $ch, CURLOPT_HTTPHEADER, array
                (
                    //'Authorization:Bearer AQVBmpQ_ehRbYyFhAKpk4f3UK5b6OvNPYCBDopu3_mVF27M48wJPlHSFnHh6C9R94XQICY5sRbJM2ymwMa2W6yuBKKFjdJBmBgAA-3obrZu9Mmc9nyqQ2m_jH3MrZUnRvKWbRYbGitpJD_1B9shMNP_a8BHg-Tuf4kteU-p7bwhgPXqp_uWrrc2U885ZgEpajpOIPoi-eCuLHFS5g-_87v0-HuDr9-WavFWCZxM0hVYu0l_lDE731THcXN3IEMfZjCzrF8vjUG8KMYo7RDib0GfSq5wEksVvLZyPdoIRlfgS0dbuZDz2obf1PEXqwvVotNwM8MB8S27whynzJ4xove5fVZmbfg'
                    'Authorization:Bearer ' . $token
                )
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $user = array
            (
                "id" => json_decode($result)->id,
                "last_name" => json_decode($result)->localizedLastName,
                "first_name" => json_decode($result)->localizedFirstName,
                "email" => $this->getEmailLinkedin("Bearer " . $token)
            );
            return $user;
        }
        catch (Exception $errorException)
        {
            return null;
        }
    }

    public function getEmailLinkedin($token)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))');
        curl_setopt
        (
            $ch, CURLOPT_HTTPHEADER, array
            (
                'Authorization:' . $token
            )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = str_replace('~', '', $result);
        $t = substr($result, strpos($result, '['), strpos($result, ']'));
        $element = substr($t, strpos($t, '{'), strpos($t, '}'));
        $element = $element . '}';
        $element = substr($element, 1, strpos($element, '}'));
        $element = substr($element, 9, strlen($element));
        $tab = json_decode($element);
        return $tab->emailAddress;
    }
    //***********************************Auth Linkedin ******************************

    //*********************************Auth Gmail ***********************************
    private function AuthGmail($object, $token_device)
    {
        try
        {
            $json = (stripslashes($object));
            $json = str_replace('“','"',$json);
            $json = rtrim($json, "\0");
            $tab = json_decode($json);
            if ($tab == null)
            {
                return response()->json
                (
                    "You should insert a valid json format"
                );
            }
            else
            {
                $mobileUser = MobileUser::where('email', $tab->email)->get()->first();

                if (!empty($mobileUser))
                {
                    if ($mobileUser->type_auth_api == "gmail")
                    {
                        if(isset($token_device))
                        {
                            if(strlen($mobileUser->token_device) > 2)
                            {
                                if (!$this->SearchTokenDevice($mobileUser->token_device,$token_device))
                                {
                                    $tab = (array)json_decode($mobileUser->token_device); //we should cast the object stdClass to array
                                    $tab[] = $token_device; //add new element in the table $tab
                                    $mobileUser->token_device = json_encode($tab) ;
                                }
                            }
                            else
                            {
                                $mobileUser->token_device = json_encode([$token_device]) ;
                            }
                        }
                        $mobileUser->save();
                        return $this->getUser($mobileUser->id);
                    }
                    else
                    {
                        return response()->json
                        (
                            "This email is already exist"
                        );
                    }
                }
                else
                {
                    $mobileUser = new MobileUser();
                    $mobileUser->nom = $tab->familyName;
                    $mobileUser->prenom = $tab->givenName;
                    $mobileUser->email = $tab->email;
                    $mobileUser->type_auth_api = "gmail";
                    $mobileUser->new = 1;
                    $mobileUser->created_at = Carbon::now();
                    $b64image = base64_encode(file_get_contents($tab->photoUrl));
                    $base64_image = "data:image/jpeg;base64," . $b64image;
                    if (preg_match('/^data:image\/(\w+);base64,/', $base64_image))
                    {
                        $data = substr($base64_image, strpos($base64_image, ',') + 1);
                        $data = base64_decode($data);
                        $img = "public/users/user".$tab->email.".gif";
                        Storage::disk('local')->put($img, $data);
                        $mobileUser->photo_de_profile = "users/user".$tab->email.".gif"; //$request->photo; //base 64 bit
                    }
                    $tab = array();
                    if(isset($token_device))
                        $tab[] = $token_device;
                    $mobileUser->token_device = json_encode($tab);
                    $mobileUser->save();
                    return $this->getUser($mobileUser->id);
                }
            }
        }
        catch (Exception $e)
        {
            return response()->json
            ("Exception error");
        }
    }

    public function cleanJsonForGmail($json)
    {
        $element = substr($json, strpos($json, 'user'), strlen($json));
        $element = substr($element, strpos($element, '{'), strpos($element, '}'));
        $element = substr($element, strpos($element, '{'), strpos($element, '}'));
        $element = preg_replace("/\s+/", "", $element);
        $element[strlen($element) - 1] = '}';
        //var_dump(json_decode($element));
        return $element;
        return json_decode($element);
    }

    public function insertImageFromGmail()
    {
        //$image = "https://lh3.googleusercontent.com/a-/AOh14Gg-R9b-kYmPlj_IQBDl27aXAOALe82gzdg_cVBccg=s96-c";
        $b64image = base64_encode(file_get_contents('https://lh3.googleusercontent.com/a-/AOh14Gg-R9b-kYmPlj_IQBDl27aXAOALe82gzdg_cVBccg=s96-c'));
        //return $b64image;
        $base64_image = "data:image/jpeg;base64," . $b64image;
        if (preg_match('/^data:image\/(\w+);base64,/', $base64_image))
        {
            $data = substr($base64_image, strpos($base64_image, ',') + 1);
            $data = base64_decode($data);
            $img = "public/users/userxx.gif";
            Storage::disk('local')->put($img, $data);
            //$userMobile->photo_de_profile = "users/user".$userMobile->id.".gif"; //$request->photo; //base 64 bit
            //$userMobile->save();
            return response()->json
            (
                [
                    'response' => 'The image has been correctly inserted'
                ], 201
            );
        }
    }

    public function searchUserByInteret($id)
    {
        try
        {
            $userIsExit = MobileUser::find($id);
            if(!isset($userIsExit))
                return response()->json("This user does not exist");
            //Else
            $listUsers = MobileUser::where('id','!=',$id)
                ->inRandomOrder()
                ->whereNotNull('nom')
                ->whereNotNull('prenom')
                ->whereNotNull('date_de_naissance')
                ->whereNotNull('pays_de_residence')
                ->whereNotNull('telephone')
                ->whereNotNull('entreprise')
                ->whereNotNull('site_web')
                ->whereNotNull('localite_entreprise')
                ->get()
                ->shuffle();
            $myInteret_user = UsersInteret::where('mobile_user_id',$id)->get();
            $tabUser=[];
            foreach ($listUsers as $v)
            {
                $reactVerif = React::where('id_user1',$id) //the id of connected user
                                    ->where('id_user2',$v->id) // the id of profile displayed in page matching
                                    ->first();
                if(($this->HaveTheSameInteret($v->id, $myInteret_user))  AND (!isset($reactVerif)))
                    $tabUser[] = $v;
            }
            $user =  $tabUser[rand(0, count($tabUser)-1)];
            $list = [];
            foreach ($tabUser as $v)
            {
                $list[] = $this->getUser($v->id); 
            }
            return response()->json($list,200);
        }
        catch(Exception $e)
        {
            return response()->json
            (
                []
            );
        }
    }

    private function HaveTheSameInteret($idUser, $Interet_user)
    {
        foreach($Interet_user as $v)
        {
            $user_interet = UsersInteret::where('mobile_user_id',$idUser)->where('interet_id',$v->interet_id)->first();
            if(isset($user_interet))
                return true;
        }
        return false;
    }

    public function dataSearchByTag()
    {
        try
        {
            $activitesPDB = Activite::where('status', 1)->where('type', 'Principale')->get();
            $role = WizardRole::where('status', 1)->get();
            $interetsDB = Interet::where('status', 1)->get();
            $interets =[];
            $tabRole = [];
            $activitesP = [];
            foreach ($role as $key => $value)
            {
                $obj = ["label" => $value->titre, "value" => $value->titre];
                $tabRole[] = $obj;
            }
            foreach ($activitesPDB as $key => $value)
            {
                $obj = ["label" => $value->titre, "value" => $value->titre];
                $activitesP[] = $obj;
            }
            foreach ($interetsDB as $key => $value)
                $interets[] = $value->titre;
            $obj =
            [
                "activitesP" => $activitesP,
                "post" => $tabRole,
                "interets" => $interets
            ];
            return response()->json($obj,200);
        }
        catch(Exception $e)
        {
            return response()->json
            (
                []
            );
        }
    }

    public function SearchFilter(Request $request)
    {
        try
        {
            $userExist = MobileUser::where('id',$request->my_id)->first();
            if(isset($userExist))
            {
                $mobile_usersDB = MobileUser::where('id', '!=', $request->my_id)->get();
                $mobile_users = [];
                foreach ($mobile_usersDB as $key => $value)
                {
                    if ($this->validationUserMobile($request->id_activite, $request->id_role, $request->interets, $value))
                        $mobile_users[] = $value;
                }
                //return response()->json($this->getUser($mobile_users), 200);
                $listUser = [];
                foreach($mobile_users as $v)
                {
                    $listUser[] = $this->getUser($v->id);
                }
                return response()->json($listUser, 200);
            }
            return response()->json
            (
                "This user is not exit"
            );
        }
        catch(Exception $e)
        {
            return response()->json
            ("Exception error");
        }
    }

    private function validationUserMobile($idActivite,$idRole,$interets,$user)
    {
        if($idActivite == $user->domaine_activite_p)
            return true;
        if($idRole == $user->role)
            return true;
        foreach($interets as $v)
        {
            $user_interet = UsersInteret::where('mobile_user_id',$user->id)->where('interet_id',$v['id'])->first();
            if(isset($user_interet))
                return true;
        }
        return false;
    }

    private function getInteretByIdUser($id)
    {
        $interets = Interet::all();
        $tabInteret=[];
        foreach ($interets as $v)
        {
            $user_interet = UsersInteret::where('mobile_user_id', $id)->where('interet_id',$v->id)->first();
            if(isset($user_interet))
                $tabInteret[] = $v;
        }
        return $tabInteret;
    }

    //test linkedin
    public function testLinkedin()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/me');
        curl_setopt
        (
            $ch, CURLOPT_HTTPHEADER, array
            (
                'Authorization:Bearer AQVBmpQ_ehRbYyFhAKpk4f3UK5b6OvNPYCBDopu3_mVF27M48wJPlHSFnHh6C9R94XQICY5sRbJM2ymwMa2W6yuBKKFjdJBmBgAA-3obrZu9Mmc9nyqQ2m_jH3MrZUnRvKWbRYbGitpJD_1B9shMNP_a8BHg-Tuf4kteU-p7bwhgPXqp_uWrrc2U885ZgEpajpOIPoi-eCuLHFS5g-_87v0-HuDr9-WavFWCZxM0hVYu0l_lDE731THcXN3IEMfZjCzrF8vjUG8KMYo7RDib0GfSq5wEksVvLZyPdoIRlfgS0dbuZDz2obf1PEXqwvVotNwM8MB8S27whynzJ4xove5fVZmbfg'
            )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function getUser($id)
    {
        try
        {
            $tab = [];
            $male = UserDetail::where('type','unknown male')->first();
            $feamle = UserDetail::where('type','unknown feamle')->first();

            $mobileUser = MobileUser::find($id);
            $photo = "";
            if(!isset($mobileUser->photo_de_profile))
            {
                if(!isset($mobileUser->photo_de_profile))
                {
                    $photo = $male->photo;
                }
                else
                {
                    if($mobileUser>sexe == "Homme")
                    {
                        $photo = $male->photo;
                    }
                    else
                    {
                        $photo = $feamle->photo;
                    }
                }
            }
            else
            {
                $photo = $mobileUser->photo_de_profile;
            }
            if(!isset($mobileUser->nom))
                $mobileUser->nom = "";
            if(!isset($mobileUser->prenom)) 
                $mobileUser->prenom = "";
            if(!isset($mobileUser->date_de_naissance))
                $mobileUser->date_de_naissance = "";
            if(!isset($mobileUser->pays_de_residence))
                $mobileUser->pays_de_residence = "";
            if(!isset($mobileUser->telephone))
                $mobileUser->telephone = "";
            if(!isset($mobileUser->sexe))
                $mobileUser->sexe = "";
            if(!isset($mobileUser->entreprise))
                $mobileUser->entreprise = "";
            if(!isset($mobileUser->site_web))
                $mobileUser->site_web = "";
            if(!isset($mobileUser->localite_entreprise))
                $mobileUser->localite_entreprise = "";
            if(!isset($mobileUser->token_api))
                $mobileUser->token_api = "";
            if(!isset($mobileUser->email_professionnel))
                $mobileUser->email_professionnel = "";
            if(!isset($mobileUser->description))
                $mobileUser->description = " ";
            if(!isset($mobileUser->access_b2b))
                $mobileUser->access_b2b = 0;
            $domaine_activite_p = Activite::where('status', 1)->where('type', 'Principale')->where('id', $mobileUser->domaine_activite_p)->first();
            $domaine_activite_s = Activite::where('status', 1)->where('id', $mobileUser->domaine_activite_s)->first();
            if(!isset($domaine_activite_p))
                $domaine_activite_p = 
                [
                    "id"=> 0,
                    "titre"=> "Aucun rôle introduit",
                    "type"=> "",
                    "status" => 0
                ];
            if(!isset($domaine_activite_s))
                $domaine_activite_s = 
                [
                    "id"=> 0,
                    "titre"=> "Aucun rôle introduit",
                    "type"=> "",
                    "status" => 0
                ];
            $role = WizardRole::find($mobileUser->role);
            if(!isset($role))
                $role = 
                [
                    "id"=> 0,
                    "titre" => "Aucun rôle introduit",
                    "type" => " ",
                    "status" => 0
                ];
            //verif formation if null or no
            /*$formations = Formation::where('user_id',$mobileUser->id)->first();
            if(!isset($formations))
            {
                $formations = 
                [
                    [
                        "id" => 0,
                        "ecole" => " ",
                        "diplome" => "",
                        "date_debut" => " ",
                        "date_fin" => " ",
                        "domaine" => "",
                        "user_id"=> 0,
                        "grade" => ""
                    ]
                ];
            }
            else */
            {
                $formations = Formation::where('user_id',$mobileUser->id)->get();
            }

            //verif experience if null or no
            /*$experiences = Experience::where('user_id',$mobileUser->id)->first();
            if(!isset($experiences))
            {
                $experiences =
                [
                    [
                        "id" => 0,
                        "role" => "",
                        "entreprise" => "",
                        "date_debut" => "",
                        "date_fin" => "",
                        "actuellement" => "0",
                        "user_id" => 0,
                        "site_web" => ""
                    ]
                ];
            }
            else */
            {
                $experiences = Experience::where('user_id',$mobileUser->id)->get();
            }
            $tab =
            [
                "id" => $mobileUser->id,
                "nom" => $mobileUser->nom,
                "prenom" => $mobileUser->prenom,
                "date_de_naissance" => $mobileUser->date_de_naissance,
                "pays_de_residence" => $mobileUser->pays_de_residence,
                "telephone" => $mobileUser->telephone,
                "sexe" => $mobileUser->sexe,
                "entreprise" => $mobileUser->entreprise,
                "site_web" => $mobileUser->site_web,
                "localite_entreprise" => $mobileUser->localite_entreprise,
                "objectifs" => $this->findObjectifByIdUserMobile($mobileUser->id),
                "interets" => $this->findInteretsByIdUserMobile($mobileUser->id),
                "formations" => $formations ,
                "experiences" => $experiences,
                "certifications" => Certification::where('user_id',$mobileUser->id)->get(),
                "token_device" => $mobileUser->token_device,
                "token_api" => $mobileUser->token_api,
                "type_auth_api" => $mobileUser->type_auth_api,
                "email" => $mobileUser->email,
                "email_professionnel" => $mobileUser->email_professionnel,
                "photo_de_profile" => asset('/storage/' . $photo),
                "domaine_activite_p" => $domaine_activite_p,
                //"domaine_activite_s" => Activite::where('status', 1)->where('type', 'Secondaire')->where('id', $mobileUser->domaine_activite_s)->first(),
                "domaine_activite_s" => $domaine_activite_s,
                "role" => $role,
                "description" => $mobileUser->description,
                "access_b2b" =>  $mobileUser->access_b2b,
                "new" => $mobileUser->new
            ];
            return $tab;
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    private function findObjectifByIdUserMobile($id)
    {
        $userObjectifs = UsersObjectif::where('mobile_user_id',$id)->get();
        $tab = [];
        foreach ($userObjectifs as $v)
            $tab[] = Objectif::find($v->objectif_id);
        return $tab;
    }

    private function findInteretsByIdUserMobile($id)
    {
        $userInteret = UsersInteret::where('mobile_user_id',$id)->get();
        $tab = [];
        foreach ($userInteret as $v)
            $tab[] = Interet::find($v->interet_id);

        return $tab;
    }

    public function match(Request $request)
    {
        try
        {
            //verif if the user1 and user2
            $user1 = MobileUser::find($request->id_user1);
            $user2 = MobileUser::find($request->id_user2);
            if((isset($user1)) AND (isset($user2)))
            {
                $reactVerif = React::where('id_user1', $request->id_user1)
                                ->where('id_user2', $request->id_user2)
                                ->first();
                if(!isset($reactVerif))
                {
                    $react = new React();
                    $react->id_user1 = $request->id_user1;
                    $react->id_user2 = $request->id_user2;
                    $react->action = $request->action; // should be 'like' or 'swipe'
                    //explication: user1 reacted to user2
                    $react->save();
                    $searchReact = React::where('id_user1', $request->id_user2)
                        ->where('id_user2', $request->id_user1)
                        ->where('action','like')
                        ->first();
                    if((isset($searchReact)) AND ($request->action == 'like'))
                    {
                        $this->insertNotification("Vous avez matché avec l'utilisateur", "connect", $user1->token_device, $user1->id,null,$user2->id); //V
                        $this->insertNotification("Vous avez matché avec l'utilisateur", "connect", $user2->token_device, $user2->id,null,$user1->id); //V
                        $this->sendNotificationToUser($user1->id, "Match", "Félicitation, il y a match avec un autre utilisateur");
                        $this->sendNotificationToUser($user2->id, "Match", "Félicitation, il y a match avec un autre utilisateur");
                        return response()->json("Congratulation there is a match");
                    }
                    return response()->json("There is no match");
                }
                return response()->json("This user is already reacted to other user");
            }
            return  response()->json("verify the existence of both users");
        }
        catch(Exception $exception)
        {
            return response()->json
            ("Exception error");
        }
    }

    public function verifStand(Request $request)
    {
        try
        {
            $mobileUser = MobileUser::find($request->id);
            $stand = Stand::where('entreprise', strtolower($request->entreprise))->where('numero_du_stand',$request->numero_du_stand)->first();
            if(isset($mobileUser))
            {
                if(($mobileUser->access_b2b == 1) OR (isset($stand)))
                    return response()->json(true);
                return response()->json(false);
            }
            return response()->json(false);
        }
        catch(Exception $e)
        {
            return response()->json(false);
        }
    }

    private function getCodeActivation()
    {
        $random = \Illuminate\Support\Str::random(6);
        return strtoupper($random);
    }

    public function insertEmailPro(Request $request)
    {
        try
        {
            $mobile_user = MobileUser::find($request->id);
            if(isset($mobile_user))
            {
                $mobile_user->email_professionnel = $request->email_professionnel;
                $mobile_user->save();
            }
            return response()->json("This is does not exist");
        }
        catch(Exception $e)
        {
            return "";
        }
        
    }

    public function confirmAccessB2bMeeting(Request $request)
    {
        try
        {
            $mobile_user = MobileUser::where('id',$request->id)->where('code_activation_b2b_meeting',$request->code_activation_b2b_meeting)->first();
            if(isset($mobile_user))
            {
                $mobile_user->access_b2b = 1;
                $mobile_user->save();
                return response()->json
                (
                    [
                        "status" => "success"
                    ]
                );
            }
            return response()->json
            (
                [
                    "status" => "error"
                ]
            );
        }
        catch(Exception $e)
        {
            return response()->json("exception error");
        }
    }

    public function listMatch($id)
    {
        try
        {
            $list = DB::select("SELECT id_user1, id_user2 FROM reacts WHERE ((id_user1=$id OR id_user2=$id) AND (ACTION = 'like'))");
            $idsLike = $this->getLikeUser($list, $id);
            $usersLikes = $this->whoLikeMe($id, $idsLike);
            
            $created_at = array_column($usersLikes, 'time');

            array_multisort($created_at, SORT_DESC, $usersLikes);

            return $usersLikes;
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    private function getLikeUser($list, $id)
    {
        try
        {
            $T = [];
            foreach($list as $v)
            {
                if(($v->id_user1 != $id) AND ($v->id_user2 == $id))
                    $T[] = $v->id_user1;
                else
                    $T[] = $v->id_user2;
            }
            return array_unique($T);
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    private function whoLikeMe($id, $idsLike)
    {
        try
        {
            $users = [];
            foreach($idsLike as $v)
            {
                $react1 = React::where('id_user1',$id)->where('id_user2',$v)->first();
                $react2 = React::where('id_user1',$v)->where('id_user2', $id)->first();
                if((isset($react1)) AND (isset($react2)))
                {
                    if($react1->created_at > $react2->created_at)
                    {
                        $users[] = 
                        [
                            "user" => $this->getUser($v),
                            "time" => $react1->created_at
                        ];
                    }
                    else
                    {
                        $users[] = 
                        [
                            "user" => $this->getUser($v),
                            "time" => $react2->created_at
                        ];
                    }
                }
            }
            return $users;
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    public function activeB2bRequest(Request $request)
    {
        try
        {
            $mobile_user = MobileUser::find($request->id);
            if(isset($mobile_user))
            {
                if(isset($mobile_user->email_professionnel))
                {
                    $mobile_user->code_activation_b2b_meeting = $this->getCodeActivation();
                    $mobile_user->access_b2b = 2;
                    $data = array
                    (
                        "email" => $mobile_user->email_professionnel,
                        "code" =>  $mobile_user->code_activation_b2b_meeting
                    );

                    Mail::send
                    (
                        'mail/mail_confirmation_b2b', ['data' => $data],
                        function ($message) use ($data)
                        {
                            $message->from('lab@xelero.io','dontreply@siticafrica.com');
                            $message->subject("Code de confirmation");
                            $message->to($data['email'], $data['code']);
                        }
                    );
                    $mobile_user->save();
                    $this->insertNotification("Permission b2b","Permission b2b",$mobile_user->token_device,$mobile_user->id,null,null); //V //admin
                    $this->sendNotificationToUser($mobile_user->id, "L’accès b2b", "L'admin a approuvé l'accès b2b, vous devez obtenir le code d'activation par e-mail et la confirmer");
                    //$this->PushNotif($mobile_user->token_device,"L’accès b2b", "L'admin a approuvé l'accès b2b, vous devez la confirmation");
                    return response()->json
                    (
                        [
                            "code" => 1,
                            "statuts" => "sucess",
                            "messageFr" => "Email envoyé",
                            "messageEn" => "Email sent"
                        ]
                    );
                }
                return response()->json
                (
                    [
                        "code" => 0,
                        "statuts" => "error",
                        "messageFr" => "Cet utilisateur n'a pas un email professionnel",
                        "messageEn" => "This user does not have a professional email"
                    ]
                );
            }
            return response()->json
            (
                [
                    "code" => 0,
                    "statuts" => "error",
                    "messageFr" => "Cet utilisateur n'existe pas",
                    "messageEn" => "this user does not exist"
                ]
            );
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "code" => 0,
                    "statuts" => "error",
                    "messageFr" => "exception ".$e->getMessage(),
                    "messageEn" => "exception " .$e->getMessage()
                ]
            );
        }
    }

    public function activeSimpleB2b(Request $request)
    {
        try
        {
            $mobileUser = MobileUser::find($request->id);
            if(isset($mobileUser))
            {

                $mobileUser->access_b2b = $request->access_b2b;
                $mobileUser->save();
                if($request->access_b2b == 1)
                {
                    if(isset($mobileUser->email_professionnel))
                    {
                        $data = array
                        (
                            "email" => $mobileUser->email_professionnel,
                            "code" =>  ""
                        );
                        Mail::send
                        (
                            'mail/mail_confirmation_b2b', ['data' => $data],
                            function ($message) use ($data)
                            {
                                $message->from('no-reply@siticafrica.com','SITIC Africa Abidjan 2022');
                                $message->subject("Activation du compte exposant / sponsor");
                                $message->to($data['email'], $data['code']);
                            }
                        );
                    }
                    return response()->json
                    (
                        [
                            "code" => 1,
                            "statuts" => "success",
                            "messageFr" => "Cet utilisateur est passè en exposant",
                            "messageEn" => "This user is exposant"
                        ]
                    );
                }
                else
                {
                    return response()->json
                    (
                        [
                            "code" => 1,
                            "statuts" => "success",
                            "messageFr" => "Cet utilisateur est passè en visiteur",
                            "messageEn" => "This user is visitor"
                        ]
                    );
                }
            }
            else
            {
                return response()->json
                (
                    [
                        "code" => 0,
                        "statuts" => "error",
                        "messageFr" => "Cet utilisateur est n'existe pas",
                        "messageEn" => "This user is not found"
                    ]
                );
            }
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "code" => 0,
                    "statuts" => "error",
                    "messageFr" => "exception ".$e->getMessage(),
                    "messageEn" => "exception " .$e->getMessage()
                ]
            );
        }
    }

    public function testEmail()
    {
        //pour activer la sécurité il faut visiter ce lien https://myaccount.google.com/security
        $data = array
        (
            "email" => "habibha.aroua82@gmail.com",
            "code" =>  $this->getCodeActivation()
        );

        Mail::send
        (
            //taemobileapp@gmail.com
            'mail/mail_confirmation_b2b', ['data' => $data],
            function ($message) use ($data)
            {
                $message->from('taemobileapp@gmail.com','dontreply@siticafrica.com');
                $message->subject("Code de confirmation");
                $message->to($data['email'], $data['code']);
            }
        );
    }

    //Notifaction

    private function SearchTokenDevice (String $tab,String $token)
    {
        $tab = json_decode($tab);
        foreach ($tab as $key => $value)
        {
            if($value == $token)
            {
                return true;
            }
        }
        return false ;
    }

    public function PushNotif(string $token, string $title, string $body)
    {
        $token1 = json_decode($token);
        $response = Http::withHeaders([
            'host' => 'exp.host',
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate',
            'content-type' => 'application/json',
        ])->post($this->UrlPushNotif, [
            'to' => $token1,
            'title' => $title,
            'body' => $body
        ]);
        return $response;
    }

    public function NotifcationGeneral($id)
    {
        $prog = Programme::all('id','titre','date_prog');
        $userMatch = $this->listMatch($id);
        return 
        [
            "programme" =>$prog,
            "matchUser" => $userMatch
            //add message
        ];
    }

    public function myNotification($id)
    {
        try
        {
            $notif = Notification::where('id_user',$id)->get();
            $tab = [];
            foreach($notif as $v)
            {
                $prog = [];
                $userLike = '';
                if(isset($v->id_prog))
                    $prog = Programme::find($v->id_prog);
                elseif(isset($v->id_like))
                {
                    $userLike = MobileUser::select('id','nom','prenom')->where('id',$v->id_like)->first();
                    $userLike = $userLike->nom.' '.$userLike->prenom;
                }
                $tab[] = 
                [
                    "id" => $v->id,
                    "titre" => $v->titre,
                    "type" => $v->type,
                    'date_notification' => $v->created_at,
                    "programme" => $prog,
                    "userLike" => $userLike
                ];
            }
            return response()->json($tab);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    private function insertNotification($titre, $type, $token_device, $id_user, $id_prog, $id_like)
    {
        $notification = new Notification();
        $notification->titre = $titre;
        $notification->type = $type;
        $notification->token_device = $token_device;
        $notification->id_user = $id_user;
        $notification->id_prog = $id_prog;
        $notification->id_like = $id_like;
        $notification->save();
    }

    public function testNotif()
    {
		$m = MobileUser::find(128);
		$array = json_decode($m->token_device);
		foreach($array as $v)
		{
			if(($v != "ExponentPushToken[SIMULATOR]") or (!isset($v)))
			{
				$token = "$v";
				$token=str_replace('"','\"',$token);
    			$token = '"'.$token.'"';
				$token = '['.$token.']';
			   $this->PushNotif($token, "test", "This a text");
			}
		}
		
		//return "notif sent";
        //$token = '["ExponentPushToken[EjG0yfDvG4OePTt6uj27je],ExponentPushToken[EjG0yfDvG4OePTt6uj27je]"]'; //Habib
		//return json_encode($token);
		//$token = '["ExponentPushToken[GxGnncCiPmVNX5rpa5jZKz]"]'; //Amine
		//return $token;
        
    }

    public function updateGeneral(Request $request) //urgent
    {
        //try
        //{
            
            $mobile_user = MobileUser::find($request->id);
            if(isset($mobile_user))
            {
                if($mobile_user->email == $request->email)
                {
                    $mobile_user->date_de_naissance = $request->date_de_naissance;
                    $mobile_user->domaine_activite_p = $request->domaine_activite_p['id'];
                    $mobile_user->domaine_activite_s = $request->domaine_activite_s['id'];
                    
                    $mobile_user->role = $request->role['id'];
                    $mobile_user->entreprise = $request->entreprise;
                    $mobile_user->nom = $request->nom;
                    $mobile_user->prenom = $request->prenom;
                    $mobile_user->pays_de_residence = $request->pays_de_residence;
                    $mobile_user->sexe = $request->sexe;
                    $mobile_user->site_web = $request->site_web;
                    $mobile_user->telephone = $request->telephone;
                    if(isset($request->email_pro))
                        $mobile_user->email_professionnel = $request->email_pro; 
                    $mobile_user->localite_entreprise = $request->localite_entreprise;
                    $mobile_user->save();
                    
                    $tab = [];
                    $tabInteret = [];
                    foreach($request->interets as $v)
                    {
                        $interet = Interet::where('titre',$v)->first();
                        if(isset($interet))
                        {
                            $tabInteret[] = $interet->id;
                        }
                    }

                    foreach($tabInteret as $v)
                    {
                        //$test = $this->VerifUsers_interets($v['id'], $request->id);
                        $userInteret = UsersInteret::where('mobile_user_id', $request->id)->where('interet_id',$v)->first();
                        if(!isset($userInteret))
                        {
                            $userInteret = new UsersInteret();
                            $userInteret->mobile_user_id = $request->id;
                            $userInteret->interet_id = $v;
                            $userInteret->save();
                            $tab[] = $v;
                        }
                        else
                        {
                            $tab[] = $v;
                        }
                    }
                    $userInteret = UsersInteret::where('mobile_user_id',$request->id)->get();
                    $this->filtreUsers_interets($tab, $userInteret, $request->id);

                    $tab = [];
                    $tabObjectif = [];
                    foreach($request->objectifs as $v)
                    {
                        $objectif = Objectif::where('titre',$v)->first();
                        if(isset($objectif))
                        {
                            $tabObjectif[] = $objectif->id;
                        }
                    }

                    foreach($tabObjectif as $v)
                    {
                        //$test = $this->VerifUsers_objectifs($v['id'], $request->id);
                        $userObjectif = UsersObjectif::where('mobile_user_id',$request->id)->where('objectif_id',$v)->first();
                        
                        if(!isset($userObjectif))
                        {
                            $usersObjectif = new UsersObjectif();
                            $usersObjectif->mobile_user_id = $request->id;
                            $usersObjectif->objectif_id = $v;
                            $usersObjectif->save();
                            $tab[] = $v;
                        }
                        else
                        {
                            $tab[] = $v;
                        }
                    }
                    $usersObjectif = UsersObjectif::where('mobile_user_id',$request->id)->get();
                    $this->filtreUsers_objectifs($tab, $usersObjectif, $request->id);

                    return response()->json($this->getUser($request->id));
                }
                else
                {
                    $m = MobileUser::where('email',$request->email)->where('id','!=',$request->id)->first();
                    if(isset($m))
                        return response()->json("This email is exist");
                    else
                    {
                        $mobile_user->date_de_naissance = $request->date_de_naissance;
                        $mobile_user->domaine_activite_p = $request->domaine_activite_p['id'];
                        $mobile_user->domaine_activite_s = $request->domaine_activite_s['id'];
                        $mobile_user->email = $request->email;
                        $mobile_user->role = $request->role['id'];
                        $mobile_user->entreprise = $request->entreprise;
                        $mobile_user->nom = $request->nom;
                        $mobile_user->prenom = $request->prenom;
                        $mobile_user->pays_de_residence = $request->pays_de_residence;
                        $mobile_user->sexe = $request->sexe;
                        $mobile_user->site_web = $request->site_web;
                        $mobile_user->telephone = $request->telephone;
                        if(isset($request->email_pro))
                            $mobile_user->email_professionnel = $request->email_pro; 
                        $mobile_user->localite_entreprise = $request->localite_entreprise;
                        $mobile_user->save();
                    
                        $tab = [];
                        $tabInteret = [];
                        foreach($request->interets as $v)
                        {
                            $interet = Interet::where('titre',$v)->first();
                            if(isset($interet))
                            {
                                $tabInteret[] = $interet->id;
                            }
                        }
                        foreach($tabInteret as $v)
                        {
                            //$test = $this->VerifUsers_interets($v['id'], $request->id);
                            $userInteret = UsersInteret::where('mobile_user_id', $request->id)->where('interet_id',$v)->first();
                            if(!isset($userInteret))
                            {
                                $userInteret = new UsersInteret();
                                $userInteret->mobile_user_id = $request->id;
                                $userInteret->interet_id = $v;
                                $userInteret->save();
                                $tab[] = $v;
                            }
                            else
                            {
                                $tab[] = $v;
                            }
                        }
                        $userInteret = UsersInteret::where('mobile_user_id',$request->id)->get();
                        $this->filtreUsers_interets($tab, $userInteret, $request->id);

                        $tab = [];
                        $tabObjectif = [];
                        foreach($request->objectifs as $v)
                        {
                            $objectif = Objectif::where('titre',$v)->first();
                            if(isset($objectif))
                            {
                                $tabObjectif[] = $objectif->id;
                            }
                        }

                        foreach($tabObjectif as $v)
                        {
                            //$test = $this->VerifUsers_objectifs($v['id'], $request->id);
                            $userObjectif = UsersObjectif::where('mobile_user_id',$request->id)->where('objectif_id',$v)->first();
                        
                            if(!isset($userObjectif))
                            {
                                $usersObjectif = new UsersObjectif();
                                $usersObjectif->mobile_user_id = $request->id;
                                $usersObjectif->objectif_id = $v;
                                $usersObjectif->save();
                                $tab[] = $v;
                            }
                            else
                            {
                                $tab[] = $v;
                            }
                        }
                        $usersObjectif = UsersObjectif::where('mobile_user_id',$request->id)->get();
                        $this->filtreUsers_objectifs($tab, $usersObjectif, $request->id);
                        Experience::where('user_id',$request->id)->delete();
                        foreach ($request->experiences as $key => $value)
                        {
                            $Experience = new Experience();
                            $Experience->role = $value['role'];
                            $Experience->entreprise = $value['entreprise'];
                            $Experience->site_web = $value['site_web'];
                            $Experience->date_debut = Carbon::createFromDate($value['date_debut']);
                            $Experience->date_fin = Carbon::createFromDate($value['date_fin']);
                            $Experience->actuellement = $value['actuellement'];
                            $Experience->user_id = $request->id;
                            $Experience->save();
                        }

                        // insert data into formations
                        Formation::where('user_id',$request->id)->delete();
                        foreach ($request->formations as $key => $value)
                        {
                            $formation = new Formation();
                            $formation->ecole = $value['ecole'];
                            $formation->diplome = $value['diplome'];
                            $formation->domaine = $value['domaine'];
                            $formation->date_debut = Carbon::createFromDate($value['date_debut']);
                            $formation->date_fin = Carbon::createFromDate($value['date_fin']);
                            $formation->grade = $value['grade'];
                            $formation->user_id = $request->id;
                            $formation->save();
                        }

                        // insert data into certifications
                        Certification::where('user_id',$request->id)->delete();
                        foreach ($request->certifications as $key => $value)
                        {
                            $certif = new Certification();
                            $certif->certification = $value['certif'];
                            $certif->discipline = $value['discipline'];
                            $certif->organisation = $value['org'];
                            $certif->date = Carbon::createFromDate($value['dateDeb']);
                            $certif->user_id = $request->id;
                            $certif->save();
                        }
                        return response()->json($this->getUser($request->id));
                    }
                }
            }
            return response()->json("This user is not exist");
        //}
        //catch(Exception $e)
        //{
            //return response()->json("error exception");
        //}
    }

    public function updateExperience(Request $request)
    {
        try
        {
            $mobile_user = MobileUser::find($request->id);
            if(isset($mobile_user))
            {
                Experience::where('user_id',$request->id)->delete();
                foreach ($request->experiences as $key => $value)
                {
                    $Experience = new Experience();
                    $Experience->role = $value['role'];
                    $Experience->entreprise = $value['company'];
                    $Experience->site_web = $value['company_website'];
                    $Experience->date_debut = Carbon::createFromDate($value['dateDeb']);
                    $Experience->date_fin = Carbon::createFromDate($value['dateFin']);
                    $Experience->actuellement = $value['currently_work'];
                    $Experience->user_id = $request->id;
                    $Experience->save();
                }
                return $this->getUser($mobile_user->id);
            }
            else
            {
                return response()->json("This user is not exist");
            }
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    public function updateCertif(Request $request)
    {
        try
        {
            $mobile_user = MobileUser::find($request->id);
            if(isset($mobile_user))
            {
                Certification::where('user_id',$request->id)->delete();
                foreach ($request->certifications as $key => $value)
                {
                    $certif = new Certification();
                    $certif->certification = $value['certif'];
                    $certif->discipline = $value['discipline'];
                    $certif->organisation = $value['org'];
                    $certif->date = Carbon::createFromDate($value['dateDeb']);
                    $certif->user_id = $request->id;
                    $certif->save();
                }
                return response()->json($this->getUser($request->id));
            }
            else
            {
                return response()->json("This user is not exist");
            }
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    public function updateFormation(Request $request)
    {
        try
        {
            $mobile_user = MobileUser::find($request->id);
            if(isset($mobile_user))
            {
                Formation::where('user_id',$request->id)->delete();
                foreach ($request->formations as $key => $value)
                {
                    $formation = new Formation();
                    $formation->ecole = $value['school'];
                    $formation->diplome = $value['degree'];
                    $formation->domaine = $value['field_of_study'];
                    $formation->date_debut = Carbon::createFromDate($value['dateDeb']);
                    $formation->date_fin = Carbon::createFromDate($value['dateFin']);
                    $formation->grade = $value['grade'];
                    $formation->user_id = $request->id;
                    $formation->save();
                }
                return response()->json($this->getUser($request->id));
            }
            else
            {
                return response()->json("This user is not exist");
            }
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    private function filtreUsers_interets($tab, $users_interets, $user_id)
    {
        foreach($users_interets as $v)
        {
            if (!in_array($v->interet_id, $tab))
            {
                $u = UsersInteret::where('interet_id',$v->interet_id)->where('mobile_user_id',$user_id)->first();
                $u->delete();
            }
        }
    }

    private function filtreUsers_objectifs($tab, $users_objectifs, $user_id)
    {
        foreach($users_objectifs as $v)
        {
            if (!in_array($v->objectif_id, $tab))
            {
                $u = UsersObjectif::where('objectif_id',$v->objectif_id)->where('mobile_user_id',$user_id)->first();
                $u->delete();
            }
        } 
    }

    private function sendNotificationToUser($id, $title, $content)
    {
        $m = MobileUser::find($id);
        if(isset($m->token_device))
        {
            $array = json_decode($m->token_device);
            foreach($array as $v)
            {
                if(($v != "ExponentPushToken[SIMULATOR]") or (!isset($v)))
                {
                    $token = "$v";
                    $token=str_replace('"','\"',$token);
                    $token = '"'.$token.'"';
                    $token = '['.$token.']';
                    $this->PushNotif($token, $title, $content);
                }
            }
        }
    }

    public function autoNotif()
    {
        $mobileUser = MobileUser::all();
        $p = Programme::where('date_prog','>',date("Y-m-d"))->orderBy("date_prog",'asc')->get();
        foreach($p as $v)
        {
            $v->heure_debut1 = date("Y-m-d H:i:s",strtotime($v->date_prog.' '.$v->heure_debut));
            $v->heure_debut1 = Carbon::createFromDate($v->heure_debut1);
            $v->before15Minutes = $v->heure_debut1->subMinutes(15);
            if(Carbon::now()->format('Y-m-d H:i') == $v->before15Minutes->format('Y-m-d H:i:s'))
            {
                foreach($mobileUser as $m)
                {
                    $this->insertNotification("Upcoming workshop ".$v->titre, "Programme", null, $m->id,$v->id,null,null);
                    $this->sendNotificationToUser($m->id, "Upcoming workshop", "Il est encore 15 minutes avant le début : ".$v->titre);
                }
            }
        }
        return $p;
    }

    public function NotifEvent()
    {
        $e = Evenement::where('titre','SITIC Africa Abidjan 2022')->first();
        //$d = Carbon::createFromDate($e->date_debut);
        /* you cannot you a variable
        echo "event ".Carbon::createFromDate($e->date_debut)->format('Y-m-d').'<br>';
        echo "before week ".Carbon::createFromDate($e->date_debut)->subWeeks(1)->format('Y-m-d').'<br>';
        echo "before 3 days ".Carbon::createFromDate($e->date_debut)->subDays(3)->format('Y-m-d').'<br>';
        echo "before 1 day ". Carbon::createFromDate($e->date_debut)->subDays(3)->format('Y-m-d');
        */
        $d = date('Y-m-d');
        $mobileUser = MobileUser::all();
        if($d == Carbon::createFromDate($e->date_debut)->subWeeks(1)->format('Y-m-d'))
        {
            //before week
            $this->insertNotification("Evenement ".$v->titre.' Il reste une semaine', "Event", null, $m->id,$v->id,null,null);
            $this->sendNotificationToUser($m->id, "Evenement ".$v->titre, "Il est encore 1 semaines avant le début de l'évenement : ".$v->titre);
        }
        elseif($d == Carbon::createFromDate($e->date_debut)->subDays(3)->format('Y-m-d'))
        {
            //before 3 days
            $this->insertNotification("Evenement ".$v->titre.' Il reste 3 jours', "Event", null, $m->id,$v->id,null,null);
            $this->sendNotificationToUser($m->id, "Evenement ".$v->titre, "Il est encore 3 jours avant le début de l'évenement : ".$v->titre);
        }
        elseif($d == Carbon::createFromDate($e->date_debut)->subDays(3)->format('Y-m-d'))
        {
            //before 1 day
            $this->insertNotification("Evenement ".$v->titre.' Il reste un jour', "Event", null, $m->id,$v->id,null,null);
            $this->sendNotificationToUser($m->id, "Evenement ".$v->titre, "Il est reste un jour avant le début de l'évenement : ".$v->titre);
        }
    }

    public function insertTokenDevice(Request $request)
    {
        try
        {
            $mobileUser = MobileUser::find($request->id);
            if (strlen($mobileUser->token_device) > 2)
            {
                if (!$this->SearchTokenDevice($mobileUser->token_device,$token_device))
                {
                    $tab = (array)json_decode($mobileUser->token_device); //we should cast the object stdClass to array
                    $tab[] = $token_device; //add new element in the table $tab
                    $mobileUser->token_device = json_encode($tab);
                }
            }
            else
            {
                $mobileUser->token_device = json_encode([$token_device]) ;
            }
            return response()->json(true);
        }
        catch(Exception $e)
        {
            return response()->json(false);
        }
    }

    public function ActiveForgetPassword(Request $request)
    {
        try
        {
            $mobileUser = MobileUser::where('email', $request->email)->where('type_auth_api','email')->first();
            if(isset($mobileUser))
            {
                $mobileUser->code_active_password = $this->getCodeActivation();
                $data = array
                (
                    "email" => $request->email,
                    "code" =>  $mobileUser->code_active_password
                );

                Mail::send
                (
                    'mail/mail_forget_password', ['data' => $data],
                    function ($message) use ($data)
                    {
                        $message->from('lab@xelero.io','dontreply@siticafrica.com');
                        $message->subject("Réinitialisation de mot de passe");
                        $message->to($data['email'], $data['code']);
                    }
                );
                $mobileUser->save(); // save the value of code_active_password in database

                //get the result
                return response()->json
                (
                    [
                        "code" => 1,
                        "status" => "success",
                        "message" => "You will receive an email"
                    ]
                );
            }
            else
            {
                return response()->json
                (
                    [
                        "code" => 0,
                        "status" => "error",
                        "message" => "This user is not found or using this email with other type of Auth"
                    ]
                );
            }
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "code" => 0,
                    "status" => "exception",
                    "message" => "Exception ".$e->getMessage()
                ]
            );
        }
    }

    public function VerifCodeForgetPassword(Request $request)
    {
        try
        {
            $mobileUser = MobileUser::where('email', $request->email)->where('type_auth_api','email')->first();
            if(isset($mobileUser))
            {
                if($mobileUser->code_active_password == $request->code_active_password)
                {
                    return response()->json
                    (
                        [
                            "code" => 1,
                            "status" => "success",
                            "message" => "Your code is correct"
                        ]
                    );
                }
                else
                {
                    return response()->json
                    (
                        [
                            "code" => 0,
                            "status" => "error",
                            "message" => "Your code is not correct"
                        ]
                    );
                }
            }
            else
            {
                return response()->json
                (
                    [
                        "code" => 0,
                        "status" => "error",
                        "message" => "Cet utilisateur est introuvable ou utilise cet e-mail avec un autre type d'authentification"
                        //"message" => "This user is not found or using this email with other type of Auth"
                    ]
                );
            }
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "code" => 0,
                    "status" => "exception",
                    "message" => "Il y un probléme technique, veuillez réessayer"
                    //"message" => "Exception ".$e->getMessage()
                ]
            );
        }
    }

    public function ResetPassword(Request $request)
    {
        try
        {
            $userMobile = MobileUser::where('email', $request->email)->where('type_auth_api','email')->where('code_active_password',$request->code_active_password)->first();
            if(isset($userMobile))
            {
                $userMobile->mot_de_passe = Hash::make($request->password);
                $userMobile->save();
                return response()->json
                (
                    [
                        "code" => 1,
                        "status" => "success",
                        "message" => "You changed your password"
                    ]
                );
            }
            else
            {
                return response()->json
                (
                    [
                        "code" => 0,
                        "status" => "error",
                        "message" => "email or code activation not correct"
                    ]
                );
            }
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "code" => 0,
                    "status" => "exception",
                    "message" => "Exception ".$e->getMessage()
                ]
            );
        }
    }

    public function getActiveB2bUsers()
    {
        try
        {
            $mobile_user = MobileUser::where('access_b2b',1)->get();
            $array = [];
            foreach($mobile_user as $v)
            {
                $array[] = $this->getUser($v->id);
            }
            return response()->json(["users"=>$array]);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function getActiveB2bUsers1($id)
    {
        try
        {
            $me = MobileUser::find($id);
            if(isset($me))
            {
                if($me->access_b2b == 1) // this user is exposant
                {
                    $mobile_user = MobileUser::where('id','!=',$id)->get(); // all people (exposant and visiteur)
                    $array = [];
                    foreach($mobile_user as $v)
                    {
                        $array[] = $this->getUser($v->id);
                    }
                    return response()->json(["users"=>$array]);
                }
                else //if the user is visiteur
                {
                    $mobile_user = MobileUser::where('access_b2b',1)->where('id','!=',$id)->get(); //only exposants
                    $array = [];
                    foreach($mobile_user as $v)
                    {
                        $array[] = $this->getUser($v->id);
                    }
                    return response()->json(["users"=>$array]);
                }
            }
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    //test notif
    public function PushNotif1()
    {
        $user = MobileUser::find(251);
        $this->PushNotif($user->token_device, "test", "the text text", "test");
        return "sent";
    }

    private function verifB2bMeetUserIsNotNull($b2b_meet_id)
    {
        $b2bMeetUser = B2bMeetUser::where('b2b_meet_id', $b2b_meet_id)->first();
        if(isset($b2bMeetUser))
            return 1;
        else
            return 0;
    }

    public function updateStautsB2bMeets()
    {
        $b2bMeets = B2bMeet::all();
        $tab = [];
        foreach($b2bMeets as $v)
        {
            if($this->verifB2bMeetUserIsNotNull($v->id) == 1)
            {
                //$b2bMeetUser = B2bMeetUser::where('b2b_meet_id', $v->id)->get();
                $this->updateStautsB2bMeet($v->id);
            }
        }
        return "updated";
    }

    public function updateStautsB2bMeet($id)
    {
        $b2bMeetUser = B2bMeetUser::where('b2b_meet_id', $id)->get();
        $b2bMeet  = B2bMeet::find($id);
        $tab = [];
        foreach($b2bMeetUser as $v)
        {
            if(isset($v))
                $tab[] = $v;
        }
        if((isset($tab[0])) AND (isset($tab[1])))
        {
            if(($tab[0]->confirmer == 0) AND ($tab[1]->confirmer == 0) AND ($tab[1]->refuser == 0) AND ($tab[1]->refuser == 0))
            {
                $b2bMeet->status1 = "En attente";
            }
            elseif(($tab[0]->confirmer == 1) OR ($tab[1]->confirmer == 1))
            {
                $b2bMeet->status1 = "Confirmer";
            }
            elseif(($tab[0]->refuser == 1) OR ($tab[1]->refuser == 1))
            {
                $b2bMeet->status1 = "Refuser";
            }
            $b2bMeet->save();
        }
    }

    public function active_b2b_or_match()
    {
        try
        {
            $b2bActive = B2bActive::find(1);
            return response()->json($b2bActive);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }
    
}
