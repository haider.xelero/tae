<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MobileUsersExport;
use App\Imports\MobileUsersImport;
use App\Exports\B2BMeetsExport;

class ExcelController extends Controller
{
    public function importExportView()
    {
       return view('import');
    }
   
    public function export() 
    {
        return Excel::download(new MobileUsersExport, 'users.xlsx');
    }
   
    public function import() 
    {
        Excel::import(new MobileUsersImport,request()->file('file'));
           
        return redirect()->back();
    }

    //for b2b meet
    public function importExportView1()
    {
       return view('import');
    }
   
    public function export1() 
    {
        return Excel::download(new B2BMeetsExport, 'b2bMeet.xlsx');
    }
   
    public function import1() 
    {
        Excel::import(new B2bMeet,request()->file('file'));
           
        return redirect()->back();
    }
}
