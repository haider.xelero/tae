<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\ManuelNotification ;
use App\Notification;
use App\MobileUser;
use App\UsersInteret;

class WebController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    private $UrlPushNotif = "https://exp.host/--/api/v2/push/send";

    public function test(Request $request)
    {
        $m = new ManuelNotification();
        $m->titre = $request->titre;
        $m->valeur = $request->valeur;
        $m->pays = $request->pays;
        $m->interet = $request->interet;
        $m->domaine_activte = $request->domaine_activte;
        if(($request->pays == "Tous") && (!isset($request->interet)) && (!isset($request->domaine_activte)))
        {
            //pays=false, interet=false, domaine_activte=false
            $mobile_user = MobileUser::all();
        }
        elseif(($request->pays == "Tous") && (!isset($request->interet)) && (isset($request->domaine_activte)))
        {
            //pays=false, interet=false, domaine_activte=true
            $mobile_user = MobileUser::where('domaine_activite_p',$request->domaine_activte)->get();
        }
        elseif(($request->pays == "Tous") && (isset($request->interet)) && (!isset($request->domaine_activte)))
        {
            //pays=false, interet=true, domaine_activte=false
            $mobile_user = MobileUser::all();
            $mobile_user = $this->getUserByInteret($request->interet, $mobile_user);
        }

        if(($request->pays != "Tous") && (!isset($request->interet)) && (!isset($request->domaine_activte)))
        {
            //pays=true, interet=false, domaine_activte=false
            $mobile_user = MobileUser::where('pays_de_residence',$request->pays)->get();
        }
        elseif(($request->pays != "Tous") && (!isset($request->interet)) && (isset($request->domaine_activte)))
        {
            //pays=true, interet=false, domaine_activte=true
            $mobile_user = MobileUser::where('domaine_activite_p',$request->domaine_activte)->get();
        }
        elseif(($request->pays != "Tous") && (isset($request->interet)) && (!isset($request->domaine_activte)))
        {
            //pays=true, interet=true, domaine_activte=false
            $mobile_user = MobileUser::where('pays_de_residence',$request->pays)->get();
            $mobile_user = $this->getUserByInteret($request->interet, $mobile_user);

        }
        elseif(($request->pays != "Tous") && (isset($request->interet)) && (isset($request->domaine_activte)))
        {
            //pays=true, interet=true, domaine_activte=true

            $mobile_user = MobileUser::where('pays_de_residence',$request->pays)->where('domaine_activite_p',$request->domaine_activte)->get();
        
            $mobile_user = $this->getUserByInteret($request->interet, $mobile_user);
        }
        if (count($mobile_user) > 0)
        {
            foreach($mobile_user as $user)
            {
                $notification = new Notification();
                $notification->titre = $request->titre.' '.$request->valeur;
                $notification->type = "manuelle notification";
                $notification->token_device = null; //$token_device;
                $notification->id_user = $user->id;
                $notification->save();
                $this->sendNotificationToUser($user->id, $request->titre, $request->valeur);
            }
            $m->save();
            return redirect()->back()->with('message', 'La notification a été envoyé avec success');
        }
        else
        {
            return redirect()->back()->with('error', 'There is no user who have what you want to notification');
        }
    }

    private function getUserByInteret($id_interet, $users)
    {
        $tab = [];
        foreach($users as $user)
        {
            $ui = UsersInteret::where('mobile_user_id', $user->id)->where('interet_id', $id_interet)->first();
            if(isset($ui))
                $tab[] = $user;
        }
        return $tab;
    }

    public function PushNotif(string $token, string $title, string $body)
    {
        $token1 = json_decode($token);
        $response = Http::withHeaders([
            'host' => 'exp.host',
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate',
            'content-type' => 'application/json',
        ])->post($this->UrlPushNotif, [
            'to' => $token1,
            'title' => $title,
            'body' => $body
        ]);
        return $response;
    }

    private function sendNotificationToUser($id, $title, $content)
    {
        $m = MobileUser::find($id);
        if(isset($m->token_device))
        {
            $array = json_decode($m->token_device);
            foreach($array as $v)
            {
                if(($v != "ExponentPushToken[SIMULATOR]") or (!isset($v)))
                {
                    $token = "$v";
                    $token=str_replace('"','\"',$token);
                    $token = '"'.$token.'"';
                    $token = '['.$token.']';
                $this->PushNotif($token, $title, $content);
                }
            }
        }
    }
}
