<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Discussion;
use App\MobileUser;
use App\UsersDiscussion;
use App\UsersMessage;
use App\Notification;
use DB;

class ChatController extends Controller
{
    
    public function listConversations($id)
    {
        try
        {
            $discussion = Discussion::join('users_discussions', 'users_discussions.id_discu', '=', 'discussions.id')
            ->where('users_discussions.id_user', $id)
            ->orderBy('discussions.updated_at', 'DESC')
            ->get('id_discu');

            $tab=[];
            foreach($discussion as $v)
            {
                $user_discussion = UsersDiscussion::where('id_user','!=',$id)->where('id_discu',$v->id_discu)->first();
                if(isset($user_discussion))
                {
                    if(count($this->getLastMessageByConversation($v->id_discu))>0)
                    {
                        $v->lastMessage = $this->getLastMessageByConversation($v->id_discu);
                        $v->user = $this->getUser($user_discussion->id_user);
                        $tab[] = $v;
                    }
                }
            }
            return $tab;
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function sendMessage(Request $request)
    {
        try
        {
            $discussion = Discussion::find($request->conversation_id);

            $user_discussion = UsersDiscussion::where('id_user',$request->my_id)
                            ->where('id_discu',$request->conversation_id)->first();

            // for the other user 
            $user_discussionOther = UsersDiscussion::where('id_user','!=',$request->my_id)
                            ->where('id_discu',$request->conversation_id)->first("id_user");
            //return $user_discussionOther->id_user;

            if((isset($discussion)) AND (isset($user_discussion)))
            {
                $users_messages = new UsersMessage();
                $users_messages->id_user = $request->my_id;
                $users_messages->id_discu = $request->conversation_id;
                $users_messages->message = $request->message;
                // $users_messages->created_at = date('Y-m-d H:i:s');
                $users_messages->save();
                $discussion->updated_at = date('Y-m-d H:i:s');
                $discussion->save();
                return response()->json($users_messages);
            }
            return response()->json([]); //if there is problem if this disscussion is exist
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function getMessagesByConversation($id_conversation, $my_id)
    {
        try
        {
            $user_discussion = UsersDiscussion::where('id_user','!=',$my_id)->where('id_discu',$id_conversation)->first('id_user');
            if(isset($user_discussion))
            {
                $message = UsersMessage::select('id_user','message','created_at')->where('id_discu',$id_conversation)->orderBy('created_at', 'DESC')->paginate(5);
                if(count($this->getUser($user_discussion->id_user)) == 0)
                {
                    $tab = [];
                }
                else
                {
                    $tab = 
                    [
                        "user" => $this->getUser($user_discussion->id_user),
                        "messages" => $message
                    ];
                }
                
            }
            return response()->json($tab);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function insertMessage(Request $request)
    {
        $users_messages = new usersMessage();
        $users_messages->id_discu = $request->id_discu;
        $users_messages->id_user = $request->id_user;
        $users_messages->message = $request->message;
        $users_messages->seen = 0;
        $users_messages->created_at = date('Y-m-d H:i:s');
        $users_messages->save();
    }

    public function createConversation(Request $request)
    {
        try
        {
            $user1 = MobileUser::find($request->sender_id);
            $user2 = MobileUser::find($request->receiver_id);


            if((!isset($user1)) OR (!isset($user2)) OR ($request->sender_id == $request->receiver_id))
                return response()->json(0);


            $id_discu = $this->Verif_users_discussions($request->sender_id, $request->receiver_id);
            if(isset($id_discu))
                return response()->json($id_discu);

            //create the conversation
            $d = new Discussion();
            //$d->created_at = date('Y-m-d H:i:s');
            //$d->updated_at = date('Y-m-d H:i:s');
            $d->save();

            //create users_discussion with the id of sender and reciver and the id of conversation
            $this->createUsers_discussion($request->sender_id, $d->id);
            $this->createUsers_discussion($request->receiver_id, $d->id);

            //return the id of conversation
            return response()->json($d->id);
        }
        catch(Exception $e)
        {
            return response()->json(0);
        }
    }

    private function Verif_users_discussions($id1, $id2)
    {
        $user_discussion = DB::select("SELECT id_discu , COUNT(*) FROM users_discussions WHERE (id_user = $id1 OR id_user = $id2)  GROUP BY id_discu HAVING COUNT(*) > 1");
        if(isset($user_discussion[0]->id_discu))
            return $user_discussion[0]->id_discu;
        return null;
    }

    //private method used in this class
    private function createUsers_discussion($idUser, $idConversation)
    {
        $user_discussion = new UsersDiscussion();
        $user_discussion->id_discu = $idConversation;
        $user_discussion->id_user = $idUser;
        $user_discussion->created_at = date('Y-m-d H:i:s');
        $user_discussion->save();
    }

    private function getMyConversation($my_id, $discussion)
    {
        $tab = [];
        foreach($discussion as $v)
        {
            $user_discussion = UsersDiscussion::where('id_user','!=',$my_id)->where('id_discu',$v->id)->first();
            if(isset($user_discussion))
            {
                $v->lastMessage = $this->getLastMessageByConversation($v->id);
                $v->user = $this->getUser($user_discussion->id_user);
                $tab[] = $v;
            }
        }
        return $tab;
    }

    private function getUser($id)
    {
        try
        {
            $tab = [];
            $mobileUser = MobileUser::find($id);
            if(isset($mobileUser))
            {
                $tab =
                [
                    "id" => $mobileUser->id,
                    "nom" => $mobileUser->nom,
                    "prenom" => $mobileUser->prenom,
                    "photo_de_profile" => asset('/storage/' . $mobileUser->photo_de_profile)
                ];
            }
            return $tab;
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    private function getLastMessageByConversation($id)
    {
        $lastMessage = UsersMessage::where('id_discu',$id)->get()->last();
        if(!isset($lastMessage))
            return [];
        if(!isset($lastMessage->seen))
            $lastMessage->seen = 0;
        return 
        [
            "last_message_id" => $lastMessage->id,
            "last_message" => $lastMessage->message, 
            "seen" => $lastMessage->seen, 
            "created_at" => $lastMessage->created_at
        ];
    }

    public function getAllUnSeenMessage($idUser, $idDiscu)
    {
        $message = UsersMessage::where('id_discu',$id)->get();
        return $message;
    }

    public function setSeenMessage(Request $request)
    {
        try
        {
            $lastMessage = UsersMessage::find($request->lastMessage['last_message_id']);
            if(isset($lastMessage))
            {
                if($lastMessage->id_user != $request->user['id'])
                {
                    $lastMessage->seen = 1;
                    $lastMessage->save();
                    return response()->json($lastMessage);
                }
                else
                {
                    if(!isset($lastMessage->seen))
                        $lastMessage->seen = 0;
                    return response()->json($lastMessage);
                }
            }
            else
            {
                return response()->json([]);
            }
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    //api de test
    public function test()
    {
        return "Hello";
    }
}
/*
SELECT id_discu , COUNT(*) FROM users_discussions WHERE (id_user = 12 OR id_user= 13)  
GROUP BY id_discu
HAVING COUNT(*) > 1
*/