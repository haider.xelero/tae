<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UsersInteret extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
