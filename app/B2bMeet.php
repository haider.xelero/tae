<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class B2bMeet extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
