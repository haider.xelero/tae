-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : lun. 17 jan. 2022 à 09:49
-- Version du serveur : 10.3.31-MariaDB-0+deb10u1
-- Version de PHP : 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tae`
--

-- --------------------------------------------------------

--
-- Structure de la table `activites`
--

CREATE TABLE `activites` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `activites`
--

INSERT INTO `activites` (`id`, `titre`, `type`, `created_at`, `updated_at`, `status`) VALUES
(2, 'Finance', 'Principale', '2021-09-14 09:00:19', '2021-09-14 09:00:19', 1),
(3, 'Technologie', 'Principale', '2021-09-14 09:00:29', '2021-09-14 09:00:29', 1),
(4, 'Informatique', 'Principale', '2021-09-14 09:00:40', '2021-09-14 09:00:40', 1),
(5, 'Comptabilité', 'Principale', '2021-09-14 09:00:50', '2021-09-14 09:00:50', 1),
(6, 'Ressources humaines', 'Principale', '2021-11-24 11:05:49', '2021-11-24 11:05:49', 1),
(7, 'Volontaire association', 'Secondaire', '2022-01-10 15:05:23', '2022-01-10 15:05:23', 1),
(8, 'Musique', 'Secondaire', '2022-01-10 15:06:05', '2022-01-10 15:06:23', 1);

-- --------------------------------------------------------

--
-- Structure de la table `b2b_meets`
--

CREATE TABLE `b2b_meets` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prog_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `b2b_meets`
--

INSERT INTO `b2b_meets` (`id`, `titre`, `date`, `created_at`, `updated_at`, `prog_id`) VALUES
(1, 'hbib to enis', NULL, '2021-06-23 06:24:01', '2021-06-23 06:24:01', NULL),
(2, 'TEST MOBILE HBIB=>TAKEO', NULL, '2021-06-23 06:40:00', '2021-06-23 23:12:50', 16),
(3, 'TEST 2 MOBILE HBIB=>ENIS', NULL, '2021-06-23 09:30:00', '2021-06-23 23:13:00', 16),
(6, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-22 08:35:41', '2021-07-22 08:35:41', 16),
(7, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-22 08:36:42', '2021-07-22 08:36:42', 16),
(8, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-22 08:42:47', '2021-07-22 08:42:47', 16),
(9, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-22 08:43:07', '2021-07-22 08:43:07', 16),
(10, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-23 06:54:49', '2021-07-23 06:54:49', 16),
(11, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-26 10:57:59', '2021-07-26 10:57:59', 16),
(12, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-26 11:00:55', '2021-07-26 11:00:55', 16),
(13, 'Rencontres B2B: Enis Ayechi -> Igarashi Takeo', NULL, '2021-07-26 11:43:41', '2021-07-26 11:43:41', 16),
(14, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-26 11:46:03', '2021-07-26 11:46:03', 16),
(15, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-07-26 12:17:10', '2021-07-26 12:17:10', 16),
(16, 'Rencontres B2B: Enis Ayechi -> Igarashi Takeo', NULL, '2021-08-02 08:43:38', '2021-08-02 08:43:38', 16),
(17, 'Rencontres B2B: Enis Ayechi -> Igarashi Takeo', NULL, '2021-08-02 09:01:01', '2021-08-02 09:01:01', 16),
(18, 'Rencontres B2B: Enis Ayechi -> Igarashi Takeo', NULL, '2021-08-02 09:02:10', '2021-08-02 09:02:10', 16),
(19, 'Rencontres B2B: Enis Ayechi -> Igarashi Takeo', NULL, '2021-08-02 09:03:01', '2021-08-02 09:03:01', 16),
(20, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-08-02 09:03:56', '2021-08-02 09:03:56', 16),
(21, 'Rencontres B2B: Enis Ayechi -> Igarashi Takeo', NULL, '2021-08-09 08:53:56', '2021-08-09 08:53:56', 16),
(22, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-08-12 16:05:00', '2021-10-20 14:13:06', 16),
(23, 'ee', '2020-02-01', '2021-09-29 14:35:00', '2021-10-20 14:12:30', NULL),
(24, 'Rencontres B2B: Lab Xelero -> Aroua Habib', NULL, '2021-10-27 13:02:38', '2021-10-27 13:02:38', 16),
(25, 'Rencontres B2B: Enis Ayechi -> Aroua Habib', NULL, '2021-11-08 09:00:15', '2021-11-08 09:00:15', 16),
(26, 'Rencontres B2B: Lab Xelero -> Aroua Habib', NULL, '2021-11-15 12:50:54', '2021-11-15 12:50:54', 16),
(27, 'Rencontres B2B: Lab Xelero -> Enis Ayéchi', NULL, '2021-11-23 11:20:22', '2021-11-23 11:20:22', 16),
(28, 'Rencontres B2B: Lab Xelero -> Aymen Touhent', NULL, '2021-12-07 11:59:13', '2021-12-07 11:59:13', 27),
(29, 'Rencontres B2B: Lab Xelero -> Aroua Habib', NULL, '2021-12-07 12:05:27', '2021-12-07 12:05:27', 27),
(30, 'Rencontres B2B: Lab Xelero -> Aroua Habib', NULL, '2021-12-08 09:39:14', '2021-12-08 09:39:14', 27),
(31, 'Rencontres B2B: Xelero Lab -> Habib Aroua', NULL, '2022-01-05 08:39:03', '2022-01-05 08:39:03', 27),
(32, 'Rencontres B2B: Xelero Lab -> Habib Aroua', NULL, '2022-01-05 08:39:04', '2022-01-05 08:39:04', 27),
(33, 'Rencontres B2B: Xelero Lab -> Habib Aroua', NULL, '2022-01-05 08:39:06', '2022-01-05 08:39:06', 27),
(34, 'Rencontres B2B: Xelero Lab -> Habib Aroua', NULL, '2022-01-05 08:39:11', '2022-01-05 08:39:11', 27);

-- --------------------------------------------------------

--
-- Structure de la table `b2b_meet_users`
--

CREATE TABLE `b2b_meet_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `mobile_user_id` int(10) UNSIGNED DEFAULT NULL,
  `b2b_meet_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_action` timestamp NULL DEFAULT NULL,
  `confirmer` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date_deb_meet` time DEFAULT NULL,
  `date_fin_meet` time DEFAULT NULL,
  `refuser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `b2b_meet_users`
--

INSERT INTO `b2b_meet_users` (`id`, `mobile_user_id`, `b2b_meet_id`, `status`, `date_action`, `confirmer`, `created_at`, `updated_at`, `date_deb_meet`, `date_fin_meet`, `refuser`) VALUES
(1, 14, 1, 'sent', '2021-06-23 08:24:00', 0, '2021-06-23 06:23:00', '2021-06-23 06:24:13', '11:00:00', '11:30:00', 1),
(2, 13, 1, 'received', NULL, 0, '2021-06-23 08:25:00', '2021-06-23 06:24:44', '11:00:00', '11:30:00', 1),
(3, 12, 2, 'sent', '2021-06-23 06:40:32', 1, '2021-06-23 06:40:32', '2021-06-23 06:40:32', '14:00:00', '14:30:00', 0),
(4, 14, 2, 'received', '2021-06-23 06:40:32', 1, '2021-06-23 06:40:32', '2021-06-23 06:40:32', '14:00:00', '14:30:00', 0),
(5, 12, 3, 'sent', '2021-06-23 09:30:40', 0, '2021-06-23 09:30:40', '2021-06-23 09:30:40', '16:00:00', '16:30:00', 1),
(6, 13, 3, 'received', '2021-06-23 09:30:40', 0, '2021-06-23 09:30:40', '2021-06-23 09:30:40', '16:00:00', '16:30:00', 1),
(11, 13, 6, 'sent', '2021-07-22 08:35:41', 0, '2021-07-22 08:35:41', '2021-07-22 08:35:41', '09:30:00', '10:00:00', 0),
(12, 14, 6, 'received', '2021-07-22 08:35:41', 0, '2021-07-22 08:35:41', '2021-07-22 08:35:41', '09:30:00', '10:00:00', 0),
(13, 13, 7, 'sent', '2021-07-22 08:36:42', 0, '2021-07-22 08:36:42', '2021-07-22 08:36:42', '09:00:00', '09:30:00', 0),
(14, 14, 7, 'received', '2021-07-22 08:36:42', 0, '2021-07-22 08:36:42', '2021-07-22 08:36:42', '09:00:00', '09:30:00', 0),
(15, 13, 8, 'sent', '2021-07-22 08:42:47', 0, '2021-07-22 08:42:47', '2021-07-22 08:42:47', '09:00:00', '09:30:00', 0),
(16, 14, 8, 'received', '2021-07-22 08:42:47', 0, '2021-07-22 08:42:47', '2021-07-22 08:42:47', '09:00:00', '09:30:00', 0),
(17, 13, 9, 'sent', '2021-07-22 08:43:07', 0, '2021-07-22 08:43:07', '2021-07-22 08:43:07', '13:00:00', '13:30:00', 0),
(18, 14, 9, 'received', '2021-07-22 08:43:07', 0, '2021-07-22 08:43:07', '2021-07-22 08:43:07', '13:00:00', '13:30:00', 0),
(19, 13, 10, 'sent', '2021-07-23 06:54:49', 0, '2021-07-23 06:54:49', '2021-07-23 06:54:49', '09:30:00', '10:00:00', 0),
(20, 14, 10, 'received', '2021-07-23 06:54:49', 0, '2021-07-23 06:54:49', '2021-07-23 06:54:49', '09:30:00', '10:00:00', 0),
(21, 13, 11, 'sent', '2021-07-26 10:57:59', 0, '2021-07-26 10:57:59', '2021-07-26 10:57:59', '09:00:00', '09:30:00', 0),
(22, 14, 11, 'received', '2021-07-26 10:57:59', 0, '2021-07-26 10:57:59', '2021-07-26 10:57:59', '09:00:00', '09:30:00', 0),
(23, 13, 12, 'sent', '2021-07-26 11:00:55', 0, '2021-07-26 11:00:55', '2021-07-26 11:00:55', '09:00:00', '09:30:00', 0),
(24, 14, 12, 'received', '2021-07-26 11:00:55', 0, '2021-07-26 11:00:55', '2021-07-26 11:00:55', '09:00:00', '09:30:00', 0),
(25, 13, 13, 'sent', '2021-07-26 11:43:41', 0, '2021-07-26 11:43:41', '2021-07-26 11:43:41', '09:30:00', '10:00:00', 0),
(26, 12, 13, 'received', '2021-07-26 11:43:41', 0, '2021-07-26 11:43:41', '2021-07-26 11:43:41', '09:30:00', '10:00:00', 0),
(27, 13, 14, 'sent', '2021-07-26 11:46:03', 0, '2021-07-26 11:46:03', '2021-07-26 11:46:03', '16:30:00', '17:00:00', 0),
(28, 14, 14, 'received', '2021-07-26 11:46:03', 0, '2021-07-26 11:46:03', '2021-07-26 11:46:03', '16:30:00', '17:00:00', 0),
(29, 13, 15, 'sent', '2021-07-26 12:17:10', 0, '2021-07-26 12:17:10', '2021-07-26 12:17:10', '16:00:00', '16:30:00', 0),
(30, 14, 15, 'received', '2021-07-26 12:17:10', 0, '2021-07-26 12:17:10', '2021-07-26 12:17:10', '16:00:00', '16:30:00', 0),
(31, 13, 16, 'sent', '2021-08-02 08:43:38', 0, '2021-08-02 08:43:38', '2021-08-02 08:43:38', '09:00:00', '09:30:00', 0),
(32, 12, 16, 'received', '2021-08-02 08:43:38', 0, '2021-08-02 08:43:38', '2021-08-02 08:43:38', '09:00:00', '09:30:00', 0),
(33, 13, 17, 'sent', '2021-08-02 09:01:01', 0, '2021-08-02 09:01:01', '2021-08-02 09:01:01', '09:30:00', '10:00:00', 0),
(34, 12, 17, 'received', '2021-08-02 09:01:01', 0, '2021-08-02 09:01:01', '2021-08-02 09:01:01', '09:30:00', '10:00:00', 0),
(35, 13, 18, 'sent', '2021-08-02 09:02:10', 0, '2021-08-02 09:02:10', '2021-08-02 09:02:10', '10:00:00', '10:30:00', 0),
(36, 12, 18, 'received', '2021-08-02 09:02:10', 0, '2021-08-02 09:02:10', '2021-08-02 09:02:10', '10:00:00', '10:30:00', 0),
(37, 13, 19, 'sent', '2021-08-02 09:03:01', 0, '2021-08-02 09:03:01', '2021-08-02 09:03:01', '10:30:00', '11:00:00', 0),
(38, 12, 19, 'received', '2021-08-02 09:03:01', 0, '2021-08-02 09:03:01', '2021-08-02 09:03:01', '10:30:00', '11:00:00', 0),
(39, 13, 20, 'sent', '2021-08-02 09:03:56', 0, '2021-08-02 09:03:56', '2021-08-02 09:03:56', '13:30:00', '14:00:00', 0),
(40, 14, 20, 'received', '2021-08-02 09:03:56', 0, '2021-08-02 09:03:56', '2021-08-02 09:03:56', '13:30:00', '14:00:00', 0),
(41, 13, 21, 'sent', '2021-08-09 08:53:56', 0, '2021-08-09 08:53:56', '2021-08-09 08:53:56', '09:00:00', '09:30:00', 0),
(42, 12, 21, 'received', '2021-08-09 08:53:56', 0, '2021-08-09 08:53:56', '2021-08-09 08:53:56', '09:00:00', '09:30:00', 0),
(46, 38, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 38, 1, NULL, '2021-09-16 16:15:00', NULL, '2021-09-29 15:15:15', '2021-09-29 15:15:15', '10:00:00', '12:00:00', NULL),
(48, 41, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 38, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 42, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 52, 24, 'sent', '2021-10-27 13:02:38', 0, '2021-10-27 13:02:38', '2021-10-27 13:02:38', '09:00:00', '09:30:00', 0),
(52, 38, 24, 'received', '2021-10-27 13:02:38', 0, '2021-10-27 13:02:38', '2021-10-27 13:02:38', '09:00:00', '09:30:00', 0),
(53, 76, 25, 'sent', '2021-11-08 09:00:15', 0, '2021-11-08 09:00:15', '2021-11-08 09:00:15', '09:00:00', '09:30:00', 0),
(54, 38, 25, 'received', '2021-11-08 09:00:15', 0, '2021-11-08 09:00:15', '2021-11-08 09:00:15', '09:00:00', '09:30:00', 0),
(55, 77, 26, 'sent', '2021-11-15 12:50:54', 0, '2021-11-15 12:50:54', '2021-11-15 12:50:54', '09:00:00', '09:30:00', 0),
(56, 38, 26, 'received', '2021-11-15 12:50:54', 0, '2021-11-15 12:50:54', '2021-11-15 12:50:54', '09:00:00', '09:30:00', 0),
(57, 77, 27, 'sent', '2021-11-23 11:20:22', 0, '2021-11-23 11:20:22', '2021-11-23 11:20:22', '11:00:00', '11:30:00', 0),
(58, 79, 27, 'received', '2021-11-23 11:20:22', 0, '2021-11-23 11:20:22', '2021-11-23 11:20:22', '11:00:00', '11:30:00', 0),
(59, 77, 28, 'sent', '2021-12-07 11:59:13', 0, '2021-12-07 11:59:13', '2021-12-07 11:59:13', '10:00:00', '10:30:00', 0),
(60, 66, 28, 'received', '2021-12-07 11:59:13', 0, '2021-12-07 11:59:13', '2021-12-07 11:59:13', '10:00:00', '10:30:00', 0),
(61, 77, 29, 'sent', '2021-12-07 12:05:27', 0, '2021-12-07 12:05:27', '2021-12-07 12:05:27', '10:30:00', '11:00:00', 0),
(62, 38, 29, 'received', '2021-12-07 12:05:27', 0, '2021-12-07 12:05:27', '2021-12-07 12:05:27', '10:30:00', '11:00:00', 0),
(63, 77, 30, 'sent', '2021-12-08 09:39:14', 0, '2021-12-08 09:39:14', '2021-12-08 09:39:14', '10:30:00', '11:00:00', 0),
(64, 38, 30, 'received', '2021-12-08 09:39:14', 0, '2021-12-08 09:39:14', '2021-12-08 09:39:14', '10:30:00', '11:00:00', 0),
(65, 125, 31, 'sent', '2022-01-05 08:39:03', 0, '2022-01-05 08:39:03', '2022-01-05 08:39:03', '10:00:00', '10:30:00', 0),
(66, 128, 31, 'received', '2022-01-05 08:39:03', 0, '2022-01-05 08:39:03', '2022-01-05 08:39:03', '10:00:00', '10:30:00', 0),
(67, 125, 32, 'sent', '2022-01-05 08:39:04', 0, '2022-01-05 08:39:05', '2022-01-05 08:39:05', '10:30:00', '11:00:00', 0),
(68, 128, 32, 'received', '2022-01-05 08:39:04', 0, '2022-01-05 08:39:05', '2022-01-05 08:39:05', '10:30:00', '11:00:00', 0),
(69, 125, 33, 'sent', '2022-01-05 08:39:06', 0, '2022-01-05 08:39:11', '2022-01-05 08:39:11', '11:00:00', '11:30:00', 0),
(70, 125, 34, 'sent', '2022-01-05 08:39:11', 0, '2022-01-05 08:39:11', '2022-01-05 08:39:11', '12:30:00', '13:00:00', 0),
(71, 128, 34, 'received', '2022-01-05 08:39:11', 0, '2022-01-05 08:39:11', '2022-01-05 08:39:11', '12:30:00', '13:00:00', 0),
(72, 128, 33, 'received', '2022-01-05 08:39:06', 0, '2022-01-05 08:39:11', '2022-01-05 08:39:11', '11:00:00', '11:30:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `back_logs`
--

CREATE TABLE `back_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `back_logs`
--

INSERT INTO `back_logs` (`id`, `titre`, `action`, `tab`, `created_at`, `updated_at`) VALUES
(13, 'test trigger', 'delete', 'programmes', '2021-04-20 11:34:28', NULL),
(14, 'test trigger 10000', 'insert', 'programmes', '2021-04-20 11:34:41', NULL),
(15, 'test trigger 10000', 'update', 'programmes', '2021-04-20 11:34:51', NULL),
(16, NULL, 'insert', 'speakers', '2021-04-20 13:50:39', NULL),
(17, 'role 1', 'delete', 'programmes', '2021-04-21 12:39:47', NULL),
(18, 'test', 'insert', 'programmes', '2021-04-21 12:45:32', NULL),
(19, 'test prog 11', 'insert', 'programmes', '2021-04-23 09:22:31', NULL),
(20, 'test PROG1', 'insert', 'programmes', '2021-06-17 15:51:08', NULL),
(21, 'b2b meet PROGRAM', 'insert', 'programmes', '2021-06-23 06:22:38', NULL),
(22, 'b2b meet PROGRAM', 'update', 'programmes', '2021-06-30 08:22:50', NULL),
(23, 'test PROG1', 'update', 'programmes', '2021-06-30 08:23:39', NULL),
(24, 'test prog 11', 'update', 'programmes', '2021-06-30 08:24:24', NULL),
(25, 'test', 'update', 'programmes', '2021-06-30 08:37:25', NULL),
(26, 'Cocktail de bienvenue', 'update', 'programmes', '2021-06-30 08:38:10', NULL),
(27, 'b2b meet PROGRAM', 'update', 'programmes', '2021-06-30 08:39:27', NULL),
(28, NULL, 'insert', 'speakers', '2021-06-30 08:39:27', NULL),
(29, 'test PROG1', 'update', 'programmes', '2021-06-30 08:39:43', NULL),
(30, NULL, 'insert', 'speakers', '2021-06-30 08:39:43', NULL),
(31, 'b2b meet PROGRAM', 'update', 'programmes', '2021-06-30 08:41:48', NULL),
(32, NULL, 'insert', 'speakers', '2021-06-30 08:41:48', NULL),
(33, 'Heures d’ouverture du Salon SITIC  AFRICA ABIDJAN 2021', 'insert', 'programmes', '2021-06-30 11:43:05', NULL),
(34, NULL, 'insert', 'speakers', '2021-06-30 11:43:05', NULL),
(35, NULL, 'insert', 'speakers', '2021-06-30 11:43:05', NULL),
(36, 'test PROG1', 'update', 'programmes', '2021-07-01 09:18:25', NULL),
(37, 'test PROG1', 'update', 'programmes', '2021-07-01 09:33:28', NULL),
(38, 'b2b meet PROGRAM', 'update', 'programmes', '2021-07-16 14:08:17', NULL),
(39, 'Heures d’ouverture du Salon SITIC  AFRICA ABIDJAN 2021', 'update', 'programmes', '2021-07-16 14:24:28', NULL),
(40, 'b2b meet PROGRAM', 'update', 'programmes', '2021-07-16 14:25:24', NULL),
(41, 'Cocktail de bienvenue', 'update', 'programmes', '2021-07-16 14:28:33', NULL),
(42, 'Cocktail de bienvenue', 'delete', 'programmes', '2021-10-20 11:27:47', NULL),
(43, 'test trigger 10000', 'delete', 'programmes', '2021-10-20 11:27:47', NULL),
(44, 'test', 'delete', 'programmes', '2021-10-20 11:27:47', NULL),
(45, 'test prog 11', 'delete', 'programmes', '2021-11-24 10:15:23', NULL),
(46, 'test PROG1', 'delete', 'programmes', '2021-11-24 10:15:23', NULL),
(47, 'b2b meet PROGRAM', 'delete', 'programmes', '2021-11-24 10:15:23', NULL),
(48, 'Heures d’ouverture du Salon SITIC  AFRICA ABIDJAN 2021', 'delete', 'programmes', '2021-11-24 10:15:23', NULL),
(49, 'Arrivée des Délégations Etrangères', 'insert', 'programmes', '2021-11-24 10:16:25', NULL),
(50, 'Heures d’ouverture du Salon SITIC AFRICA ABIDJAN 2022', 'insert', 'programmes', '2021-11-24 10:17:19', NULL),
(51, 'Arrivée des Délégations Etrangères', 'update', 'programmes', '2021-11-24 10:17:31', NULL),
(52, 'Inauguration du SITIC AFRICA ABIDJAN 2022', 'insert', 'programmes', '2021-11-24 10:18:24', NULL),
(53, 'Inauguration du SITIC AFRICA ABIDJAN 2022', 'update', 'programmes', '2021-11-24 10:19:27', NULL),
(54, 'Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'insert', 'programmes', '2021-11-24 10:20:13', NULL),
(55, 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'insert', 'programmes', '2021-11-24 10:20:58', NULL),
(56, 'FORUM INTERNATIONAL INDUSTRIE 4.0', 'insert', 'programmes', '2021-11-24 10:21:41', NULL),
(57, 'Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'insert', 'programmes', '2021-11-24 10:22:12', NULL),
(58, 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'insert', 'programmes', '2021-11-24 10:22:59', NULL),
(59, 'Heures d’ouverture du Salon SITIC AFRICA ABIDJAN 2022', 'delete', 'programmes', '2021-11-24 10:23:08', NULL),
(60, 'FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'insert', 'programmes', '2021-11-24 10:23:48', NULL),
(61, 'Rencontres B2B', 'insert', 'programmes', '2021-11-24 10:24:28', NULL),
(62, 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'insert', 'programmes', '2021-11-24 10:25:04', NULL),
(63, 'Poursuite des Rencontres B2B', 'insert', 'programmes', '2021-11-24 10:25:37', NULL),
(64, 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'update', 'programmes', '2021-11-24 10:25:56', NULL),
(65, 'FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'insert', 'programmes', '2021-11-24 10:29:34', NULL),
(66, 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'update', 'programmes', '2021-11-24 10:29:50', NULL),
(67, 'FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'update', 'programmes', '2021-11-24 10:30:02', NULL),
(68, 'FORUM INTERNATIONAL E-AGRICULTURE', 'insert', 'programmes', '2021-11-24 10:30:37', NULL),
(69, 'Poursuite et fin des rencontres B2B', 'insert', 'programmes', '2021-11-24 10:31:19', NULL),
(70, 'Clôture du salon', 'insert', 'programmes', '2021-11-24 10:32:19', NULL),
(71, 'Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'update', 'programmes', '2021-11-24 10:52:11', NULL),
(72, 'Inauguration du SITIC AFRICA ABIDJAN 2022', 'update', 'programmes', '2021-11-24 10:52:38', NULL),
(73, 'Clôture du salon', 'update', 'programmes', '2021-11-24 10:52:55', NULL),
(74, 'POST-COVID: L’ innovation Made in Africa', 'update', 'programmes', '2021-11-24 10:54:27', NULL),
(75, 'POST-COVID: L’ innovation Made in Africa', 'update', 'programmes', '2021-11-24 10:59:26', NULL),
(76, 'Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'update', 'programmes', '2021-12-07 09:34:02', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `boardings`
--

CREATE TABLE `boardings` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `boardings`
--

INSERT INTO `boardings` (`id`, `titre`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'boardings\\April2021\\9uj5AMxtTKEjOu5GqbA7.jpg', '2021-04-19 12:15:00', '2021-04-19 12:15:25');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
(2, NULL, 1, 'Category 2', 'category-2', '2021-03-31 11:37:12', '2021-03-31 11:37:12');

-- --------------------------------------------------------

--
-- Structure de la table `certifications`
--

CREATE TABLE `certifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `certification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discipline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organisation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `certifications`
--

INSERT INTO `certifications` (`id`, `certification`, `discipline`, `organisation`, `date`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'certification 1', NULL, NULL, NULL, '2021-04-08 11:34:35', '2021-04-08 11:34:35', NULL),
(2, 'certification 2', NULL, NULL, NULL, '2021-04-08 11:34:40', '2021-04-08 11:34:40', NULL),
(3, 'Cet1', 'Disp', 'Orgg', '2019-04-13', '2021-04-13 10:42:36', '2021-04-13 10:42:36', 4),
(4, NULL, NULL, NULL, '2021-04-13', '2021-04-13 10:44:11', '2021-04-13 10:44:11', 4),
(5, 'Cert1', 'Disp1', 'Org1', '2018-04-13', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5),
(6, 'Cert2', 'Disp2', 'Orgi', '2019-04-13', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5),
(7, NULL, NULL, NULL, '2021-04-13', '2021-04-13 12:06:05', '2021-04-13 12:06:05', 5),
(8, NULL, NULL, NULL, '2021-04-13', '2021-04-13 12:19:17', '2021-04-13 12:19:17', 5),
(9, 'Certif', 'Angular', 'IT SIDI DAOUED', '2018-01-07', '2021-04-16 09:41:37', '2021-04-16 09:41:37', 10),
(10, 'CCNA', 'CISCO', 'CISCO', '2020-08-01', '2021-06-01 07:47:49', '2021-06-01 07:47:56', 10),
(11, 'CCNA', 'RESEAUX', 'CISCO', '2020-06-04', '2021-06-08 08:38:56', '2021-06-08 08:38:56', 14),
(12, 'CCNA', 'RES', 'CISCO', '2020-06-08', '2021-06-08 10:18:15', '2021-06-08 10:18:15', 13),
(13, 'CCNA', 'DIS', 'Org', '2020-08-04', '2021-08-04 10:43:17', '2021-08-04 10:43:17', 20),
(14, 'Cert1', 'Disp1', 'Org1', '2019-08-12', '2021-08-12 15:52:59', '2021-08-12 15:52:59', 21),
(15, 'Cert1', 'Disp1', 'Org1', '2017-09-28', '2021-09-28 11:33:42', '2021-09-28 11:33:43', 32),
(16, 'Ccna', 'Cisco', 'Cisco', '2021-09-28', '2021-09-28 11:47:46', '2021-09-28 11:47:47', 33),
(17, 'cerf1', 'disp1', 'org1', '2019-09-28', '2021-09-28 16:35:43', '2021-09-28 16:35:43', 35),
(18, 'Java Oracle', 'Niveau basic', 'Oracle', '2019-09-29', '2021-09-29 13:54:40', '2021-09-29 13:54:40', 38),
(19, 'Ik', 'Ik', 'Ik', '2021-10-15', '2021-10-11 14:06:41', '2021-10-11 14:06:42', 39),
(20, 'ccna', 'cisco', 'cisco', '2020-10-20', '2021-10-20 11:18:46', '2021-10-20 11:18:46', 42),
(21, 'Cert1', 'Dispa', 'Org1', '2021-10-08', '2021-10-21 08:52:30', '2021-10-21 08:52:30', 43),
(22, 'Cert1', 'Disp1', 'Orig', '2020-10-22', '2021-10-22 10:17:08', '2021-10-22 10:17:08', 45),
(23, 'Cert1', 'Disp1', 'Org1', '2021-10-18', '2021-10-22 15:15:04', '2021-10-22 15:15:04', 46),
(24, 'Cert1', 'Disp1', 'Org1', '2019-10-27', '2021-10-27 11:02:08', '2021-10-27 11:02:08', 47),
(25, 'Cert1', 'Disp1', 'Org1', '2018-10-27', '2021-10-27 11:10:27', '2021-10-27 11:10:27', 48),
(26, 'Cert1', 'Disp1', 'Org1', '2019-10-27', '2021-10-27 11:32:28', '2021-10-27 11:32:28', 49),
(27, 'Cert1', 'Disp1', 'Org1', '2020-10-27', '2021-10-27 12:10:11', '2021-10-27 12:10:11', 50),
(28, 'cert1', 'disp', 'org', '2018-10-27', '2021-10-27 12:18:02', '2021-10-27 12:18:02', 51),
(29, 'Cert', 'Disp', 'Org', '2020-10-27', '2021-10-27 13:01:08', '2021-10-27 13:01:08', 52),
(30, 'Java', 'Oracle', 'Oracle', '2021-11-12', '2021-11-01 10:10:12', '2021-11-01 10:10:12', 53),
(31, 'Java', 'Oca', 'Oracle', '2021-11-23', '2021-11-01 10:52:36', '2021-11-01 10:52:36', 55),
(32, 'Java', 'Oracle', 'Oracle', '2021-11-10', '2021-11-01 11:22:26', '2021-11-01 11:22:26', 59),
(33, 'Java', 'Oca', 'Oracle', '2021-11-24', '2021-11-01 12:33:22', '2021-11-01 12:33:22', 60),
(34, 'Java', 'Oca', 'Oracle', '2021-11-11', '2021-11-01 12:43:28', '2021-11-01 12:43:28', 62),
(35, 'Java', 'Oca', 'Oracle', '2021-11-17', '2021-11-01 12:48:31', '2021-11-01 12:48:31', 63),
(36, 'C++', 'C+×', 'Microsoft', '2021-11-24', '2021-11-02 09:00:45', '2021-11-02 09:00:45', 38),
(37, 'C××', 'C++', 'Microsoft', '2021-11-17', '2021-11-02 09:24:25', '2021-11-02 09:24:25', 65),
(38, 'Cisco', 'Hello', 'Hello', '2020-10-01', '2021-11-03 09:35:54', '2021-11-03 09:35:54', 66),
(39, 'Java', 'Oca', 'Oracle', '2021-11-25', '2021-11-03 10:06:41', '2021-11-03 10:06:41', 38),
(40, 'Des', 'Des', 'Desinet', '2021-11-17', '2021-11-03 11:05:50', '2021-11-03 11:05:50', 69),
(41, 'Cert', 'Disp', 'Org', '2019-11-04', '2021-11-04 08:35:48', '2021-11-04 08:35:48', 70),
(42, 'Cert', 'Dip', 'Org', '2019-11-04', '2021-11-04 09:33:38', '2021-11-04 09:33:39', 71),
(43, 'cert', 'disp', 'org', '2020-11-04', '2021-11-04 09:43:48', '2021-11-04 09:43:48', 67),
(44, 'cert', 'disp', 'orgh', '2019-11-04', '2021-11-04 09:54:06', '2021-11-04 09:54:06', 67),
(45, 'cert', 'fisp', 'org', '2021-11-04', '2021-11-04 09:59:06', '2021-11-04 09:59:06', 67),
(46, 'cert', 'disp', 'org', '2021-11-04', '2021-11-04 11:13:03', '2021-11-04 11:13:03', 75),
(47, 'cert', 'disp', 'org', '2019-11-04', '2021-11-04 11:17:33', '2021-11-04 11:17:33', 75),
(48, 'CCNA', 'Cisco', 'Cisco', '2021-08-08', '2021-11-08 08:58:00', '2021-11-08 08:58:00', 76),
(49, 'Cert', 'Disp', 'Org', '2021-11-10', '2021-11-08 09:03:47', '2021-11-08 09:03:47', 77),
(50, 'CCNA', 'Cisco', 'Cisco', '2020-06-01', '2021-11-10 09:22:09', '2021-11-10 09:22:09', 79),
(51, 'Marketing', 'Marketing digitale', 'Sesame', '2016-11-24', '2021-11-24 13:04:08', '2021-11-24 13:04:08', 80),
(52, 'Java OCA', 'Java', 'Oracle', '2020-11-24', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 81),
(53, 'C2i', 'C2i', 'Université virtuelle', '2016-11-18', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 81),
(54, 'Organisateur des événements', 'Commissaire', 'SITIC AFRICA', '2016-06-01', '2021-12-06 09:16:51', '2021-12-06 09:16:51', 84),
(55, 'Vvv', 'Ffg', 'Fgnmm', '2016-12-08', '2021-12-08 18:32:49', '2021-12-08 18:32:49', 85),
(56, 'cert1', 'disp', 'org', '2021-12-16', '2021-12-16 12:36:56', '2021-12-16 12:36:56', 90),
(57, 'cerrt', 'dispp', 'origg', '2018-12-21', '2021-12-21 15:20:56', '2021-12-21 15:20:56', 95),
(58, 'Aucune', 'Aucune', 'Aucune', '2021-12-22', '2021-12-22 17:10:07', '2021-12-22 17:10:08', 97),
(59, 'Sdfrddd', 'Cfgg', 'Dfhh', '2021-06-22', '2021-12-22 22:45:45', '2021-12-22 22:45:46', 91),
(60, 'Pas de certif', 'Pas de decipline', 'Pas d’organisation', '2012-12-23', '2021-12-23 00:42:28', '2021-12-23 00:42:28', 99),
(61, 'Ok', 'Ok', 'Ok', '2021-12-23', '2021-12-23 09:40:36', '2021-12-23 09:40:37', 100),
(62, 'CCNA', 'Cisco', 'Cisco', '2019-12-23', '2021-12-23 11:43:38', '2021-12-23 11:43:38', 101),
(63, NULL, NULL, NULL, '2021-12-23', '2021-12-23 13:38:30', '2021-12-23 13:38:31', 104),
(64, 'Hata chay', 'Marche khodhra', 'Ma3joun', '2017-12-23', '2021-12-23 13:54:43', '2021-12-23 13:54:44', 105),
(65, NULL, NULL, NULL, '2021-12-23', '2021-12-23 14:03:45', '2021-12-23 14:03:45', 108),
(66, NULL, NULL, NULL, '2021-12-23', '2021-12-23 14:29:19', '2021-12-23 14:29:19', 109),
(67, NULL, NULL, NULL, '2021-12-23', '2021-12-23 14:32:54', '2021-12-23 14:32:54', 102),
(68, NULL, NULL, NULL, '2021-12-23', '2021-12-23 14:33:32', '2021-12-23 14:33:32', 102),
(69, NULL, NULL, NULL, '2021-12-23', '2021-12-23 14:38:32', '2021-12-23 14:38:32', 102),
(70, 'Certification', 'Discipline', 'Organisation', '2021-01-01', '2021-12-23 14:39:15', '2021-12-23 14:39:15', 110),
(71, NULL, NULL, NULL, '2021-12-24', '2021-12-24 07:19:25', '2021-12-24 07:19:25', 111),
(72, NULL, NULL, NULL, '2021-12-24', '2021-12-24 08:17:43', '2021-12-24 08:17:44', 112),
(73, NULL, NULL, NULL, '2021-12-26', '2021-12-26 15:58:45', '2021-12-26 15:58:46', 115),
(75, NULL, NULL, NULL, '2022-01-03', '2022-01-03 09:32:35', '2022-01-03 09:32:36', 126),
(76, NULL, NULL, NULL, '2022-01-03', '2022-01-03 15:03:03', '2022-01-03 15:03:03', 98),
(77, NULL, NULL, NULL, '2022-01-04', '2022-01-04 13:19:57', '2022-01-04 13:19:57', 129),
(78, NULL, NULL, NULL, '2022-01-05', '2022-01-05 11:01:47', '2022-01-05 11:01:47', 128),
(79, NULL, NULL, NULL, '2022-01-05', '2022-01-05 13:34:16', '2022-01-05 13:34:16', 130),
(80, NULL, NULL, NULL, '2022-01-05', '2022-01-05 16:27:54', '2022-01-05 16:27:54', 131),
(81, NULL, NULL, NULL, '2022-01-05', '2022-01-05 16:33:56', '2022-01-05 16:33:56', 134),
(82, NULL, NULL, NULL, '2022-01-05', '2022-01-05 19:37:08', '2022-01-05 19:37:08', 135),
(83, 'cert', 'disp', 'org', '2022-11-11', '2022-01-11 14:52:58', '2022-01-11 14:52:58', 125),
(84, NULL, NULL, NULL, '2022-01-11', '2022-01-11 15:03:45', '2022-01-11 15:03:45', 140);

-- --------------------------------------------------------

--
-- Structure de la table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `users` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'certification', 'text', 'Certification', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 2),
(58, 7, 'discipline', 'text', 'Discipline', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 3),
(59, 7, 'organisation', 'text', 'Organisation', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 4),
(60, 7, 'date', 'date', 'Date', 0, 1, 1, 1, 1, 1, '{}', 5),
(61, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(62, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(63, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(64, 8, 'role', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 2),
(65, 8, 'entreprise', 'text', 'Entreprise', 0, 1, 1, 1, 1, 1, '{}', 3),
(66, 8, 'date_debut', 'date', 'Date Debut', 0, 1, 1, 1, 1, 1, '{}', 4),
(67, 8, 'date_fin', 'date', 'Date Fin', 0, 1, 1, 1, 1, 1, '{}', 5),
(68, 8, 'actuellement', 'text', 'Actuellement', 0, 1, 1, 1, 1, 1, '{}', 6),
(69, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(70, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(71, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(72, 9, 'ecole', 'text', 'Ecole', 0, 1, 1, 1, 1, 1, '{}', 2),
(74, 9, 'date_debut', 'date', 'Date Debut', 0, 1, 1, 1, 1, 1, '{}', 4),
(75, 9, 'date_fin', 'date', 'Date Fin', 0, 1, 1, 1, 1, 1, '{}', 5),
(76, 9, 'diplome', 'text', 'Diplome', 0, 1, 1, 1, 1, 1, '{}', 6),
(77, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(78, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(79, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(80, 10, 'nom', 'text', 'Nom', 0, 1, 1, 1, 1, 1, '{}', 3),
(81, 10, 'prenom', 'text', 'Prenom', 0, 1, 1, 1, 1, 1, '{}', 4),
(82, 10, 'date_de_naissance', 'date', 'Date De Naissance', 0, 1, 1, 1, 1, 1, '{}', 6),
(83, 10, 'pays_de_residence', 'text', 'Pays De Residence', 0, 1, 1, 1, 1, 1, '{}', 7),
(84, 10, 'telephone', 'text', 'Telephone', 0, 1, 1, 1, 1, 1, '{}', 8),
(85, 10, 'sexe', 'text', 'Sexe', 0, 1, 1, 1, 1, 1, '{}', 9),
(86, 10, 'entreprise', 'text', 'Entreprise', 0, 1, 1, 1, 1, 1, '{}', 10),
(87, 10, 'site_web', 'text', 'Site Web', 0, 1, 1, 1, 1, 1, '{}', 11),
(88, 10, 'localite_entreprise', 'text', 'Localite Entreprise', 0, 1, 1, 1, 1, 1, '{}', 12),
(89, 10, 'domaine_activite_p', 'text', 'Domaine Activite P', 0, 1, 1, 1, 1, 1, '{}', 13),
(90, 10, 'domaine_activite_s', 'text', 'Domaine Activite S', 0, 0, 0, 1, 1, 1, '{}', 14),
(91, 10, 'role', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 15),
(92, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 1, 0, 1, '{}', 16),
(93, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 17),
(94, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(95, 11, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(96, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(97, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(98, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(99, 12, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(100, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(101, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(102, 10, 'mobile_user_belongstomany_objectif_relationship', 'relationship', 'objectifs', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Objectif\",\"table\":\"objectifs\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"users_objectifs\",\"pivot\":\"1\",\"taggable\":\"0\"}', 18),
(103, 10, 'mobile_user_belongstomany_interet_relationship', 'relationship', 'interets', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Interet\",\"table\":\"interets\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"users_interets\",\"pivot\":\"1\",\"taggable\":\"0\"}', 20),
(104, 10, 'mobile_user_belongstomany_formation_relationship', 'relationship', 'formations', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Formation\",\"table\":\"formations\",\"type\":\"hasMany\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"ecole\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":\"0\"}', 22),
(105, 10, 'mobile_user_belongstomany_certification_relationship', 'relationship', 'certifications', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Certification\",\"table\":\"certifications\",\"type\":\"hasMany\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"certification\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":\"0\"}', 24),
(106, 10, 'mobile_user_belongstomany_experience_relationship', 'relationship', 'experiences', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Experience\",\"table\":\"experiences\",\"type\":\"hasMany\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"entreprise\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":\"0\"}', 26),
(107, 10, 'objectifs', 'text', 'Objectifs', 0, 0, 0, 0, 0, 0, '{}', 19),
(108, 10, 'interets', 'text', 'Interets', 0, 0, 0, 0, 0, 0, '{}', 21),
(109, 10, 'formations', 'text', 'Formations', 0, 0, 0, 0, 0, 0, '{}', 23),
(110, 10, 'experiences', 'text', 'Experiences', 0, 0, 0, 0, 0, 0, '{}', 25),
(111, 10, 'certifications', 'text', 'Certifications', 0, 0, 0, 0, 0, 0, '{}', 27),
(112, 10, 'token_device', 'text', 'Token Device', 0, 0, 0, 0, 0, 0, '{}', 28),
(113, 10, 'token_api', 'text', 'Token Api', 0, 0, 0, 0, 0, 0, '{}', 29),
(114, 10, 'type_auth_api', 'text', 'Type Auth Api', 0, 1, 1, 1, 1, 1, '{}', 30),
(115, 10, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 2),
(116, 10, 'mot_de_passe', 'text', 'Mot De Passe', 0, 0, 0, 0, 0, 0, '{}', 31),
(117, 7, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 8),
(118, 8, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 9),
(119, 9, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 9),
(120, 11, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Active\",\"off\":\"Inactive\",\"checked\":\"true\"}', 5),
(121, 12, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Active\",\"off\":\"Inactive\",\"checked\":\"true\"}', 5),
(122, 7, 'certification_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MobileUser\",\"table\":\"mobile_users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(123, 8, 'experience_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MobileUser\",\"table\":\"mobile_users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(124, 8, 'site_web', 'text', 'Site Web', 0, 1, 1, 1, 1, 1, '{}', 10),
(125, 9, 'formation_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MobileUser\",\"table\":\"mobile_users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(126, 9, 'domaine', 'text', 'Domaine', 0, 1, 1, 1, 1, 1, '{}', 6),
(127, 9, 'grade', 'text', 'Grade', 0, 1, 1, 1, 1, 1, '{}', 10),
(128, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(129, 13, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 3),
(131, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 2),
(132, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(143, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(144, 15, 'nom', 'text', 'Nom', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 2),
(145, 15, 'poste', 'text', 'Poste', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 3),
(146, 15, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 4),
(147, 15, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(148, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(149, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(150, 16, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(151, 16, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(152, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(154, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(155, 17, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(156, 17, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(159, 17, 'type_id', 'text', 'Type Id', 0, 1, 1, 1, 1, 1, '{}', 7),
(160, 17, 'event_id', 'text', 'Event Id', 0, 1, 1, 1, 1, 1, '{}', 8),
(161, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(162, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(163, 17, 'programme_belongsto_evenement_relationship', 'relationship', 'evenements', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Evenement\",\"table\":\"evenements\",\"type\":\"belongsTo\",\"column\":\"event_id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(164, 17, 'programme_belongsto_type_prog_relationship', 'relationship', 'type_progs', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\TypeProg\",\"table\":\"type_progs\",\"type\":\"belongsTo\",\"column\":\"type_id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(165, 17, 'programme_hasmany_speaker_relationship', 'relationship', 'speakers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Speaker\",\"table\":\"speakers\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"nom\",\"pivot_table\":\"speakers_progs\",\"pivot\":\"1\",\"taggable\":\"0\"}', 14),
(166, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(167, 18, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(168, 18, 'date', 'date', 'Date', 0, 1, 1, 1, 1, 1, '{}', 3),
(169, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(170, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(171, 18, 'b2b_meet_belongstomany_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MobileUser\",\"table\":\"mobile_users\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"b2b_meet_users\",\"pivot\":\"1\",\"taggable\":\"0\"}', 6),
(172, 18, 'b2b_meet_belongsto_programme_relationship', 'relationship', 'programmes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Programme\",\"table\":\"programmes\",\"type\":\"belongsTo\",\"column\":\"prog_id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"b2b_meet_users\",\"pivot\":\"0\",\"taggable\":null}', 7),
(173, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(174, 19, 'mobile_user_id', 'text', 'Mobile User Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(175, 19, 'b2b_meet_id', 'text', 'B2b Meet Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(176, 19, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 6),
(180, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(181, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(182, 19, 'date_deb_meet', 'time', 'Date Deb Meet', 0, 1, 1, 1, 1, 1, '{}', 12),
(183, 19, 'date_fin_meet', 'time', 'Date Fin Meet', 0, 1, 1, 1, 1, 1, '{}', 13),
(184, 19, 'b2b_meet_user_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MobileUser\",\"table\":\"mobile_users\",\"type\":\"belongsTo\",\"column\":\"mobile_user_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"b2b_meet_users\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(185, 19, 'b2b_meet_user_belongsto_b2b_meet_relationship', 'relationship', 'b2b_meets', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\B2bMeet\",\"table\":\"b2b_meets\",\"type\":\"belongsTo\",\"column\":\"b2b_meet_id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"b2b_meet_users\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(186, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(187, 20, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 2),
(188, 20, 'type', 'select_dropdown', 'Type', 0, 1, 1, 1, 1, 1, '{\"default\":\"Principale\",\"options\":{\"Principale\":\"Principale\",\"Secondaire\":\"Secondaire\"}}', 3),
(189, 20, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(190, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(191, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(192, 21, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(193, 21, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(194, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(195, 20, 'status', 'checkbox', 'Statu', 0, 1, 1, 1, 1, 1, '{\"on\":\"Active\",\"off\":\"Inactive\",\"checked\":\"true\"}', 6),
(196, 21, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Active\",\"off\":\"Inactive\",\"checked\":\"true\"}', 5),
(197, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(198, 22, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(199, 22, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(200, 22, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(201, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(202, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(203, 23, 'mobile_user_id', 'text', 'Mobile User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(204, 23, 'objectif_id', 'text', 'Objectif Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(205, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(206, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(207, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(208, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(209, 24, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 2),
(210, 24, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 3),
(211, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(212, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(213, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(214, 25, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 2),
(215, 25, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 3),
(216, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(217, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(218, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(219, 26, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(220, 26, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(221, 26, 'sous_desc', 'text_area', 'Sous Desc', 0, 1, 1, 1, 1, 1, '{}', 4),
(222, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(223, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(224, 13, 'location_event', 'text', 'Location Event', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 8),
(225, 13, 'image_event', 'image', 'Image Event', 0, 1, 1, 1, 1, 1, '{}', 9),
(226, 13, 'longitude', 'text', 'Longitude', 0, 1, 1, 1, 1, 1, '{}', 11),
(227, 13, 'latitude', 'text', 'Latitude', 0, 1, 1, 1, 1, 1, '{}', 12),
(229, 19, 'date_action', 'timestamp', 'Date Action', 0, 1, 1, 1, 1, 1, '{}', 5),
(230, 19, 'confirmer', 'text', 'Confirmer', 0, 1, 1, 1, 1, 1, '{}', 6),
(231, 19, 'refuser', 'text', 'Refuser', 0, 1, 1, 1, 1, 1, '{}', 11),
(232, 17, 'location_programme', 'text', 'Location Programme', 0, 1, 1, 1, 1, 1, '{}', 12),
(233, 17, 'date_debut_pause', 'time', 'Date Debut Pause', 0, 1, 1, 1, 1, 1, '{}', 15),
(234, 17, 'date_fin_pause', 'time', 'Date Fin Pause', 0, 1, 1, 1, 1, 1, '{}', 16),
(235, 13, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(236, 13, 'icon_event', 'image', 'Icon Event', 0, 1, 1, 1, 1, 1, '{}', 10),
(237, 17, 'heure_debut', 'time', 'Heure Debut', 0, 1, 1, 1, 1, 1, '{}', 5),
(238, 17, 'heure_fin', 'time', 'Heure Fin', 0, 1, 1, 1, 1, 1, '{}', 6),
(239, 17, 'date_prog', 'date', 'Date Prog', 0, 1, 1, 1, 1, 1, '{}', 4),
(240, 13, 'date_debut', 'date', 'Date Debut', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 4),
(241, 13, 'date_fin', 'date', 'Date Fin', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"]}}', 5),
(242, 13, 'evenement_belongstomany_organisateur_relationship', 'relationship', 'organisateurs', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Organisateur\",\"table\":\"organisateurs\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"orgs_events\",\"pivot\":\"1\",\"taggable\":\"0\"}', 13),
(243, 13, 'evenement_belongstomany_partenaire_relationship', 'relationship', 'partenaires', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Partenaire\",\"table\":\"partenaires\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"partenaires_events\",\"pivot\":\"1\",\"taggable\":\"0\"}', 14),
(244, 10, 'photo_de_profile', 'image', 'Photo De Profile', 0, 1, 1, 0, 0, 1, '{}', 5),
(245, 24, 'second_titre', 'text', 'Second Titre', 0, 1, 1, 1, 1, 1, '{}', 6),
(246, 25, 'second_titre', 'text', 'Second Titre', 0, 1, 1, 1, 1, 1, '{}', 6),
(247, 10, 'mobile_user_belongsto_activite_relationship', 'relationship', 'Domaine d\'activité principale', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Activite\",\"table\":\"activites\",\"type\":\"belongsTo\",\"column\":\"domaine_activite_p\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":\"0\"}', 32),
(248, 10, 'mobile_user_belongsto_activite_relationship_1', 'relationship', 'activites', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Activite\",\"table\":\"activites\",\"type\":\"belongsTo\",\"column\":\"domaine_activite_s\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":\"0\"}', 33),
(249, 10, 'mobile_user_belongsto_wizard_role_relationship', 'relationship', 'wizard_roles', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\WizardRole\",\"table\":\"wizard_roles\",\"type\":\"belongsTo\",\"column\":\"role\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":\"0\"}', 34),
(250, 13, 'image_couverture', 'image', 'Image Couverture', 0, 1, 1, 1, 1, 1, '{}', 13),
(251, 27, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(252, 27, 'id_user1', 'number', 'Id User1', 0, 1, 1, 1, 1, 1, '{}', 2),
(253, 27, 'id_user2', 'number', 'Id User2', 0, 1, 1, 1, 1, 1, '{}', 3),
(254, 27, 'action', 'text', 'Action', 0, 1, 1, 1, 1, 1, '{}', 4),
(255, 27, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(256, 27, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(258, 27, 'react_belongsto_mobile_user_relationship', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MobileUser\",\"table\":\"mobile_users\",\"type\":\"belongsTo\",\"column\":\"id_user1\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":null}', 7),
(259, 27, 'react_belongsto_mobile_user_relationship_1', 'relationship', 'mobile_users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MobileUser\",\"table\":\"mobile_users\",\"type\":\"belongsTo\",\"column\":\"id_user2\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":null}', 8),
(260, 10, 'access_b2b', 'checkbox', 'Access B2b', 0, 1, 1, 1, 1, 1, '{}', 27),
(261, 29, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(262, 29, 'numero_du_stand', 'text', 'Numero Du Stand', 0, 1, 1, 1, 1, 1, '{}', 2),
(263, 29, 'entreprise', 'text', 'Entreprise', 0, 1, 1, 1, 1, 1, '{}', 3),
(264, 29, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(265, 29, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(266, 30, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(267, 30, 'type', 'text', 'Type', 0, 1, 1, 1, 1, 1, '{}', 2),
(268, 30, 'photo', 'image', 'Photo', 0, 1, 1, 1, 1, 1, '{}', 3),
(269, 30, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(270, 30, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(271, 31, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(272, 31, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{}', 2),
(273, 31, 'type', 'text', 'Type', 0, 1, 1, 1, 1, 1, '{}', 3),
(274, 31, 'token_device', 'text', 'Token Device', 0, 1, 1, 1, 1, 1, '{}', 4),
(275, 31, 'id_user', 'text', 'Id User', 0, 1, 1, 1, 1, 1, '{}', 5),
(276, 31, 'id_prog', 'text', 'Id Prog', 0, 1, 1, 1, 1, 1, '{}', 6),
(277, 31, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(278, 31, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(279, 32, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(280, 32, 'titre', 'text', 'Titre', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(281, 32, 'valeur', 'text', 'Valeur', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(282, 32, 'pays', 'select_dropdown', 'Pays', 0, 1, 1, 1, 1, 1, '{\"\":\"\",\"options\":{\"Tous\":\"Tous\",\"Afrique du Sud\":\"Afrique du Sud\",\"Albanie\":\"Albanie\",\"Alg\\u00e9rie\":\"Alg\\u00e9rie\",\"Allemagne\":\"Allemagne\",\"Andorre\":\"Andorre\",\"Angola\":\"Angola\",\"Antigua-et-Barbuda\":\"Antigua-et-Barbuda\",\"Arabie saoudite\":\"Arabie saoudite\",\"Argentine\":\"Argentine\",\"Arm\\u00e9nie\":\"Arm\\u00e9nie\",\"Australie\":\"Australie\",\"Autriche \":\"Autriche\",\"Azerba\\u00efdjan\":\"Azerba\\u00efdjan\",\"Bahamas\":\"Bahamas\",\"Bahre\\u00efn\":\"Bahre\\u00efn\",\"Bangladesh\":\"Bangladesh\",\"Barbade\":\"Barbade\",\"Belgique\":\"Belgique\",\"B\\u00e9lize\":\"B\\u00e9lize\",\"B\\u00e9nin\":\"B\\u00e9nin\",\"Bhoutan\":\"Bhoutan\",\"Bi\\u00e9lorussie\":\"Bi\\u00e9lorussie\",\"Myanmar\":\"Myanmar\",\"Bolivie\":\"Bolivie\",\"Bosnie et Herz\\u00e9govine\":\"Bosnie et Herz\\u00e9govine\",\"Botswana\":\"Botswana\",\"Br\\u00e9sil\":\"Br\\u00e9sil\",\"Brunei\":\"Brunei\",\"Bulgarie\":\"Bulgarie\",\"Burkina Faso\":\"Burkina Faso\",\"Burundi\":\"Burundi\",\"Cambodge\":\"Cambodge\",\"Cameroun\":\"Cameroun\",\"Canada\":\"Canada\",\"Cap-Vert\":\"Cap-Vert\",\"R\\u00e9publique centrafricaine\":\"R\\u00e9publique centrafricaine\",\"Chili\":\"Chili\",\"Chine\":\"Chine\",\"Chypre\":\"Chypre\",\"Colombie\":\"Colombie\",\"Comores\":\"Comores\",\"Congo\":\"Congo\",\"R\\u00e9publique d\\u00e9mocratique du Congo\":\"R\\u00e9publique d\\u00e9mocratique du Congo\",\"Cor\\u00e9e du Nord\":\"Cor\\u00e9e du Nord\",\"Cor\\u00e9e du Sud \":\"Cor\\u00e9e du Sud\",\"Costa Rica\":\"Costa Rica\",\"C\\u00f4te d\'Ivoire\":\"C\\u00f4te d\'Ivoire\",\"Croatie\":\"Croatie\",\"Cuba\":\"Cuba\",\"Danemark\":\"Danemark\",\"Djibouti\":\"Djibouti\",\"R\\u00e9publique dominicaine\":\"R\\u00e9publique dominicaine\",\"Dominique\":\"Dominique\",\"\\u00c9gypte\":\"\\u00c9gypte\",\"\\u00c9mirats arabes unis\":\"\\u00c9mirats arabes unis\",\"\\u00c9quateur\":\"\\u00c9quateur\",\"\\u00c9rythr\\u00e9e\":\"\\u00c9rythr\\u00e9e\",\"Espagne\":\"Espagne\",\"Estonie\":\"Estonie\",\"Swaziland\":\"Swaziland\",\"\\u00c9tats-Unis\":\"\\u00c9tats-Unis\",\"\\u00c9thiopie\":\"\\u00c9thiopie\",\"\\u00eeles Fiji\":\"\\u00eeles Fiji\",\"Finlande\":\"Finlande\",\"France\":\"France\",\"Gabon\":\"Gabon\",\"Gambie\":\"Gambie\",\"G\\u00e9orgie\":\"G\\u00e9orgie\",\"Ghana\":\"Ghana\",\"Gr\\u00e8ce\":\"Gr\\u00e8ce\",\"Grenade\":\"Grenade\",\"Guat\\u00e9mala\":\"Guat\\u00e9mala\",\"Guin\\u00e9e\":\"Guin\\u00e9e\",\"Guin\\u00e9e-Bissau\":\"Guin\\u00e9e-Bissau\",\"Guin\\u00e9e \\u00e9quatoriale\":\"Guin\\u00e9e \\u00e9quatoriale\",\"Guyana\":\"Guyana\",\"Ha\\u00efti\":\"Ha\\u00efti\",\"Honduras\":\"Honduras\",\"Hongrie\":\"Hongrie\",\"Inde\":\"Inde\",\"Indon\\u00e9sie\":\"Indon\\u00e9sie\",\"Iraq\":\"Iraq\",\"Iran\":\"Iran\",\"Irlande\":\"Irlande\",\"Islande\":\"Islande\",\"Isra\\u00ebl\":\"Isra\\u00ebl\",\"Italie\":\"Italie\",\"Jama\\u00efque\":\"Jama\\u00efque\",\"Japon\":\"Japon\",\"Jordanie\":\"Jordanie\",\"Kazakstan\":\" Kazakstan\",\"Kenya\":\"Kenya\",\"Kirghizstan\":\"Kirghizstan\",\"Kiribati\":\"Kiribati\",\"Kowe\\u00eft\":\"Kowe\\u00eft\",\"Laos\":\"Laos\",\"Lesotho\":\"Lesotho\",\"Lettonie\":\"Lettonie\",\"Liban\":\"Liban\",\"Lib\\u00e9ria\":\"Lib\\u00e9ria\",\"Libye\":\"Libye\",\"Liechtenstein\":\"Liechtenstein\",\"Lituanie\":\"Lituanie\",\"Luxembourg\":\"Luxembourg\",\"Mac\\u00e9doine du Nord\":\"Mac\\u00e9doine du Nord\",\"Madagascar\":\"Madagascar\",\"Malaisie\":\"Malaisie\",\"Malawi\":\"Malawi\",\"Maldives\":\"Maldives\",\"Mali\":\"Mali\",\"Malte\":\"Malte\",\"Maroc\":\"Maroc\",\"Marshall\":\"Marshall\",\"Maurice\":\"Maurice\",\"Mauritanie\":\"Mauritanie\",\"Mexique\":\"Mexique\",\"Micron\\u00e9sie\":\"Micron\\u00e9sie\",\"Moldavie\":\"Moldavie\",\"Monaco\":\"Monaco\",\"Mongolie\":\"Mongolie\",\"Mont\\u00e9n\\u00e9gro\":\"Mont\\u00e9n\\u00e9gro\",\"Mozambique\":\"Mozambique\",\"Namibie\":\"Namibie\",\"Nauru\":\"Nauru\",\"N\\u00e9pal\":\"N\\u00e9pal\",\"Nicaragua\":\"Nicaragua\",\"Niger\":\"Niger\",\"Nig\\u00e9ria\":\"Nig\\u00e9ria\",\"Niue\":\"Niue\",\"Norv\\u00e8ge\":\"Norv\\u00e8ge\",\"Nouvelle-Z\\u00e9lande\":\"Nouvelle-Z\\u00e9lande\",\"Oman\":\"Oman\",\"Ouganda\":\"Ouganda\",\"Ouzb\\u00e9kistan\":\"Ouzb\\u00e9kistan\",\"Pakistan\":\"Pakistan\",\"Palaos\":\"Palaos\",\"Palestine\":\"Palestine\",\"Panama\":\"Panama\",\"Papouasie-Nouvelle-Guin\\u00e9e\":\"Papouasie-Nouvelle-Guin\\u00e9e\",\"Paraguay\":\"Paraguay\",\"Pays-Bas\":\"Pays-Bas\",\"P\\u00e9rou\":\"P\\u00e9rou\",\"Philippines\":\"Philippines\",\"Pologne\":\"Pologne\",\"Portugal\":\"Portugal\",\"Qatar\":\"Qatar\",\"Roumanie\":\"Roumanie\",\"Royaume-Uni\":\"Royaume-Uni\",\"Russie\":\"Russie\",\"Rwanda\":\"Rwanda\",\"Saint-Christophe-et-Ni\\u00e9v\\u00e8s\":\"Saint-Christophe-et-Ni\\u00e9v\\u00e8s\",\"Sainte-Lucie\":\"Sainte-Lucie\",\"Saint-Marin\":\"Saint-Marin\",\"Saint-Vincent-et-les Grenadines\":\"Saint-Vincent-et-les Grenadines\",\"\\u00celes Salomon\":\"\\u00celes Salomon\",\"Salvador\":\"Salvador\",\"Samoa\":\"Samoa\",\"Sao Tom\\u00e9-et-Principe\":\"Sao Tom\\u00e9-et-Principe\",\"S\\u00e9n\\u00e9gal\":\"S\\u00e9n\\u00e9gal\",\"Seychelles\":\"Seychelles\",\"Sierra Leone\":\"Sierra Leone\",\"Singapour\":\"Singapour\",\"Slovaquie\":\"Slovaquie\",\"Slov\\u00e9nie\":\"Slov\\u00e9nie\",\"Somalie\":\"Somalie\",\"Soudan\":\"Soudan\",\"Soudan du Sud\":\"Soudan du Sud\",\"Sri Lanka\":\"Sri Lanka\",\"Su\\u00e8de\":\"Su\\u00e8de\",\"Suisse\":\"Suisse\",\"Suriname\":\"Suriname\",\"Syrie\":\"Syrie\",\"Tadjikistan\":\"Tadjikistan\",\"Tanzanie\":\"Tanzanie\",\"Tchad\":\"Tchad\",\"Tch\\u00e9quie\":\"Tch\\u00e9quie\",\"Tha\\u00eflande\":\"Tha\\u00eflande\",\"Timor oriental\":\"Timor oriental\",\"Togo\":\"Togo\",\"Tonga\":\"Tonga\",\"Trinit\\u00e9-et-Tobago\":\"Trinit\\u00e9-et-Tobago\",\"Tunisie\":\"Tunisie\",\"Turkm\\u00e9nistan\":\"Turkm\\u00e9nistan\",\"Turquie\":\"Turquie\",\"Tuvalu\":\"Tuvalu\",\"Ukraine\":\"Ukraine\",\"Uruguay\":\"Uruguay\",\"Vanuatu\":\"Vanuatu\",\"Vatican\":\"Vatican\",\"Venezuela\":\"Venezuela\",\"Vi\\u00eat Nam\":\"Vi\\u00eat Nam\",\"Y\\u00e9men\":\"Y\\u00e9men\",\"Zambie\":\"Zambie\",\"Zimbabwe\":\"Zimbabwe\"}}', 4),
(283, 32, 'domaine_activte', 'text', 'Domaine Activte', 0, 1, 1, 1, 1, 1, '{}', 5),
(284, 32, 'interet', 'text', 'Interet', 0, 1, 1, 1, 1, 1, '{}', 6),
(285, 32, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(286, 32, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(287, 32, 'manuel_notification_belongsto_activite_relationship', 'relationship', 'activites', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Activite\",\"table\":\"activites\",\"type\":\"belongsTo\",\"column\":\"domaine_activte\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(288, 32, 'manuel_notification_belongsto_interet_relationship', 'relationship', 'interets', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Interet\",\"table\":\"interets\",\"type\":\"belongsTo\",\"column\":\"interet\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"activites\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(289, 10, 'appel_id', 'text', 'Appel Id', 0, 0, 0, 0, 0, 0, '{}', 28),
(290, 10, 'email_professionnel', 'text', 'Email Professionnel', 0, 1, 1, 1, 1, 1, '{}', 29),
(291, 10, 'code_activation_b2b_meeting', 'text', 'Code Activation B2b Meeting', 0, 0, 0, 0, 0, 0, '{}', 30);

-- --------------------------------------------------------

--
-- Structure de la table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-03-31 11:36:56', '2021-03-31 11:36:56'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-03-31 11:36:56', '2021-03-31 11:36:56'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-03-31 11:36:56', '2021-03-31 11:36:56'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-03-31 11:37:10', '2021-07-19 12:31:35'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2021-03-31 11:37:13', '2021-03-31 11:37:13'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2021-03-31 11:37:15', '2021-03-31 11:37:15'),
(7, 'certifications', 'certifications', 'Certification', 'Certifications', NULL, 'App\\Certification', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-08 10:25:25', '2021-07-19 12:32:11'),
(8, 'experiences', 'experiences', 'Experience', 'Experiences', NULL, 'App\\Experience', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-08 10:25:54', '2021-04-13 10:47:09'),
(9, 'formations', 'formations', 'Formation', 'Formations', NULL, 'App\\Formation', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-08 10:26:12', '2021-04-13 10:48:02'),
(10, 'mobile_users', 'mobile-users', 'Mobile User', 'Mobile Users', NULL, 'App\\MobileUser', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-08 10:26:31', '2022-01-10 13:35:48'),
(11, 'interets', 'interets', 'Interet', 'Interets', NULL, 'App\\Interet', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-08 10:26:41', '2021-04-12 11:49:27'),
(12, 'objectifs', 'objectifs', 'Objectif', 'Objectifs', NULL, 'App\\Objectif', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-08 10:26:54', '2021-04-12 11:50:32'),
(13, 'evenements', 'evenements', 'Evenement', 'Evenements', NULL, 'App\\Evenement', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-13 12:56:56', '2021-10-21 09:20:02'),
(15, 'speakers', 'speakers', 'Speaker', 'Speakers', NULL, 'App\\Speaker', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-13 12:59:51', '2021-07-19 12:36:08'),
(16, 'type_progs', 'type-progs', 'Type Prog', 'Type Progs', NULL, 'App\\TypeProg', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-13 13:00:50', '2021-04-13 13:01:05'),
(17, 'programmes', 'programmes', 'Programme', 'Programmes', NULL, 'App\\Programme', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-13 13:18:25', '2021-07-16 13:31:13'),
(18, 'b2b_meets', 'b2b-meets', 'B2B Meet', 'B2B Meets', NULL, 'App\\B2bMeet', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-13 13:41:07', '2021-04-14 09:50:04'),
(19, 'b2b_meet_users', 'b2b-meet-users', 'B2B Meet User', 'B2B Meet Users', NULL, 'App\\B2bMeetUser', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-14 14:56:45', '2021-06-14 17:32:50'),
(20, 'activites', 'activites', 'Activite', 'Activites', NULL, 'App\\Activite', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-16 10:29:25', '2021-07-19 12:30:22'),
(21, 'wizard_roles', 'wizard-roles', 'Wizard Role', 'Wizard Roles', NULL, 'App\\WizardRole', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-16 10:29:40', '2021-04-19 10:06:09'),
(22, 'boardings', 'boardings', 'Boarding', 'Boardings', NULL, 'App\\Boarding', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
(23, 'users_objectifs', 'users-objectifs', 'Users Objectif', 'Users Objectifs', NULL, 'App\\UsersObjectif', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
(24, 'organisateurs', 'organisateurs', 'Organisateur', 'Organisateurs', NULL, 'App\\Organisateur', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-23 09:14:26', '2021-07-19 12:34:32'),
(25, 'partenaires', 'partenaires', 'Partenaire', 'Partenaires', NULL, 'App\\Partenaire', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-23 09:14:36', '2021-07-19 12:35:20'),
(26, 'propos', 'propos', 'Propo', 'Propos', NULL, 'App\\Propo', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-23 09:14:51', '2021-04-23 10:00:24'),
(27, 'reacts', 'reacts', 'React', 'Reacts', NULL, 'App\\React', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 16:05:29', '2021-11-09 16:05:29'),
(29, 'stands', 'stands', 'Stand', 'Stands', NULL, 'App\\Stand', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-12-07 11:05:02', '2021-12-07 11:05:02'),
(30, 'user_details', 'user-details', 'User Detail', 'User Details', NULL, 'App\\UserDetail', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-12-13 13:44:37', '2021-12-13 13:44:37'),
(31, 'notifications', 'notifications', 'Notification', 'Notifications', NULL, 'App\\Notification', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2022-01-05 14:38:12', '2022-01-05 14:38:12'),
(32, 'manuel_notifications', 'manuel-notifications', 'Manuel Notification', 'Manuel Notifications', NULL, 'App\\ManuelNotification', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2022-01-07 10:40:53', '2022-01-13 08:35:47');

-- --------------------------------------------------------

--
-- Structure de la table `discussions`
--

CREATE TABLE `discussions` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `discussions`
--

INSERT INTO `discussions` (`id`, `created_at`, `updated_at`) VALUES
(31, '2022-01-03 10:47:29', '2022-01-03 11:26:04');

-- --------------------------------------------------------

--
-- Structure de la table `evenements`
--

CREATE TABLE `evenements` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location_event` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_event` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_event` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `image_couverture` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `evenements`
--

INSERT INTO `evenements` (`id`, `titre`, `date_debut`, `created_at`, `updated_at`, `location_event`, `image_event`, `longitude`, `latitude`, `description`, `icon_event`, `date_fin`, `image_couverture`) VALUES
(7, 'SITIC Africa Abidjan 2022', '2022-02-21', '2021-11-24 10:14:00', '2021-12-07 09:19:29', 'Abidjan', 'evenements/November2021/ltIRyXTuxWIojOTeKe4s.png', '-4.004489250376203', '5.3278699393030315', 'SITIC AFRICA ABIDJAN 2022 est une véritable plateforme internationale du Business pour faire connaitre l’Offre Africaine et Internationale dans le Numérique.', 'evenements/November2021/Dg7zmReQvzkFAgH9Kfxp.png', '2022-02-24', 'evenements/November2021/327wIQXVkn76jdxiQCX7.png');

-- --------------------------------------------------------

--
-- Structure de la table `experiences`
--

CREATE TABLE `experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `actuellement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `site_web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `experiences`
--

INSERT INTO `experiences` (`id`, `role`, `entreprise`, `date_debut`, `date_fin`, `actuellement`, `created_at`, `updated_at`, `user_id`, `site_web`) VALUES
(1, NULL, 'experience 1', NULL, NULL, NULL, '2021-04-08 11:34:10', '2021-04-08 11:34:10', NULL, NULL),
(2, NULL, 'experience 2', NULL, NULL, NULL, '2021-04-08 11:34:19', '2021-04-08 11:34:19', NULL, NULL),
(3, 'ole1', 'Soc1', '2017-04-13', '2020-04-13', '0', '2021-04-13 10:39:48', '2021-04-13 10:39:48', 4, 'Www.t.tn'),
(4, 'ole1', 'Soc1', '2018-04-13', '2019-04-13', '0', '2021-04-13 10:40:52', '2021-04-13 10:40:53', 4, 'Yguyu.com'),
(5, NULL, NULL, '2021-04-13', '2021-04-13', '0', '2021-04-13 10:42:36', '2021-04-13 10:42:36', 4, NULL),
(6, NULL, NULL, '2021-04-13', '2021-04-13', '0', '2021-04-13 10:44:11', '2021-04-13 10:44:11', 4, NULL),
(7, 'CEO', 'Lezats', '2013-04-13', '2017-04-13', '0', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5, 'Lzd.fr'),
(8, 'Exp2', 'Societe2', '2021-04-13', '2021-05-13', '0', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5, 'web.tn2'),
(9, NULL, NULL, '2021-04-13', '2021-04-13', '0', '2021-04-13 12:06:05', '2021-04-13 12:06:05', 5, NULL),
(10, NULL, NULL, '2021-04-13', '2021-04-13', '0', '2021-04-13 12:19:17', '2021-04-13 12:19:17', 5, NULL),
(11, 'Dev', 'Xelero', '2021-01-01', '2021-04-16', '0', '2021-04-16 09:41:37', '2021-04-16 09:41:37', 10, 'Xelero.io'),
(12, 'Developer', 'Lzd', '2019-09-01', '2020-07-01', '0', '2021-06-01 07:47:49', '2021-06-01 07:47:56', 10, 'Lzd.com'),
(13, 'Developer mobile', 'Xelero', '2021-01-01', '2021-06-01', '1', '2021-06-01 07:47:49', '2021-06-01 07:47:56', 10, 'Xelero.io'),
(14, 'Developer', 'Xelero', '2021-02-01', '2021-06-08', '1', '2021-06-08 08:38:56', '2021-06-08 08:38:56', 14, 'Xelero.io'),
(15, 'RoleExp', 'Xelero', '2021-01-01', '2021-05-08', '0', '2021-06-08 10:18:15', '2021-06-08 10:18:15', 13, 'Xelero.io'),
(16, 'RoleTest', 'Xelero', '2020-08-04', '2021-08-04', '1', '2021-08-04 10:43:17', '2021-08-04 10:43:17', 20, 'Xelero.io'),
(17, 'CEO', 'AfghaSoc', '2013-06-12', '2021-08-12', '1', '2021-08-12 15:52:59', '2021-08-12 15:52:59', 21, 'AfghaSoc.com'),
(18, 'Exp1', 'Soc1', '2018-09-28', '2021-09-28', '1', '2021-09-28 11:33:42', '2021-09-28 11:33:43', 32, 'soc1.io'),
(19, 'Exp1', 'Xelero', '2020-09-28', '2021-09-28', '1', '2021-09-28 11:47:46', '2021-09-28 11:47:47', 33, 'Xelero.io'),
(20, 'role1', 'soc1', '2003-09-28', '2021-09-28', '1', '2021-09-28 16:35:43', '2021-09-28 16:35:43', 35, 'site.com'),
(21, 'Ing web', 'Slnee', '2020-01-10', '2020-06-28', '0', '2021-09-29 13:54:40', '2021-09-29 13:54:40', 38, 'slnee.com'),
(22, 'CEO', 'Xelero', '2020-12-13', '2021-10-11', '1', '2021-10-11 14:06:41', '2021-10-11 14:06:42', 39, 'www.xelero.ii'),
(23, 'role1', 'soc1', '2006-10-20', '2021-10-20', '1', '2021-10-20 11:18:46', '2021-10-20 11:18:46', 42, 'www.site.com'),
(24, 'Role1', 'Soc1', '2021-10-06', '2021-10-22', '1', '2021-10-21 08:52:30', '2021-10-21 08:52:30', 43, 'Soc.com'),
(25, 'Role1', 'Soci1', '2020-10-22', '2021-10-22', '1', '2021-10-22 10:17:08', '2021-10-22 10:17:08', 45, 'Soc.com'),
(26, 'Role1', 'Soc1', '2021-10-13', '2021-10-22', '1', '2021-10-22 15:15:04', '2021-10-22 15:15:04', 46, 'Soc1.com'),
(27, 'Role1', 'Soc1', '2020-10-27', '2021-10-27', '1', '2021-10-27 11:02:08', '2021-10-27 11:02:08', 47, 'Site.com'),
(28, 'Role1', 'Soc1', '2019-10-27', '2021-10-27', '1', '2021-10-27 11:10:27', '2021-10-27 11:10:27', 48, 'Site22.com'),
(29, 'Role1', 'Soc1', '2020-10-27', '2021-10-27', '1', '2021-10-27 11:32:28', '2021-10-27 11:32:28', 49, 'Site.com'),
(30, 'Role1', 'sscco1', '2017-10-27', '2021-10-27', '1', '2021-10-27 12:10:11', '2021-10-27 12:10:11', 50, 'Soc.com'),
(31, 'role1', 'soc1', '2020-10-27', '2021-10-27', '1', '2021-10-27 12:18:02', '2021-10-27 12:18:02', 51, 'site.fr'),
(32, 'Eae', 'Soc', '2021-09-27', '2021-10-27', '1', '2021-10-27 13:01:08', '2021-10-27 13:01:08', 52, 'Sur.com'),
(33, 'Web dec', 'Xelero', '2020-02-10', '2021-07-11', '0', '2021-11-01 10:10:12', '2021-11-01 10:10:12', 53, 'www.xelero.io'),
(34, 'Ghj', 'Gyu', '2021-10-12', '2021-11-01', '0', '2021-11-01 10:52:36', '2021-11-01 10:52:36', 55, 'www.exemple.com'),
(35, 'Ing', 'Xgu', '2021-11-09', '2021-11-01', '0', '2021-11-01 11:22:26', '2021-11-01 11:22:26', 59, 'www.gg.com'),
(36, 'Hhh', 'Ghh', '2021-11-17', '2021-11-01', '0', '2021-11-01 12:33:22', '2021-11-01 12:33:22', 60, 'www.hh.com'),
(37, 'Inh', 'Cchy', '2021-11-10', '2021-11-01', '0', '2021-11-01 12:43:28', '2021-11-01 12:43:28', 62, 'www.xelero.io'),
(38, 'Ing', 'Xelero', '2021-11-17', '2021-11-01', '0', '2021-11-01 12:48:31', '2021-11-01 12:48:31', 63, 'www.xelero.io'),
(39, 'Ing', 'Suzuki', '2021-11-10', '2021-11-02', '0', '2021-11-02 09:00:45', '2021-11-02 09:00:45', 38, 'www.suzuki.com'),
(40, 'Gh', 'Xelero', '2021-11-17', '2021-11-02', '0', '2021-11-02 09:24:25', '2021-11-02 09:24:25', 65, 'www.gg.com'),
(41, 'Director', 'Xelero', '2020-10-22', '2021-11-03', '1', '2021-11-03 09:35:54', '2021-11-03 09:35:54', 66, 'Www.xelero.io'),
(42, 'Ing', 'Xelero', '2021-11-17', '2021-11-03', '0', '2021-11-03 10:06:41', '2021-11-03 10:06:41', 38, 'www.xelero.io'),
(43, 'Designer', 'Xelero', '2021-11-17', '2021-11-03', '0', '2021-11-03 11:05:50', '2021-11-03 11:05:50', 69, 'www.xelero.io'),
(44, 'Role1', 'Soc1', '2020-11-04', '2021-11-04', '1', '2021-11-04 08:35:48', '2021-11-04 08:35:48', 70, 'Site.fr'),
(45, 'Role1', 'Soc1', '2019-11-04', '2021-11-04', '1', '2021-11-04 09:33:38', '2021-11-04 09:33:39', 71, 'Site.fr'),
(46, 'role', 'soc', '2019-11-04', '2021-11-04', '1', '2021-11-04 09:43:48', '2021-11-04 09:43:48', 67, 'soc1.com'),
(47, 'role1', 'soc', '2018-11-04', '2021-11-04', '1', '2021-11-04 09:54:06', '2021-11-04 09:54:06', 67, 'soc.com'),
(48, 'role', 'soc', '2019-11-04', '2021-11-04', '0', '2021-11-04 09:59:06', '2021-11-04 09:59:06', 67, 'soc.fr'),
(49, 'role1', 'sofc', '2018-11-04', '2021-11-04', '1', '2021-11-04 11:13:03', '2021-11-04 11:13:03', 75, 'soc.com'),
(50, 'role1', 'soc1', '2018-11-04', '2021-11-04', '0', '2021-11-04 11:17:33', '2021-11-04 11:17:33', 75, 'sutz.fr'),
(51, 'Dev', 'Xelero', '2020-01-01', '2021-11-08', '1', '2021-11-08 08:58:00', '2021-11-08 08:58:00', 76, 'Xelero.io'),
(52, 'Dev', 'Xelero', '2021-11-08', '2021-11-08', '1', '2021-11-08 09:03:47', '2021-11-08 09:03:47', 77, 'Xelero.io'),
(53, 'Developpeur Web & Mobile', 'Xelero', '2020-01-01', '2021-11-10', '1', '2021-11-10 09:22:09', '2021-11-10 09:22:09', 79, 'Xelero.io'),
(54, 'Developpeur Mobile', 'Lezarts.digital', '2019-09-03', '2020-12-31', '0', '2021-11-10 09:22:09', '2021-11-10 09:22:09', 79, 'lezarts.digital'),
(55, 'CEO', 'Xelero.io', '2020-01-01', '2021-11-24', '1', '2021-11-24 13:04:08', '2021-11-24 13:04:08', 80, 'Xelero.io'),
(56, 'Développeur Laravel', 'Xelero', '2021-05-26', '2021-11-24', '1', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 81, 'Xelero'),
(57, 'Développeur ERP', 'Slnee', '2020-02-10', '2020-06-30', '0', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 81, 'www.Slnee.sa'),
(58, 'Développeur Ruby on Rails', 'Cash Expert', '2019-07-01', '2019-08-30', '0', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 81, NULL),
(59, 'Développeur c#', 'Asteelflash', '2018-08-06', '2018-09-03', '0', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 81, 'www.asteelflash.com'),
(60, 'Directeur général', 'Tunisie Afrique Export', '2015-12-06', '2021-12-06', '1', '2021-12-06 09:16:51', '2021-12-06 09:16:51', 84, 'TAE.tn'),
(61, 'Teet', 'Test', '2015-09-08', '2021-12-08', '0', '2021-12-08 18:32:49', '2021-12-08 18:32:49', 85, 'Www.test.com'),
(62, 'role', 'soc', '2021-12-16', '2021-12-16', '1', '2021-12-16 12:36:56', '2021-12-16 12:36:56', 90, 'site.com'),
(63, 'Role', 'soc1', '2019-12-21', '2021-12-21', '1', '2021-12-21 15:20:56', '2021-12-21 15:20:56', 95, 'site.com'),
(64, 'DG', 'Codix', '2016-07-01', '2021-12-22', '0', '2021-12-22 17:10:07', '2021-12-22 17:10:07', 97, 'Codix.com'),
(65, 'Ffg', 'Connais', '2021-08-22', '2021-12-22', '1', '2021-12-22 22:45:45', '2021-12-22 22:45:46', 91, 'Ghj'),
(66, 'Dggbb', 'Ffggfd', '2021-12-22', '2021-12-22', '1', '2021-12-22 22:45:45', '2021-12-22 22:45:46', 91, 'Www.testrrr.com'),
(67, 'CEO', 'Xelero', '2020-12-13', '2021-12-23', '1', '2021-12-23 00:42:28', '2021-12-23 00:42:28', 99, 'www.xelero.io'),
(68, 'Dg', 'Taw', '2021-12-23', '2021-12-23', '1', '2021-12-23 09:40:36', '2021-12-23 09:40:37', 100, 'Www.tae.tn'),
(69, 'Web Developer', 'Xelero', '2020-01-01', '2021-12-23', '1', '2021-12-23 11:43:38', '2021-12-23 11:43:38', 101, 'Xelero.io'),
(70, 'Hello', 'Moto', '2017-12-23', '2021-12-23', '1', '2021-12-23 13:54:43', '2021-12-23 13:54:44', 105, 'Yes.com'),
(71, 'Directeur', 'NET Opportunity', '1991-01-01', '2021-12-23', '1', '2021-12-23 14:39:15', '2021-12-23 14:39:15', 110, 'www.net-opportunity.com'),
(72, 'Gérante', 'Elegance Intemporelle', '2020-06-23', '2021-12-24', '1', '2021-12-24 08:17:43', '2021-12-24 08:17:44', 112, 'Eleganceintemporelle.com'),
(73, 'Développeur web', 'Intelis', '2020-08-01', '2021-12-26', '1', '2021-12-26 15:58:45', '2021-12-26 15:58:46', 115, 'Intelis.ml'),
(74, 'Assistant Trésorier', 'Bank Of Africa', '2020-12-03', '2021-06-04', '0', '2022-01-03 15:03:03', '2022-01-03 15:03:03', 98, 'www.boacoteivoire.com'),
(75, 'SG', 'Club DSI', '2016-01-04', '2022-01-04', '1', '2022-01-04 13:19:57', '2022-01-04 13:19:57', 129, 'www.club-dsi.tn'),
(81, 'role', 'soc', '2021-01-11', '2022-01-11', '0', '2022-01-11 14:52:58', '2022-01-11 14:52:58', 125, 'site.tn');

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `formations`
--

CREATE TABLE `formations` (
  `id` int(10) UNSIGNED NOT NULL,
  `ecole` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diplome` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `domaine` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `formations`
--

INSERT INTO `formations` (`id`, `ecole`, `diplome`, `date_debut`, `date_fin`, `domaine`, `created_at`, `updated_at`, `user_id`, `grade`) VALUES
(1, 'formation 1', NULL, NULL, NULL, NULL, '2021-04-08 11:33:00', '2021-04-12 11:30:36', 2, NULL),
(2, 'formation 2', NULL, NULL, NULL, NULL, '2021-04-08 11:33:00', '2021-04-12 11:30:23', 2, NULL),
(3, 'ECole espit', 'Ingenieur', '2020-04-13', '2020-04-13', 'IT', '2021-04-13 10:44:11', '2021-04-13 10:44:11', 4, NULL),
(4, 'Ecole1', 'Diplome1', '2006-04-13', '2008-04-13', 'DOmain1', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5, 'Ingénieur'),
(5, 'Ecole2', 'Diplome2', '2015-04-13', '2016-04-13', 'Domaine2', '2021-04-13 10:50:10', '2021-04-13 10:50:10', 5, NULL),
(6, NULL, NULL, '2021-04-13', '2021-04-13', NULL, '2021-04-13 12:06:05', '2021-04-13 12:06:05', 5, NULL),
(7, NULL, NULL, '2021-04-13', '2021-04-13', NULL, '2021-04-13 12:19:17', '2021-04-13 12:19:17', 5, NULL),
(8, 'Esprit', 'Ingenieur', '2019-09-01', '2021-04-16', 'IT', '2021-04-16 09:41:37', '2021-04-16 09:41:37', 10, '3eme cycle'),
(9, 'IHEC CARTHAGE', 'Licence', '2016-09-01', '2019-08-01', 'IT', '2021-06-01 07:47:49', '2021-06-01 07:47:56', 10, '3émé'),
(10, 'Esprit', 'Ingénieur', '2020-09-01', '2021-12-31', 'IT', '2021-06-01 07:47:49', '2021-06-01 07:47:56', 10, 'Ingénieur'),
(11, 'Esprit', 'Ing it', '2019-09-01', '2023-07-31', 'IT', '2021-06-08 08:38:56', '2021-06-08 08:38:56', 14, '3eme cycle'),
(12, 'Esprit', 'Ingenieur', '2019-06-08', '2023-06-08', 'IT', '2021-06-08 10:18:15', '2021-06-08 10:18:15', 13, 'INGENIEUR'),
(13, 'Esprit', 'Ing', '2019-08-04', '2021-08-04', 'Ing', '2021-08-04 10:43:17', '2021-08-04 10:43:17', 20, '3eme'),
(14, 'AfghaESP', 'Dip1', '2012-08-12', '2013-08-12', 'Dom1', '2021-08-12 15:52:59', '2021-08-12 15:52:59', 21, '3Eme'),
(15, 'Eco1', 'Dip1', '2018-09-28', '2021-09-28', 'Dom1', '2021-09-28 11:33:42', '2021-09-28 11:33:43', 32, 'Niv1'),
(16, 'Esprit', 'Ingénieurie', '2019-09-28', '2021-09-28', 'Informatique', '2021-09-28 11:47:46', '2021-09-28 11:47:47', 33, '3ème cycle'),
(17, 'eco1', 'disp1', '2021-09-28', '2021-09-28', 'dom1', '2021-09-28 16:35:43', '2021-09-28 16:35:43', 35, 'niv1'),
(18, 'Sesame', 'Ing informatique', '2017-09-17', '2020-07-27', 'Ing', '2021-09-29 13:54:40', '2021-09-29 13:54:40', 38, 'Gl'),
(19, 'Iset Charguia', 'Licence appliquée en technologie de l\'informatique', '2014-09-29', '2017-06-13', 'DSI', '2021-09-29 13:54:40', '2021-09-29 13:54:40', 38, 'Dsi'),
(20, 'Ok', 'Ok', '2017-10-12', '2020-10-14', 'Ok', '2021-10-11 14:06:41', '2021-10-11 14:06:42', 39, '3ali'),
(21, 'esprit', 'ingenieur', '2019-10-20', '2021-10-20', 'cloud', '2021-10-20 11:18:46', '2021-10-20 11:18:46', 42, '3eme'),
(22, 'Eci1', 'Dip1', '2021-10-04', '2021-10-21', 'Dom1', '2021-10-21 08:52:30', '2021-10-21 08:52:30', 43, '3dx'),
(23, 'Eco1', 'Dip1', '2020-10-22', '2021-10-22', 'Dm1', '2021-10-22 10:17:08', '2021-10-22 10:17:08', 45, '3ODm'),
(24, 'Eco1', 'Dip1', '2021-10-11', '2021-10-22', 'Dom1', '2021-10-22 15:15:04', '2021-10-22 15:15:04', 46, '3kddk'),
(25, 'Eco1', 'Dip1', '2019-10-27', '2021-10-27', 'Dom1', '2021-10-27 11:02:08', '2021-10-27 11:02:08', 47, 'Niv1'),
(26, 'Eco1', 'Dip1', '2019-10-27', '2021-10-27', 'Dom3', '2021-10-27 11:10:27', '2021-10-27 11:10:27', 48, 'Ea'),
(27, 'Eco1', 'Dip1', '2018-10-27', '2021-10-27', 'Dom1', '2021-10-27 11:32:28', '2021-10-27 11:32:28', 49, 'Azae'),
(28, 'Eco1', 'Dip1', '2019-10-27', '2021-10-27', 'Dom1', '2021-10-27 12:10:11', '2021-10-27 12:10:11', 50, '3eme'),
(29, 'eco1', 'dip', '2019-10-27', '2021-10-27', 'dom', '2021-10-27 12:18:02', '2021-10-27 12:18:02', 51, '3eme'),
(30, 'Eae', 'Ipdo', '2021-10-25', '2021-10-27', 'Dom', '2021-10-27 13:01:08', '2021-10-27 13:01:08', 52, '« &dazd'),
(31, 'Université Sesame', 'Ing diplome', '2017-09-15', '2020-07-27', 'Gl', '2021-11-01 10:10:12', '2021-11-01 10:10:12', 53, 'Ing'),
(32, 'Iset Charguia', 'Dsi', '2014-09-15', '2017-06-14', 'Dsi', '2021-11-01 10:10:12', '2021-11-01 10:10:12', 53, 'Dsi'),
(33, 'Sesame', 'Inh', '2021-11-14', '2021-11-01', 'Ing', '2021-11-01 10:52:36', '2021-11-01 10:52:36', 55, 'Dsi'),
(34, 'Sesame', 'Ing', '2021-11-16', '2021-11-01', 'Ing', '2021-11-01 11:22:26', '2021-11-01 11:22:26', 59, 'Dsi'),
(35, 'Ses', 'Inh', '2021-11-18', '2021-11-01', 'Ing', '2021-11-01 12:33:22', '2021-11-01 12:33:22', 60, 'DSI'),
(36, 'Sesame', 'Ing', '2021-11-18', '2021-11-01', 'Ing', '2021-11-01 12:43:28', '2021-11-01 12:43:28', 62, 'Ddd'),
(37, 'Sesame', 'Ing', '2021-11-24', '2021-11-01', 'Ing', '2021-11-01 12:48:31', '2021-11-01 12:48:31', 63, 'Si'),
(38, 'Université of tokyo', 'Ing', '2021-11-16', '2021-11-02', 'Ing', '2021-11-02 09:00:45', '2021-11-02 09:00:45', 38, 'Hhj'),
(39, 'Université sesame', 'Inj', '2021-11-18', '2021-11-02', 'Dsi', '2021-11-02 09:24:25', '2021-11-02 09:24:25', 65, 'Dsi'),
(40, 'Esprit', 'Ingenieur', '2016-11-10', '2021-11-03', 'Informatique', '2021-11-03 09:35:54', '2021-11-03 09:35:54', 66, 'Sup'),
(41, 'Sesame', 'Ing', '2021-11-17', '2021-11-03', 'Ing', '2021-11-03 10:06:41', '2021-11-03 10:06:41', 38, 'Dsi'),
(42, 'ISAM', 'Multimedia', '2012-11-03', '2017-11-03', 'Multimedia', '2021-11-03 11:05:50', '2021-11-03 11:05:50', 69, 'Multimedia'),
(43, 'Eco', 'Dip', '2019-11-04', '2021-11-04', 'Dom', '2021-11-04 08:35:48', '2021-11-04 08:35:48', 70, '3mee'),
(44, 'Eco', 'Dip', '2018-11-04', '2021-11-04', 'DomEtu', '2021-11-04 09:33:38', '2021-11-04 09:33:39', 71, '3mee'),
(45, 'rco', 'dip', '2020-11-04', '2021-11-04', 'dom', '2021-11-04 09:43:48', '2021-11-04 09:43:48', 67, '3eme'),
(46, 'eco1', 'dip', '2019-11-04', '2021-11-04', 'dom', '2021-11-04 09:54:06', '2021-11-04 09:54:06', 67, '3eme'),
(47, 'eco', 'dip', '2017-11-04', '2021-11-04', 'dom', '2021-11-04 09:59:06', '2021-11-04 09:59:06', 67, '3eme'),
(48, 'eco', 'dip', '2019-11-04', '2021-11-04', 'dom', '2021-11-04 11:13:03', '2021-11-04 11:13:03', 75, '3eme'),
(49, 'eco', 'dip', '2021-11-04', '2021-11-04', 'dom1', '2021-11-04 11:17:33', '2021-11-04 11:17:33', 75, '3eme'),
(50, 'Esprit', 'Ingenieurie', '2019-09-01', '2021-11-08', 'Informatique', '2021-11-08 08:58:00', '2021-11-08 08:58:00', 76, '3eme cycle'),
(51, 'Esprit', 'Dip', '2018-11-08', '2021-11-08', 'Dom', '2021-11-08 09:03:47', '2021-11-08 09:03:47', 77, '3eme'),
(52, 'IHEC Carthage', 'Informatique De Gestion', '2017-09-15', '2019-07-03', 'Gestion', '2021-11-10 09:22:09', '2021-11-10 09:22:09', 79, '2eme cycle'),
(53, 'Esprit Cours Du Soir', 'Ingenieurie Cloud', '2019-09-15', '2023-09-15', 'Informatique', '2021-11-10 09:22:09', '2021-11-10 09:22:09', 79, '3eme cycle'),
(54, 'Sesame', 'Marketing', '2021-11-10', '2021-11-24', 'Marketing', '2021-11-24 13:04:08', '2021-11-24 13:04:08', 80, '3eme'),
(55, 'Université Sesame', 'Ing Informatique', '2017-09-18', '2020-07-27', 'Génie logiciel', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 81, '3 ans'),
(56, 'Iset Charguia', 'Technologies de l\'informatique', '2014-09-15', '2017-06-30', 'Développement des systèmes d\'information', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 81, '3 ans'),
(57, 'Essect', 'Mastère de recherche', '2011-09-01', '2014-07-30', 'Économie', '2021-12-06 09:16:51', '2021-12-06 09:16:51', 84, 'Mastère'),
(58, 'Ggv', 'Fff', '2017-12-08', '2021-12-08', 'Ghnnn', '2021-12-08 18:32:49', '2021-12-08 18:32:49', 85, 'Bbb'),
(59, 'eco1', 'eco2', '2021-12-16', '2021-12-16', 'eco3', '2021-12-16 12:36:56', '2021-12-16 12:36:56', 90, '3eme'),
(60, 'eco', 'dip', '2018-12-21', '2021-12-21', 'dom', '2021-12-21 15:20:56', '2021-12-21 15:20:56', 95, 'niv1'),
(61, 'Isg', 'Maitrise', '1989-12-22', '1994-12-22', 'Finance', '2021-12-22 17:10:07', '2021-12-22 17:10:08', 97, 'Maitrise'),
(62, 'Ddf', 'Xcvb', '2019-05-22', '2021-12-22', 'Zfbbn', '2021-12-22 22:45:45', '2021-12-22 22:45:46', 91, 'Fggddf'),
(63, 'Esprit', 'Ingenieur', '2004-12-23', '2011-12-23', 'Telecom', '2021-12-23 00:42:28', '2021-12-23 00:42:28', 99, 'Ingenieur'),
(64, 'Ik', 'Ik', '2021-12-23', '2021-12-23', 'Ok', '2021-12-23 09:40:36', '2021-12-23 09:40:37', 100, 'Bac'),
(65, 'Esprit', 'Ingénieur', '2019-12-23', '2023-12-23', 'Cloud Computing', '2021-12-23 11:43:38', '2021-12-23 11:43:38', 101, '3éme cycle'),
(66, 'Hello', 'Moto', '2010-12-23', '2011-12-23', 'Ka3ba gato', '2021-12-23 13:54:43', '2021-12-23 13:54:44', 105, 'Seb3a farachet'),
(67, 'Université Lorraine', 'DEA', '1985-01-01', '1987-01-01', 'Gestion d’Entreprise', '2021-12-23 14:39:15', '2021-12-23 14:39:15', 110, 'Bac + 5'),
(68, 'CEFIB', 'DUT', '2020-01-01', '2021-06-30', 'Informatique de gestion', '2021-12-26 15:58:45', '2021-12-26 15:58:46', 115, 'Technicien'),
(69, 'Groupe ISFCG ABIDJAN', 'Master pro 2', '2018-11-05', '2020-10-01', 'Finance', '2022-01-03 15:03:03', '2022-01-03 15:03:03', 98, 'Master 2'),
(72, 'for', 'dip', '2020-01-11', '2022-01-11', 'dom', '2022-01-11 14:52:58', '2022-01-11 14:52:58', 125, 'niv');

-- --------------------------------------------------------

--
-- Structure de la table `interets`
--

CREATE TABLE `interets` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `interets`
--

INSERT INTO `interets` (`id`, `titre`, `created_at`, `updated_at`, `status`) VALUES
(4, 'Marketing', '2021-04-13 10:37:00', '2021-09-28 10:02:24', 1),
(5, 'UX/UI', '2021-04-13 10:39:48', '2021-04-13 10:39:48', 0),
(6, 'Finance', '2021-04-13 10:39:48', '2021-04-13 10:39:48', 0),
(7, 'Stratégie digitale', '2021-04-13 10:50:00', '2021-09-28 10:02:14', 1),
(8, 'Sports', '2021-06-01 07:47:49', '2021-06-01 07:47:49', 0),
(13, 'Fintech', '2021-09-28 10:00:44', '2021-09-28 10:00:44', 1),
(14, 'Agriculture', '2021-09-28 10:00:51', '2021-09-28 10:00:51', 1),
(15, 'Formation 4.0', '2021-09-28 10:01:08', '2021-09-28 10:01:08', 1),
(16, 'Industrie 4.0', '2021-09-28 10:01:16', '2021-09-28 10:01:16', 1),
(17, 'eHealth', '2021-09-28 10:01:30', '2021-09-28 10:01:30', 1),
(18, 'Sport', '2021-11-01 10:10:12', '2021-11-01 10:10:12', 0);

-- --------------------------------------------------------

--
-- Structure de la table `manuel_notifications`
--

CREATE TABLE `manuel_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valeur` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domaine_activte` int(11) DEFAULT NULL,
  `interet` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `manuel_notifications`
--

INSERT INTO `manuel_notifications` (`id`, `titre`, `valeur`, `pays`, `domaine_activte`, `interet`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'Tunisie', 4, 4, '2022-01-10 13:47:39', '2022-01-10 13:47:39'),
(2, NULL, NULL, 'Afrique du Sud', NULL, NULL, '2022-01-12 10:40:06', '2022-01-12 10:40:06'),
(3, NULL, NULL, 'Afrique du Sud', NULL, NULL, '2022-01-12 10:40:42', '2022-01-12 10:40:42'),
(4, 'test', 'test', 'Tunisie', NULL, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40'),
(5, 'test', 'Bonjour les Tunisiens', 'Tous', NULL, NULL, '2022-01-13 08:50:01', '2022-01-13 08:50:01'),
(6, 'Spam', 'Hani n\'spami fikom :p :p', 'Tous', NULL, NULL, '2022-01-13 08:51:34', '2022-01-13 08:51:34');

-- --------------------------------------------------------

--
-- Structure de la table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-03-31 11:36:57', '2021-03-31 11:36:57');

-- --------------------------------------------------------

--
-- Structure de la table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-03-31 11:36:57', '2021-03-31 11:36:57', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 15, 3, '2021-03-31 11:36:57', '2021-04-16 10:31:42', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 15, 1, '2021-03-31 11:36:57', '2021-04-06 11:13:34', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 15, 2, '2021-03-31 11:36:57', '2021-04-16 10:31:42', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2021-03-31 11:36:57', '2021-04-23 09:15:51', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2021-03-31 11:36:57', '2021-04-06 11:13:32', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2021-03-31 11:36:58', '2021-04-06 11:13:32', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2021-03-31 11:36:58', '2021-04-06 11:13:32', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2021-03-31 11:36:58', '2021-04-06 11:13:32', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2021-03-31 11:36:58', '2021-04-23 09:15:51', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, 15, 6, '2021-03-31 11:37:12', '2021-04-19 12:52:42', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, 15, 4, '2021-03-31 11:37:14', '2021-04-19 12:52:42', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, 15, 5, '2021-03-31 11:37:16', '2021-04-19 12:52:42', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2021-03-31 11:37:21', '2021-04-06 11:13:32', 'voyager.hooks', NULL),
(15, 1, 'Back Office', '', '_self', 'voyager-params', '#000000', NULL, 7, '2021-04-06 11:13:23', '2021-04-23 09:16:01', NULL, ''),
(16, 1, 'Certifications', '', '_self', NULL, NULL, 22, 4, '2021-04-08 10:25:25', '2021-04-16 10:31:58', 'voyager.certifications.index', NULL),
(17, 1, 'Experiences', '', '_self', NULL, NULL, 22, 3, '2021-04-08 10:25:54', '2021-04-16 10:31:58', 'voyager.experiences.index', NULL),
(18, 1, 'Formations', '', '_self', NULL, NULL, 22, 2, '2021-04-08 10:26:12', '2021-04-16 10:31:58', 'voyager.formations.index', NULL),
(19, 1, 'Users', '', '_self', NULL, '#000000', 22, 1, '2021-04-08 10:26:31', '2021-04-08 10:29:04', 'voyager.mobile-users.index', 'null'),
(20, 1, 'Interets', '', '_self', NULL, NULL, 34, 3, '2021-04-08 10:26:41', '2021-04-16 10:32:11', 'voyager.interets.index', NULL),
(21, 1, 'Objectifs', '', '_self', NULL, NULL, 34, 4, '2021-04-08 10:26:54', '2021-04-16 10:32:11', 'voyager.objectifs.index', NULL),
(22, 1, 'Mobile Users', '', '_self', 'voyager-person', '#000000', NULL, 3, '2021-04-08 10:28:53', '2021-04-13 13:03:09', NULL, ''),
(23, 1, 'Evenements', '', '_self', NULL, NULL, 27, 1, '2021-04-13 12:56:56', '2021-04-13 13:01:39', 'voyager.evenements.index', NULL),
(25, 1, 'Speakers', '', '_self', NULL, NULL, 27, 3, '2021-04-13 12:59:51', '2021-04-13 13:20:25', 'voyager.speakers.index', NULL),
(26, 1, 'Type Programmes', '', '_self', NULL, '#000000', 27, 4, '2021-04-13 13:00:50', '2021-04-13 13:20:22', 'voyager.type-progs.index', 'null'),
(27, 1, 'Evenements', '', '_self', 'voyager-calendar', '#000000', NULL, 2, '2021-04-13 13:01:33', '2021-04-13 13:03:09', NULL, ''),
(28, 1, 'Programmes', '', '_self', NULL, NULL, 27, 2, '2021-04-13 13:18:25', '2021-04-13 13:20:25', 'voyager.programmes.index', NULL),
(29, 1, 'B2B Meets', '', '_self', NULL, NULL, 33, 1, '2021-04-13 13:41:07', '2021-04-16 10:30:37', 'voyager.b2b-meets.index', NULL),
(30, 1, 'B2B Meet Users', '', '_self', NULL, NULL, 33, 5, '2021-04-14 14:56:45', '2021-06-23 06:20:45', 'voyager.b2b-meet-users.index', NULL),
(31, 1, 'Activites', '', '_self', NULL, NULL, 34, 1, '2021-04-16 10:29:25', '2021-04-16 10:32:11', 'voyager.activites.index', NULL),
(32, 1, 'Roles', '', '_self', NULL, '#000000', 34, 2, '2021-04-16 10:29:40', '2021-04-16 10:32:21', 'voyager.wizard-roles.index', 'null'),
(33, 1, 'Meets', '', '_self', 'voyager-people', '#000000', NULL, 6, '2021-04-16 10:30:31', '2021-04-23 09:16:04', NULL, ''),
(34, 1, 'Info Inscription', '', '_self', 'voyager-file-text', '#000000', NULL, 4, '2021-04-16 10:31:34', '2021-04-19 12:52:55', NULL, ''),
(35, 1, 'Boardings', '', '_self', NULL, NULL, 40, 1, '2021-04-19 12:52:24', '2021-04-23 09:15:51', 'voyager.boardings.index', NULL),
(36, 1, 'Users Objectifs', '', '_self', NULL, NULL, NULL, 10, '2021-04-19 12:10:27', '2021-04-23 09:15:51', 'voyager.users-objectifs.index', NULL),
(37, 1, 'Organisateurs', '', '_self', NULL, NULL, 40, 3, '2021-04-23 09:14:26', '2021-04-23 09:15:55', 'voyager.organisateurs.index', NULL),
(38, 1, 'Partenaires', '', '_self', NULL, NULL, 40, 2, '2021-04-23 09:14:36', '2021-04-23 09:15:55', 'voyager.partenaires.index', NULL),
(39, 1, 'Propos', '', '_self', NULL, NULL, 40, 4, '2021-04-23 09:14:51', '2021-04-23 09:15:55', 'voyager.propos.index', NULL),
(40, 1, 'info pages', '', '_self', 'voyager-phone', '#000000', NULL, 5, '2021-04-23 09:15:25', '2021-04-23 09:16:42', NULL, ''),
(41, 1, 'Pending meeting', '', '_self', NULL, '#000000', 33, 2, '2021-06-23 06:19:39', '2021-06-23 06:20:31', 'voyager.b2b-meet-users.index', '{\"status\":\"sent\"}'),
(42, 1, 'Confirmed meeting', '', '_self', NULL, '#000000', 33, 3, '2021-06-23 06:20:00', '2021-06-23 06:20:38', 'voyager.b2b-meet-users.index', '{\"status\":\"confirmed\"}'),
(43, 1, 'Declined meeting', '', '_self', NULL, '#000000', 33, 4, '2021-06-23 06:20:19', '2021-06-23 06:20:45', 'voyager.b2b-meet-users.index', '{\"status\":\"declined\"}'),
(44, 1, 'Reacts', '', '_self', NULL, NULL, NULL, 11, '2021-11-09 16:05:30', '2021-11-09 16:05:30', 'voyager.reacts.index', NULL),
(46, 1, 'Stands', '', '_self', NULL, NULL, NULL, 12, '2021-12-07 11:05:02', '2021-12-07 11:05:02', 'voyager.stands.index', NULL),
(47, 1, 'User Details', '', '_self', NULL, NULL, NULL, 13, '2021-12-13 13:44:38', '2021-12-13 13:44:38', 'voyager.user-details.index', NULL),
(48, 1, 'Notifications', '', '_self', NULL, NULL, NULL, 14, '2022-01-05 14:38:12', '2022-01-05 14:38:12', 'voyager.notifications.index', NULL),
(49, 1, 'Manuel Notifications', '', '_self', NULL, NULL, NULL, 15, '2022-01-07 10:40:53', '2022-01-07 10:40:53', 'voyager.manuel-notifications.index', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_seen` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `conversation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2016_01_01_000000_create_pages_table', 2),
(25, '2016_01_01_000000_create_posts_table', 2),
(26, '2016_02_15_204651_create_categories_table', 2),
(27, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Structure de la table `mobile_users`
--

CREATE TABLE `mobile_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_de_naissance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays_de_residence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `localite_entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `objectifs` int(11) DEFAULT NULL,
  `interets` int(11) DEFAULT NULL,
  `formations` int(11) DEFAULT NULL,
  `experiences` int(11) DEFAULT NULL,
  `certifications` int(11) DEFAULT NULL,
  `token_device` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_api` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_auth_api` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mot_de_passe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_de_profile` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domaine_activite_p` int(11) DEFAULT NULL,
  `domaine_activite_s` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `access_b2b` int(11) DEFAULT NULL,
  `appel_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_professionnel` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_activation_b2b_meeting` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `mobile_users`
--

INSERT INTO `mobile_users` (`id`, `nom`, `prenom`, `date_de_naissance`, `pays_de_residence`, `telephone`, `sexe`, `entreprise`, `site_web`, `localite_entreprise`, `created_at`, `updated_at`, `objectifs`, `interets`, `formations`, `experiences`, `certifications`, `token_device`, `token_api`, `type_auth_api`, `email`, `mot_de_passe`, `photo_de_profile`, `domaine_activite_p`, `domaine_activite_s`, `role`, `access_b2b`, `appel_id`, `email_professionnel`, `code_activation_b2b_meeting`) VALUES
(97, 'Kerkeni', 'Fodhiel', '1970-12-22 00:00:00', 'Tunisie', '+21623349690', 'Homme', 'Vocalcom', 'www.vocalcom.com', 'Tunisie', '2021-12-22 16:53:37', '2021-12-22 17:10:07', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'gmail', 'fodhiel@gmail.com', '$2y$10$x5ROj.ZUWpwtOwmizSAuAeGFU3m8OmHrvmsX7l.dtP8eNUAq6064.', 'users/user97.gif', 4, 3, 7, NULL, NULL, NULL, NULL),
(98, 'TAGO', 'Richard', '2022-01-03 15:03:03', 'Côte d\'Ivoire', '+2250748346888', 'Homme', 'SITIC AFRICA', 'www.siticafrica.com', 'Côte d\'Ivoire', '2021-12-22 17:42:47', '2022-01-03 15:03:03', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'linkedin', 'richardtago@yahoo.fr', '$2y$10$5FdxWXxe/epT979vF8gCteYlyt/utuo2zE/3I5hKZajjIOcO30DJa', 'users/user98.gif', 3, 2, 5, NULL, NULL, NULL, NULL),
(100, 'Mansouri', 'Chaker', '1984-09-01 00:00:00', 'Tunisie', '+21622988089', 'Homme', 'Tunisie Afrique export', 'Www.tae.tn', 'Tunisie', '2021-12-23 09:36:36', '2021-12-24 11:12:02', NULL, NULL, NULL, NULL, NULL, '[null,\"ExponentPushToken[maSQU6Nm_AzzOhY7PIb3y-]\"]', NULL, 'email', 'mansourichaker@hotmail.fr', '$2y$10$tTMUnV3UH0wsGn0QBNlKHekkoanK4HMbpl22GYYETY8LBkLpHly82', 'users/user100.gif', 3, 2, 6, NULL, NULL, NULL, NULL),
(110, 'ROTH', 'Bruno', '1958-11-12 00:00:00', 'France', '+33608809933', 'Homme', 'NET Opportunity', 'www.net-Opportunity.com', 'France', '2021-12-23 14:29:15', '2021-12-23 14:12:19', NULL, NULL, NULL, NULL, NULL, '[null,\"ExponentPushToken[zwSEZ1BdByPL_rEeAVX8cK]\"]', NULL, 'email', 'broth@net-opportunity.com', '$2y$10$.xRVrJKtZQbkkxhIyk4ATep2.SQoymznFchbHoGrBCLw6YYms/dN2', 'users/user110.gif', 3, 4, 7, NULL, NULL, NULL, NULL),
(111, 'Tounsi', 'Aymen', '2021-12-24 07:19:25', 'Tunisie', '+21698738738', 'Homme', 'TOUNSI XYZ', 'Https://tounsi.xyz', 'Tunisie', '2021-12-24 07:14:44', '2021-12-24 07:19:25', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'gmail', 'aymen.tounsi.tt@gmail.com', '$2y$10$M3QLo303mPtOhPsDBtaZV.Kvzc14oiVC6lxgDbMiAZcwgEILw6bvu', 'user-details/December2021/nP76aDsvdBeRM3JoJcDh.png', 4, NULL, 7, NULL, NULL, NULL, NULL),
(112, 'Mander', 'Souha', '1994-10-03 00:00:00', 'Tunisie', '+21656344177', 'Femme', 'Eleganceintemporelle', 'Eleganceintemporelle.com', 'France', '2021-12-24 08:13:12', '2021-12-24 08:17:43', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[4qrLJaMy50axTlgvYGt-mo]\"]', NULL, 'gmail', 'souha.mander@gmail.com', '$2y$10$nKPID3mR.TfIijB7kK3X0eEP2fneUixhAM3XGNUKXC/gi9/nl4Wp.', 'users/user112.gif', 5, NULL, 7, NULL, NULL, NULL, NULL),
(120, 'mannai', 'Fethi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-28 10:42:42', '2021-12-28 10:42:42', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'gmail', 'mannai2012.fm@gmail.com', NULL, 'users/usermannai2012.fm@gmail.com.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-29 07:07:43', '2022-01-04 10:20:29', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'email', 'myriamayadi323@gmail.com', '$2y$10$QSqNWCc1/t7QBKgCEaq6G.gZgxidQG5whSS6PY7/FJS.u41S674zG', 'users/user121.gif', NULL, NULL, NULL, 2, NULL, 'habibha.aroua82@gmail.com', 'N6XM1A'),
(122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-29 07:07:43', '2021-12-29 07:07:43', NULL, NULL, NULL, NULL, NULL, '[null]', NULL, 'email', 'myriamayadi323@gmail.com', '$2y$10$eCDmQX0Cnkb6AyyqpvWYFuYE82gfTYPsi8o8adV3cqfG/Yjld2EuO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-29 07:07:43', '2021-12-29 07:11:15', NULL, NULL, NULL, NULL, NULL, '[null]', NULL, 'email', 'myriamayadi323@gmail.com', '$2y$10$sH.H4Min9sVW2maqulpCReAtPuqolJJhg3zaEpSITJkcW4.iFISBS', 'users/user123.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 'Kayapınar', 'Serdar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-03 02:50:03', '2022-01-03 02:50:03', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'gmail', 'serdarkype58583406@gmail.com', NULL, 'users/userserdarkype58583406@gmail.com.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 'Lab12', 'Xelero2333', '1995-12-24 00:00:00', 'Tunisie', '+4955555444', 'Homme', 'XeleroRR', 'Xelero.io', 'Allemagne', '2022-01-03 09:16:00', '2022-01-11 14:27:08', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\",\"ExponentPushToken[6Au-3fLBAcuyluT1UJ7YOf]\"]', NULL, 'gmail', 'xelero.lab@gmail.com', '$2y$10$rBD8nzAoMDDUF6vs6PdLW.HqhYO.9yzQYpc5tBtveNwpHFjkKfxaO', 'users/user125.gif', 3, 3, 3, 1, NULL, 'xelero.lab@gmail.com', 'VQI4ED'),
(126, 'AYACHI', 'Anis', '2000-01-03 00:00:00', 'Afrique du Sud', NULL, 'Homme', 'Xelero', 'Xelero.io', 'Algérie', '2022-01-03 09:31:42', '2022-01-03 09:01:39', NULL, NULL, NULL, NULL, NULL, '[null,\"ExponentPushToken[hPO48jPCfCr_H_PP6jbpYI]\"]', NULL, 'email', 'enis@xelero.io', '$2y$10$ZKxOhmY2xYoWCre.SbFiweVf8UOFv.WmvwPGp1ShNbcSs8SdokuWm', 'user-details/December2021/nP76aDsvdBeRM3JoJcDh.png', 3, 4, 3, NULL, NULL, NULL, NULL),
(127, 'meddeb', 'sabri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-03 10:53:42', '2022-01-07 23:34:35', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'linkedin', 'sabri.meddeb@spectrumgroupe.fr', NULL, 'users/user127.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 'Habib', 'Aroua', '2022-01-05 11:01:47', 'Tunisie', '+21626053091', 'Homme', 'Xelero', 'www.xelero.com', 'Tunisie', '2022-01-03 14:31:11', '2022-01-13 09:34:46', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\",\"ExponentPushToken[EjG0yfDvG4OePTt6uj27je]\",\"ExponentPushToken[EjG0yfDvG4OePTt6uj27je]\"]', NULL, 'linkedin', 'habibha.aroua82@gmail.com', '$2y$10$oWk1GKLb0yAg.AIRyBuvauht4nQV/EKqRRAAsZgmUMS0iQrd6EvTS', 'users/user128.gif', 3, NULL, 6, 2, NULL, 'habibha.aroua82@gmail.com', 'VTDLZJ'),
(129, 'Abdelkefi', 'Haithem', '2022-01-04 13:19:57', 'Tunisie', '+21695968035', NULL, 'Club DSI Tunisie', 'www.club-dsi.tn', 'Tunisie', '2022-01-04 13:14:58', '2022-01-04 13:19:57', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[ZDFvXMNngRP-3ySL4vZi3x]\"]', NULL, 'gmail', 'haithem.abdelkefi.216@gmail.com', '$2y$10$6PzkfpozjSM14SdRJ.RJRunwn07AlZLnTTU1iUXaRAsVlDMi6mcXO', 'user-details/December2021/nP76aDsvdBeRM3JoJcDh.png', 3, 2, 7, NULL, NULL, NULL, NULL),
(130, 'Yousfi', 'Mohamed amine', '1992-12-19 00:00:00', 'Tunisie', '+21621647479', 'Homme', 'Xelero', 'Www.xelero.io', 'Tunisie', '2022-01-05 10:43:57', '2022-01-13 09:39:22', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\",\"hello\",\"ExponentPushToken[GxGnncCiPmVNX5rpa5jZKz]\",\"[expo@token]\"]', NULL, 'gmail', 'mohamedamineyousfi@gmail.com', '$2y$10$VmFtZXPYhNwKq7PuLqUbIeilIrsZOTu/A4SLW.lm65QYZVyhlO50y', 'user-details/December2021/nP76aDsvdBeRM3JoJcDh.png', 4, 3, 6, 2, NULL, 'mohamedamineyousfi@gmail.com', 'ZGFO7Y'),
(131, 'Bahri', 'Sami', '2022-01-05 16:27:54', 'Tunisie', NULL, 'Homme', 'Lifespaces', 'Lifespac.es', 'Tunisie', '2022-01-05 16:24:59', '2022-01-05 16:27:54', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'appel', 'vwxr5x869j@privaterelay.appleid.com', '$2y$10$ssj99PoqGJpLck88r94TMuyVF/UBSa9sWFroxtkOfGQOApdyv647a', 'user-details/December2021/nP76aDsvdBeRM3JoJcDh.png', 3, 4, 7, NULL, '001808.46c1c157d9fc4ca2a9143d86c4a449eb.1624', NULL, NULL),
(132, 'Ziadi', 'Nada', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 16:29:45', '2022-01-05 16:29:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'linkedin', 'nadou143@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 16:30:40', '2022-01-05 16:30:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'pdg@tunisie-afrique-export.com', '$2y$10$zpUKCriuRnzGhqwhUyJj0euwZsXcxrmOFm8Tkgy7wT2FoJVkopiUK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 'Bahri', 'Sami', '2022-01-05 16:33:56', 'Tunisie', NULL, 'Homme', 'lifespaces', 'lifespac.es', 'Tunisie', '2022-01-05 16:31:28', '2022-01-05 16:01:01', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'email', 'sami.bahri@forumtn.net', '$2y$10$.AWAwg/eyk2Zy.yiA.WuAeN2nRe9GVHCRbFv90B1YUxUez7Sq9pWq', 'user-details/December2021/nP76aDsvdBeRM3JoJcDh.png', 3, 4, 7, NULL, NULL, NULL, NULL),
(135, 'Bonnemains', 'Carole', '2022-01-05 19:37:08', 'France', '+330783341856', 'Femme', 'Mbc solutions', 'https://www.mbcsolutions.fr', 'France', '2022-01-05 17:07:14', '2022-01-05 19:37:08', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[SIMULATOR]\"]', NULL, 'linkedin', 'sandok@hotmail.fr', '$2y$10$duEUDY6yJwSc1wIoM5Ysx.Whm4KZMmrzjicL9tYx/hXEkGr90WiAq', 'user-details/December2021/nP76aDsvdBeRM3JoJcDh.png', 3, 4, 7, NULL, NULL, NULL, NULL),
(136, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-08 16:51:41', '2022-01-08 16:51:41', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[k3BPkeAK91yWb0apfYl5yu]\"]', NULL, 'appel', 'kasimarslan33@icloud.com', NULL, NULL, NULL, NULL, NULL, NULL, '000817.a96b1993a93e4de583f5aa5be88839ff.1651', NULL, NULL),
(143, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-13 16:38:25', '2022-01-13 16:39:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'habib.aroua@hotmail.fr', '$2y$10$q2VGsldrnxrd.DDOWPBRneivPNDELdMV9OTrHieqb3GCOAc0bYQlO', 'users/user143.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, 'AMANI', 'kouassi camilie', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-13 20:11:46', '2022-01-13 20:11:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'linkedin', 'kamskovamani@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-16 18:04:27', '2022-01-16 18:07:21', NULL, NULL, NULL, NULL, NULL, '[\"ExponentPushToken[Vjf0gYMLTc3N8Kzt09J881]\"]', NULL, 'appel', 'Ikolosarahdieuveille@gmail.com', NULL, 'users/user145.gif', NULL, NULL, NULL, NULL, '000052.d49722c4aa9c4a9aa22b75ecdb5e68dd.1803', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_device` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_prog` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_like` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `notifications`
--

INSERT INTO `notifications` (`id`, `titre`, `type`, `token_device`, `id_user`, `id_prog`, `created_at`, `updated_at`, `id_like`) VALUES
(1, 'Upcoming workshop', 'Programme', 'token[]', 130, 33, '2022-01-05 14:40:00', '2022-01-05 15:00:24', NULL),
(2, 'Upcoming workshop', 'Programme', 'token[]', 130, 32, '2022-01-05 14:42:13', '2022-01-05 14:42:13', NULL),
(3, 'Upcoming workshop', 'Programme', 'token[]', 130, 30, '2022-01-05 14:42:51', '2022-01-05 14:42:51', NULL),
(4, 'Permission b2b', 'L’accès b2b', '[\"ExponentPushToken[SIMULATOR]\"]', 130, NULL, '2022-01-05 15:33:36', '2022-01-05 15:33:36', NULL),
(6, 'Vous avez matché avec l\'utilisateur', 'connect', '[\"ExponentPushToken[SIMULATOR]\"]', 130, NULL, '2022-01-06 09:16:00', '2022-01-06 09:42:08', 128),
(7, 'Vous avez matché avec l\'utilisateur', 'connect', '[\"ExponentPushToken[SIMULATOR]\"]', 128, NULL, '2022-01-06 09:16:00', '2022-01-06 09:41:49', 130),
(8, 'Vous avez matché avec l\'utilisateur', 'connect', '[\"ExponentPushToken[SIMULATOR]\"]', 125, NULL, '2022-01-07 14:38:46', '2022-01-07 14:38:46', 130),
(9, 'Vous avez matché avec l\'utilisateur', 'connect', '[\"ExponentPushToken[SIMULATOR]\"]', 130, NULL, '2022-01-07 14:38:46', '2022-01-07 14:38:46', 125),
(10, 'test', 'manuelle notification', NULL, 111, NULL, '2022-01-10 13:47:39', '2022-01-10 13:47:39', NULL),
(11, 'test', 'manuelle notification', NULL, 130, NULL, '2022-01-10 13:47:39', '2022-01-10 13:47:39', NULL),
(12, ' ', 'manuelle notification', NULL, 126, NULL, '2022-01-12 10:40:06', '2022-01-12 10:40:06', NULL),
(13, ' ', 'manuelle notification', NULL, 126, NULL, '2022-01-12 10:40:42', '2022-01-12 10:40:42', NULL),
(14, 'test test', 'manuelle notification', NULL, 97, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(15, 'test test', 'manuelle notification', NULL, 100, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(16, 'test test', 'manuelle notification', NULL, 111, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(17, 'test test', 'manuelle notification', NULL, 112, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(18, 'test test', 'manuelle notification', NULL, 125, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(19, 'test test', 'manuelle notification', NULL, 128, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(20, 'test test', 'manuelle notification', NULL, 129, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(21, 'test test', 'manuelle notification', NULL, 130, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(22, 'test test', 'manuelle notification', NULL, 131, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(23, 'test test', 'manuelle notification', NULL, 134, NULL, '2022-01-12 10:46:40', '2022-01-12 10:46:40', NULL),
(24, 'test test text', 'manuelle notification', NULL, 97, NULL, '2022-01-13 08:36:29', '2022-01-13 08:36:29', NULL),
(25, 'test test text', 'manuelle notification', NULL, 98, NULL, '2022-01-13 08:36:29', '2022-01-13 08:36:29', NULL),
(26, 'test test text', 'manuelle notification', NULL, 100, NULL, '2022-01-13 08:36:29', '2022-01-13 08:36:29', NULL),
(27, 'test test', 'manuelle notification', NULL, 97, NULL, '2022-01-13 08:40:06', '2022-01-13 08:40:06', NULL),
(28, 'test test', 'manuelle notification', NULL, 98, NULL, '2022-01-13 08:40:06', '2022-01-13 08:40:06', NULL),
(29, 'test test', 'manuelle notification', NULL, 100, NULL, '2022-01-13 08:40:06', '2022-01-13 08:40:06', NULL),
(30, 'test test', 'manuelle notification', NULL, 110, NULL, '2022-01-13 08:40:09', '2022-01-13 08:40:09', NULL),
(31, 'test test', 'manuelle notification', NULL, 111, NULL, '2022-01-13 08:40:10', '2022-01-13 08:40:10', NULL),
(32, 'test test', 'manuelle notification', NULL, 112, NULL, '2022-01-13 08:40:10', '2022-01-13 08:40:10', NULL),
(33, 'test test', 'manuelle notification', NULL, 120, NULL, '2022-01-13 08:40:10', '2022-01-13 08:40:10', NULL),
(34, 'test test', 'manuelle notification', NULL, 121, NULL, '2022-01-13 08:40:10', '2022-01-13 08:40:10', NULL),
(35, 'test test', 'manuelle notification', NULL, 122, NULL, '2022-01-13 08:40:10', '2022-01-13 08:40:10', NULL),
(36, 'test test', 'manuelle notification', NULL, 123, NULL, '2022-01-13 08:40:11', '2022-01-13 08:40:11', NULL),
(37, 'test test', 'manuelle notification', NULL, 124, NULL, '2022-01-13 08:40:11', '2022-01-13 08:40:11', NULL),
(38, 'test test', 'manuelle notification', NULL, 125, NULL, '2022-01-13 08:40:11', '2022-01-13 08:40:11', NULL),
(39, 'test test', 'manuelle notification', NULL, 126, NULL, '2022-01-13 08:40:12', '2022-01-13 08:40:12', NULL),
(40, 'test test', 'manuelle notification', NULL, 127, NULL, '2022-01-13 08:40:12', '2022-01-13 08:40:12', NULL),
(41, 'test test', 'manuelle notification', NULL, 128, NULL, '2022-01-13 08:40:12', '2022-01-13 08:40:12', NULL),
(42, 'test test', 'manuelle notification', NULL, 129, NULL, '2022-01-13 08:40:14', '2022-01-13 08:40:14', NULL),
(43, 'test test', 'manuelle notification', NULL, 130, NULL, '2022-01-13 08:40:15', '2022-01-13 08:40:15', NULL),
(44, 'test test', 'manuelle notification', NULL, 131, NULL, '2022-01-13 08:40:17', '2022-01-13 08:40:17', NULL),
(45, 'test test', 'manuelle notification', NULL, 132, NULL, '2022-01-13 08:40:17', '2022-01-13 08:40:17', NULL),
(46, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 97, NULL, '2022-01-13 08:49:50', '2022-01-13 08:49:50', NULL),
(47, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 98, NULL, '2022-01-13 08:49:50', '2022-01-13 08:49:50', NULL),
(48, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 100, NULL, '2022-01-13 08:49:50', '2022-01-13 08:49:50', NULL),
(49, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 110, NULL, '2022-01-13 08:49:51', '2022-01-13 08:49:51', NULL),
(50, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 111, NULL, '2022-01-13 08:49:51', '2022-01-13 08:49:51', NULL),
(51, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 112, NULL, '2022-01-13 08:49:51', '2022-01-13 08:49:51', NULL),
(52, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 120, NULL, '2022-01-13 08:49:52', '2022-01-13 08:49:52', NULL),
(53, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 121, NULL, '2022-01-13 08:49:52', '2022-01-13 08:49:52', NULL),
(54, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 122, NULL, '2022-01-13 08:49:52', '2022-01-13 08:49:52', NULL),
(55, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 123, NULL, '2022-01-13 08:49:52', '2022-01-13 08:49:52', NULL),
(56, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 124, NULL, '2022-01-13 08:49:52', '2022-01-13 08:49:52', NULL),
(57, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 125, NULL, '2022-01-13 08:49:52', '2022-01-13 08:49:52', NULL),
(58, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 126, NULL, '2022-01-13 08:49:56', '2022-01-13 08:49:56', NULL),
(59, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 127, NULL, '2022-01-13 08:49:57', '2022-01-13 08:49:57', NULL),
(60, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 128, NULL, '2022-01-13 08:49:57', '2022-01-13 08:49:57', NULL),
(61, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 129, NULL, '2022-01-13 08:49:59', '2022-01-13 08:49:59', NULL),
(62, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 130, NULL, '2022-01-13 08:49:59', '2022-01-13 08:49:59', NULL),
(63, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 131, NULL, '2022-01-13 08:50:01', '2022-01-13 08:50:01', NULL),
(64, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 132, NULL, '2022-01-13 08:50:01', '2022-01-13 08:50:01', NULL),
(65, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 133, NULL, '2022-01-13 08:50:01', '2022-01-13 08:50:01', NULL),
(66, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 134, NULL, '2022-01-13 08:50:01', '2022-01-13 08:50:01', NULL),
(67, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 135, NULL, '2022-01-13 08:50:01', '2022-01-13 08:50:01', NULL),
(68, 'test Bonjour les Tunisiens', 'manuelle notification', NULL, 136, NULL, '2022-01-13 08:50:01', '2022-01-13 08:50:01', NULL),
(69, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 97, NULL, '2022-01-13 08:51:24', '2022-01-13 08:51:24', NULL),
(70, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 98, NULL, '2022-01-13 08:51:24', '2022-01-13 08:51:24', NULL),
(71, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 100, NULL, '2022-01-13 08:51:24', '2022-01-13 08:51:24', NULL),
(72, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 110, NULL, '2022-01-13 08:51:25', '2022-01-13 08:51:25', NULL),
(73, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 111, NULL, '2022-01-13 08:51:26', '2022-01-13 08:51:26', NULL),
(74, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 112, NULL, '2022-01-13 08:51:26', '2022-01-13 08:51:26', NULL),
(75, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 120, NULL, '2022-01-13 08:51:26', '2022-01-13 08:51:26', NULL),
(76, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 121, NULL, '2022-01-13 08:51:26', '2022-01-13 08:51:26', NULL),
(77, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 122, NULL, '2022-01-13 08:51:26', '2022-01-13 08:51:26', NULL),
(78, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 123, NULL, '2022-01-13 08:51:27', '2022-01-13 08:51:27', NULL),
(79, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 124, NULL, '2022-01-13 08:51:27', '2022-01-13 08:51:27', NULL),
(80, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 125, NULL, '2022-01-13 08:51:27', '2022-01-13 08:51:27', NULL),
(81, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 126, NULL, '2022-01-13 08:51:28', '2022-01-13 08:51:28', NULL),
(82, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 127, NULL, '2022-01-13 08:51:29', '2022-01-13 08:51:29', NULL),
(83, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 128, NULL, '2022-01-13 08:51:29', '2022-01-13 08:51:29', NULL),
(84, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 129, NULL, '2022-01-13 08:51:32', '2022-01-13 08:51:32', NULL),
(85, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 130, NULL, '2022-01-13 08:51:32', '2022-01-13 08:51:32', NULL),
(86, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 131, NULL, '2022-01-13 08:51:33', '2022-01-13 08:51:33', NULL),
(87, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 132, NULL, '2022-01-13 08:51:33', '2022-01-13 08:51:33', NULL),
(88, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 133, NULL, '2022-01-13 08:51:33', '2022-01-13 08:51:33', NULL),
(89, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 134, NULL, '2022-01-13 08:51:33', '2022-01-13 08:51:33', NULL),
(90, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 135, NULL, '2022-01-13 08:51:33', '2022-01-13 08:51:33', NULL),
(91, 'Spam Hani n\'spami fikom :p :p', 'manuelle notification', NULL, 136, NULL, '2022-01-13 08:51:33', '2022-01-13 08:51:33', NULL),
(92, 'Permission b2b', 'Permission b2b', '[\"ExponentPushToken[SIMULATOR]\",\"ExponentPushToken[EjG0yfDvG4OePTt6uj27je]\",\"ExponentPushToken[EjG0yfDvG4OePTt6uj27je]\"]', 128, NULL, '2022-01-13 09:27:27', '2022-01-13 09:27:27', NULL),
(93, 'Permission b2b', 'Permission b2b', '[\"ExponentPushToken[SIMULATOR]\",\"ExponentPushToken[EjG0yfDvG4OePTt6uj27je]\",\"ExponentPushToken[EjG0yfDvG4OePTt6uj27je]\"]', 128, NULL, '2022-01-13 09:34:46', '2022-01-13 09:34:46', NULL),
(94, 'Permission b2b', 'Permission b2b', '[\"ExponentPushToken[SIMULATOR]\",\"hello\",\"ExponentPushToken[GxGnncCiPmVNX5rpa5jZKz]\",\"[expo@token]\"]', 130, NULL, '2022-01-13 09:39:22', '2022-01-13 09:39:22', NULL),
(95, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 97, 18, '2022-01-13 14:19:54', '2022-01-13 14:19:54', NULL),
(96, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 98, 18, '2022-01-13 14:19:54', '2022-01-13 14:19:54', NULL),
(97, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 100, 18, '2022-01-13 14:19:54', '2022-01-13 14:19:54', NULL),
(98, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 110, 18, '2022-01-13 14:19:54', '2022-01-13 14:19:54', NULL),
(99, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 111, 18, '2022-01-13 14:19:56', '2022-01-13 14:19:56', NULL),
(100, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 112, 18, '2022-01-13 14:19:56', '2022-01-13 14:19:56', NULL),
(101, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 120, 18, '2022-01-13 14:19:57', '2022-01-13 14:19:57', NULL),
(102, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 121, 18, '2022-01-13 14:19:57', '2022-01-13 14:19:57', NULL),
(103, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 122, 18, '2022-01-13 14:19:57', '2022-01-13 14:19:57', NULL),
(104, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 123, 18, '2022-01-13 14:19:57', '2022-01-13 14:19:57', NULL),
(105, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 124, 18, '2022-01-13 14:19:57', '2022-01-13 14:19:57', NULL),
(106, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 125, 18, '2022-01-13 14:19:57', '2022-01-13 14:19:57', NULL),
(107, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 126, 18, '2022-01-13 14:19:58', '2022-01-13 14:19:58', NULL),
(108, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 127, 18, '2022-01-13 14:19:59', '2022-01-13 14:19:59', NULL),
(109, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 128, 18, '2022-01-13 14:19:59', '2022-01-13 14:19:59', NULL),
(110, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 129, 18, '2022-01-13 14:20:00', '2022-01-13 14:20:00', NULL),
(111, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 130, 18, '2022-01-13 14:20:00', '2022-01-13 14:20:00', NULL),
(112, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 131, 18, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(113, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 132, 18, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(114, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 133, 18, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(115, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 134, 18, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(116, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 135, 18, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(117, 'Upcoming workshop Arrivée des Délégations Etrangères', 'Programme', NULL, 136, 18, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(118, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 97, 20, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(119, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 98, 20, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(120, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 100, 20, '2022-01-13 14:20:02', '2022-01-13 14:20:02', NULL),
(121, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 110, 20, '2022-01-13 14:20:03', '2022-01-13 14:20:03', NULL),
(122, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 111, 20, '2022-01-13 14:20:04', '2022-01-13 14:20:04', NULL),
(123, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 112, 20, '2022-01-13 14:20:04', '2022-01-13 14:20:04', NULL),
(124, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 120, 20, '2022-01-13 14:20:04', '2022-01-13 14:20:04', NULL),
(125, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 121, 20, '2022-01-13 14:20:04', '2022-01-13 14:20:04', NULL),
(126, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 122, 20, '2022-01-13 14:20:04', '2022-01-13 14:20:04', NULL),
(127, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 123, 20, '2022-01-13 14:20:05', '2022-01-13 14:20:05', NULL),
(128, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 124, 20, '2022-01-13 14:20:05', '2022-01-13 14:20:05', NULL),
(129, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 125, 20, '2022-01-13 14:20:05', '2022-01-13 14:20:05', NULL),
(130, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 126, 20, '2022-01-13 14:20:06', '2022-01-13 14:20:06', NULL),
(131, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 127, 20, '2022-01-13 14:20:07', '2022-01-13 14:20:07', NULL),
(132, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 128, 20, '2022-01-13 14:20:07', '2022-01-13 14:20:07', NULL),
(133, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 129, 20, '2022-01-13 14:20:08', '2022-01-13 14:20:08', NULL),
(134, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 130, 20, '2022-01-13 14:20:09', '2022-01-13 14:20:09', NULL),
(135, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 131, 20, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(136, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 132, 20, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(137, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 133, 20, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(138, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 134, 20, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(139, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 135, 20, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(140, 'Upcoming workshop Inauguration du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 136, 20, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(141, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 97, 21, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(142, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 98, 21, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(143, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 100, 21, '2022-01-13 14:20:10', '2022-01-13 14:20:10', NULL),
(144, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 110, 21, '2022-01-13 14:20:11', '2022-01-13 14:20:11', NULL),
(145, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 111, 21, '2022-01-13 14:20:12', '2022-01-13 14:20:12', NULL),
(146, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 112, 21, '2022-01-13 14:20:12', '2022-01-13 14:20:12', NULL),
(147, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 120, 21, '2022-01-13 14:20:12', '2022-01-13 14:20:12', NULL),
(148, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 121, 21, '2022-01-13 14:20:12', '2022-01-13 14:20:12', NULL),
(149, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 122, 21, '2022-01-13 14:20:12', '2022-01-13 14:20:12', NULL),
(150, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 123, 21, '2022-01-13 14:20:12', '2022-01-13 14:20:12', NULL),
(151, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 124, 21, '2022-01-13 14:20:13', '2022-01-13 14:20:13', NULL),
(152, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 125, 21, '2022-01-13 14:20:13', '2022-01-13 14:20:13', NULL),
(153, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 126, 21, '2022-01-13 14:20:13', '2022-01-13 14:20:13', NULL),
(154, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 127, 21, '2022-01-13 14:20:14', '2022-01-13 14:20:14', NULL),
(155, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 128, 21, '2022-01-13 14:20:14', '2022-01-13 14:20:14', NULL),
(156, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 129, 21, '2022-01-13 14:20:15', '2022-01-13 14:20:15', NULL),
(157, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 130, 21, '2022-01-13 14:20:16', '2022-01-13 14:20:16', NULL),
(158, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 131, 21, '2022-01-13 14:20:18', '2022-01-13 14:20:18', NULL),
(159, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 132, 21, '2022-01-13 14:20:18', '2022-01-13 14:20:18', NULL),
(160, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 133, 21, '2022-01-13 14:20:18', '2022-01-13 14:20:18', NULL),
(161, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 134, 21, '2022-01-13 14:20:18', '2022-01-13 14:20:18', NULL),
(162, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 135, 21, '2022-01-13 14:20:18', '2022-01-13 14:20:18', NULL),
(163, 'Upcoming workshop Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Programme', NULL, 136, 21, '2022-01-13 14:20:19', '2022-01-13 14:20:19', NULL),
(164, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 97, 22, '2022-01-13 14:20:19', '2022-01-13 14:20:19', NULL),
(165, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 98, 22, '2022-01-13 14:20:19', '2022-01-13 14:20:19', NULL),
(166, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 100, 22, '2022-01-13 14:20:19', '2022-01-13 14:20:19', NULL),
(167, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 110, 22, '2022-01-13 14:20:20', '2022-01-13 14:20:20', NULL),
(168, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 111, 22, '2022-01-13 14:20:21', '2022-01-13 14:20:21', NULL),
(169, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 112, 22, '2022-01-13 14:20:21', '2022-01-13 14:20:21', NULL),
(170, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 120, 22, '2022-01-13 14:20:21', '2022-01-13 14:20:21', NULL),
(171, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 121, 22, '2022-01-13 14:20:21', '2022-01-13 14:20:21', NULL),
(172, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 122, 22, '2022-01-13 14:20:21', '2022-01-13 14:20:21', NULL),
(173, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 123, 22, '2022-01-13 14:20:22', '2022-01-13 14:20:22', NULL),
(174, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 124, 22, '2022-01-13 14:20:22', '2022-01-13 14:20:22', NULL),
(175, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 125, 22, '2022-01-13 14:20:22', '2022-01-13 14:20:22', NULL),
(176, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 126, 22, '2022-01-13 14:20:22', '2022-01-13 14:20:22', NULL),
(177, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 127, 22, '2022-01-13 14:20:23', '2022-01-13 14:20:23', NULL),
(178, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 128, 22, '2022-01-13 14:20:23', '2022-01-13 14:20:23', NULL),
(179, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 129, 22, '2022-01-13 14:20:25', '2022-01-13 14:20:25', NULL),
(180, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 130, 22, '2022-01-13 14:20:26', '2022-01-13 14:20:26', NULL),
(181, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 131, 22, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(182, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 132, 22, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(183, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 133, 22, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(184, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 134, 22, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(185, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 135, 22, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(186, 'Upcoming workshop POST-COVID: L’ innovation Made in Africa', 'Programme', NULL, 136, 22, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(187, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 97, 23, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(188, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 98, 23, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(189, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 100, 23, '2022-01-13 14:20:27', '2022-01-13 14:20:27', NULL),
(190, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 110, 23, '2022-01-13 14:20:28', '2022-01-13 14:20:28', NULL),
(191, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 111, 23, '2022-01-13 14:20:29', '2022-01-13 14:20:29', NULL),
(192, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 112, 23, '2022-01-13 14:20:29', '2022-01-13 14:20:29', NULL),
(193, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 120, 23, '2022-01-13 14:20:29', '2022-01-13 14:20:29', NULL),
(194, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 121, 23, '2022-01-13 14:20:29', '2022-01-13 14:20:29', NULL),
(195, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 122, 23, '2022-01-13 14:20:29', '2022-01-13 14:20:29', NULL),
(196, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 123, 23, '2022-01-13 14:20:30', '2022-01-13 14:20:30', NULL),
(197, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 124, 23, '2022-01-13 14:20:30', '2022-01-13 14:20:30', NULL),
(198, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 125, 23, '2022-01-13 14:20:30', '2022-01-13 14:20:30', NULL),
(199, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 126, 23, '2022-01-13 14:20:31', '2022-01-13 14:20:31', NULL),
(200, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 127, 23, '2022-01-13 14:20:31', '2022-01-13 14:20:31', NULL),
(201, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 128, 23, '2022-01-13 14:20:31', '2022-01-13 14:20:31', NULL),
(202, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 129, 23, '2022-01-13 14:20:34', '2022-01-13 14:20:34', NULL),
(203, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 130, 23, '2022-01-13 14:20:34', '2022-01-13 14:20:34', NULL),
(204, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 131, 23, '2022-01-13 14:20:37', '2022-01-13 14:20:37', NULL),
(205, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 132, 23, '2022-01-13 14:20:37', '2022-01-13 14:20:37', NULL),
(206, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 133, 23, '2022-01-13 14:20:37', '2022-01-13 14:20:37', NULL),
(207, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 134, 23, '2022-01-13 14:20:37', '2022-01-13 14:20:37', NULL),
(208, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 135, 23, '2022-01-13 14:20:37', '2022-01-13 14:20:37', NULL),
(209, 'Upcoming workshop FORUM INTERNATIONAL INDUSTRIE 4.0', 'Programme', NULL, 136, 23, '2022-01-13 14:20:37', '2022-01-13 14:20:37', NULL),
(210, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 97, 24, '2022-01-13 14:20:38', '2022-01-13 14:20:38', NULL),
(211, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 98, 24, '2022-01-13 14:20:38', '2022-01-13 14:20:38', NULL),
(212, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 100, 24, '2022-01-13 14:20:38', '2022-01-13 14:20:38', NULL),
(213, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 110, 24, '2022-01-13 14:20:38', '2022-01-13 14:20:38', NULL),
(214, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 111, 24, '2022-01-13 14:20:39', '2022-01-13 14:20:39', NULL),
(215, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 112, 24, '2022-01-13 14:20:39', '2022-01-13 14:20:39', NULL),
(216, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 120, 24, '2022-01-13 14:20:40', '2022-01-13 14:20:40', NULL),
(217, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 121, 24, '2022-01-13 14:20:40', '2022-01-13 14:20:40', NULL),
(218, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 122, 24, '2022-01-13 14:20:40', '2022-01-13 14:20:40', NULL),
(219, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 123, 24, '2022-01-13 14:20:40', '2022-01-13 14:20:40', NULL),
(220, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 124, 24, '2022-01-13 14:20:40', '2022-01-13 14:20:40', NULL),
(221, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 125, 24, '2022-01-13 14:20:40', '2022-01-13 14:20:40', NULL),
(222, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 126, 24, '2022-01-13 14:20:41', '2022-01-13 14:20:41', NULL),
(223, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 127, 24, '2022-01-13 14:20:41', '2022-01-13 14:20:41', NULL),
(224, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 128, 24, '2022-01-13 14:20:41', '2022-01-13 14:20:41', NULL),
(225, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 129, 24, '2022-01-13 14:20:42', '2022-01-13 14:20:42', NULL),
(226, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 130, 24, '2022-01-13 14:20:43', '2022-01-13 14:20:43', NULL),
(227, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 131, 24, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(228, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 132, 24, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(229, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 133, 24, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(230, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 134, 24, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(231, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 135, 24, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(232, 'Upcoming workshop Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Programme', NULL, 136, 24, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(233, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 97, 25, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(234, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 98, 25, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(235, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 100, 25, '2022-01-13 14:20:44', '2022-01-13 14:20:44', NULL),
(236, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 110, 25, '2022-01-13 14:20:45', '2022-01-13 14:20:45', NULL),
(237, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 111, 25, '2022-01-13 14:20:46', '2022-01-13 14:20:46', NULL),
(238, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 112, 25, '2022-01-13 14:20:46', '2022-01-13 14:20:46', NULL),
(239, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 120, 25, '2022-01-13 14:20:46', '2022-01-13 14:20:46', NULL),
(240, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 121, 25, '2022-01-13 14:20:46', '2022-01-13 14:20:46', NULL),
(241, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 122, 25, '2022-01-13 14:20:46', '2022-01-13 14:20:46', NULL),
(242, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 123, 25, '2022-01-13 14:20:47', '2022-01-13 14:20:47', NULL),
(243, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 124, 25, '2022-01-13 14:20:47', '2022-01-13 14:20:47', NULL),
(244, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 125, 25, '2022-01-13 14:20:47', '2022-01-13 14:20:47', NULL),
(245, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 126, 25, '2022-01-13 14:20:47', '2022-01-13 14:20:47', NULL),
(246, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 127, 25, '2022-01-13 14:20:48', '2022-01-13 14:20:48', NULL),
(247, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 128, 25, '2022-01-13 14:20:48', '2022-01-13 14:20:48', NULL),
(248, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 129, 25, '2022-01-13 14:20:49', '2022-01-13 14:20:49', NULL),
(249, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 130, 25, '2022-01-13 14:20:50', '2022-01-13 14:20:50', NULL),
(250, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 131, 25, '2022-01-13 14:20:52', '2022-01-13 14:20:52', NULL),
(251, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 132, 25, '2022-01-13 14:20:52', '2022-01-13 14:20:52', NULL),
(252, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 133, 25, '2022-01-13 14:20:52', '2022-01-13 14:20:52', NULL),
(253, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 134, 25, '2022-01-13 14:20:52', '2022-01-13 14:20:52', NULL),
(254, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 135, 25, '2022-01-13 14:20:52', '2022-01-13 14:20:52', NULL),
(255, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 136, 25, '2022-01-13 14:20:52', '2022-01-13 14:20:52', NULL),
(256, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 97, 26, '2022-01-13 14:20:53', '2022-01-13 14:20:53', NULL),
(257, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 98, 26, '2022-01-13 14:20:53', '2022-01-13 14:20:53', NULL),
(258, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 100, 26, '2022-01-13 14:20:53', '2022-01-13 14:20:53', NULL),
(259, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 110, 26, '2022-01-13 14:20:53', '2022-01-13 14:20:53', NULL),
(260, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 111, 26, '2022-01-13 14:20:54', '2022-01-13 14:20:54', NULL),
(261, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 112, 26, '2022-01-13 14:20:54', '2022-01-13 14:20:54', NULL),
(262, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 120, 26, '2022-01-13 14:20:55', '2022-01-13 14:20:55', NULL),
(263, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 121, 26, '2022-01-13 14:20:55', '2022-01-13 14:20:55', NULL),
(264, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 122, 26, '2022-01-13 14:20:55', '2022-01-13 14:20:55', NULL),
(265, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 123, 26, '2022-01-13 14:20:55', '2022-01-13 14:20:55', NULL),
(266, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 124, 26, '2022-01-13 14:20:56', '2022-01-13 14:20:56', NULL),
(267, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 125, 26, '2022-01-13 14:20:56', '2022-01-13 14:20:56', NULL),
(268, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 126, 26, '2022-01-13 14:20:57', '2022-01-13 14:20:57', NULL),
(269, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 127, 26, '2022-01-13 14:20:58', '2022-01-13 14:20:58', NULL),
(270, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 128, 26, '2022-01-13 14:20:58', '2022-01-13 14:20:58', NULL),
(271, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 129, 26, '2022-01-13 14:20:59', '2022-01-13 14:20:59', NULL),
(272, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 130, 26, '2022-01-13 14:21:00', '2022-01-13 14:21:00', NULL),
(273, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 131, 26, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(274, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 132, 26, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(275, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 133, 26, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(276, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 134, 26, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(277, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 135, 26, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(278, 'Upcoming workshop FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'Programme', NULL, 136, 26, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(279, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 97, 27, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(280, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 98, 27, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(281, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 100, 27, '2022-01-13 14:21:02', '2022-01-13 14:21:02', NULL),
(282, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 110, 27, '2022-01-13 14:21:03', '2022-01-13 14:21:03', NULL),
(283, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 111, 27, '2022-01-13 14:21:04', '2022-01-13 14:21:04', NULL),
(284, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 112, 27, '2022-01-13 14:21:04', '2022-01-13 14:21:04', NULL),
(285, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 120, 27, '2022-01-13 14:21:04', '2022-01-13 14:21:04', NULL),
(286, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 121, 27, '2022-01-13 14:21:04', '2022-01-13 14:21:04', NULL),
(287, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 122, 27, '2022-01-13 14:21:04', '2022-01-13 14:21:04', NULL),
(288, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 123, 27, '2022-01-13 14:21:04', '2022-01-13 14:21:04', NULL),
(289, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 124, 27, '2022-01-13 14:21:05', '2022-01-13 14:21:05', NULL),
(290, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 125, 27, '2022-01-13 14:21:05', '2022-01-13 14:21:05', NULL),
(291, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 126, 27, '2022-01-13 14:21:05', '2022-01-13 14:21:05', NULL),
(292, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 127, 27, '2022-01-13 14:21:06', '2022-01-13 14:21:06', NULL),
(293, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 128, 27, '2022-01-13 14:21:06', '2022-01-13 14:21:06', NULL),
(294, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 129, 27, '2022-01-13 14:21:07', '2022-01-13 14:21:07', NULL),
(295, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 130, 27, '2022-01-13 14:21:07', '2022-01-13 14:21:07', NULL),
(296, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 131, 27, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(297, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 132, 27, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(298, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 133, 27, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(299, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 134, 27, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(300, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 135, 27, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(301, 'Upcoming workshop Rencontres B2B', 'Programme', NULL, 136, 27, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(302, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 97, 28, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(303, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 98, 28, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(304, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 100, 28, '2022-01-13 14:21:09', '2022-01-13 14:21:09', NULL),
(305, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 110, 28, '2022-01-13 14:21:10', '2022-01-13 14:21:10', NULL),
(306, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 111, 28, '2022-01-13 14:21:11', '2022-01-13 14:21:11', NULL),
(307, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 112, 28, '2022-01-13 14:21:11', '2022-01-13 14:21:11', NULL),
(308, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 120, 28, '2022-01-13 14:21:12', '2022-01-13 14:21:12', NULL),
(309, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 121, 28, '2022-01-13 14:21:12', '2022-01-13 14:21:12', NULL),
(310, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 122, 28, '2022-01-13 14:21:12', '2022-01-13 14:21:12', NULL),
(311, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 123, 28, '2022-01-13 14:21:12', '2022-01-13 14:21:12', NULL),
(312, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 124, 28, '2022-01-13 14:21:12', '2022-01-13 14:21:12', NULL),
(313, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 125, 28, '2022-01-13 14:21:12', '2022-01-13 14:21:12', NULL),
(314, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 126, 28, '2022-01-13 14:21:13', '2022-01-13 14:21:13', NULL),
(315, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 127, 28, '2022-01-13 14:21:14', '2022-01-13 14:21:14', NULL),
(316, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 128, 28, '2022-01-13 14:21:14', '2022-01-13 14:21:14', NULL),
(317, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 129, 28, '2022-01-13 14:21:15', '2022-01-13 14:21:15', NULL),
(318, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 130, 28, '2022-01-13 14:21:15', '2022-01-13 14:21:15', NULL),
(319, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 131, 28, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(320, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 132, 28, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(321, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 133, 28, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(322, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 134, 28, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(323, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 135, 28, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(324, 'Upcoming workshop FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'Programme', NULL, 136, 28, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(325, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 97, 29, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(326, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 98, 29, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(327, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 100, 29, '2022-01-13 14:21:16', '2022-01-13 14:21:16', NULL),
(328, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 110, 29, '2022-01-13 14:21:17', '2022-01-13 14:21:17', NULL),
(329, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 111, 29, '2022-01-13 14:21:18', '2022-01-13 14:21:18', NULL),
(330, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 112, 29, '2022-01-13 14:21:18', '2022-01-13 14:21:18', NULL),
(331, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 120, 29, '2022-01-13 14:21:18', '2022-01-13 14:21:18', NULL),
(332, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 121, 29, '2022-01-13 14:21:18', '2022-01-13 14:21:18', NULL),
(333, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 122, 29, '2022-01-13 14:21:18', '2022-01-13 14:21:18', NULL),
(334, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 123, 29, '2022-01-13 14:21:19', '2022-01-13 14:21:19', NULL),
(335, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 124, 29, '2022-01-13 14:21:19', '2022-01-13 14:21:19', NULL),
(336, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 125, 29, '2022-01-13 14:21:19', '2022-01-13 14:21:19', NULL),
(337, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 126, 29, '2022-01-13 14:21:20', '2022-01-13 14:21:20', NULL),
(338, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 127, 29, '2022-01-13 14:21:20', '2022-01-13 14:21:20', NULL),
(339, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 128, 29, '2022-01-13 14:21:20', '2022-01-13 14:21:20', NULL),
(340, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 129, 29, '2022-01-13 14:21:21', '2022-01-13 14:21:21', NULL),
(341, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 130, 29, '2022-01-13 14:21:22', '2022-01-13 14:21:22', NULL),
(342, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 131, 29, '2022-01-13 14:21:24', '2022-01-13 14:21:24', NULL),
(343, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 132, 29, '2022-01-13 14:21:24', '2022-01-13 14:21:24', NULL),
(344, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 133, 29, '2022-01-13 14:21:24', '2022-01-13 14:21:24', NULL),
(345, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 134, 29, '2022-01-13 14:21:24', '2022-01-13 14:21:24', NULL);
INSERT INTO `notifications` (`id`, `titre`, `type`, `token_device`, `id_user`, `id_prog`, `created_at`, `updated_at`, `id_like`) VALUES
(346, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 135, 29, '2022-01-13 14:21:24', '2022-01-13 14:21:24', NULL),
(347, 'Upcoming workshop Poursuite des Rencontres B2B', 'Programme', NULL, 136, 29, '2022-01-13 14:21:24', '2022-01-13 14:21:24', NULL),
(348, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 97, 30, '2022-01-13 14:21:25', '2022-01-13 14:21:25', NULL),
(349, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 98, 30, '2022-01-13 14:21:25', '2022-01-13 14:21:25', NULL),
(350, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 100, 30, '2022-01-13 14:21:25', '2022-01-13 14:21:25', NULL),
(351, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 110, 30, '2022-01-13 14:21:26', '2022-01-13 14:21:26', NULL),
(352, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 111, 30, '2022-01-13 14:21:27', '2022-01-13 14:21:27', NULL),
(353, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 112, 30, '2022-01-13 14:21:27', '2022-01-13 14:21:27', NULL),
(354, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 120, 30, '2022-01-13 14:21:27', '2022-01-13 14:21:27', NULL),
(355, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 121, 30, '2022-01-13 14:21:27', '2022-01-13 14:21:27', NULL),
(356, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 122, 30, '2022-01-13 14:21:27', '2022-01-13 14:21:27', NULL),
(357, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 123, 30, '2022-01-13 14:21:28', '2022-01-13 14:21:28', NULL),
(358, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 124, 30, '2022-01-13 14:21:28', '2022-01-13 14:21:28', NULL),
(359, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 125, 30, '2022-01-13 14:21:28', '2022-01-13 14:21:28', NULL),
(360, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 126, 30, '2022-01-13 14:21:29', '2022-01-13 14:21:29', NULL),
(361, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 127, 30, '2022-01-13 14:21:29', '2022-01-13 14:21:29', NULL),
(362, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 128, 30, '2022-01-13 14:21:29', '2022-01-13 14:21:29', NULL),
(363, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 129, 30, '2022-01-13 14:21:31', '2022-01-13 14:21:31', NULL),
(364, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 130, 30, '2022-01-13 14:21:32', '2022-01-13 14:21:32', NULL),
(365, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 131, 30, '2022-01-13 14:21:33', '2022-01-13 14:21:33', NULL),
(366, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 132, 30, '2022-01-13 14:21:33', '2022-01-13 14:21:33', NULL),
(367, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 133, 30, '2022-01-13 14:21:33', '2022-01-13 14:21:33', NULL),
(368, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 134, 30, '2022-01-13 14:21:33', '2022-01-13 14:21:33', NULL),
(369, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 135, 30, '2022-01-13 14:21:33', '2022-01-13 14:21:33', NULL),
(370, 'Upcoming workshop FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'Programme', NULL, 136, 30, '2022-01-13 14:21:33', '2022-01-13 14:21:33', NULL),
(371, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 97, 31, '2022-01-13 14:21:34', '2022-01-13 14:21:34', NULL),
(372, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 98, 31, '2022-01-13 14:21:34', '2022-01-13 14:21:34', NULL),
(373, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 100, 31, '2022-01-13 14:21:34', '2022-01-13 14:21:34', NULL),
(374, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 110, 31, '2022-01-13 14:21:35', '2022-01-13 14:21:35', NULL),
(375, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 111, 31, '2022-01-13 14:21:36', '2022-01-13 14:21:36', NULL),
(376, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 112, 31, '2022-01-13 14:21:36', '2022-01-13 14:21:36', NULL),
(377, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 120, 31, '2022-01-13 14:21:36', '2022-01-13 14:21:36', NULL),
(378, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 121, 31, '2022-01-13 14:21:36', '2022-01-13 14:21:36', NULL),
(379, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 122, 31, '2022-01-13 14:21:36', '2022-01-13 14:21:36', NULL),
(380, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 123, 31, '2022-01-13 14:21:37', '2022-01-13 14:21:37', NULL),
(381, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 124, 31, '2022-01-13 14:21:37', '2022-01-13 14:21:37', NULL),
(382, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 125, 31, '2022-01-13 14:21:37', '2022-01-13 14:21:37', NULL),
(383, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 126, 31, '2022-01-13 14:21:37', '2022-01-13 14:21:37', NULL),
(384, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 127, 31, '2022-01-13 14:21:39', '2022-01-13 14:21:39', NULL),
(385, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 128, 31, '2022-01-13 14:21:39', '2022-01-13 14:21:39', NULL),
(386, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 129, 31, '2022-01-13 14:21:40', '2022-01-13 14:21:40', NULL),
(387, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 130, 31, '2022-01-13 14:21:41', '2022-01-13 14:21:41', NULL),
(388, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 131, 31, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(389, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 132, 31, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(390, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 133, 31, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(391, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 134, 31, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(392, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 135, 31, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(393, 'Upcoming workshop FORUM INTERNATIONAL E-AGRICULTURE', 'Programme', NULL, 136, 31, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(394, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 97, 32, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(395, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 98, 32, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(396, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 100, 32, '2022-01-13 14:21:42', '2022-01-13 14:21:42', NULL),
(397, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 110, 32, '2022-01-13 14:21:43', '2022-01-13 14:21:43', NULL),
(398, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 111, 32, '2022-01-13 14:21:44', '2022-01-13 14:21:44', NULL),
(399, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 112, 32, '2022-01-13 14:21:44', '2022-01-13 14:21:44', NULL),
(400, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 120, 32, '2022-01-13 14:21:44', '2022-01-13 14:21:44', NULL),
(401, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 121, 32, '2022-01-13 14:21:45', '2022-01-13 14:21:45', NULL),
(402, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 122, 32, '2022-01-13 14:21:45', '2022-01-13 14:21:45', NULL),
(403, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 123, 32, '2022-01-13 14:21:45', '2022-01-13 14:21:45', NULL),
(404, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 124, 32, '2022-01-13 14:21:45', '2022-01-13 14:21:45', NULL),
(405, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 125, 32, '2022-01-13 14:21:45', '2022-01-13 14:21:45', NULL),
(406, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 126, 32, '2022-01-13 14:21:46', '2022-01-13 14:21:46', NULL),
(407, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 127, 32, '2022-01-13 14:21:47', '2022-01-13 14:21:47', NULL),
(408, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 128, 32, '2022-01-13 14:21:47', '2022-01-13 14:21:47', NULL),
(409, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 129, 32, '2022-01-13 14:21:47', '2022-01-13 14:21:47', NULL),
(410, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 130, 32, '2022-01-13 14:21:48', '2022-01-13 14:21:48', NULL),
(411, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 131, 32, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(412, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 132, 32, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(413, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 133, 32, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(414, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 134, 32, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(415, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 135, 32, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(416, 'Upcoming workshop Poursuite et fin des rencontres B2B', 'Programme', NULL, 136, 32, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(417, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 97, 33, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(418, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 98, 33, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(419, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 100, 33, '2022-01-13 14:21:49', '2022-01-13 14:21:49', NULL),
(420, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 110, 33, '2022-01-13 14:21:50', '2022-01-13 14:21:50', NULL),
(421, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 111, 33, '2022-01-13 14:21:51', '2022-01-13 14:21:51', NULL),
(422, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 112, 33, '2022-01-13 14:21:51', '2022-01-13 14:21:51', NULL),
(423, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 120, 33, '2022-01-13 14:21:51', '2022-01-13 14:21:51', NULL),
(424, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 121, 33, '2022-01-13 14:21:51', '2022-01-13 14:21:51', NULL),
(425, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 122, 33, '2022-01-13 14:21:51', '2022-01-13 14:21:51', NULL),
(426, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 123, 33, '2022-01-13 14:21:52', '2022-01-13 14:21:52', NULL),
(427, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 124, 33, '2022-01-13 14:21:52', '2022-01-13 14:21:52', NULL),
(428, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 125, 33, '2022-01-13 14:21:52', '2022-01-13 14:21:52', NULL),
(429, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 126, 33, '2022-01-13 14:21:53', '2022-01-13 14:21:53', NULL),
(430, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 127, 33, '2022-01-13 14:21:54', '2022-01-13 14:21:54', NULL),
(431, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 128, 33, '2022-01-13 14:21:54', '2022-01-13 14:21:54', NULL),
(432, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 129, 33, '2022-01-13 14:21:54', '2022-01-13 14:21:54', NULL),
(433, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 130, 33, '2022-01-13 14:21:55', '2022-01-13 14:21:55', NULL),
(434, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 131, 33, '2022-01-13 14:21:56', '2022-01-13 14:21:56', NULL),
(435, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 132, 33, '2022-01-13 14:21:56', '2022-01-13 14:21:56', NULL),
(436, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 133, 33, '2022-01-13 14:21:56', '2022-01-13 14:21:56', NULL),
(437, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 134, 33, '2022-01-13 14:21:56', '2022-01-13 14:21:56', NULL),
(438, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 135, 33, '2022-01-13 14:21:56', '2022-01-13 14:21:56', NULL),
(439, 'Upcoming workshop Clôture du salon', 'Programme', NULL, 136, 33, '2022-01-13 14:21:56', '2022-01-13 14:21:56', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `objectifs`
--

CREATE TABLE `objectifs` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `objectifs`
--

INSERT INTO `objectifs` (`id`, `titre`, `created_at`, `updated_at`, `status`) VALUES
(4, 'Découvrir', '2021-04-13 10:37:00', '2021-10-20 11:07:12', 1),
(5, 'Elargir mon réseau professionnel', '2021-04-13 10:37:00', '2021-10-20 11:07:18', 1),
(6, 'Trouver de nouveaux partenaires', '2021-04-13 10:39:00', '2021-10-20 11:07:06', 1),
(7, 'Trouver un emploi', '2021-04-13 10:50:00', '2021-04-13 11:42:54', 1),
(9, 'Relationships', '2021-06-01 07:47:00', '2021-10-20 11:07:00', 1),
(10, 'Collaboration', '2021-11-24 14:50:02', '2021-11-24 14:50:02', 0),
(11, 'Clients', '2022-01-05 19:37:08', '2022-01-05 19:37:08', 0);

-- --------------------------------------------------------

--
-- Structure de la table `organisateurs`
--

CREATE TABLE `organisateurs` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `second_titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `organisateurs`
--

INSERT INTO `organisateurs` (`id`, `titre`, `image`, `created_at`, `updated_at`, `second_titre`) VALUES
(3, 'Tunisie Afrique Export', 'organisateurs/October2021/B9fByfYXkEvc7uJ95PjR.jpg', '2021-10-12 13:49:23', '2021-10-12 13:49:23', NULL),
(4, 'Net Opportunity', 'organisateurs/October2021/9WQK12kkofEDE4k6Auyj.jpg', '2021-10-12 13:49:42', '2021-10-12 13:49:42', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `orgs_events`
--

CREATE TABLE `orgs_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `organisateur_id` int(11) DEFAULT NULL,
  `evenement_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `orgs_events`
--

INSERT INTO `orgs_events` (`id`, `organisateur_id`, `evenement_id`, `created_at`, `updated_at`) VALUES
(3, 3, 5, NULL, NULL),
(4, 4, 5, NULL, NULL),
(5, 3, 7, NULL, NULL),
(6, 4, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2021-03-31 11:37:16', '2021-03-31 11:37:16');

-- --------------------------------------------------------

--
-- Structure de la table `partenaires`
--

CREATE TABLE `partenaires` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `second_titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `partenaires`
--

INSERT INTO `partenaires` (`id`, `titre`, `image`, `created_at`, `updated_at`, `second_titre`) VALUES
(1, 'Xelero', 'partenaires/October2021/SGm1ZT37i1DOwmctWOw3.png', '2021-07-02 13:43:00', '2021-10-12 13:42:40', 'Digital Transformation'),
(3, 'AICTO', 'partenaires/October2021/xd4Pi2oNMMi7FdNjhpid.jpg', '2021-10-12 13:41:46', '2021-10-12 13:41:46', NULL),
(4, 'ABTBEF', 'partenaires/October2021/nLLBeOlesGrduj1V7h4j.jpg', '2021-10-12 13:43:04', '2021-10-12 13:43:04', NULL),
(5, 'Chambre de Commerce et d\'Industrie de la Côte d\'Ivoire', 'partenaires/October2021/fGek4mZnZ4N0ZlAxBbgw.png', '2021-10-12 13:43:40', '2021-10-12 13:43:40', NULL),
(6, 'CHOOSE FRANCE', 'partenaires/October2021/fz3tuZy81FwdmJ7H7dNv.png', '2021-10-12 13:43:58', '2021-10-12 13:43:58', NULL),
(7, 'CLOUD TEMPLE', 'partenaires/October2021/sLZ7n8pZv87Y2dsP1bsC.png', '2021-10-12 13:44:17', '2021-10-12 13:44:17', NULL),
(8, 'Club DSI - Cote d\'Ivoire', 'partenaires/October2021/RsYyCooaOq5POImijzJU.png', '2021-10-12 13:44:37', '2021-10-12 13:44:37', NULL),
(9, 'CONECT', 'partenaires/October2021/V9HiDaZGbKmtDjnvWOqm.jpg', '2021-10-12 13:44:48', '2021-10-12 13:44:48', NULL),
(10, 'CONFEDERATION GENERALE DES ENTREPRISES DE COTE D\'IVOIRE', 'partenaires/October2021/gAHSeRI8sQjGdjFECtz6.png', '2021-10-12 13:45:15', '2021-10-12 13:45:15', NULL),
(12, 'Entreprises Magazine', 'partenaires/October2021/r7kGywq5NKjRNCxBfRVC.png', '2021-10-12 13:45:35', '2021-10-12 13:45:35', NULL),
(13, 'Fondation jeunesse Numérique', 'partenaires/October2021/3DnZRUzivaPePynCWOTb.jpg', '2021-10-12 13:46:04', '2021-10-12 13:46:04', NULL),
(14, 'French Tech Abidjan', 'partenaires/October2021/cyzrfc0OlflpeSjrWTn0.png', '2021-10-12 13:46:21', '2021-10-12 13:46:21', NULL),
(15, 'GOTIC', 'partenaires/October2021/sjGeIhKP2p0jexBttv9X.jpg', '2021-10-12 13:46:39', '2021-10-12 13:46:39', NULL),
(16, 'ASENSIA', 'partenaires/October2021/BesBVzZ0hjcgsEqRaz3Z.png', '2021-10-12 13:46:57', '2021-10-12 13:46:57', NULL),
(17, 'Club DSI - Tunisie', 'partenaires/October2021/X4Tbvd9fdLmSda066REq.png', '2021-10-12 13:47:11', '2021-10-12 13:47:11', NULL),
(18, 'CIO Mag', 'partenaires/October2021/xcRg62MMtYPhDK4LtKN0.png', '2021-10-12 13:47:32', '2021-10-12 13:47:32', NULL),
(19, 'UNETEL', 'partenaires/October2021/VHGBCxENoo4Q6PQd6D87.png', '2021-10-12 13:47:47', '2021-10-12 13:47:47', NULL),
(20, 'UTICA', 'partenaires/October2021/5InJKEeZeojpjcu0sv9z.gif', '2021-10-12 13:47:58', '2021-10-12 13:47:58', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `partenaires_events`
--

CREATE TABLE `partenaires_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `evenement_id` int(11) DEFAULT NULL,
  `partenaire_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `partenaires_events`
--

INSERT INTO `partenaires_events` (`id`, `evenement_id`, `partenaire_id`, `created_at`, `updated_at`) VALUES
(1, 5, 1, NULL, NULL),
(3, 5, 3, NULL, NULL),
(4, 7, 1, NULL, NULL),
(5, 7, 4, NULL, NULL),
(6, 7, 5, NULL, NULL),
(7, 7, 6, NULL, NULL),
(8, 7, 7, NULL, NULL),
(9, 7, 8, NULL, NULL),
(10, 7, 9, NULL, NULL),
(12, 7, 13, NULL, NULL),
(13, 7, 14, NULL, NULL),
(14, 7, 15, NULL, NULL),
(15, 7, 16, NULL, NULL),
(16, 7, 17, NULL, NULL),
(17, 7, 18, NULL, NULL),
(18, 7, 20, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
(2, 'browse_bread', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
(3, 'browse_database', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
(4, 'browse_media', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
(5, 'browse_compass', NULL, '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
(6, 'browse_menus', 'menus', '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
(7, 'read_menus', 'menus', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(8, 'edit_menus', 'menus', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(9, 'add_menus', 'menus', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(10, 'delete_menus', 'menus', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(11, 'browse_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(12, 'read_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(13, 'edit_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(14, 'add_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(15, 'delete_roles', 'roles', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(16, 'browse_users', 'users', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(17, 'read_users', 'users', '2021-03-31 11:36:59', '2021-03-31 11:36:59'),
(18, 'edit_users', 'users', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
(19, 'add_users', 'users', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
(20, 'delete_users', 'users', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
(21, 'browse_settings', 'settings', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
(22, 'read_settings', 'settings', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
(23, 'edit_settings', 'settings', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
(24, 'add_settings', 'settings', '2021-03-31 11:37:00', '2021-03-31 11:37:00'),
(25, 'delete_settings', 'settings', '2021-03-31 11:37:01', '2021-03-31 11:37:01'),
(26, 'browse_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
(27, 'read_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
(28, 'edit_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
(29, 'add_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
(30, 'delete_categories', 'categories', '2021-03-31 11:37:12', '2021-03-31 11:37:12'),
(31, 'browse_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
(32, 'read_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
(33, 'edit_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
(34, 'add_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
(35, 'delete_posts', 'posts', '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
(36, 'browse_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
(37, 'read_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
(38, 'edit_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
(39, 'add_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
(40, 'delete_pages', 'pages', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
(41, 'browse_hooks', NULL, '2021-03-31 11:37:21', '2021-03-31 11:37:21'),
(42, 'browse_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
(43, 'read_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
(44, 'edit_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
(45, 'add_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
(46, 'delete_certifications', 'certifications', '2021-04-08 10:25:25', '2021-04-08 10:25:25'),
(47, 'browse_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
(48, 'read_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
(49, 'edit_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
(50, 'add_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
(51, 'delete_experiences', 'experiences', '2021-04-08 10:25:54', '2021-04-08 10:25:54'),
(52, 'browse_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
(53, 'read_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
(54, 'edit_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
(55, 'add_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
(56, 'delete_formations', 'formations', '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
(57, 'browse_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
(58, 'read_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
(59, 'edit_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
(60, 'add_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
(61, 'delete_mobile_users', 'mobile_users', '2021-04-08 10:26:31', '2021-04-08 10:26:31'),
(62, 'browse_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
(63, 'read_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
(64, 'edit_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
(65, 'add_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
(66, 'delete_interets', 'interets', '2021-04-08 10:26:41', '2021-04-08 10:26:41'),
(67, 'browse_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
(68, 'read_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
(69, 'edit_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
(70, 'add_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
(71, 'delete_objectifs', 'objectifs', '2021-04-08 10:26:54', '2021-04-08 10:26:54'),
(72, 'browse_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
(73, 'read_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
(74, 'edit_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
(75, 'add_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
(76, 'delete_evenements', 'evenements', '2021-04-13 12:56:56', '2021-04-13 12:56:56'),
(82, 'browse_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
(83, 'read_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
(84, 'edit_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
(85, 'add_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
(86, 'delete_speakers', 'speakers', '2021-04-13 12:59:51', '2021-04-13 12:59:51'),
(87, 'browse_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
(88, 'read_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
(89, 'edit_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
(90, 'add_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
(91, 'delete_type_progs', 'type_progs', '2021-04-13 13:00:50', '2021-04-13 13:00:50'),
(92, 'browse_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
(93, 'read_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
(94, 'edit_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
(95, 'add_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
(96, 'delete_programmes', 'programmes', '2021-04-13 13:18:25', '2021-04-13 13:18:25'),
(97, 'browse_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
(98, 'read_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
(99, 'edit_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
(100, 'add_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
(101, 'delete_b2b_meets', 'b2b_meets', '2021-04-13 13:41:07', '2021-04-13 13:41:07'),
(102, 'browse_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
(103, 'read_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
(104, 'edit_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
(105, 'add_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
(106, 'delete_b2b_meet_users', 'b2b_meet_users', '2021-04-14 14:56:45', '2021-04-14 14:56:45'),
(107, 'browse_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
(108, 'read_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
(109, 'edit_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
(110, 'add_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
(111, 'delete_activites', 'activites', '2021-04-16 10:29:25', '2021-04-16 10:29:25'),
(112, 'browse_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
(113, 'read_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
(114, 'edit_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
(115, 'add_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
(116, 'delete_wizard_roles', 'wizard_roles', '2021-04-16 10:29:40', '2021-04-16 10:29:40'),
(117, 'browse_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
(118, 'read_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
(119, 'edit_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
(120, 'add_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
(121, 'delete_boardings', 'boardings', '2021-04-19 12:52:24', '2021-04-19 12:52:24'),
(122, 'browse_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
(123, 'read_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
(124, 'edit_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
(125, 'add_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
(126, 'delete_users_objectifs', 'users_objectifs', '2021-04-19 12:10:27', '2021-04-19 12:10:27'),
(127, 'browse_organisateurs', 'organisateurs', '2021-04-23 09:14:26', '2021-04-23 09:14:26'),
(128, 'read_organisateurs', 'organisateurs', '2021-04-23 09:14:26', '2021-04-23 09:14:26'),
(129, 'edit_organisateurs', 'organisateurs', '2021-04-23 09:14:26', '2021-04-23 09:14:26'),
(130, 'add_organisateurs', 'organisateurs', '2021-04-23 09:14:26', '2021-04-23 09:14:26'),
(131, 'delete_organisateurs', 'organisateurs', '2021-04-23 09:14:26', '2021-04-23 09:14:26'),
(132, 'browse_partenaires', 'partenaires', '2021-04-23 09:14:36', '2021-04-23 09:14:36'),
(133, 'read_partenaires', 'partenaires', '2021-04-23 09:14:36', '2021-04-23 09:14:36'),
(134, 'edit_partenaires', 'partenaires', '2021-04-23 09:14:36', '2021-04-23 09:14:36'),
(135, 'add_partenaires', 'partenaires', '2021-04-23 09:14:36', '2021-04-23 09:14:36'),
(136, 'delete_partenaires', 'partenaires', '2021-04-23 09:14:36', '2021-04-23 09:14:36'),
(137, 'browse_propos', 'propos', '2021-04-23 09:14:51', '2021-04-23 09:14:51'),
(138, 'read_propos', 'propos', '2021-04-23 09:14:51', '2021-04-23 09:14:51'),
(139, 'edit_propos', 'propos', '2021-04-23 09:14:51', '2021-04-23 09:14:51'),
(140, 'add_propos', 'propos', '2021-04-23 09:14:51', '2021-04-23 09:14:51'),
(141, 'delete_propos', 'propos', '2021-04-23 09:14:51', '2021-04-23 09:14:51'),
(142, 'browse_reacts', 'reacts', '2021-11-09 16:05:29', '2021-11-09 16:05:29'),
(143, 'read_reacts', 'reacts', '2021-11-09 16:05:29', '2021-11-09 16:05:29'),
(144, 'edit_reacts', 'reacts', '2021-11-09 16:05:29', '2021-11-09 16:05:29'),
(145, 'add_reacts', 'reacts', '2021-11-09 16:05:29', '2021-11-09 16:05:29'),
(146, 'delete_reacts', 'reacts', '2021-11-09 16:05:29', '2021-11-09 16:05:29'),
(152, 'browse_stands', 'stands', '2021-12-07 11:05:02', '2021-12-07 11:05:02'),
(153, 'read_stands', 'stands', '2021-12-07 11:05:02', '2021-12-07 11:05:02'),
(154, 'edit_stands', 'stands', '2021-12-07 11:05:02', '2021-12-07 11:05:02'),
(155, 'add_stands', 'stands', '2021-12-07 11:05:02', '2021-12-07 11:05:02'),
(156, 'delete_stands', 'stands', '2021-12-07 11:05:02', '2021-12-07 11:05:02'),
(157, 'browse_user_details', 'user_details', '2021-12-13 13:44:37', '2021-12-13 13:44:37'),
(158, 'read_user_details', 'user_details', '2021-12-13 13:44:38', '2021-12-13 13:44:38'),
(159, 'edit_user_details', 'user_details', '2021-12-13 13:44:38', '2021-12-13 13:44:38'),
(160, 'add_user_details', 'user_details', '2021-12-13 13:44:38', '2021-12-13 13:44:38'),
(161, 'delete_user_details', 'user_details', '2021-12-13 13:44:38', '2021-12-13 13:44:38'),
(162, 'browse_notifications', 'notifications', '2022-01-05 14:38:12', '2022-01-05 14:38:12'),
(163, 'read_notifications', 'notifications', '2022-01-05 14:38:12', '2022-01-05 14:38:12'),
(164, 'edit_notifications', 'notifications', '2022-01-05 14:38:12', '2022-01-05 14:38:12'),
(165, 'add_notifications', 'notifications', '2022-01-05 14:38:12', '2022-01-05 14:38:12'),
(166, 'delete_notifications', 'notifications', '2022-01-05 14:38:12', '2022-01-05 14:38:12'),
(167, 'browse_manuel_notifications', 'manuel_notifications', '2022-01-07 10:40:53', '2022-01-07 10:40:53'),
(168, 'read_manuel_notifications', 'manuel_notifications', '2022-01-07 10:40:53', '2022-01-07 10:40:53'),
(169, 'edit_manuel_notifications', 'manuel_notifications', '2022-01-07 10:40:53', '2022-01-07 10:40:53'),
(170, 'add_manuel_notifications', 'manuel_notifications', '2022-01-07 10:40:53', '2022-01-07 10:40:53'),
(171, 'delete_manuel_notifications', 'manuel_notifications', '2022-01-07 10:40:53', '2022-01-07 10:40:53');

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1);

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-31 11:37:14', '2021-03-31 11:37:14'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-31 11:37:15', '2021-03-31 11:37:15');

-- --------------------------------------------------------

--
-- Structure de la table `programmes`
--

CREATE TABLE `programmes` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heure_debut` time DEFAULT NULL,
  `heure_fin` time DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location_programme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_debut_pause` time DEFAULT NULL,
  `date_fin_pause` time DEFAULT NULL,
  `date_prog` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `programmes`
--

INSERT INTO `programmes` (`id`, `titre`, `description`, `heure_debut`, `heure_fin`, `type_id`, `event_id`, `created_at`, `updated_at`, `location_programme`, `date_debut_pause`, `date_fin_pause`, `date_prog`) VALUES
(18, 'Arrivée des Délégations Etrangères', 'Cocktail de bienvenue en l’honneur des Délégations Etrangères offert par les Autorités Ivoiriennes', '20:00:00', '22:00:00', 1, 7, '2021-11-24 10:16:00', '2021-11-24 10:17:31', NULL, NULL, NULL, '2022-02-21'),
(20, 'Inauguration du SITIC AFRICA ABIDJAN 2022', 'Inauguration du SITIC AFRICA ABIDJAN 2022', '10:30:00', '11:30:00', 6, 7, '2021-11-24 10:18:00', '2021-11-24 10:52:38', NULL, NULL, NULL, '2022-02-22'),
(21, 'Ouverture officielle du SITIC AFRICA ABIDJAN 2022', 'Cérémonie d’ouverture officielle du SITIC AFRICA ABIDJAN 2022 et du FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', '11:30:00', '13:00:00', 5, 7, '2021-11-24 10:20:00', '2021-12-07 09:34:02', NULL, NULL, NULL, '2022-02-22'),
(22, 'POST-COVID: L’ innovation Made in Africa', 'Modérateur : Monsieur Kandas Camara, Head of MS&A and Lead Fintech pour la région Afrique de l’Ouest et Centrale VISA Inc •	Banques Africaines : Stratégie et Innovation : quel rôle pour les fintechs africaines Post-covid : Deloitte.   •	Banques Africaines : comment exploiter le potentiel de l’open Innovation : M Khaled Ben Driss, Associé & directeur Exécutive WEVIO. •	Banques, Fintech et opérateurs télécoms : le cocktail pour des paiements digitaux Innovants en Afrique (orange, BOA ou SG, Ms-solutions). •	Sucess story Fintech ( Weblogy, Syca pay, Djamo, Cinetpay)', '15:00:00', '17:30:00', 2, 7, '2021-11-24 10:20:00', '2021-11-24 10:59:26', NULL, NULL, NULL, '2022-02-22'),
(23, 'FORUM INTERNATIONAL INDUSTRIE 4.0', 'FORUM INTERNATIONAL INDUSTRIE 4.0', '15:00:00', '17:30:00', 3, 7, '2021-11-24 10:21:41', '2021-11-24 10:21:41', NULL, NULL, NULL, '2022-02-22'),
(24, 'Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', 'Cocktail dinatoire en l’honneur des Exposants et des Délégations Etrangères', '20:00:00', '22:00:00', 1, 7, '2021-11-24 10:22:12', '2021-11-24 10:22:12', NULL, NULL, NULL, '2022-02-22'),
(25, 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', '10:00:00', '12:30:00', 2, 7, '2021-11-24 10:22:00', '2021-11-24 10:29:50', NULL, NULL, NULL, '2022-02-23'),
(26, 'FORUM INTERNATIONAL E-SANTE EN AFRIQUE', 'FORUM INTERNATIONAL E-SANTE EN AFRIQUE', '10:00:00', '12:30:00', 3, 7, '2021-11-24 10:23:48', '2021-11-24 10:23:48', NULL, NULL, NULL, '2022-02-23'),
(27, 'Rencontres B2B', 'Rencontres B2B', '10:00:00', '13:00:00', 4, 7, '2021-11-24 10:24:28', '2021-11-24 10:24:28', NULL, NULL, NULL, '2022-02-23'),
(28, 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', 'FORUM INTERNATIONAL SUR LES INSTRUMENTS FINANCIERS NUMÉRIQUES INNOVANTS', '15:00:00', '17:30:00', 2, 7, '2021-11-24 10:25:00', '2021-11-24 10:25:56', NULL, NULL, NULL, '2022-02-23'),
(29, 'Poursuite des Rencontres B2B', 'Poursuite des Rencontres B2B', '15:30:00', '18:00:00', 4, 7, '2021-11-24 10:25:37', '2021-11-24 10:25:37', NULL, NULL, NULL, '2022-02-23'),
(30, 'FORUM INTERNATIONAL SUR LA FORMATION 4.0', 'FORUM INTERNATIONAL SUR LA FORMATION 4.0', '10:00:00', '12:30:00', 3, 7, '2021-11-24 10:29:00', '2021-11-24 10:30:02', NULL, NULL, NULL, '2022-02-24'),
(31, 'FORUM INTERNATIONAL E-AGRICULTURE', 'FORUM INTERNATIONAL E-AGRICULTURE', '10:00:00', '12:00:00', 3, 7, '2021-11-24 10:30:37', '2021-11-24 10:30:37', NULL, NULL, NULL, '2022-02-24'),
(32, 'Poursuite et fin des rencontres B2B', 'Poursuite et fin des rencontres B2B', '10:00:00', '13:00:00', 4, 7, '2021-11-24 10:31:19', '2021-11-24 10:31:19', NULL, NULL, NULL, '2022-02-24'),
(33, 'Clôture du salon', 'Clôture du salon', '18:00:00', '18:00:00', 8, 7, '2021-11-24 10:32:00', '2021-11-24 10:52:55', NULL, NULL, NULL, '2022-02-24');

--
-- Déclencheurs `programmes`
--
DELIMITER $$
CREATE TRIGGER `trig_delete_progs` AFTER DELETE ON `programmes` FOR EACH ROW BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (old.titre,'delete','programmes',SYSDATE());
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trig_insert_progs` AFTER INSERT ON `programmes` FOR EACH ROW BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (NEW.titre,'insert','programmes',SYSDATE());
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trig_update_progs` AFTER UPDATE ON `programmes` FOR EACH ROW BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (NEW.titre,'update','programmes',SYSDATE());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `propos`
--

CREATE TABLE `propos` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sous_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reacts`
--

CREATE TABLE `reacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user1` int(11) DEFAULT NULL,
  `id_user2` int(11) DEFAULT NULL,
  `action` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `reacts`
--

INSERT INTO `reacts` (`id`, `id_user1`, `id_user2`, `action`, `created_at`, `updated_at`) VALUES
(14, 79, 75, 'like', '2021-11-10 12:00:20', '2021-11-10 12:00:20'),
(15, 79, 77, 'like', '2021-11-10 12:00:23', '2021-11-10 12:00:23'),
(16, 77, 79, 'like', '2021-11-10 12:00:30', '2021-11-10 12:00:30'),
(17, 77, 75, 'like', '2021-11-10 12:01:42', '2021-11-10 12:01:42'),
(18, 77, 69, 'swipe', '2021-11-10 12:55:33', '2021-11-10 12:55:33'),
(19, 77, 63, 'like', '2021-11-10 14:39:15', '2021-11-10 14:39:15'),
(20, 77, 38, 'like', '2021-11-10 14:55:34', '2021-11-10 14:55:34'),
(21, 77, 65, 'like', '2021-11-10 14:55:37', '2021-11-10 14:55:37'),
(22, 79, 66, 'like', '2021-11-10 15:04:53', '2021-11-10 15:04:53'),
(23, 79, 63, 'like', '2021-11-10 15:04:55', '2021-11-10 15:04:55'),
(24, 79, 38, 'like', '2021-11-10 15:04:58', '2021-11-10 15:04:58'),
(25, 79, 65, 'like', '2021-11-10 15:05:00', '2021-11-10 15:05:00'),
(26, 79, 69, 'like', '2021-11-10 15:05:02', '2021-11-10 15:05:02'),
(27, 77, 66, 'like', '2021-11-12 12:41:10', '2021-11-12 12:41:10'),
(28, 77, 59, 'like', '2021-11-15 12:32:08', '2021-11-15 12:32:08'),
(29, 81, 69, 'like', '2021-11-24 14:53:49', '2021-11-24 14:53:49'),
(30, 81, 38, 'like', '2021-11-24 14:53:59', '2021-11-24 14:53:59'),
(31, 81, 59, 'like', '2021-11-24 14:54:04', '2021-11-24 14:54:04'),
(32, 81, 75, 'like', '2021-11-24 14:54:08', '2021-11-24 14:54:08'),
(33, 38, 59, 'like', '2021-11-24 14:55:36', '2021-11-24 14:55:36'),
(34, 38, 79, 'like', '2021-11-24 14:55:40', '2021-11-24 14:55:40'),
(35, 38, 69, 'like', '2021-11-24 14:56:23', '2021-11-24 14:56:23'),
(36, 38, 81, 'like', '2021-11-24 14:56:27', '2021-11-24 14:56:27'),
(37, 99, 97, 'like', '2021-12-23 00:44:27', '2021-12-23 00:44:27'),
(38, 99, 79, 'like', '2021-12-23 00:44:36', '2021-12-23 00:44:36'),
(39, 99, 90, 'like', '2021-12-23 00:44:44', '2021-12-23 00:44:44'),
(40, 99, 95, 'like', '2021-12-23 00:44:48', '2021-12-23 00:44:48'),
(41, 125, 128, 'like', '2022-01-05 08:39:11', '2022-01-05 08:39:11'),
(42, 125, 110, 'like', '2022-01-05 09:11:10', '2022-01-05 09:11:10'),
(43, 125, 126, 'like', '2022-01-05 09:53:31', '2022-01-05 09:53:31'),
(44, 135, 131, 'like', '2022-01-05 19:39:45', '2022-01-05 19:39:45'),
(45, 135, 98, 'like', '2022-01-05 19:40:02', '2022-01-05 19:40:02'),
(46, 135, 110, 'like', '2022-01-05 19:40:09', '2022-01-05 19:40:09'),
(47, 128, 130, 'like', '2022-01-06 09:16:32', '2022-01-06 09:16:32'),
(48, 130, 128, 'like', '2022-01-06 09:16:46', '2022-01-06 09:16:46'),
(49, 130, 125, 'like', '2022-01-07 14:38:34', '2022-01-07 14:38:34'),
(50, 125, 130, 'like', '2022-01-07 14:38:46', '2022-01-07 14:38:46');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-03-31 11:36:58', '2021-03-31 11:36:58'),
(2, 'user', 'Normal User', '2021-03-31 11:36:58', '2021-03-31 11:36:58');

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'TAE', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'TAE', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings/April2021/bgvMhGMNgrjmyodojbU6.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings/April2021/DZdamJPPwlxYCJBA8L5y.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Back Office', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'TAE', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings/April2021/KPHtdwDLKicEAVnw1gf5.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/April2021/ebmPYEUC3FTsYQy2i8yH.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Structure de la table `speakers`
--

CREATE TABLE `speakers` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `speakers_progs`
--

CREATE TABLE `speakers_progs` (
  `id` int(10) UNSIGNED NOT NULL,
  `speaker_id` int(10) UNSIGNED DEFAULT NULL,
  `programme_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `speakers_progs`
--

INSERT INTO `speakers_progs` (`id`, `speaker_id`, `programme_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 1, 1, NULL, NULL),
(4, 1, 4, NULL, NULL),
(5, 1, 10, NULL, NULL),
(6, 1, 16, NULL, NULL),
(7, 2, 15, NULL, NULL),
(8, 2, 16, NULL, NULL),
(9, 1, 17, NULL, NULL),
(10, 2, 17, NULL, NULL);

--
-- Déclencheurs `speakers_progs`
--
DELIMITER $$
CREATE TRIGGER `trig_delete_speakers` AFTER DELETE ON `speakers_progs` FOR EACH ROW BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (null,'delete','speakers',SYSDATE());
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trig_insert_speakers` AFTER INSERT ON `speakers_progs` FOR EACH ROW BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (null,'insert','speakers',SYSDATE());
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trig_update_speakers` AFTER UPDATE ON `speakers_progs` FOR EACH ROW BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (null,'update','speakers',SYSDATE());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `stands`
--

CREATE TABLE `stands` (
  `id` int(10) UNSIGNED NOT NULL,
  `numero_du_stand` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entreprise` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `stands`
--

INSERT INTO `stands` (`id`, `numero_du_stand`, `entreprise`, `created_at`, `updated_at`) VALUES
(1, 'A10', 'Xelero', '2021-12-07 11:05:18', '2021-12-07 11:05:18');

-- --------------------------------------------------------

--
-- Structure de la table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2021-03-31 11:37:16', '2021-03-31 11:37:16'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2021-03-31 11:37:17', '2021-03-31 11:37:17'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2021-03-31 11:37:18', '2021-03-31 11:37:18'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2021-03-31 11:37:19', '2021-03-31 11:37:19'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2021-03-31 11:37:19', '2021-03-31 11:37:19');

-- --------------------------------------------------------

--
-- Structure de la table `type_progs`
--

CREATE TABLE `type_progs` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `type_progs`
--

INSERT INTO `type_progs` (`id`, `titre`, `created_at`, `updated_at`) VALUES
(1, 'Cocktail', '2021-04-13 13:20:44', '2021-04-13 13:20:44'),
(2, 'Panel', '2021-04-13 13:21:06', '2021-04-13 13:21:06'),
(3, 'Forum', '2021-04-13 13:21:19', '2021-04-13 13:21:19'),
(4, 'B2B meeting', '2021-06-17 15:50:11', '2021-11-24 10:18:51'),
(5, 'Ouverture', '2021-06-30 11:41:05', '2021-06-30 11:41:05'),
(6, 'Inauguration', '2021-11-24 10:18:38', '2021-11-24 10:18:38'),
(7, 'Cérémonie', '2021-11-24 10:19:06', '2021-11-24 10:19:06'),
(8, 'Clôture', '2021-11-24 10:31:39', '2021-11-24 10:31:39');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$wW7/KNKoOtguoBucOE2Jn.HhNM/7SnZcHV1/OVwahzxOpD4N49mQ6', 'aHTESAh21NrXk4oxK5xlQslyDqV2yfEqfAQfmcdvD8kAJq5GgXZd2HHbGT3Z', NULL, '2021-03-31 11:37:13', '2021-03-31 11:37:13'),
(2, 1, 'Admin', 'tae@xelero.io', 'users/April2021/niWGtZ4BG2p0XCGKVZtd.png', NULL, '$2y$10$REhBTrkiFOiiuPKI.d9jveYJe3fW0DEQAVPNrZE//VoJrMWjqBUeC', 'RY4mUw3yp39TNKKqIHytbcpP53dIh2MSa8i7fqdOTV6zlbGNlZ51mFGFVk83', '{\"locale\":\"fr\"}', '2021-04-06 10:59:43', '2021-04-06 10:59:43'),
(3, 1, 'enis', 'enis.a@xelero.io', 'users/June2021/QKsFIIXGLsN4cfpHXi1l.JPG', NULL, '$2y$10$MR3FADqjcGcYJ9w.ImC4h.5r8zOUgK.GicGXh6WLWG81vIJXnFrUy', 'ibeCQDhvzLN0te0DadA4dJQYsYRe81GYTPOvJjtt7r5sNnxOhubwinoprIzR', '{\"locale\":\"fr\"}', '2021-04-13 10:05:07', '2021-06-03 08:09:53');

-- --------------------------------------------------------

--
-- Structure de la table `users_discussions`
--

CREATE TABLE `users_discussions` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_discu` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users_discussions`
--

INSERT INTO `users_discussions` (`id`, `id_user`, `id_discu`, `created_at`, `updated_at`) VALUES
(61, 125, 31, '2022-01-03 10:47:29', '2022-01-03 10:47:29'),
(62, 126, 31, '2022-01-03 10:47:29', '2022-01-03 10:47:29');

-- --------------------------------------------------------

--
-- Structure de la table `users_interets`
--

CREATE TABLE `users_interets` (
  `id` int(10) UNSIGNED NOT NULL,
  `mobile_user_id` int(10) UNSIGNED NOT NULL,
  `interet_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users_interets`
--

INSERT INTO `users_interets` (`id`, `mobile_user_id`, `interet_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 4, 3, '2021-04-13 10:37:34', '2021-04-13 10:37:34'),
(4, 4, 4, '2021-04-13 10:37:34', '2021-04-13 10:37:34'),
(5, 4, 5, '2021-04-13 10:39:48', '2021-04-13 10:39:48'),
(6, 4, 6, '2021-04-13 10:39:48', '2021-04-13 10:39:48'),
(7, 5, 6, '2021-04-13 10:50:10', '2021-04-13 10:50:10'),
(8, 5, 7, '2021-04-13 10:50:10', '2021-04-13 10:50:10'),
(9, 10, 3, '2021-04-16 09:41:37', '2021-04-16 09:41:37'),
(10, 10, 6, '2021-04-16 09:41:37', '2021-04-16 09:41:37'),
(11, 10, 8, '2021-06-01 07:47:49', '2021-06-01 07:47:49'),
(12, 14, 9, '2021-06-08 08:38:56', '2021-06-08 08:38:56'),
(13, 13, 10, '2021-06-08 10:18:15', '2021-06-08 10:18:15'),
(14, 13, 11, '2021-06-08 10:18:15', '2021-06-08 10:18:15'),
(15, 20, 12, '2021-08-04 10:43:17', '2021-08-04 10:43:17'),
(16, 21, 8, '2021-08-12 15:52:59', '2021-08-12 15:52:59'),
(17, 32, 4, '2021-09-28 11:33:42', '2021-09-28 11:33:43'),
(18, 33, 13, '2021-09-28 11:47:46', '2021-09-28 11:47:47'),
(19, 35, 7, '2021-09-28 16:35:43', '2021-09-28 16:35:43'),
(20, 38, 4, '2021-09-29 13:54:40', '2021-09-29 13:54:40'),
(21, 38, 14, '2021-09-29 13:54:40', '2021-09-29 13:54:40'),
(22, 38, 16, '2021-09-29 13:54:40', '2021-09-29 13:54:40'),
(23, 39, 7, '2021-10-11 14:06:41', '2021-10-11 14:06:42'),
(24, 39, 4, '2021-10-11 14:06:41', '2021-10-11 14:06:42'),
(25, 39, 15, '2021-10-11 14:06:41', '2021-10-11 14:06:42'),
(26, 42, 13, '2021-10-20 11:18:46', '2021-10-20 11:18:46'),
(27, 43, 13, '2021-10-21 08:52:30', '2021-10-21 08:52:30'),
(28, 43, 14, '2021-10-21 08:52:30', '2021-10-21 08:52:30'),
(29, 43, 15, '2021-10-21 08:52:30', '2021-10-21 08:52:30'),
(30, 45, 13, '2021-10-22 10:17:08', '2021-10-22 10:17:08'),
(31, 46, 4, '2021-10-22 15:15:04', '2021-10-22 15:15:04'),
(32, 46, 7, '2021-10-22 15:15:04', '2021-10-22 15:15:04'),
(33, 47, 4, '2021-10-27 11:02:08', '2021-10-27 11:02:08'),
(34, 47, 7, '2021-10-27 11:02:08', '2021-10-27 11:02:08'),
(35, 48, 16, '2021-10-27 11:10:27', '2021-10-27 11:10:27'),
(36, 48, 15, '2021-10-27 11:10:27', '2021-10-27 11:10:27'),
(37, 49, 14, '2021-10-27 11:32:28', '2021-10-27 11:32:28'),
(38, 49, 15, '2021-10-27 11:32:28', '2021-10-27 11:32:28'),
(39, 50, 7, '2021-10-27 12:10:11', '2021-10-27 12:10:11'),
(40, 50, 4, '2021-10-27 12:10:11', '2021-10-27 12:10:11'),
(41, 51, 13, '2021-10-27 12:18:02', '2021-10-27 12:18:02'),
(42, 51, 14, '2021-10-27 12:18:02', '2021-10-27 12:18:02'),
(43, 52, 4, '2021-10-27 13:01:08', '2021-10-27 13:01:08'),
(44, 53, 4, '2021-11-01 10:10:12', '2021-11-01 10:10:12'),
(45, 53, 15, '2021-11-01 10:10:12', '2021-11-01 10:10:12'),
(46, 53, 14, '2021-11-01 10:10:12', '2021-11-01 10:10:12'),
(47, 53, 17, '2021-11-01 10:10:12', '2021-11-01 10:10:12'),
(48, 53, 18, '2021-11-01 10:10:12', '2021-11-01 10:10:12'),
(49, 55, 4, '2021-11-01 10:52:36', '2021-11-01 10:52:36'),
(50, 55, 14, '2021-11-01 10:52:36', '2021-11-01 10:52:36'),
(51, 55, 17, '2021-11-01 10:52:36', '2021-11-01 10:52:36'),
(52, 59, 13, '2021-11-01 11:22:26', '2021-11-01 11:22:26'),
(53, 59, 14, '2021-11-01 11:22:26', '2021-11-01 11:22:26'),
(54, 60, 15, '2021-11-01 12:33:22', '2021-11-01 12:33:22'),
(55, 60, 14, '2021-11-01 12:33:22', '2021-11-01 12:33:22'),
(56, 60, 4, '2021-11-01 12:33:22', '2021-11-01 12:33:22'),
(57, 62, 7, '2021-11-01 12:43:28', '2021-11-01 12:43:28'),
(58, 62, 13, '2021-11-01 12:43:28', '2021-11-01 12:43:28'),
(59, 62, 16, '2021-11-01 12:43:28', '2021-11-01 12:43:28'),
(60, 63, 7, '2021-11-01 12:48:31', '2021-11-01 12:48:31'),
(61, 63, 14, '2021-11-01 12:48:31', '2021-11-01 12:48:31'),
(62, 63, 16, '2021-11-01 12:48:31', '2021-11-01 12:48:31'),
(63, 38, 13, '2021-11-02 09:00:45', '2021-11-02 09:00:45'),
(64, 38, 15, '2021-11-02 09:00:45', '2021-11-02 09:00:45'),
(65, 65, 13, '2021-11-02 09:24:25', '2021-11-02 09:24:25'),
(66, 65, 14, '2021-11-02 09:24:25', '2021-11-02 09:24:25'),
(67, 65, 16, '2021-11-02 09:24:25', '2021-11-02 09:24:25'),
(68, 66, 4, '2021-11-03 09:35:54', '2021-11-03 09:35:54'),
(69, 66, 7, '2021-11-03 09:35:54', '2021-11-03 09:35:54'),
(70, 38, 7, '2021-11-03 10:06:41', '2021-11-03 10:06:41'),
(71, 69, 13, '2021-11-03 11:05:50', '2021-11-03 11:05:50'),
(72, 69, 15, '2021-11-03 11:05:50', '2021-11-03 11:05:50'),
(73, 69, 16, '2021-11-03 11:05:50', '2021-11-03 11:05:50'),
(74, 70, 4, '2021-11-04 08:35:48', '2021-11-04 08:35:48'),
(75, 70, 7, '2021-11-04 08:35:48', '2021-11-04 08:35:48'),
(76, 71, 7, '2021-11-04 09:33:38', '2021-11-04 09:33:39'),
(77, 71, 4, '2021-11-04 09:33:38', '2021-11-04 09:33:39'),
(78, 67, 14, '2021-11-04 09:43:48', '2021-11-04 09:43:48'),
(79, 67, 13, '2021-11-04 09:43:48', '2021-11-04 09:43:48'),
(80, 67, 7, '2021-11-04 09:54:06', '2021-11-04 09:54:06'),
(81, 67, 4, '2021-11-04 09:54:06', '2021-11-04 09:54:06'),
(82, 75, 4, '2021-11-04 11:13:03', '2021-11-04 11:13:03'),
(83, 75, 7, '2021-11-04 11:13:03', '2021-11-04 11:13:03'),
(84, 75, 13, '2021-11-04 11:13:03', '2021-11-04 11:13:03'),
(85, 75, 17, '2021-11-04 11:17:33', '2021-11-04 11:17:33'),
(86, 76, 15, '2021-11-08 08:58:00', '2021-11-08 08:58:00'),
(87, 76, 16, '2021-11-08 08:58:00', '2021-11-08 08:58:00'),
(88, 77, 15, '2021-11-08 09:03:47', '2021-11-08 09:03:47'),
(89, 77, 16, '2021-11-08 09:03:47', '2021-11-08 09:03:47'),
(90, 79, 15, '2021-11-10 09:22:09', '2021-11-10 09:22:09'),
(91, 79, 16, '2021-11-10 09:22:09', '2021-11-10 09:22:09'),
(92, 79, 4, '2021-11-10 09:22:09', '2021-11-10 09:22:09'),
(93, 75, 15, NULL, NULL),
(94, 75, 16, NULL, NULL),
(95, 80, 17, '2021-11-24 13:04:08', '2021-11-24 13:04:08'),
(96, 80, 7, '2021-11-24 13:04:08', '2021-11-24 13:04:08'),
(97, 80, 4, '2021-11-24 13:04:08', '2021-11-24 13:04:08'),
(98, 81, 17, '2021-11-24 14:50:02', '2021-11-24 14:50:02'),
(99, 81, 4, '2021-11-24 14:50:02', '2021-11-24 14:50:02'),
(100, 81, 13, '2021-11-24 14:50:02', '2021-11-24 14:50:02'),
(101, 84, 4, '2021-12-06 09:16:51', '2021-12-06 09:16:51'),
(102, 84, 7, '2021-12-06 09:16:51', '2021-12-06 09:16:51'),
(103, 84, 13, '2021-12-06 09:16:51', '2021-12-06 09:16:51'),
(104, 84, 14, '2021-12-06 09:16:51', '2021-12-06 09:16:51'),
(105, 84, 15, '2021-12-06 09:16:51', '2021-12-06 09:16:51'),
(106, 84, 16, '2021-12-06 09:16:51', '2021-12-06 09:16:51'),
(107, 84, 17, '2021-12-06 09:16:51', '2021-12-06 09:16:51'),
(108, 85, 15, '2021-12-08 18:32:49', '2021-12-08 18:32:49'),
(109, 90, 7, '2021-12-16 12:36:56', '2021-12-16 12:36:56'),
(110, 90, 4, '2021-12-16 12:36:56', '2021-12-16 12:36:56'),
(111, 90, 13, '2021-12-16 12:36:56', '2021-12-16 12:36:56'),
(112, 90, 14, '2021-12-16 12:36:56', '2021-12-16 12:36:56'),
(113, 90, 15, '2021-12-16 12:36:56', '2021-12-16 12:36:56'),
(114, 95, 7, '2021-12-21 15:20:56', '2021-12-21 15:20:56'),
(115, 95, 13, '2021-12-21 15:20:56', '2021-12-21 15:20:56'),
(116, 95, 14, '2021-12-21 15:20:56', '2021-12-21 15:20:56'),
(117, 97, 13, '2021-12-22 17:10:07', '2021-12-22 17:10:07'),
(118, 97, 7, '2021-12-22 17:10:07', '2021-12-22 17:10:07'),
(119, 91, 14, '2021-12-22 22:45:45', '2021-12-22 22:45:46'),
(120, 99, 4, '2021-12-23 00:42:28', '2021-12-23 00:42:28'),
(121, 99, 13, '2021-12-23 00:42:28', '2021-12-23 00:42:28'),
(122, 99, 15, '2021-12-23 00:42:28', '2021-12-23 00:42:28'),
(123, 99, 16, '2021-12-23 00:42:28', '2021-12-23 00:42:28'),
(124, 99, 17, '2021-12-23 00:42:28', '2021-12-23 00:42:28'),
(125, 100, 4, '2021-12-23 09:40:36', '2021-12-23 09:40:37'),
(126, 101, 15, '2021-12-23 11:43:38', '2021-12-23 11:43:38'),
(127, 101, 16, '2021-12-23 11:43:38', '2021-12-23 11:43:38'),
(128, 101, 4, '2021-12-23 11:43:38', '2021-12-23 11:43:38'),
(129, 101, 7, '2021-12-23 11:43:38', '2021-12-23 11:43:38'),
(130, 104, 7, '2021-12-23 13:38:30', '2021-12-23 13:38:31'),
(131, 104, 13, '2021-12-23 13:38:30', '2021-12-23 13:38:31'),
(132, 105, 15, '2021-12-23 13:54:43', '2021-12-23 13:54:44'),
(133, 105, 16, '2021-12-23 13:54:43', '2021-12-23 13:54:44'),
(134, 105, 7, '2021-12-23 13:54:43', '2021-12-23 13:54:44'),
(135, 105, 13, '2021-12-23 13:54:43', '2021-12-23 13:54:44'),
(136, 105, 4, '2021-12-23 13:54:43', '2021-12-23 13:54:44'),
(137, 106, 13, '2021-12-23 14:00:34', '2021-12-23 14:00:34'),
(138, 106, 14, '2021-12-23 14:00:34', '2021-12-23 14:00:34'),
(139, 106, 15, '2021-12-23 14:00:34', '2021-12-23 14:00:34'),
(140, 108, 7, '2021-12-23 14:03:03', '2021-12-23 14:03:03'),
(141, 109, 7, '2021-12-23 14:29:19', '2021-12-23 14:29:19'),
(142, 109, 13, '2021-12-23 14:29:19', '2021-12-23 14:29:19'),
(143, 109, 4, '2021-12-23 14:29:19', '2021-12-23 14:29:19'),
(144, 110, 7, '2021-12-23 14:39:15', '2021-12-23 14:39:15'),
(145, 110, 13, '2021-12-23 14:39:15', '2021-12-23 14:39:15'),
(146, 110, 14, '2021-12-23 14:39:15', '2021-12-23 14:39:15'),
(147, 110, 15, '2021-12-23 14:39:15', '2021-12-23 14:39:15'),
(148, 110, 16, '2021-12-23 14:39:15', '2021-12-23 14:39:15'),
(149, 111, 14, '2021-12-24 07:19:25', '2021-12-24 07:19:25'),
(150, 111, 13, '2021-12-24 07:19:25', '2021-12-24 07:19:25'),
(151, 111, 7, '2021-12-24 07:19:25', '2021-12-24 07:19:25'),
(152, 111, 4, '2021-12-24 07:19:25', '2021-12-24 07:19:25'),
(153, 112, 7, '2021-12-24 08:17:43', '2021-12-24 08:17:44'),
(154, 112, 4, '2021-12-24 08:17:43', '2021-12-24 08:17:44'),
(155, 115, 4, '2021-12-26 15:58:45', '2021-12-26 15:58:46'),
(156, 115, 7, '2021-12-26 15:58:45', '2021-12-26 15:58:46'),
(157, 115, 15, '2021-12-26 15:58:45', '2021-12-26 15:58:46'),
(158, 115, 16, '2021-12-26 15:58:45', '2021-12-26 15:58:46'),
(159, 115, 13, '2021-12-26 15:58:45', '2021-12-26 15:58:46'),
(162, 126, 15, '2022-01-03 09:32:35', '2022-01-03 09:32:35'),
(163, 126, 16, '2022-01-03 09:32:35', '2022-01-03 09:32:36'),
(164, 126, 14, '2022-01-03 09:32:35', '2022-01-03 09:32:36'),
(165, 98, 7, '2022-01-03 15:03:03', '2022-01-03 15:03:03'),
(166, 98, 13, '2022-01-03 15:03:03', '2022-01-03 15:03:03'),
(167, 98, 4, '2022-01-03 15:03:03', '2022-01-03 15:03:03'),
(168, 129, 13, '2022-01-04 13:19:57', '2022-01-04 13:19:57'),
(169, 129, 15, '2022-01-04 13:19:57', '2022-01-04 13:19:57'),
(170, 129, 16, '2022-01-04 13:19:57', '2022-01-04 13:19:57'),
(171, 128, 14, '2022-01-05 11:01:47', '2022-01-05 11:01:47'),
(172, 128, 16, '2022-01-05 11:01:47', '2022-01-05 11:01:47'),
(173, 128, 4, '2022-01-05 11:01:47', '2022-01-05 11:01:47'),
(174, 130, 7, '2022-01-05 13:34:16', '2022-01-05 13:34:16'),
(175, 130, 4, '2022-01-05 13:34:16', '2022-01-05 13:34:16'),
(176, 130, 15, '2022-01-05 13:34:16', '2022-01-05 13:34:16'),
(177, 131, 7, '2022-01-05 16:27:54', '2022-01-05 16:27:54'),
(178, 131, 15, '2022-01-05 16:27:54', '2022-01-05 16:27:54'),
(179, 131, 4, '2022-01-05 16:27:54', '2022-01-05 16:27:54'),
(180, 134, 4, '2022-01-05 16:33:56', '2022-01-05 16:33:56'),
(181, 134, 7, '2022-01-05 16:33:56', '2022-01-05 16:33:56'),
(182, 134, 15, '2022-01-05 16:33:56', '2022-01-05 16:33:56'),
(183, 135, 15, '2022-01-05 19:37:08', '2022-01-05 19:37:08'),
(184, 135, 13, '2022-01-05 19:37:08', '2022-01-05 19:37:08'),
(185, 135, 7, '2022-01-05 19:37:08', '2022-01-05 19:37:08'),
(186, 135, 4, '2022-01-05 19:37:08', '2022-01-05 19:37:08'),
(203, 125, 15, '2022-01-11 11:33:42', '2022-01-11 11:33:42'),
(204, 125, 16, '2022-01-11 11:33:42', '2022-01-11 11:33:42'),
(205, 140, 13, '2022-01-11 15:03:45', '2022-01-11 15:03:45');

-- --------------------------------------------------------

--
-- Structure de la table `users_messages`
--

CREATE TABLE `users_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_discu` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users_messages`
--

INSERT INTO `users_messages` (`id`, `id_discu`, `id_user`, `message`, `created_at`, `updated_at`) VALUES
(34, 31, 125, 'Bien, gracias :)', '2022-01-03 11:25:44', '2022-01-03 11:25:44'),
(35, 31, 126, 'Hello', '2022-01-03 11:26:04', '2022-01-03 11:26:04');

-- --------------------------------------------------------

--
-- Structure de la table `users_objectifs`
--

CREATE TABLE `users_objectifs` (
  `mobile_user_id` int(10) UNSIGNED NOT NULL,
  `objectif_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users_objectifs`
--

INSERT INTO `users_objectifs` (`mobile_user_id`, `objectif_id`, `created_at`, `updated_at`, `id`) VALUES
(1, 1, NULL, NULL, 1),
(2, 1, NULL, NULL, 2),
(2, 2, NULL, NULL, 3),
(4, 4, '2021-04-13 10:37:34', '2021-04-13 10:37:34', 4),
(4, 5, '2021-04-13 10:37:34', '2021-04-13 10:37:34', 5),
(4, 6, '2021-04-13 10:39:48', '2021-04-13 10:39:48', 6),
(5, 6, '2021-04-13 10:50:10', '2021-04-13 10:50:10', 7),
(5, 7, '2021-04-13 10:50:10', '2021-04-13 10:50:10', 8),
(10, 4, '2021-04-16 09:41:37', '2021-04-16 09:41:37', 9),
(10, 5, '2021-04-16 09:41:37', '2021-04-16 09:41:37', 10),
(10, 8, '2021-04-16 09:41:37', '2021-04-16 09:41:37', 11),
(10, 7, '2021-06-01 07:47:49', '2021-06-01 07:47:56', 12),
(10, 9, '2021-06-01 07:47:49', '2021-06-01 07:47:56', 13),
(14, 7, '2021-06-08 08:38:56', '2021-06-08 08:38:56', 14),
(13, 7, '2021-06-08 10:18:15', '2021-06-08 10:18:15', 15),
(20, 7, '2021-08-04 10:43:17', '2021-08-04 10:43:17', 16),
(21, 7, '2021-08-12 15:52:59', '2021-08-12 15:52:59', 17),
(32, 7, '2021-09-28 11:33:42', '2021-09-28 11:33:43', 18),
(33, 7, '2021-09-28 11:47:46', '2021-09-28 11:47:47', 19),
(35, 7, '2021-09-28 16:35:43', '2021-09-28 16:35:43', 20),
(38, 7, '2021-09-29 13:54:40', '2021-09-29 13:54:40', 21),
(39, 7, '2021-10-11 14:06:41', '2021-10-11 14:06:42', 22),
(42, 4, '2021-10-20 11:18:46', '2021-10-20 11:18:46', 23),
(42, 5, '2021-10-20 11:18:46', '2021-10-20 11:18:46', 24),
(43, 7, '2021-10-21 08:52:30', '2021-10-21 08:52:30', 25),
(43, 6, '2021-10-21 08:52:30', '2021-10-21 08:52:30', 26),
(45, 5, '2021-10-22 10:17:08', '2021-10-22 10:17:08', 27),
(46, 4, '2021-10-22 15:15:04', '2021-10-22 15:15:04', 28),
(46, 5, '2021-10-22 15:15:04', '2021-10-22 15:15:04', 29),
(46, 6, '2021-10-22 15:15:04', '2021-10-22 15:15:04', 30),
(47, 4, '2021-10-27 11:02:08', '2021-10-27 11:02:08', 31),
(47, 5, '2021-10-27 11:02:08', '2021-10-27 11:02:08', 32),
(48, 9, '2021-10-27 11:10:27', '2021-10-27 11:10:27', 33),
(48, 7, '2021-10-27 11:10:27', '2021-10-27 11:10:27', 34),
(49, 4, '2021-10-27 11:32:28', '2021-10-27 11:32:28', 35),
(49, 5, '2021-10-27 11:32:28', '2021-10-27 11:32:28', 36),
(50, 4, '2021-10-27 12:10:11', '2021-10-27 12:10:11', 37),
(50, 5, '2021-10-27 12:10:11', '2021-10-27 12:10:11', 38),
(51, 4, '2021-10-27 12:18:02', '2021-10-27 12:18:02', 39),
(51, 5, '2021-10-27 12:18:02', '2021-10-27 12:18:02', 40),
(52, 4, '2021-10-27 13:01:08', '2021-10-27 13:01:08', 41),
(52, 5, '2021-10-27 13:01:08', '2021-10-27 13:01:08', 42),
(53, 4, '2021-11-01 10:10:12', '2021-11-01 10:10:12', 43),
(53, 6, '2021-11-01 10:10:12', '2021-11-01 10:10:12', 44),
(53, 9, '2021-11-01 10:10:12', '2021-11-01 10:10:12', 45),
(53, 7, '2021-11-01 10:10:12', '2021-11-01 10:10:12', 46),
(55, 4, '2021-11-01 10:52:36', '2021-11-01 10:52:36', 47),
(55, 7, '2021-11-01 10:52:36', '2021-11-01 10:52:36', 48),
(55, 9, '2021-11-01 10:52:36', '2021-11-01 10:52:36', 49),
(59, 7, '2021-11-01 11:22:26', '2021-11-01 11:22:26', 50),
(59, 6, '2021-11-01 11:22:26', '2021-11-01 11:22:26', 51),
(60, 4, '2021-11-01 12:33:22', '2021-11-01 12:33:22', 52),
(60, 6, '2021-11-01 12:33:22', '2021-11-01 12:33:22', 53),
(60, 7, '2021-11-01 12:33:22', '2021-11-01 12:33:22', 54),
(63, 4, '2021-11-01 12:48:31', '2021-11-01 12:48:31', 55),
(63, 7, '2021-11-01 12:48:31', '2021-11-01 12:48:31', 56),
(63, 9, '2021-11-01 12:48:31', '2021-11-01 12:48:31', 57),
(38, 4, '2021-11-02 09:00:45', '2021-11-02 09:00:45', 58),
(38, 9, '2021-11-02 09:00:45', '2021-11-02 09:00:45', 59),
(65, 4, '2021-11-02 09:24:25', '2021-11-02 09:24:25', 60),
(65, 7, '2021-11-02 09:24:25', '2021-11-02 09:24:25', 61),
(65, 9, '2021-11-02 09:24:25', '2021-11-02 09:24:25', 62),
(66, 4, '2021-11-03 09:35:54', '2021-11-03 09:35:54', 63),
(66, 6, '2021-11-03 09:35:54', '2021-11-03 09:35:54', 64),
(38, 5, '2021-11-03 10:06:41', '2021-11-03 10:06:41', 65),
(69, 7, '2021-11-03 11:05:50', '2021-11-03 11:05:50', 66),
(69, 6, '2021-11-03 11:05:50', '2021-11-03 11:05:50', 67),
(69, 4, '2021-11-03 11:05:50', '2021-11-03 11:05:50', 68),
(70, 4, '2021-11-04 08:35:48', '2021-11-04 08:35:48', 69),
(71, 4, '2021-11-04 09:33:38', '2021-11-04 09:33:39', 70),
(71, 5, '2021-11-04 09:33:38', '2021-11-04 09:33:39', 71),
(67, 4, '2021-11-04 09:43:48', '2021-11-04 09:43:48', 72),
(67, 5, '2021-11-04 09:43:48', '2021-11-04 09:43:48', 73),
(75, 4, '2021-11-04 11:13:03', '2021-11-04 11:13:03', 74),
(75, 5, '2021-11-04 11:13:03', '2021-11-04 11:13:03', 75),
(75, 6, '2021-11-04 11:13:03', '2021-11-04 11:13:03', 76),
(76, 5, '2021-11-08 08:58:00', '2021-11-08 08:58:00', 77),
(77, 5, '2021-11-08 09:03:47', '2021-11-08 09:03:47', 78),
(77, 6, '2021-11-08 09:03:47', '2021-11-08 09:03:47', 79),
(79, 5, '2021-11-10 09:22:09', '2021-11-10 09:22:09', 80),
(79, 6, '2021-11-10 09:22:09', '2021-11-10 09:22:09', 81),
(79, 7, '2021-11-10 09:22:09', '2021-11-10 09:22:09', 82),
(80, 9, '2021-11-24 13:04:08', '2021-11-24 13:04:08', 83),
(80, 6, '2021-11-24 13:04:08', '2021-11-24 13:04:08', 84),
(81, 4, '2021-11-24 14:50:02', '2021-11-24 14:50:02', 85),
(81, 6, '2021-11-24 14:50:02', '2021-11-24 14:50:02', 86),
(81, 10, '2021-11-24 14:50:02', '2021-11-24 14:50:02', 87),
(81, 7, '2021-11-24 14:50:02', '2021-11-24 14:50:02', 88),
(84, 6, '2021-12-06 09:16:51', '2021-12-06 09:16:51', 89),
(84, 9, '2021-12-06 09:16:51', '2021-12-06 09:16:51', 90),
(85, 4, '2021-12-08 18:32:49', '2021-12-08 18:32:49', 91),
(90, 4, '2021-12-16 12:36:56', '2021-12-16 12:36:56', 92),
(90, 5, '2021-12-16 12:36:56', '2021-12-16 12:36:56', 93),
(90, 6, '2021-12-16 12:36:56', '2021-12-16 12:36:56', 94),
(95, 4, '2021-12-21 15:20:56', '2021-12-21 15:20:56', 95),
(95, 5, '2021-12-21 15:20:56', '2021-12-21 15:20:56', 96),
(97, 5, '2021-12-22 17:10:07', '2021-12-22 17:10:07', 97),
(97, 6, '2021-12-22 17:10:07', '2021-12-22 17:10:07', 98),
(91, 4, '2021-12-22 22:45:45', '2021-12-22 22:45:46', 99),
(99, 4, '2021-12-23 00:42:28', '2021-12-23 00:42:28', 100),
(99, 5, '2021-12-23 00:42:28', '2021-12-23 00:42:28', 101),
(99, 6, '2021-12-23 00:42:28', '2021-12-23 00:42:28', 102),
(100, 6, '2021-12-23 09:40:36', '2021-12-23 09:40:37', 103),
(101, 4, '2021-12-23 11:43:38', '2021-12-23 11:43:38', 104),
(101, 5, '2021-12-23 11:43:38', '2021-12-23 11:43:38', 105),
(101, 6, '2021-12-23 11:43:38', '2021-12-23 11:43:38', 106),
(101, 7, '2021-12-23 11:43:38', '2021-12-23 11:43:38', 107),
(101, 9, '2021-12-23 11:43:38', '2021-12-23 11:43:38', 108),
(104, 7, '2021-12-23 13:38:30', '2021-12-23 13:38:31', 109),
(104, 9, '2021-12-23 13:38:30', '2021-12-23 13:38:31', 110),
(105, 4, '2021-12-23 13:54:43', '2021-12-23 13:54:44', 111),
(106, 4, '2021-12-23 14:00:34', '2021-12-23 14:00:34', 112),
(106, 5, '2021-12-23 14:00:34', '2021-12-23 14:00:34', 113),
(108, 6, '2021-12-23 14:03:03', '2021-12-23 14:03:03', 114),
(109, 4, '2021-12-23 14:29:19', '2021-12-23 14:29:19', 115),
(109, 5, '2021-12-23 14:29:19', '2021-12-23 14:29:19', 116),
(109, 6, '2021-12-23 14:29:19', '2021-12-23 14:29:19', 117),
(110, 5, '2021-12-23 14:39:15', '2021-12-23 14:39:15', 118),
(110, 6, '2021-12-23 14:39:15', '2021-12-23 14:39:15', 119),
(110, 9, '2021-12-23 14:39:15', '2021-12-23 14:39:15', 120),
(111, 4, '2021-12-24 07:19:25', '2021-12-24 07:19:25', 121),
(111, 5, '2021-12-24 07:19:25', '2021-12-24 07:19:25', 122),
(111, 6, '2021-12-24 07:19:25', '2021-12-24 07:19:25', 123),
(112, 5, '2021-12-24 08:17:43', '2021-12-24 08:17:44', 124),
(115, 4, '2021-12-26 15:58:45', '2021-12-26 15:58:46', 125),
(115, 5, '2021-12-26 15:58:45', '2021-12-26 15:58:46', 126),
(115, 6, '2021-12-26 15:58:45', '2021-12-26 15:58:46', 127),
(115, 9, '2021-12-26 15:58:45', '2021-12-26 15:58:46', 128),
(126, 5, '2022-01-03 09:32:35', '2022-01-03 09:32:36', 132),
(126, 6, '2022-01-03 09:32:35', '2022-01-03 09:32:36', 133),
(126, 7, '2022-01-03 09:32:35', '2022-01-03 09:32:36', 134),
(98, 4, '2022-01-03 15:03:03', '2022-01-03 15:03:03', 135),
(98, 5, '2022-01-03 15:03:03', '2022-01-03 15:03:03', 136),
(98, 6, '2022-01-03 15:03:03', '2022-01-03 15:03:03', 137),
(98, 9, '2022-01-03 15:03:03', '2022-01-03 15:03:03', 138),
(129, 4, '2022-01-04 13:19:57', '2022-01-04 13:19:57', 139),
(129, 5, '2022-01-04 13:19:57', '2022-01-04 13:19:57', 140),
(129, 6, '2022-01-04 13:19:57', '2022-01-04 13:19:57', 141),
(128, 7, '2022-01-05 11:01:47', '2022-01-05 11:01:47', 142),
(128, 9, '2022-01-05 11:01:47', '2022-01-05 11:01:47', 143),
(128, 4, '2022-01-05 11:01:47', '2022-01-05 11:01:47', 144),
(130, 5, '2022-01-05 13:34:16', '2022-01-05 13:34:16', 145),
(131, 4, '2022-01-05 16:27:54', '2022-01-05 16:27:54', 146),
(131, 5, '2022-01-05 16:27:54', '2022-01-05 16:27:54', 147),
(131, 6, '2022-01-05 16:27:54', '2022-01-05 16:27:54', 148),
(134, 4, '2022-01-05 16:33:56', '2022-01-05 16:33:56', 149),
(134, 5, '2022-01-05 16:33:56', '2022-01-05 16:33:56', 150),
(134, 6, '2022-01-05 16:33:56', '2022-01-05 16:33:56', 151),
(135, 5, '2022-01-05 19:37:08', '2022-01-05 19:37:08', 152),
(135, 6, '2022-01-05 19:37:08', '2022-01-05 19:37:08', 153),
(135, 9, '2022-01-05 19:37:08', '2022-01-05 19:37:08', 154),
(135, 11, '2022-01-05 19:37:08', '2022-01-05 19:37:08', 155),
(125, 9, '2022-01-11 11:33:56', '2022-01-11 11:33:56', 169),
(125, 10, '2022-01-11 11:33:56', '2022-01-11 11:33:56', 170),
(140, 5, '2022-01-11 15:03:45', '2022-01-11 15:03:45', 171);

-- --------------------------------------------------------

--
-- Structure de la table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_details`
--

INSERT INTO `user_details` (`id`, `type`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'unknown male', 'user-details/December2021/nP76aDsvdBeRM3JoJcDh.png', '2021-12-13 13:47:00', '2021-12-23 14:37:36'),
(2, 'unknown feamle', 'user-details/December2021/jgg7LBVMv39zB4YAmYv3.png', '2021-12-13 13:48:52', '2021-12-13 13:48:52');

-- --------------------------------------------------------

--
-- Structure de la table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `wizard_roles`
--

CREATE TABLE `wizard_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wizard_roles`
--

INSERT INTO `wizard_roles` (`id`, `titre`, `created_at`, `updated_at`, `status`) VALUES
(2, 'En chômage', '2021-09-14 08:50:29', '2021-09-14 08:50:29', 1),
(3, 'Etudiant', '2021-09-14 08:50:41', '2021-09-14 08:50:41', 1),
(4, 'Designer', '2021-09-14 08:50:54', '2021-09-14 08:50:54', 1),
(5, 'Expert comptable', '2021-09-14 08:51:07', '2021-09-14 08:51:07', 1),
(6, 'Développeur web', '2021-09-14 08:51:19', '2021-09-14 08:51:19', 1),
(7, 'Directeur Général', '2021-09-28 10:03:00', '2021-09-28 10:03:00', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `activites`
--
ALTER TABLE `activites`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `b2b_meets`
--
ALTER TABLE `b2b_meets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b2b_meets_prog_id_index` (`prog_id`);

--
-- Index pour la table `b2b_meet_users`
--
ALTER TABLE `b2b_meet_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b2b_meet_users_mobile_user_id_index` (`mobile_user_id`),
  ADD KEY `b2b_meet_users_b2b_meet_id_index` (`b2b_meet_id`);

--
-- Index pour la table `back_logs`
--
ALTER TABLE `back_logs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `boardings`
--
ALTER TABLE `boardings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Index pour la table `certifications`
--
ALTER TABLE `certifications`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Index pour la table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Index pour la table `discussions`
--
ALTER TABLE `discussions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `evenements`
--
ALTER TABLE `evenements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formations`
--
ALTER TABLE `formations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `interets`
--
ALTER TABLE `interets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `manuel_notifications`
--
ALTER TABLE `manuel_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Index pour la table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mobile_users`
--
ALTER TABLE `mobile_users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `objectifs`
--
ALTER TABLE `objectifs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `organisateurs`
--
ALTER TABLE `organisateurs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `orgs_events`
--
ALTER TABLE `orgs_events`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Index pour la table `partenaires`
--
ALTER TABLE `partenaires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `partenaires_events`
--
ALTER TABLE `partenaires_events`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Index pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Index pour la table `programmes`
--
ALTER TABLE `programmes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `propos`
--
ALTER TABLE `propos`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reacts`
--
ALTER TABLE `reacts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Index pour la table `speakers`
--
ALTER TABLE `speakers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `speakers_progs`
--
ALTER TABLE `speakers_progs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_formations_mobile_user_id_index` (`speaker_id`),
  ADD KEY `users_formations_formation_id_index` (`programme_id`);

--
-- Index pour la table `stands`
--
ALTER TABLE `stands`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Index pour la table `type_progs`
--
ALTER TABLE `type_progs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Index pour la table `users_discussions`
--
ALTER TABLE `users_discussions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users_interets`
--
ALTER TABLE `users_interets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_interets_mobile_user_id_index` (`mobile_user_id`),
  ADD KEY `users_interets_interets_id_index` (`interet_id`);

--
-- Index pour la table `users_messages`
--
ALTER TABLE `users_messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users_objectifs`
--
ALTER TABLE `users_objectifs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_objectifs_objectif_id_index` (`objectif_id`),
  ADD KEY `users_objectifs_mobile_user_id_index` (`mobile_user_id`);

--
-- Index pour la table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Index pour la table `wizard_roles`
--
ALTER TABLE `wizard_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `activites`
--
ALTER TABLE `activites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `b2b_meets`
--
ALTER TABLE `b2b_meets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT pour la table `b2b_meet_users`
--
ALTER TABLE `b2b_meet_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT pour la table `back_logs`
--
ALTER TABLE `back_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT pour la table `boardings`
--
ALTER TABLE `boardings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `certifications`
--
ALTER TABLE `certifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT pour la table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=292;

--
-- AUTO_INCREMENT pour la table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `discussions`
--
ALTER TABLE `discussions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `evenements`
--
ALTER TABLE `evenements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `experiences`
--
ALTER TABLE `experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `formations`
--
ALTER TABLE `formations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT pour la table `interets`
--
ALTER TABLE `interets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `manuel_notifications`
--
ALTER TABLE `manuel_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT pour la table `mobile_users`
--
ALTER TABLE `mobile_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT pour la table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=440;

--
-- AUTO_INCREMENT pour la table `objectifs`
--
ALTER TABLE `objectifs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `organisateurs`
--
ALTER TABLE `organisateurs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `orgs_events`
--
ALTER TABLE `orgs_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `partenaires`
--
ALTER TABLE `partenaires`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `partenaires_events`
--
ALTER TABLE `partenaires_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `programmes`
--
ALTER TABLE `programmes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT pour la table `propos`
--
ALTER TABLE `propos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reacts`
--
ALTER TABLE `reacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `speakers`
--
ALTER TABLE `speakers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `speakers_progs`
--
ALTER TABLE `speakers_progs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `stands`
--
ALTER TABLE `stands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `type_progs`
--
ALTER TABLE `type_progs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users_discussions`
--
ALTER TABLE `users_discussions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT pour la table `users_interets`
--
ALTER TABLE `users_interets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT pour la table `users_messages`
--
ALTER TABLE `users_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pour la table `users_objectifs`
--
ALTER TABLE `users_objectifs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT pour la table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `wizard_roles`
--
ALTER TABLE `wizard_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Contraintes pour la table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
