<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\MobileUser;
use Carbon\Carbon;
use App\Evenement;

class NotificationEventCron extends Command
{
    private $UrlPushNotif = "https://exp.host/--/api/v2/push/send";
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notificationEvent:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");
        $e = Evenement::where('titre','SITIC Africa Abidjan 2022')->first();
        //$d = Carbon::createFromDate($e->date_debut);
        /* you cannot you a variable
        echo "event ".Carbon::createFromDate($e->date_debut)->format('Y-m-d').'<br>';
        echo "before week ".Carbon::createFromDate($e->date_debut)->subWeeks(1)->format('Y-m-d').'<br>';
        echo "before 3 days ".Carbon::createFromDate($e->date_debut)->subDays(3)->format('Y-m-d').'<br>';
        echo "before 1 day ". Carbon::createFromDate($e->date_debut)->subDays(3)->format('Y-m-d');
        */
        $d = date('Y-m-d');
        $mobileUser = MobileUser::all();
        if($d == Carbon::createFromDate($e->date_debut)->subWeeks(1)->format('Y-m-d'))
        {
            //before week
            $this->insertNotification("Evenement ".$v->titre.' Il reste une semaine', "Event", null, $m->id,$v->id,null,null);
            $this->sendNotificationToUser($m->id, "Evenement ".$v->titre, "Il est encore 1 semaines avant le début de l'évenement : ".$v->titre);
        }
        elseif($d == Carbon::createFromDate($e->date_debut)->subDays(3)->format('Y-m-d'))
        {
            //before 3 days
            $this->insertNotification("Evenement ".$v->titre.' Il reste 3 jours', "Event", null, $m->id,$v->id,null,null);
            $this->sendNotificationToUser($m->id, "Evenement ".$v->titre, "Il est encore 3 jours avant le début de l'évenement : ".$v->titre);
        }
        elseif($d == Carbon::createFromDate($e->date_debut)->subDays(3)->format('Y-m-d'))
        {
            //before 1 day
            $this->insertNotification("Evenement ".$v->titre.' Il reste un jour', "Event", null, $m->id,$v->id,null,null);
            $this->sendNotificationToUser($m->id, "Evenement ".$v->titre, "Il est reste un jour avant le début de l'évenement : ".$v->titre);
        }
        $this->info('notificationEvent:cron Cummand Run successfully!');
    }

    private function sendNotificationToUser($id, $title, $content)
    {
        $m = MobileUser::find($id);
        if(isset($m->token_device))
        {
            $array = json_decode($m->token_device);
            foreach($array as $v)
            {
                if(($v != "ExponentPushToken[SIMULATOR]") or (!isset($v)))
                {
                    $token = "$v";
                    $token=str_replace('"','\"',$token);
                    $token = '"'.$token.'"';
                    $token = '['.$token.']';
                    $this->PushNotif($token, $title, $content);
                }
            }
        }
    }

    private function insertNotification($titre, $type, $token_device, $id_user, $id_prog, $id_like)
    {
        $notification = new Notification();
        $notification->titre = $titre;
        $notification->type = $type;
        $notification->token_device = $token_device;
        $notification->id_user = $id_user;
        $notification->id_prog = $id_prog;
        $notification->id_like = $id_like;
        $notification->save();
    }

    private function PushNotif(string $token, string $title, string $body)
    {
        $token1 = json_decode($token);
        $response = Http::withHeaders([
            'host' => 'exp.host',
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate',
            'content-type' => 'application/json',
        ])->post($this->UrlPushNotif, [
            'to' => $token1,
            'title' => $title,
            'body' => $body
        ]);
        return $response;
    }
}
