<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use App\Notification;
use App\MobileUser ;
use App\Programme;

class NotificationCron extends Command
{
    private $UrlPushNotif = "https://exp.host/--/api/v2/push/send";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void 

     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");
     
        $mobileUser = MobileUser::all();
        $p = Programme::where('date_prog','>',date("Y-m-d"))->orderBy("date_prog",'asc')->get();
        foreach($p as $v)
        {
            $v->heure_debut1 = date("Y-m-d H:i:s",strtotime($v->date_prog.' '.$v->heure_debut));
            $v->heure_debut1 = Carbon::createFromDate($v->heure_debut1);
            $v->before15Minutes = $v->heure_debut1->subMinutes(15);
            if(Carbon::now()->format('Y-m-d H:i') == $v->before15Minutes->format('Y-m-d H:i:s'))
            {
                foreach($mobileUser as $m)
                {
                    $this->insertNotification("Upcoming workshop ".$v->titre, "Programme", null, $m->id,$v->id,null,null);
                    $this->sendNotificationToUser($m->id, "Upcoming workshop", "Il est encore 15 minutes avant le début : ".$v->titre);
                }
            }
        }
      
        $this->info('notification:cron Cummand Run successfully!');
    }

    private function sendNotificationToUser($id, $title, $content)
    {
        $m = MobileUser::find($id);
        if(isset($m->token_device))
        {
            $array = json_decode($m->token_device);
            foreach($array as $v)
            {
                if(($v != "ExponentPushToken[SIMULATOR]") or (!isset($v)))
                {
                    $token = "$v";
                    $token=str_replace('"','\"',$token);
                    $token = '"'.$token.'"';
                    $token = '['.$token.']';
                $this->PushNotif($token, $title, $content);
                }
            }
        }
    }

    private function insertNotification($titre, $type, $token_device, $id_user, $id_prog, $id_like)
    {
        $notification = new Notification();
        $notification->titre = $titre;
        $notification->type = $type;
        $notification->token_device = $token_device;
        $notification->id_user = $id_user;
        $notification->id_prog = $id_prog;
        $notification->id_like = $id_like;
        $notification->save();
    }

    private function PushNotif(string $token, string $title, string $body)
    {
        $token1 = json_decode($token);
        $response = Http::withHeaders([
            'host' => 'exp.host',
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate',
            'content-type' => 'application/json',
        ])->post($this->UrlPushNotif, [
            'to' => $token1,
            'title' => $title,
            'body' => $body
        ]);
        return $response;
    }
}