<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public  $var = null ;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($var)
    {
        $this->var = $var ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');

          return $this->from('lab@xelero.io')
                ->view('mail')
                ->subject('test mail')
                ->with([
           //         'orderName' => $this->order->name,
           //         'orderPrice' => $this->order->price,
                ]);

    }
}
