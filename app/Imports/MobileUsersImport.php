<?php

namespace App\Imports;

use App\MobileUser;
use Maatwebsite\Excel\Concerns\ToModel;

class MobileUsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MobileUser([
            'nom'     => $row['prenom'],
            'prenom'    => $row['prenom'], 
            'email' => $row['email'] ,
        ]);
    }
}
