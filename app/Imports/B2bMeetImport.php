<?php

namespace App\Imports;

use App\B2bMeet;
use Maatwebsite\Excel\Concerns\ToModel;

class B2bMeetImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new B2bMeet([
            //
        ]);
    }
}
