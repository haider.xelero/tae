<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Discussion extends Model
{
    public function UsersDiscussions()
    {
        // return $this->hasMany(UsersDiscussion::class);
        return $this->hasMany('App\UsersDiscussion', 'id_discu');
    }
}
