<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        
        Bonjour,
        <br>
        Votre demande de rendez-vous B2B est envoyé avec succès.
        <br>
        Details :
        <br>
        Destinataire : <strong>{{$data['prenom']}} {{$data['nom']}} - {{$data['entreprise']}}</strong>  
        <br>
        Heure : <strong>{{$data['heure']}}</strong>
        <br>
        Programme : <strong>{{$data['programme']}}</strong>
        <br>
        ID du RDV : <strong>{{$data['id_rendez_vous']}}</strong>
        <br>
        Rendez-vous sur l’application SITIC Africa pour gérer votre calendrier des rencontres B2B
        <br>
        Bon salon
        <br>
        L’équipe SITIC
    </body>
</html>