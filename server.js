const express = require('express');
const app = express();
var cors = require('cors');
app.use(cors());
const http = require("http");
const server = require('http').createServer(app);
const axios = require('axios')
const io = require('socket.io')(server, {
    cors: { origin: "*"}
});


io.on('connection', (socket) => {
    console.log('connection');
    // console.log(getRequest("http://tae.test/api/test"));

    socket.on('sendChatToServer', (obj) => {
        console.log("obj ==> ",obj);
        // we can use axios if we want to store the data in the database

        axios.post('https://tae.xelero.io/api/sendMessage', {
            conversation_id: obj.id_conver,
            my_id:  obj.id_user,
            message:  obj.message
          })
          .then(function (response) 
          {
            // console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });

        // io.sockets.emit('sendChatToClient', message);
        socket.broadcast.emit('sendChatToClient', obj.message);
    });

    socket.on('test', (obj) => {
      //console.log("obj ==> "+obj);
      // we can use axios if we want to store the data in the database
      
      axios.get('https://tae.xelero.io/api/listConversations/93', {})
          .then(function (response) 
          {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });

      // io.sockets.emit('sendChatToClient', message);
      socket.broadcast.emit('sendChatToClient', obj.message);
  });
/*
    socket.on('listConversation', (obj) => {
        console.log("obj ==> "+obj);
        // we can use axios if we want to store the data in the database

        axios.post('http://tae.test/api/sendMessage', {
            conversation_id: obj.id_conver,
            my_id:  obj.id_user,
            message:  obj.message
          })
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });


        // io.sockets.emit('sendChatToClient', message);
        socket.broadcast.emit('sendChatToClient', obj.message);
    }); */


    socket.on('disconnect', (socket) => {
        console.log('Disconnect');
    });
});

function getRequest(url)
{
    // return new Promise((resolve, reject) => {
    //     if (err) {
    //       reject(err)  // calling `reject` will cause the promise to fail with or without the error passed as an argument
    //       return        // and we don't want to go any further
    //     }
        
    //     axios.get(url)
    //     .then(function(response) {
    //         if(response.status == 200){
    //             resolve(response.data)
    //         }
    //         // console.log(response.status);
    //     });

    // })

    try {

        axios.get(url)
        .then(function(response) {
            // return response.data;
            if(response.status == 200){
            }
            console.log(response.status);
        });
        
    }
    catch (err) {
        return (err)
    }
}


server.listen(3000, () => {
    console.log('Server is running');
});
