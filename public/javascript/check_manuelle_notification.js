$(document).ready
(
	function()
	{
		$("#btClick").click
        (
			function(e)
			{
				var titre = $('input[name="titre"]').val();
                var valeur = $('input[name="valeur"]').val();

                if(titre === "")
                {
                    $('input[name="titre"]').focus();
                    $(
						function() 
						{
							toastr.error("Veuillez saisir le titre de cette notification !!", 'Error');
						}
					);
                    e.preventDefault();
                }
                else
                {
                    if(valeur === "")
                    {
                        $('input[name="valeur"]').focus();
                        $(
                            function() 
                            {
                                toastr.error("Veuillez saisir le valeur!!", 'Error');
                            }
                        );
                        e.preventDefault();
                    }
                }
            }
        );
    }
);