$(document).ready
(
	function()
	{
		$("#btClick").click
        (
			function(e)
			{
				var titre = $('input[name="titre"]').val();
				var date_prog = $('input[name="date_prog"]').val();
				var event_id = $('select[name="event_id"]').val();
				var heure_debut = $('input[name="heure_debut"]').val();
				var heure_fin = $('input[name="heure_fin"]').val();
				var location_programme = $('input[name="location_programme"]').val();
				var date_debut_pause = $('input[name="date_debut_pause"]').val();
				var date_fin_pause = $('input[name="date_fin_pause"]').val();
				
				//start the testing of the importants fields
				if(titre === "")
				{
					$('input[name="titre"]').focus();
					$(
						function() 
						{
							toastr.error("Veuillez saisir le titre du programme !!", 'Error');
						}
					);
					e.preventDefault();
				}
				else
				{
					if(date_prog === "")
					{
						$('input[name="date_prog"]').focus();
						$(
							function() 
							{
								toastr.error("Veuillez saisir la date de ce programme !!", 'Error');
							}
						);
						e.preventDefault();
					}
					else
					{
						if(heure_debut === "")
						{
							$('input[name="heure_debut"]').focus();
							$(
								function() 
								{
									toastr.error("Veuillez saisir à quelle heure le programme commence !!", 'Error');
								}
							);
							e.preventDefault();
						}
						else
						{
							if(heure_fin === "")
							{
								$('input[name="heure_fin"]').focus();
								$(
									function() 
									{
										toastr.error("Veuillez saisir à quelle heure le programme termine !!", 'Error');
									}
								);
								e.preventDefault();
							}
							else
							{
								if(event_id === "")
								{
									$('select[name="event_id"]').focus();
									$(
										function() 
										{
											toastr.error("Veuillez choisir l'évenement !! ", 'Error');
										}
									);
									e.preventDefault();
								}
								else
								{
									if(location_programme === "")
									{
										$('input[name="location_programme"]').focus();
										$(
											function() 
											{
												toastr.error("Veuillez saisir la location de ce programme!!", 'Error');
											}
										);
										e.preventDefault();
									}
									else
									{
										if(date_debut_pause === "")
										{
											$('input[name="date_debut_pause"]').focus();
											$(
												function() 
												{
													toastr.error("Veuillez saisir à quelle heure la pause se commence !!", 'Error');
												}
											);
											e.preventDefault();
										}
										else
										{
											if(date_fin_pause === "")
											{
												$('input[name="date_fin_pause"]').focus();
												$(
													function() 
													{
														toastr.error("Veuillez saisir à quelle heure la pause se termine !!", 'Error');
													}
												);
												e.preventDefault();
											}
											else
											{
												if(heure_debut > heure_fin)
												{
													$('input[name="heure_debut"]').focus();
													$('input[name="heure_fin"]').focus();
													$(
														function() 
														{
															toastr.error("l'heure de début de programme doit antérieur que l'heure de fin de programme !!", 'Error');
														}
													);
													e.preventDefault();
												}
												else
												{
													if(date_debut_pause > date_fin_pause)
													{
														$('input[name="date_debut_pause"]').focus();
														$('input[name="date_fin_pause"]').focus();
														$(
															function() 
															{
																toastr.error("l'heure de début de pause doit antérieur que l'heure de fin de pause !!", 'Error');
															}
														);
														e.preventDefault();
													}
													else
													{
														if((heure_debut > date_debut_pause) || (heure_debut > date_fin_pause) ||
															(heure_fin < date_debut_pause) || (heure_fin < date_fin_pause) )
														{
															$('input[name="date_debut_pause"]').focus();
															$('input[name="date_fin_pause"]').focus();
															$(
																function() 
																{
																	toastr.error("l'heure de début de pause et l'heure de fin de pause doivent être entre l'heure de debut de programme et l'heure de fin programme !!", 'Error');
																}
															);
														e.preventDefault();
														}
														else
														{
															var res = "";
															$.ajax
															(
																{
																	async: false, //if you want to change a global variable you should add this instruction
																	type: 'POST',
																	url: "/api/verifyDateProg",
																	data:
																	{
																		'date_prog' : date_prog,
																		'event_id' : event_id
																	},
																	success: 
																	function(result)
																	{
																		res = (result);
																	}
																}
															);
															if(res.code == 0)
															{
																$('input[name="date_prog"]').focus();
																$(
																	function() 
																	{
																		toastr.error("La date de ce programme doit être planifié entre "+res.date_debut+" et "+res.date_fin, 'Error');
																	}
																);
																e.preventDefault();
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		);
	}
);