$(document).ready
(
	function()
	{
		$("#btClick").click
        (
			function(e)
			{
				var titre = $('input[name="titre"]').val();
				var location_event = $('input[name="location_event"]').val();
				var date_debut =  new Date($('input[name="date_debut"]').val());
				var date_fin =  new Date($('input[name="date_fin"]').val());
				if(titre === "")
				{
					$('input[name="titre"]').focus();
					$(
						function() 
						{
							toastr.error("Veuillez saisir le titre de l'évenement", 'Error');
						}
					);
					e.preventDefault();
				}
				else
				{
					if (isNaN(date_debut))
					{
						$('input[name="date_debut"]').focus();
						$(
							function() 
							{
								toastr.error("Veuillez saisir la date début", 'Error');
							}
						);
						e.preventDefault();
					}
					else
					{
						if(isNaN(date_fin))
						{
							$('input[name="date_fin"]').focus();
							$(
								function()
								{
									toastr.error("Veuillez saisir la date fin",'Error');
								}
							)
							e.preventDefault();
						}
						else
						{
							if(location_event === "")
							{
								$('input[name="location_event"]').focus();
								$(
									function()
									{
										toastr.error("Veuillez saisir la location de l'évenemnt",'Error');
									}
								)
								e.preventDefault();
							}
							else
							{
								if(date_debut.getTime() >= date_fin.getTime())
								{
									$('input[name="date_debut"]').focus();
									$('input[name="date_fin"]').focus();
									$(
										function() 
										{
											toastr.error("La date de début d'activité doit être antérieur que la date fin d'activité", 'Error');
										}
									);
									e.preventDefault();
								}								
							}
						}
					}
				}
			}
		);
	}
);