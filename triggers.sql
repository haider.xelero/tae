DELIMITER |
DROP TRIGGER IF EXISTS trig_insert_progs ;
CREATE TRIGGER trig_insert_progs AFTER INSERT
ON programmes FOR EACH ROW
BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (NEW.titre,'insert','programmes',SYSDATE());
END |

DELIMITER |
DROP TRIGGER IF EXISTS trig_update_progs ;
CREATE TRIGGER trig_update_progs AFTER update
ON programmes FOR EACH ROW
BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (NEW.titre,'update','programmes',SYSDATE());
END |

DELIMITER |
DROP TRIGGER IF EXISTS trig_delete_progs ;
CREATE TRIGGER trig_delete_progs AFTER delete
ON programmes FOR EACH ROW
BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (old.titre,'delete','programmes',SYSDATE());
END |


DELIMITER |
DROP TRIGGER IF EXISTS trig_insert_speakers ;
CREATE TRIGGER trig_insert_speakers AFTER INSERT
ON speakers_progs FOR EACH ROW
BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (null,'insert','speakers',SYSDATE());
END |

DELIMITER |
DROP TRIGGER IF EXISTS trig_update_speakers ;
CREATE TRIGGER trig_update_speakers AFTER update
ON speakers_progs FOR EACH ROW
BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (null,'update','speakers',SYSDATE());
END |

DELIMITER |
DROP TRIGGER IF EXISTS trig_delete_speakers ;
CREATE TRIGGER trig_delete_speakers AFTER delete
ON speakers_progs FOR EACH ROW
BEGIN
   INSERT INTO back_logs (titre,ACTION,tab,created_at) VALUES (null,'delete','speakers',SYSDATE());
END |