<?php

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
    */

    Route::middleware
    ('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

	Route::get('/SendEmail', 'ApisController@testSendEmail');
	Route::get('/WizardInfo', 'ApisController@WizardInfo');
	Route::post('/Inscription', 'ApisController@Inscription');
	Route::post('/Wizard', 'ApisController@Wizard');
	Route::post('/Auth', 'ApisController@Auth');
	Route::get('/ListParticipant', 'ApisController@ListParticipant');
	Route::get('/Onboarding', 'ApisController@Onboarding');
	Route::get('/Partenaires', 'ApisController@Partenaires');
	Route::get('/Organisateurs', 'ApisController@Organisateurs');
	Route::get('/Propos', 'ApisController@Propos');
	Route::get('/Events', 'ApisController@Events');
	Route::post('/SendB2b', 'ApisController@SendB2b');
	Route::get('/ListMeetB2b/{id}/{status?}', 'ApisController@ListMeetB2b'); // status = (sent/received/confirmed/refused) ou status = ( ) récupérer toutes la liste
	Route::get('/DetailProfil/{id}', 'ApisController@DetailProfil');
	Route::get('/CheckUpdate', 'ApisController@CheckUpdate');
	Route::post('/Search', 'ApisController@Search');
	Route::post('/showFreeTimeSlots', 'ApisController@showFreeTimeSlots');
	Route::post('/chnageB2Bstatus', 'ApisController@chnageB2Bstatus'); // status = (confirmed/refused)
    Route::post('/Logout', 'ApisController@Logout');
    Route::get('/ListParticipant', 'ApisController@ListParticipant');
    Route::get('/Onboarding', 'ApisController@Onboarding');
    Route::get('/Partenaires', 'ApisController@Partenaires');
    Route::get('/Organisateurs', 'ApisController@Organisateurs');
    Route::get('/Propos', 'ApisController@Propos');
    Route::get('/Events', 'ApisController@Events');
    Route::post('/SendB2b', 'ApisController@SendB2b');
    Route::get('/ListMeetB2b/{id}/{status?}', 'ApisController@ListMeetB2b'); // status = (sent/received/confirmed/refused) ou status = (all) récupérer toutes la liste
    Route::get('/getUser/{id}','ApisController@getUser');

    //add new Route 09/06/2021
    Route::get('/Programmes','ApisController@Programmes');
    Route::get('/getEvent/{id}','ApisController@getEvent');
    Route::get('/findSpeakers/{id}','ApisController@findSpeakersByProgramme');
    Route::get('/getAllEvent','ApisController@getAllEvent');
	Route::post('/verifyDateProg','ApisController@verifyDateProg');
	Route::post('/insertImage','ApisController@insertImage');
	Route::get('/getAllMobileUser','ApisController@getAllMobileUser');
	Route::get('/findMobileUserById/{id}','ApisController@findMobileUserById');
	Route::get('/social_media/{site}','ApisController@social_media');
	Route::post('/getUserLinkedin','ApisController@getUserLinkedin');
	Route::get('/getEmailLinkedin','ApisController@getEmailLinkedin');
	Route::post('/AuthLinkedin','ApisController@AuthLinkedin');
	Route::post('/AuthGmail','ApisController@AuthGmail');
    Route::get('/insertImageFromGmail','ApisController@insertImageFromGmail');
    Route::get('/searchUserByInteret/{id}','ApisController@searchUserByInteret'); //searchByTag
    Route::get('/dataSearchByTag','ApisController@dataSearchByTag');
    Route::get('/testLinkedin','ApisController@testLinkedin');
    Route::post('/SearchFilter','ApisController@SearchFilter');
    Route::get('/getUser','ApisController@getUser');
    Route::post('/match','ApisController@match');
    Route::post('/verifStand','ApisController@verifStand');
    Route::post('/activeB2bRequest','ApisController@activeB2bRequest');
    Route::post('/confirmAccessB2bMeeting','ApisController@confirmAccessB2bMeeting');
    Route::get('/listMatch/{id}','ApisController@listMatch');
    Route::get('/NotifcationGeneral/{id}','ApisController@NotifcationGeneral');
    Route::get('/myNotification/{id}','ApisController@myNotification');
    Route::post('/updateGeneral','ApisController@updateGeneral');
    Route::post('/insertTokenDevice','ApisController@insertTokenDevice');
    Route::post('/updateExperience','ApisController@updateExperience');
    Route::post('/updateCertif','ApisController@updateCertif');
    Route::post('/updateFormation','ApisController@updateFormation');
    Route::post('/ActiveForgetPassword','ApisController@ActiveForgetPassword');
    Route::post('/VerifCodeForgetPassword','ApisController@VerifCodeForgetPassword');
    Route::post('/ResetPassword','ApisController@ResetPassword');
    Route::get('/getActiveB2bUsers','ApisController@getActiveB2bUsers');
    Route::get('/getActiveB2bUsers1/{id}','ApisController@getActiveB2bUsers1');
    Route::post('/getTimeSlotsofAllB2bPrograms','ApisController@getTimeSlotsofAllB2bPrograms');
    Route::post('/activeSimpleB2b','ApisController@activeSimpleB2b');

    //chat part
    Route::post('/sendMessageOneToOne', 'ApiMessageController@sendMessageOneToOne');
    Route::get('/getAllMessagesByConversation/{conversation_id}/{my_id}','ApiMessageController@getAllMessagesByConversation');
    Route::post('/seenMessage', 'ApiMessageController@seenMessage');
    Route::get('/getConversationByUser/{id}', 'ApiMessageController@getConversationByUser');

    //update the part chat
    Route::get('/listConversations/{id}','ChatController@listConversations'); 
    Route::post('/sendMessage','ChatController@sendMessage');
    Route::post('/createConversation','ChatController@createConversation');
    Route::get('/getMessagesByConversation/{id_conversation}/{my_id}','ChatController@getMessagesByConversation');
    Route::get('/getAllUnSeenMessage/{idUser}/{idDiscu}', 'ChatController@getAllUnSeenMessage');
    Route::post('/setSeenMessage','ChatController@setSeenMessage');
    
    //api de test
    Route::get('/getCodeActivation','ApisController@getCodeActivation');
    Route::get('/testEmail','ApisController@testEmail');
    Route::get('/testNotif','ApisController@testNotif');
    Route::get('/autoNotif','ApisController@autoNotif');
    Route::get('/NotifEvent' ,'ApisController@NotifEvent');
    Route::get('/PushNotif1','ApisController@PushNotif1');
    Route::get('/testVerifB2b','ApisController@testVerifB2b');
    Route::get('/testBookedB2bProgram','ApisController@testBookedB2bProgram');
    Route::get('/updateStautsB2bMeets','ApisController@updateStautsB2bMeets');
    Route::get('/active_b2b_or_match','ApisController@active_b2b_or_match');