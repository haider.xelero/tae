<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get
(
    '/', function () 
    {
        return redirect('/admin');
    }
);


Route::group
(
    [
        'prefix' => 'admin'
    ], 
    function () 
    {
        Voyager::routes();
    }
);

//Route::get('vendor.voyager.evenements.edit-add.blade', 'ApisController@sleep');
Route::get('/exemple','VoyagerController@sleep');
Route::post('/test','WebController@test');

Route::get('send-mail', function () {
   
    $details = 
    [
        'title' => 'Mail from SITIC Africa',
        'body' => 'This is for testing email using smtp'
    ];
   
    \Mail::to('habibha.aroua82@gmail.com')->send(new \App\Mail\SendExposantEmail($details));
   
    dd("Email is Sent.");
});

//Route for import and export files
Route::get('export', 'ExcelController@export')->name('export');
Route::get('importExportView', 'ExcelController@importExportView');
Route::post('import', 'ExcelController@import')->name('import');

//Route
Route::get('export1', 'ExcelController@export1')->name('export1');
Route::get('importExportView1', 'ExcelController@importExportView1');
Route::post('import1', 'ExcelController@import1')->name('import1');